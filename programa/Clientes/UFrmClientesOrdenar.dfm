inherited FrmClientesOrdenar: TFrmClientesOrdenar
  Caption = 'Ordenar Clientes'
  OnClose = FormClose
  OnCreate = FormCreate
  ExplicitWidth = 874
  PixelsPerInch = 96
  TextHeight = 13
  inherited pCentro: TPanel
    inherited pBotoes: TPanel
      inherited btnOrdenar: TPngSpeedButton
        OnClick = btnOrdenarClick
      end
      inherited btnLimparOrdem: TPngSpeedButton
        OnClick = btnLimparOrdemClick
      end
    end
    inherited rgOpcoes: TRadioGroup
      AlignWithMargins = True
      Left = 3
      Top = 3
      Width = 524
      Columns = 3
      ItemIndex = 0
      Items.Strings = (
        'C'#243'digo'
        'Raz'#227'o Social'
        'Nome Fantas'#237'a'
        'CEP'
        'Bairro'
        'Cidade'
        'Estado'
        'Data Sistema Preventivo'
        #193'rea de Atua'#231#227'o')
      ExplicitLeft = 3
      ExplicitTop = 3
      ExplicitWidth = 524
    end
    inherited rgOrdem: TRadioGroup
      AlignWithMargins = True
      Left = 3
      Top = 114
      Width = 524
      Height = 35
      ExplicitLeft = 3
      ExplicitTop = 114
      ExplicitWidth = 524
      ExplicitHeight = 35
    end
  end
end
