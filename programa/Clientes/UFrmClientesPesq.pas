unit UFrmClientesPesq;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UFrmPesquisaPadrao, DB, Buttons, PngSpeedButton, StdCtrls, ExtCtrls;

type
  TFrmClientesPesq = class(TFrmPesquisaPadrao)
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure btnPesqPrxClick(Sender: TObject);
  private

  public
    { Public declarations }
  end;

var
  FrmClientesPesq: TFrmClientesPesq;

implementation

uses UFrmClientes, UDtmPrincipal, UFrmPrincipal;

{$R *.dfm}

procedure TFrmClientesPesq.btnPesqPrxClick(Sender: TObject);
begin
  inherited;
  //
  FrmPrincipal.SetFocus;
end;

procedure TFrmClientesPesq.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  FrmClientes.MudaPesquisa(FALSE);
end;

procedure TFrmClientesPesq.FormShow(Sender: TObject);
begin
  inherited;
  FrmClientes.MudaPesquisa(TRUE);
end;

end.
