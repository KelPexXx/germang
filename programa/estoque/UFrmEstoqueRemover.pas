unit UFrmEstoqueRemover;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UFrmDelRegPadrao, Buttons, PngSpeedButton, StdCtrls, pngimage,
  ExtCtrls;

type
  TFrmEstoqueRemover = class(TFrmDelRegPadrao)
    procedure btnRemoverClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmEstoqueRemover: TFrmEstoqueRemover;

implementation

uses UDtmPrincipal;

{$R *.dfm}

procedure TFrmEstoqueRemover.btnCancelarClick(Sender: TObject);
begin
  inherited;
  FrmEstoqueRemover.Close;
end;

procedure TFrmEstoqueRemover.btnRemoverClick(Sender: TObject);
begin
  inherited;
  try
    if DtmPrincipal.cdsEstoque.Active then
      begin
    DtmPrincipal.cdsEstoque.Filtered := FALSE;
    DtmPrincipal.cdsEstoque.Filter := 'CODESTOQUE = ' + DtmPrincipal.cdsEstoque_VCODESTOQUE.AsString;
    DtmPrincipal.cdsEstoque.Filtered := TRUE;
    DtmPrincipal.cdsEstoque.Delete;
    DtmPrincipal.cdsEstoque.ApplyUpdates(0);
    DtmPrincipal.cdsEstoque.Filtered := FALSE;
    DtmPrincipal.cdsEstoque.Filter := '';
    DtmPrincipal.cdsEstoque_V.Refresh;
    DtmPrincipal.cdsEstoque_V.Refresh;
    FrmEstoqueRemover.Close;
      end;
  except
   on e : Exception do
      begin
        DtmPrincipal.cdsEstoque.Cancel;
        DtmPrincipal.cdsEstoque.Open;
        MessageDlg('Erro: ' + E.Message, mtError, [mbOK], 0);
          MessageDlg('N�o foi possivel remover o item, j� existem registros no sistema.\n'
          +' Remova primeiro os registros de testes e certificados!\n'+'C�d Error:' + e.Message, mtWarning, mbOKCancel, 0);
      end;
  end;
end;

procedure TFrmEstoqueRemover.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  DtmPrincipal.cdsEstoque.Close;
  DtmPrincipal.sdsEstoque.Close;
  FreeAndNil(FrmEstoqueRemover);
end;

procedure TFrmEstoqueRemover.FormShow(Sender: TObject);
begin
  inherited;
  DtmPrincipal.sdsEstoque.Open;
  DtmPrincipal.cdsEstoque.Open;
end;

end.
