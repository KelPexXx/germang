unit UFrmEstoqueEdit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, JvExExtCtrls, JvExtComponent, JvDBRadioPanel, StdCtrls, DBCtrls,
  Mask, ComCtrls, Buttons, PngBitBtn, ExtCtrls, db, JvExMask, JvToolEdit,
  JvMaskEdit, JvCheckedMaskEdit, JvDatePickerEdit, JvDBDatePickerEdit;

type
  TFrmEstoqueEdit = class(TForm)
    pgCadPeca: TPageControl;
    tbInfoGerais: TTabSheet;
    lblCod: TLabel;
    lblDesc: TLabel;
    lblFabricante: TLabel;
    lblQuantidade: TLabel;
    lblValor: TLabel;
    lblSerial: TLabel;
    Label2: TLabel;
    lblQntMin: TLabel;
    lblUltCOmpra: TLabel;
    dbCodigo: TDBEdit;
    dbDesc: TDBEdit;
    dbFabricante: TDBEdit;
    dbQuantidade: TDBEdit;
    dbValor: TDBEdit;
    dbSerie: TDBEdit;
    dbBarras: TDBEdit;
    dbQntMin: TDBEdit;
    dbUltCompra: TJvDBDatePickerEdit;
    pBottom: TPanel;
    btnSalvarCad: TPngBitBtn;
    btnCancelarCad: TPngBitBtn;
    btnEditar: TPngBitBtn;
    dsEdit: TDataSource;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure btnCancelarCadClick(Sender: TObject);
    procedure btnEditarClick(Sender: TObject);
    procedure btnSalvarCadClick(Sender: TObject);
  private
    procedure AbrirDataSets;
    procedure FecharDataSets;
    procedure ModoEdit(modo:boolean);
  public
    procedure Seleciona(cod:integer);
  end;

var
  FrmEstoqueEdit: TFrmEstoqueEdit;

implementation

uses UDtmPrincipal;

{$R *.dfm}

{ TFrmEstoqueEdit }

procedure TFrmEstoqueEdit.AbrirDataSets;
begin
  with DtmPrincipal do
    begin
      sdsEstoque.Open;
      cdsEstoque.Open;
    end
end;

procedure TFrmEstoqueEdit.btnCancelarCadClick(Sender: TObject);
begin
  try
    FrmEstoqueEdit.Close;
  except on E: Exception do
    begin
      DtmPrincipal.cdsEstoque.Cancel;
      DtmPrincipal.cdsEstoque.Open;
      MessageDlg('Erro: '+E.Message, mtError, [mbOK], 0);
    end;
  end;
end;

procedure TFrmEstoqueEdit.btnEditarClick(Sender: TObject);
begin
  try
    DtmPrincipal.cdsEstoque.Edit;
    ModoEdit(false);
    dbDesc.SetFocus;
  except
    on E: Exception do
      begin
        DtmPrincipal.cdsEstoque.Cancel;
        DtmPrincipal.cdsEstoque.Open;
        MessageDlg('Erro: ' + E.Message, mtError, [mbOK], 0);
      end;
  end;
end;

procedure TFrmEstoqueEdit.btnSalvarCadClick(Sender: TObject);
begin
  try
    if (DtmPrincipal.cdsEstoque.State in [db.dsEdit, db.dsInsert]) then
    begin
      if MessageDlg(
        'Confirmar dados?', mtInformation, mbYesNo, 0) = mrYes then
      begin
        DtmPrincipal.cdsEstoque.Post;
        DtmPrincipal.cdsEstoque.ApplyUpdates(0);
        DtmPrincipal.cdsEstoque_V.Refresh;
        DtmPrincipal.cdsEstoque_V.Refresh;
        DtmPrincipal.cdsEstoque.Refresh;
        DtmPrincipal.cdsEstoque.Refresh;
        FrmEstoqueEdit.Close;
      end;
    end;
  except
    on E: Exception do
    begin
      DtmPrincipal.cdsEstoque.Cancel;
      DtmPrincipal.cdsEstoque.Open;
      MessageDlg('Erro: ' + E.Message, mtError, [mbOK], 0);
    end;
  end;
end;

procedure TFrmEstoqueEdit.FecharDataSets;
begin
  with DtmPrincipal do
    begin
      cdsEstoque.Close;
      sdsEstoque.Close;
    end
end;

procedure TFrmEstoqueEdit.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  FreeAndNil(FrmEstoqueEdit);
end;

procedure TFrmEstoqueEdit.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
var
  Resposta: Integer;
begin
  if (dsEdit.DataSet <> nil) then
    begin
       if (DtmPrincipal.cdsEstoque.Active) and (DtmPrincipal.cdsEstoque.State in [db.dsEdit, db.dsInsert]) then
        begin
          Resposta := MessageDlg('O registro atual ainda n�o foi salvo! Deseja Salvar as altera��es antes de sair?',mtInformation, mbYesNoCancel, 0);
          case Resposta of
            mrYes:
              begin
                DtmPrincipal.cdsEstoque.Post;
                DtmPrincipal.cdsEstoque.ApplyUpdates(0);
                FecharDataSets;
                CanClose := True;
              end;
            mrNo:
              begin
                DtmPrincipal.cdsEstoque.Cancel;
                FecharDataSets;
                CanClose := True;
              end;
            else
              begin
                CanClose := False;
                exit;
              end;
          end;
        end
      else
        CanClose := True;
    end;
end;

procedure TFrmEstoqueEdit.FormShow(Sender: TObject);
begin
  try
    AbrirDataSets;
    Seleciona(DtmPrincipal.cdsEstoque_VCODESTOQUE.AsInteger);
  except
    on E: Exception do
    begin
      DtmPrincipal.cdsEstoque.Cancel;
      DtmPrincipal.cdsEstoque.Open;
      MessageDlg('Erro: ' + E.Message, mtError, [mbOK], 0);
    end;
  end;
  ModoEdit(true);
end;

procedure TFrmEstoqueEdit.ModoEdit(modo: boolean);
begin
  dbDesc.ReadOnly := modo;
  dbFabricante.ReadOnly := modo;
  dbQuantidade.ReadOnly := modo;
  dbValor.ReadOnly := modo;
  dbSerie.ReadOnly := modo;
  dbBarras.ReadOnly := modo;
  dbQntMin.ReadOnly := modo;
  dbUltCompra.ReadOnly := modo;
  btnEditar.Enabled :=  modo;
  btnSalvarCad.Enabled := not modo;
end;

procedure TFrmEstoqueEdit.Seleciona(cod: integer);
begin
  DtmPrincipal.cdsEstoque.Open;
  DtmPrincipal.cdsEstoque.Filtered := false;
  DtmPrincipal.cdsEstoque.Filter := 'CODESTOQUE = ' + IntToStr(cod);
  DtmPrincipal.cdsEstoque.Filtered := true;
  DtmPrincipal.cdsEstoque.First;
end;

end.
