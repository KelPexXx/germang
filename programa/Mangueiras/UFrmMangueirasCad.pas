unit UFrmMangueirasCad;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, PngBitBtn, ExtCtrls, JvExExtCtrls, JvExtComponent,
  JvDBRadioPanel, DB, Mask, DBCtrls, ComCtrls, JvExMask, JvToolEdit, JvDBLookup,
  JvDBLookupComboEdit, PngSpeedButton, JvMaskEdit, JvDBFindEdit, JvExStdCtrls,
  JvCombobox, JvDBSearchComboBox, JvEdit, JvDBSearchEdit, JvExControls,
  JvCheckedMaskEdit, JvDatePickerEdit, JvDBDatePickerEdit;

type
  TFrmMangueirasCad = class(TForm)
    pBottom: TPanel;
    btnSalvarCad: TPngBitBtn;
    btnCancelarCad: TPngBitBtn;
    pCentro: TPanel;
    pgPrincipal: TPageControl;
    tsInfoGer: TTabSheet;
    lblCodMang: TLabel;
    dbCodigo: TDBEdit;
    dsMangueiras: TDataSource;
    lblNumSerie: TLabel;
    dbNumSerie: TDBEdit;
    lblTipo: TLabel;
    lblCompReal: TLabel;
    dbCompReal: TDBEdit;
    lblDiametro: TLabel;
    lblCompNominal: TLabel;
    lblFabricante: TLabel;
    dbFab: TDBEdit;
    lblMarcaduto: TLabel;
    lblMarcaUniao: TLabel;
    dbMarcUniao: TDBEdit;
    lblEstado: TLabel;
    lblCodCLi: TLabel;
    dbUso: TJvDBRadioPanel;
    btnBuscarCodCliente: TPngSpeedButton;
    dsClientes: TDataSource;
    edtBuscaCliente: TEdit;
    gbInfoCliente: TGroupBox;
    dbCnpjCliente: TDBText;
    lblCNPJCLI: TLabel;
    lblFantasiaCli: TLabel;
    lblRazaoCLi: TLabel;
    dbRazaoCliente: TDBText;
    dbFantasiaCliente: TDBText;
    lblDtUltInsp: TLabel;
    lblDtProxManut: TLabel;
    lblDtProxInsp: TLabel;
    lblDtUltManut: TLabel;
    tsLacre: TTabSheet;
    lblCodLacre: TLabel;
    dbCodLacre: TDBEdit;
    dsLacre: TDataSource;
    btnBuscaLacre: TPngSpeedButton;
    lblDtUsoLacre: TLabel;
    lblNumLacre: TLabel;
    dbNumLacre: TDBText;
    dbDataLacre: TDBText;
    lblRevestimento: TLabel;
    dbTipo: TDBLookupComboBox;
    dsMangTipo: TDataSource;
    lblDtFab: TLabel;
    lblCompLuva: TLabel;
    dbCompLuva: TDBEdit;
    dbDTProxManut: TJvDBDatePickerEdit;
    dbDtUltManut: TJvDBDatePickerEdit;
    dbDtUltInsp: TJvDBDatePickerEdit;
    dbDtProxInsp: TJvDBDatePickerEdit;
    dbDtFab: TJvDBDatePickerEdit;
    dbDiametro: TDBComboBox;
    dbCompNom: TDBComboBox;
    dbCarcaca: TDBComboBox;
    dbMarcDuto: TDBEdit;
    lblMCompNom: TLabel;
    lblMCompReal: TLabel;
    lblMMDiametro: TLabel;
    lblMMCompLuva: TLabel;
    procedure btnCancelarCadClick(Sender: TObject);
    procedure btnSalvarCadClick(Sender: TObject);
    procedure btnBuscarCodClienteClick(Sender: TObject);
    procedure btnBuscaLacreClick(Sender: TObject);
    procedure dbCodLacreExit(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure edtBuscaClienteExit(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure dbCompRealExit(Sender: TObject);
  private
    procedure AbrirDataSets;
    procedure FecharDataSets;
    function PesquisaCliente(codigo:integer):boolean;
    function PesquisaLacre(codigo:integer):boolean;
    function ValidaForm:Boolean;
    procedure AlteraUsoLacre;
  public
    procedure ScanOpen;
  end;

var
  FrmMangueirasCad: TFrmMangueirasCad;

implementation

uses UDtmPrincipal, UFrmLacre, UFrmMangueirasBuscaCliCad,
  UFrmMangueirasBuscaCliEdit;

{$R *.dfm}

procedure TFrmMangueirasCad.AbrirDataSets;
begin
  with DtmPrincipal do
    begin
      sdsMangueiras.Open;
      sdsClientes.Open;
      sdsMangTipo.Open;
      sdsMangLacre.Open;
      cdsMangueiras.Open;
      cdsClientes.Open;
      cdsMangTipo.Open;
      cdsMangLacre.Open;
    end;
end;

procedure TFrmMangueirasCad.FecharDataSets;
begin
  with DtmPrincipal do
    begin
      cdsMangueiras.Close;
      cdsClientes.Close;
      cdsMangTipo.Close;
      cdsMangLacre.Close;
      sdsMangueiras.Close;
      sdsClientes.Close;
      sdsMangTipo.Close;
      sdsMangLacre.Close;
    end;
end;

Procedure TFrmMangueirasCad.dbCodLacreExit(Sender: TObject);
begin
  if Length(dbCodLacre.Text) <> 0 then
    if PesquisaLacre(strtoint(dbCodLacre.Text)) then
      begin
        DtmPrincipal.cdsMangLacre.Refresh;
        dbNumLacre.Visible := true;
        dbDataLacre.Visible := true;
      end
    else
      begin
        dbNumLacre.Visible := false;
        dbDataLacre.Visible := false;
      end;
end;

procedure TFrmMangueirasCad.dbCompRealExit(Sender: TObject);
begin
  if dbCompReal.Text <> '' then
    begin
      if (strtoint(dbCompNom.Text)*0.98 > StrToCurr(dbCompReal.Text)) then
        Showmessage('Aten��o o comprimento real da mangueira ultrapassa o limite m�nimo definido pela norma!');
    end
  else
    begin
      Showmessage('Erro: Comprimento real n�o informado!');
    end;
end;

procedure TFrmMangueirasCad.edtBuscaClienteExit(Sender: TObject);
begin
  btnBuscarCodClienteClick(sender);
end;

function TFrmMangueirasCad.PesquisaCliente(codigo: integer): boolean;
begin
  result := FALSE;
  DtmPrincipal.cdsClientes.Filtered := FALSE;
  DtmPrincipal.cdsClientes.Filter:= 'CODCLI = ' + inttostr(codigo);
  DtmPrincipal.cdsClientes.Filtered := TRUE;
  if DtmPrincipal.cdsClientes.RecordCount > 0 then
    begin
    DtmPrincipal.cdsMangueirasCODCLI.AsInteger := DtmPrincipal.cdsClientesCODCLI.AsInteger;
    result := TRUE;
    end
  else
    ShowMessage('Cliente n�o encontrado!');
end;

function TFrmMangueirasCad.PesquisaLacre(codigo: integer): boolean;
begin
  result := FALSE;
  DtmPrincipal.cdsMangLacre.Filtered := FALSE;
  DtmPrincipal.cdsMangLacre.Filter := 'ESTADO = 0 AND CODLACRE = ' + inttostr(codigo);
  DtmPrincipal.cdsMangLacre.Filtered := TRUE;
  if DtmPrincipal.cdsMangLacre.RecordCount > 0 then
    begin
      DtmPrincipal.cdsMangueirasCODLACRE.AsInteger := DtmPrincipal.cdsMangLacreCODLACRE.AsInteger;
      result := TRUE;
    end
  else
    Showmessage('Lacre j� em uso ou n�o encontrado!');
end;

procedure TFrmMangueirasCad.AlteraUsoLacre;
begin
  if (trim(dbCodLacre.Text) <> '0') AND (Length(trim(dbCodLacre.Text)) <> 0) then
    begin
      DtmPrincipal.sdsAux.CommandText := 'UPDATE TB_MANG_LACRE SET ESTADO = 1, DATALACRE = CURRENT_TIMESTAMP WHERE CODLACRE = ' + dbCodLacre.Text;
      DtmPrincipal.sdsAux.ExecSQL(TRUE);
    end;
end;

procedure TFrmMangueirasCad.ScanOpen;
begin
  if FrmMangueirasBuscaCliCad <> nil then
    FrmMangueirasBuscaCliCad.Close;
  if FrmLacre <> nil then
    FrmLacre.Close;
end;

function TFrmMangueirasCad.ValidaForm: Boolean;
begin
  result := TRUE;
  if edtBuscaCliente.Text = '' then
    begin
      Showmessage('� necessario selecionar um cliente para adicionar a mangueira');
      result := FALSE;
    end;
  if dbCodLacre.Text = '' then
    begin
      Showmessage('Aten��o o lacre quando n�o existir deve ser igual a "0"!');
      result := FALSE;
    end;
end;

procedure TFrmMangueirasCad.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  FreeAndNil(FrmMangueirasCad);
end;

procedure TFrmMangueirasCad.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
var
  resposta:integer;
begin
   if (dsMangueiras.DataSet <> nil) then
    begin
      if (DtmPrincipal.cdsMangueiras.Active) and (DtmPrincipal.cdsMangueiras.State in [dsEdit, dsInsert]) then
        begin
          Resposta := MessageDlg('O registro atual ainda n�o foi salvo! Deseja Salvar as altera��es antes de sair?',mtInformation, mbYesNoCancel, 0);
          case Resposta of
            mrYes:
              begin
                if ValidaForm then
                  begin
                    DtmPrincipal.cdsMangueiras.Post;
                    DtmPrincipal.cdsMangueiras.ApplyUpdates(0);
                    AlteraUsoLacre;
                    DtmPrincipal.cdsMangueiras_V.Refresh;
                    DtmPrincipal.cdsMangueiras_V.Refresh;
                    DtmPrincipal.cdsMangueiras.Refresh;
                    DtmPrincipal.cdsMangueiras.Refresh;
                    DtmPrincipal.cdsMangLacre.Refresh;
                    DtmPrincipal.cdsMangLacre.Refresh;
                    FecharDataSets;
                    CanClose := TRUE;
                  end
                else
                  CanClose := FALSE;
              end;
            mrNo:
              begin
                DtmPrincipal.cdsMangueiras.Cancel;
                FecharDataSets;
                CanCLose := TRUE;
              end;
            else
              begin
                CanClose := FALSE;
                exit;
              end;
          end;
        end;
    end;
end;

procedure TFrmMangueirasCad.FormShow(Sender: TObject);
begin
  dbUso.Value := '1';
  try
    AbrirDataSets;
    DtmPrincipal.cdsMangueiras.Append;
    dbCodLacre.Text := '0';
    dbNumSerie.SetFocus;
  except
    on E: Exception do
    begin
      DtmPrincipal.cdsMangueiras.Cancel;
      DtmPrincipal.cdsMangueiras.Open;
      MessageDlg('Erro: ' + E.Message, mtError, [mbOK], 0);
    end;
  end;
end;

procedure TFrmMangueirasCad.btnBuscaLacreClick(Sender: TObject);
begin
  if not Assigned(FrmLacre) then
    Application.CreateForm(TFrmLacre,FrmLacre);
  FrmLacre.Show;
  FrmLacre.Destino := 1;
end;

procedure TFrmMangueirasCad.btnBuscarCodClienteClick(Sender: TObject);
begin
  if length(edtBuscaCliente.Text) <> 0  then
    if PesquisaCliente(strtoint(edtBuscaCliente.Text)) then
      begin
        dbRazaoCliente.Visible := TRUE;
        dbFantasiaCliente.Visible := TRUE;
        dbCnpjCliente.Visible := TRUE;
      end
    else
      begin
        dbRazaoCliente.Visible := FALSE;
        dbFantasiaCliente.Visible := FALSE;
        dbCnpjCliente.Visible := FALSE;
      end
  else
    begin
      DtmPrincipal.cdsClientes.Filtered := FALSE;
      DtmPrincipal.cdsClientes.Filter := '';
      if not Assigned(FrmMangueirasBuscaCliCad) then
        Application.CreateForm(TFrmMangueirasBuscaCliCad,FrmMangueirasBuscaCliCad);
      FrmMangueirasBuscaCliCad.Show;
    end;
end;

procedure TFrmMangueirasCad.btnCancelarCadClick(Sender: TObject);
begin
  try
    if (DtmPrincipal.cdsMangueiras.State in [dsEdit, dsInsert]) then
      begin
        FrmMangueirasCad.Close;
      end;
  except on E: Exception do
    begin
      DtmPrincipal.cdsMangueiras.Cancel;
      DtmPrincipal.cdsMangueiras.Open;
      MessageDlg('Erro: '+E.Message, mtError, [mbOK], 0);
    end;
  end;
end;

procedure TFrmMangueirasCad.btnSalvarCadClick(Sender: TObject);
begin
  try
    if (DtmPrincipal.cdsMangueiras.State in [dsEdit, dsInsert]) AND (ValidaForm) then
    begin
      if MessageDlg(
        'Confirmar dados?', mtInformation, mbYesNo, 0) = mrYes then
      begin
        DtmPrincipal.cdsMangueiras.Post;
        DtmPrincipal.cdsMangueiras.ApplyUpdates(0);
        AlteraUsoLacre;
        DtmPrincipal.cdsMangueiras_V.Refresh;
        DtmPrincipal.cdsMangueiras_V.Refresh;
        DtmPrincipal.cdsMangueiras.Refresh;
        DtmPrincipal.cdsMangueiras.Refresh;
        DtmPrincipal.cdsMangLacre.Refresh;
        DtmPrincipal.cdsMangLacre.Refresh;
        DtmPrincipal.cdsClientes.Filtered := FALSE;
        DtmPrincipal.cdsClientes.Filter := '';
        FrmMangueirasCad.Close;
      end;
    end;
  except
    on E: Exception do
    begin
      DtmPrincipal.cdsMangueiras.Cancel;
      DtmPrincipal.cdsMangueiras.Open;
      MessageDlg('Erro: ' + E.Message, mtError, [mbOK], 0);
    end;
  end;
end;

end.

