inherited FrmEstoqueOrdenar: TFrmEstoqueOrdenar
  Caption = 'Ordenar Itens'
  OnClose = FormClose
  OnCreate = FormCreate
  ExplicitWidth = 874
  PixelsPerInch = 96
  TextHeight = 13
  inherited pCentro: TPanel
    inherited pBotoes: TPanel
      inherited btnOrdenar: TPngSpeedButton
        OnClick = btnOrdenarClick
      end
      inherited btnLimparOrdem: TPngSpeedButton
        OnClick = btnLimparOrdemClick
      end
      inherited btnFechar: TPngSpeedButton
        ExplicitLeft = 215
        ExplicitTop = 6
        ExplicitWidth = 107
      end
    end
    inherited rgOpcoes: TRadioGroup
      Columns = 3
      ItemIndex = 0
      Items.Strings = (
        'C'#243'digo'
        'Descri'#231#227'o'
        'Fabricante'
        'N'#250'mero de S'#233'rie'
        'C'#243'digo de Barras'
        'Valor'
        'Quantidade'
        'Quantidade Minima'
        'Ultima Compra')
    end
  end
end
