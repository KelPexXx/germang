unit UFrmUsuariosCad;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, PngBitBtn, ExtCtrls, DB, Mask, DBCtrls;

type
  TFrmUsuariosCad = class(TForm)
    pTop: TPanel;
    pBottom: TPanel;
    btnSalvarCad: TPngBitBtn;
    btnCancelarCad: TPngBitBtn;
    lblCodigo: TLabel;
    dbCodigo: TDBEdit;
    dsUsuarios: TDataSource;
    lblNome: TLabel;
    dbNome: TDBEdit;
    lblRg: TLabel;
    dbRg: TDBEdit;
    lblCpf: TLabel;
    dbCPF: TDBEdit;
    lblObservacao: TLabel;
    dbObserva��o: TDBEdit;
    procedure FormShow(Sender: TObject);
    procedure btnCancelarCadClick(Sender: TObject);
    procedure btnSalvarCadClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    procedure AbrirDataSets;

    function ValidaForm:Boolean;
  public
    { Public declarations }
  end;

var
  FrmUsuariosCad: TFrmUsuariosCad;

implementation

uses UDtmPrincipal;

{$R *.dfm}

procedure TFrmUsuariosCad.AbrirDataSets;
begin
  DtmPrincipal.cdsTesteTecnico.Open;
end;

procedure TFrmUsuariosCad.btnCancelarCadClick(Sender: TObject);
begin
  try
    if (DtmPrincipal.cdsTesteTecnico.State in [dsEdit, dsInsert]) then
      begin
        FrmUsuariosCad.Close;
      end;
  except on E: Exception do
    begin
      DtmPrincipal.cdsTesteTecnico.Cancel;
      DtmPrincipal.cdsTesteTecnico.Open;
      MessageDlg('Erro: '+E.Message, mtError, [mbOK], 0);
    end;
  end;
end;

procedure TFrmUsuariosCad.btnSalvarCadClick(Sender: TObject);
begin
  try
    if (DtmPrincipal.cdsTesteTecnico.State in [dsEdit, dsInsert]) AND (ValidaForm) then
    begin
      if MessageDlg(
        'Confirmar dados?', mtInformation, mbYesNo, 0) = mrYes then
      begin
        DtmPrincipal.cdsTesteTecnico.Post;
        DtmPrincipal.cdsTesteTecnico.ApplyUpdates(0);
        DtmPrincipal.cdsTesteTecnico.Refresh;
        DtmPrincipal.cdsTesteTecnico.Refresh;
        DtmPrincipal.cdsTesteTecnico.Refresh;
        FrmUsuariosCad.Close;
      end;
    end;
  except
    on E: Exception do
    begin
      DtmPrincipal.cdsTesteTecnico.Cancel;
      DtmPrincipal.cdsTesteTecnico.Open;
      MessageDlg('Erro: ' + E.Message, mtError, [mbOK], 0);
    end;
  end;
end;

procedure TFrmUsuariosCad.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  FreeAndNil(FrmUsuariosCad);
end;

procedure TFrmUsuariosCad.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
var
  resposta:integer;
begin
   if (dsUsuarios.DataSet <> nil) then
    begin
      if (DtmPrincipal.cdsTesteTecnico.Active) and (DtmPrincipal.cdsTesteTecnico.State in [dsEdit, dsInsert]) then
        begin
          Resposta := MessageDlg('O registro atual ainda n�o foi salvo! Deseja Salvar as altera��es antes de sair?',mtInformation, mbYesNoCancel, 0);
          case Resposta of
            mrYes:
              begin
                if ValidaForm then
                  begin
                    DtmPrincipal.cdsTesteTecnico.Post;
                    DtmPrincipal.cdsTesteTecnico.ApplyUpdates(0);
                    DtmPrincipal.cdsTesteTecnico.Refresh;
                    DtmPrincipal.cdsTesteTecnico.Refresh;
                    DtmPrincipal.cdsTesteTecnico.Refresh;
                    CanClose := TRUE;
                  end
                else
                  CanClose := FALSE;
              end;
            mrNo:
              begin
                DtmPrincipal.cdsTesteTecnico.Cancel;
                CanClose := TRUE;
              end;
            else
              begin
                CanClose := FALSE;
                exit;
              end;
          end;
        end;
    end;
end;

procedure TFrmUsuariosCad.FormShow(Sender: TObject);
begin
try
    AbrirDataSets;
    DtmPrincipal.cdsTesteTecnico.Append;
    dbNome.SetFocus;
  except
    on E: Exception do
    begin
      DtmPrincipal.cdsTesteTecnico.Cancel;
      DtmPrincipal.cdsTesteTecnico.Open;
      MessageDlg('Erro: ' + E.Message, mtError, [mbOK], 0);
    end;
  end;
end;

function TFrmUsuariosCad.ValidaForm: Boolean;
begin
  Result := FALSE;
  if Length(dbNome.Text) = 0 then
    begin
      MessageDlg('Erro! � necess�rio preencher o campo nome!',mtWarning,[mbOk],0);
    end
  else
    begin
      Result := TRUE;
    end;
end;

end.
