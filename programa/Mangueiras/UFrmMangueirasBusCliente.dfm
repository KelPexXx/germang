inherited FrmMangueirasBusCliente: TFrmMangueirasBusCliente
  Caption = 'Buscar Cliente'
  ClientWidth = 680
  OnClose = FormClose
  ExplicitWidth = 686
  PixelsPerInch = 96
  TextHeight = 13
  inherited pTopo: TPanel
    Width = 680
    Height = 97
    ExplicitWidth = 680
    ExplicitHeight = 97
    inherited btnBuscar: TPngSpeedButton
      Left = 495
      OnClick = btnBuscarClick
      ExplicitLeft = 495
    end
    inherited edtBusca: TLabeledEdit
      Width = 369
      ExplicitWidth = 369
    end
    inherited rgOpcoes: TRadioGroup
      Top = 48
      Width = 672
      Height = 45
      Columns = 4
      ItemIndex = 0
      Items.Strings = (
        'C'#243'digo'
        'CNPJ'
        'Raz'#227'o Social'
        'Nome Fantasia')
      ExplicitTop = 48
      ExplicitWidth = 672
      ExplicitHeight = 45
    end
  end
  inherited dgBusca: TDBGrid
    Top = 97
    Width = 680
    Height = 271
    Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
    ReadOnly = True
    Columns = <
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'CODCLI'
        Title.Alignment = taCenter
        Width = 57
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CNPJ'
        Title.Alignment = taCenter
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'RAZAO'
        Width = 233
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'FANTASIA'
        Width = 233
        Visible = True
      end>
  end
  inherited pBaixo: TPanel
    Width = 680
    ExplicitWidth = 680
    inherited btnSelecionar: TPngSpeedButton
      Left = 556
      OnClick = btnSelecionarClick
      ExplicitLeft = 556
    end
  end
  inherited dsBusca: TDataSource
    DataSet = DtmPrincipal.cdsClientes
  end
end
