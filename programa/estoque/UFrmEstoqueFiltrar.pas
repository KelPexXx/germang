unit UFrmEstoqueFiltrar;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UFrmFiltrarPadrao, DB, StdCtrls, ExtCtrls, Buttons, PngSpeedButton;

type
  TFrmEstoqueFiltrar = class(TFrmFiltrarPadrao)
    procedure rgOpcoesClick(Sender: TObject);
    procedure btnLimparFiltroClick(Sender: TObject);
    procedure btnFiltrarClick(Sender: TObject);
  private
    function convTipo(filtro:string):String;
  public
    { Public declarations }
  end;

var
  FrmEstoqueFiltrar: TFrmEstoqueFiltrar;

implementation

uses UDtmPrincipal;

{$R *.dfm}

procedure TFrmEstoqueFiltrar.btnFiltrarClick(Sender: TObject);
begin
  inherited;
  if Trim(edtFiltro.Text) <> '' then
    begin
      dsFiltro.DataSet.Filtered := false;
      case rgOpcoes.ItemIndex of
        0:Begin
          dsFiltro.DataSet.Filter := 'CODESTOQUE = ' + QuotedStr(edtFiltro.Text);
        End;
        1:Begin
          dsFiltro.DataSet.Filter := 'DESCESTOQUE like ' + QuotedStr((convTipo(edtFiltro.Text)));
        End;
        2:Begin
          dsFiltro.DataSet.Filter := 'FABRICESTOQUE like ' + QuotedStr((convTipo(edtFiltro.Text)));
        End;
        3:Begin
          dsFiltro.DataSet.Filter := 'SERIALESTOQUE = ' + QuotedStr(edtFiltro.Text);
        End;
        4:Begin
          dsFiltro.DataSet.Filter := 'BARRASESTOQUE = ' + QuotedStr(edtFiltro.Text);
        End;
        5:Begin
          dsFiltro.DataSet.Filter := 'VALORESTOQUE = ' + QuotedStr(edtFiltro.Text);
        End;
        6:Begin
          dsFiltro.DataSet.Filter := 'QTDESTOQUE = ' + QuotedStr(edtFiltro.Text);
        End;
        7:Begin
          dsFiltro.DataSet.Filter := 'QTDMINESTOQUE like ' + QuotedStr(edtFiltro.Text);
        End;
        10:Begin
          dsFiltro.DataSet.Filter := 'ULTCOMPESTOQUE like ' + QuotedStr((convTipo(edtFiltro.Text)));
        End;
      end;
      dsFiltro.DataSet.Filtered := true;
      if dsFiltro.DataSet.RecordCount = 0 then
        begin
          if MessageDlg('Nenhum registro encontrado! Limpar filtros?', mtInformation, mbYesNo, 0)= mrYes then
            btnLimparFiltroClick(sender);
        end
    end
  else
    Showmessage('Erro, entre com um dado para o filtro!');
end;

procedure TFrmEstoqueFiltrar.btnLimparFiltroClick(Sender: TObject);
begin
  inherited;
  dsFiltro.DataSet.Filtered := FALSE;
  dsFiltro.DataSet.Filter := '';
end;

function TFrmEstoqueFiltrar.convTipo(filtro: string): String;
begin
  case cbTipoPesquisa.ItemIndex of
    0:begin
      result := edtFiltro.Text+'%';
    end;
    1:begin
      result := edtFiltro.Text;
    end;
    2:begin
      result := '%'+edtFiltro.Text+'%';
    end;
  end;
end;

procedure TFrmEstoqueFiltrar.rgOpcoesClick(Sender: TObject);
begin
  inherited;
  case rgOpcoes.ItemIndex of
    0,3,4,5,6,7:begin
      cbTipoPesquisa.ItemIndex := 1;
      cbTipoPesquisa.Enabled := FALSE;
    end;
  else
    begin
      cbTipoPesquisa.Enabled := TRUE;
      cbTipoPesquisa.ItemIndex := 0;
    end;
  end;
end;

end.
