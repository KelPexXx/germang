unit UFrmMangueirasOrdenar;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UFrmOrdenarPadrao, StdCtrls, ExtCtrls, Buttons, PngSpeedButton;

type
  TFrmMangueirasOrdenar = class(TFrmOrdenarPadrao)
    procedure btnLimparOrdemClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure btnOrdenarClick(Sender: TObject);
  private
    { Private declarations }
  public
    sSelect:String;
  end;

var
  FrmMangueirasOrdenar: TFrmMangueirasOrdenar;

implementation

uses UDtmPrincipal;

{$R *.dfm}

procedure TFrmMangueirasOrdenar.btnLimparOrdemClick(Sender: TObject);
begin
  DtmPrincipal.cdsMangueiras_V.Close;
  DtmPrincipal.cdsMangueiras_V.Close;
  DtmPrincipal.cdsMangueiras_V.CommandText := sSelect + ' ORDER BY CODCLI';
  DtmPrincipal.cdsMangueiras_V.Open;
  DtmPrincipal.cdsMangueiras_V.Open;
end;

procedure TFrmMangueirasOrdenar.btnOrdenarClick(Sender: TObject);
var
  ordem,de:String;
begin
  inherited;
  ordem := '';
  DtmPrincipal.cdsMangueiras_V.Close;
  DtmPrincipal.sdsMangueiras_V.Close;
  case rgOpcoes.ItemIndex of
    0:begin ordem := 'CODCLI'; end;
    1:begin ordem := 'NUMERO'; end;
    2:begin ordem := 'FABRICANTE'; end;
    3:begin ordem := 'DIAMETRO'; end;
    4:begin ordem := 'DATAFAB'; end;
    5:begin ordem := 'DATAPRXMANUT'; end;
    6:begin ordem := 'DATAULTMANUT'; end;
    7:begin ordem := 'DATAPRXINSP'; end;
    8:begin ordem := 'DATAULTINSP'; end;
    9:begin ordem := 'LACRE'; end;
    10:begin ordem := 'DATA_LACRE'; end;
  end;
  if rgOrdem.ItemIndex = 0 then
     de := ' ASC'
  else
    de := ' DESC';
  DtmPrincipal.sdsMangueiras_V.CommandText := sSelect+' ORDER BY '+ordem+de;
  DtmPrincipal.sdsMangueiras_V.Open;
  DtmPrincipal.cdsMangueiras_V.Open;
end;

procedure TFrmMangueirasOrdenar.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  FreeAndNil(FrmMangueirasOrdenar);
end;

procedure TFrmMangueirasOrdenar.FormCreate(Sender: TObject);
begin
  inherited;
  sSelect := 'SELECT * FROM TB_MANGUEIRAS_V';
end;

end.
