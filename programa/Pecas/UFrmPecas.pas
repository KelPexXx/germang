unit UFrmPecas;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UFrmFormularioPadrao, DB, ImgList, PngImageList, ExtCtrls, Buttons,
  PngSpeedButton, Grids, DBGrids, ComCtrls, ToolWin, pngimage;

type
  TFrmPecas = class(TFrmFormularioPadrao)
    procedure btnMaddClick(Sender: TObject);
  private
    { Private declarations }
  public
    procedure ScanOpen;
  end;

var
  FrmPecas: TFrmPecas;

implementation

uses UDtmPrincipal, UFrmPecasCad;

{$R *.dfm}

{ TFrmPecas }

procedure TFrmPecas.btnMaddClick(Sender: TObject);
begin
  inherited;
  ScanOpen;
  if not Assigned(FrmPecasCad) then
    Application.CreateForm(TFrmPecasCad,FrmPecasCad);
  FrmPecasCad.Show;
end;

procedure TFrmPecas.ScanOpen;
begin
  if FrmPecasCad <> nil then
    FrmPecasCad.Close;
end;

end.
