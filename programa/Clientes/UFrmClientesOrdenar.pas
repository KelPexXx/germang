unit UFrmClientesOrdenar;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UFrmOrdenarPadrao, StdCtrls, ExtCtrls, Buttons, PngSpeedButton;

type
  TFrmClientesOrdenar = class(TFrmOrdenarPadrao)
    procedure FormCreate(Sender: TObject);
    procedure btnOrdenarClick(Sender: TObject);
    procedure btnLimparOrdemClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    sSelect:String;
  end;

var
  FrmClientesOrdenar: TFrmClientesOrdenar;

implementation

uses UDtmPrincipal;

{$R *.dfm}

procedure TFrmClientesOrdenar.btnLimparOrdemClick(Sender: TObject);
begin
  DtmPrincipal.cdsClientes_V.Close;
  DtmPrincipal.sdsClientes_V.Close;
  DtmPrincipal.sdsClientes_V.CommandText := sSelect + ' ORDER BY CODCLI';
  DtmPrincipal.sdsClientes_V.Open;
  DtmPrincipal.cdsClientes_V.Open;
end;

procedure TFrmClientesOrdenar.btnOrdenarClick(Sender: TObject);
var
  ordem,de:string;
begin
  ordem := '';
  DtmPrincipal.cdsClientes_V.Close;
  DtmPrincipal.sdsClientes_V.Close;
  case rgOpcoes.ItemIndex of
    0:begin ordem := 'CODCLI'; end;
    1:begin ordem := 'RAZAO'; end;
    2:begin ordem := 'FANTASIA'; end;
    3:begin ordem := 'ENDCEP'; end;
    4:begin ordem := 'ENDBAIRRO'; end;
    5:begin ordem := 'CIDADE'; end;
    6:begin ordem := 'ESTADO'; end;
    7:begin ordem := 'DATAINSSISPREV'; end;
    8:begin ordem := 'DESC_ATUA'; end;
  end;
  if rgOrdem.ItemIndex = 0 then
     de := ' ASC'
  else
    de := ' DESC';
  DtmPrincipal.sdsClientes_V.CommandText := sSelect+' ORDER BY '+ordem+de;
  DtmPrincipal.sdsClientes_V.Open;
  DtmPrincipal.cdsClientes_V.Open;
end;

procedure TFrmClientesOrdenar.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  FreeAndNil(FrmClientesOrdenar);
end;

procedure TFrmClientesOrdenar.FormCreate(Sender: TObject);
begin
  inherited;
  sSelect := 'SELECT * FROM TB_CLIENTE_V';
end;

end.
