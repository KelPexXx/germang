object FrmEstoqueEdit: TFrmEstoqueEdit
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'Editar Item'
  ClientHeight = 315
  ClientWidth = 485
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pgCadPeca: TPageControl
    Left = 0
    Top = 0
    Width = 485
    Height = 258
    ActivePage = tbInfoGerais
    Align = alClient
    MultiLine = True
    TabOrder = 0
    object tbInfoGerais: TTabSheet
      Caption = 'Informa'#231#245'es Gerais'
      object lblCod: TLabel
        Left = 59
        Top = 16
        Width = 33
        Height = 13
        Caption = 'C'#243'digo'
        FocusControl = dbCodigo
      end
      object lblDesc: TLabel
        Left = 46
        Top = 43
        Width = 46
        Height = 13
        Caption = 'Descri'#231#227'o'
      end
      object lblFabricante: TLabel
        Left = 41
        Top = 70
        Width = 51
        Height = 13
        Caption = 'Fabricante'
      end
      object lblQuantidade: TLabel
        Left = 36
        Top = 97
        Width = 56
        Height = 13
        Caption = 'Quantidade'
      end
      object lblValor: TLabel
        Left = 161
        Top = 97
        Width = 24
        Height = 13
        Caption = 'Valor'
      end
      object lblSerial: TLabel
        Left = 40
        Top = 124
        Width = 52
        Height = 13
        Caption = 'Num. S'#233'rie'
      end
      object Label2: TLabel
        Left = 35
        Top = 151
        Width = 57
        Height = 13
        Caption = 'C'#243'd. Barras'
      end
      object lblQntMin: TLabel
        Left = 279
        Top = 97
        Width = 41
        Height = 13
        Caption = 'Qnt. Min'
      end
      object lblUltCOmpra: TLabel
        Left = 35
        Top = 178
        Width = 57
        Height = 13
        Caption = 'Ult. Compra'
      end
      object dbCodigo: TDBEdit
        Left = 105
        Top = 13
        Width = 50
        Height = 21
        DataField = 'CODESTOQUE'
        DataSource = dsEdit
        ReadOnly = True
        TabOrder = 0
      end
      object dbDesc: TDBEdit
        Left = 105
        Top = 40
        Width = 312
        Height = 21
        DataField = 'DESCESTOQUE'
        DataSource = dsEdit
        TabOrder = 1
      end
      object dbFabricante: TDBEdit
        Left = 105
        Top = 67
        Width = 312
        Height = 21
        DataField = 'FABRICESTOQUE'
        DataSource = dsEdit
        TabOrder = 2
      end
      object dbQuantidade: TDBEdit
        Left = 105
        Top = 94
        Width = 50
        Height = 21
        DataField = 'QTDESTOQUE'
        DataSource = dsEdit
        TabOrder = 3
      end
      object dbValor: TDBEdit
        Left = 191
        Top = 94
        Width = 82
        Height = 21
        DataField = 'VALORESTOQUE'
        DataSource = dsEdit
        TabOrder = 4
      end
      object dbSerie: TDBEdit
        Left = 105
        Top = 121
        Width = 168
        Height = 21
        DataField = 'SERIALESTOQUE'
        DataSource = dsEdit
        TabOrder = 6
      end
      object dbBarras: TDBEdit
        Left = 105
        Top = 148
        Width = 168
        Height = 21
        DataField = 'BARRASESTOQUE'
        DataSource = dsEdit
        TabOrder = 7
      end
      object dbQntMin: TDBEdit
        Left = 326
        Top = 94
        Width = 51
        Height = 21
        DataField = 'QTDMINESTOQUE'
        DataSource = dsEdit
        TabOrder = 5
      end
      object dbUltCompra: TJvDBDatePickerEdit
        Left = 105
        Top = 175
        Width = 87
        Height = 21
        AllowNoDate = True
        DataField = 'ULTCOMPESTOQUE'
        DataSource = dsEdit
        Flat = False
        ParentCtl3D = False
        TabOrder = 8
      end
    end
  end
  object pBottom: TPanel
    Left = 0
    Top = 258
    Width = 485
    Height = 57
    Align = alBottom
    TabOrder = 1
    object btnSalvarCad: TPngBitBtn
      AlignWithMargins = True
      Left = 362
      Top = 4
      Width = 119
      Height = 49
      Align = alRight
      Caption = 'Salvar'
      Default = True
      DoubleBuffered = True
      Enabled = False
      ParentDoubleBuffered = False
      Spacing = 3
      TabOrder = 0
      OnClick = btnSalvarCadClick
      PngImage.Data = {
        89504E470D0A1A0A0000000D49484452000000320000003208060000001E3F88
        B10000022E4944415478DA636418268071A01D30EA91518F0C7230FC3DF2C5C9
        740250561FC47EF3E597C4FB6F7F2430D458D8B07D35B56423D552167696EF4C
        CC2C7FD1C5C5AF9E7D217FEAC00B10FB3F03E3079EBFBF12190F5CF84091473E
        3B991E606464B407B15F7EFAC9F0F2F34F0C355FBDFD81D88F6AA1AA74682783
        D2E11D4822FF2F70FFFDED488C67E8EA9119D3A6016D4458E9E7E7C720252D8D
        C723C47B86AE1E292B2946E1A767643228ABA810F008719E19221E21EC9921E4
        115001C0B09177EFA980A1EF91FFFF0FF2EE3BED30EA91518F0C158F7C737461
        F8121A49B447C0F50812F0F3F747A947D476AD67903B7D88761E79FFF517C3E3
        0F3F30D4FC15166178DBDC49B4470801F339DD0CBC2F9FD1CE2320008A95AF3F
        FF60A8FBAA6BC0F0D1DE992887822C636561626066C4B45616181362372FE3D5
        4F158F0C0630EA91518F8C7A64D423C47984D9DA9E81A3B16B401CFFA3BE8CE1
        EFD183D4F1085B5C2A036B5CCA8078E4DFC5730CDF8B33473D82D7232FDEBC63
        78F1F63D4D1C2E212CC820212234EA11923CF2EBF71F20FE4D138FB0B1B20231
        0B7D3C424F40538FBCFBF899E1DDA7CF3471B8101F2F83103F2F7D3C326CF2C8
        976FDFC1981680878B138CE9E2117A02DAC7C8F71F0CB4003C9C1CF48B91D13C
        420418CD2383DD23A0263CA8294F158F3029AB823DC3C8C3CB404FF0FFCB6786
        5F8B6633FCBB7B9B3A1E192C802C8F802643FF3330180CB4E3D11C7B8167DFE9
        02923C32D4C0A847061B18F5C86003C3C6230004238B5181E0E70B0000000049
        454E44AE426082}
    end
    object btnCancelarCad: TPngBitBtn
      AlignWithMargins = True
      Left = 117
      Top = 4
      Width = 122
      Height = 49
      Align = alRight
      Caption = 'Cancelar'
      Default = True
      DoubleBuffered = True
      ParentDoubleBuffered = False
      Spacing = 3
      TabOrder = 1
      OnClick = btnCancelarCadClick
      PngImage.Data = {
        89504E470D0A1A0A0000000D49484452000000320000003208060000001E3F88
        B1000003434944415478DAED995152DB301086D7E5AD7DA0DCC0796CC34C9313
        E09EA0B9017002C209484E50E7049813949E80F4040933D03E066ED03CB46F34
        FD17C98C11B22C5972E261B2331E1C63AFF65BAD56D22AA25722D1A60DD8826C
        415A2E5B9032F94514AF88BEE07ACF171EF5E4BFE668ECB7BCBE7F20BA6B1D08
        1BFF4074026549C1F02A990374BA43340901E505C200FF88CE707BE46947F686
        68EC03541BE41600F0E83012E1E32D2B11726917406B0159C0F0BF445FC9DC0B
        F724BC3B558C8DD1E000B7BB866FB3B744A71D8035062221AE483F0E96303483
        C20C5E9D9BF4A0377BB237CBA0E680F9EC02630D6282805193774423572FB2CE
        3FF80E469CF8C25883C08BE7F4329C96B892AA1EB0D03D22913454E1DE3D0E06
        A26B08BD708DD439F04D9DB7C239E78657C65DD1BE1F889C23664A765A225DF6
        D600F198CDE0B07E555B95203762001F2A8FFB01C2A90C82D3AFDAFB17FB1573
        9511444E780B45E9044A870D411CC341199C97AA090011D031F58A1144A37089
        4C12BB66271708BE9119928D7E4ACD550E3482A0D11915D2AD6F6FD840149CA8
        86F41CEFF49D417461451E63C30542BECF0E9C159F99C2AB14E4273CBF124B91
        5CEED160BC0E88C2771CC24FE105634F3F22DC9D403473C78FAE58A6AF058245
        135EA5734A6450A20E74AB89291484CE99A6316AEA9129FE1CD405F185D08190
        212A1A010901110CA46E688582D081D40D2D5549E532212484D43725CBA8B04E
        BFBC7803C8DEBA20A44E9EC7E282B1EEE9D765426C0822CC84C872236A519FF2
        DFBAF06A0242B6FD6C8CF2FE67DF506A725A347278614BDBC9178D4D41C82DF0
        A2B807F25A349A96F14D41E81CC8E2B58C974A751BAB11E9B38737846E6C786F
        AC5864AFF000DFAD78D51BC2675B5DBBF8D004041CF68D5E0EE830C5875C4A42
        CCBAA10A4771C1EE4A2DBFDA84943388CC24D3623A2E3498229B8D6B16E8CE22
        4D36E2740B9D49F0029D050C17A12F713BB12C9972EF1EE98AE0AE10CE200598
        B424CC54A83BE579128925476CF8F60210C3468BD88A57472442A22A9BD90A97
        5FD3BAE3CDFBA0E74114A10F7DF4702FEC40CF460E7A3440DC3B896EFC94187F
        4DE2E82DDDF8D15B19148C1CE487A1919C1756CF0F432F5B7918DA06D982B44D
        B6206D935703F21FAFB46142365907040000000049454E44AE426082}
    end
    object btnEditar: TPngBitBtn
      AlignWithMargins = True
      Left = 245
      Top = 4
      Width = 111
      Height = 49
      Align = alRight
      Caption = 'Editar'
      Default = True
      DoubleBuffered = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentDoubleBuffered = False
      ParentFont = False
      Spacing = 3
      TabOrder = 2
      OnClick = btnEditarClick
      PngImage.Data = {
        89504E470D0A1A0A0000000D49484452000000320000003208060000001E3F88
        B1000002124944415478DAEDD4CB2B44511C07F0DF198C478C6C941A56285158
        58A188B214A5883C2265832C943F607662A341CA5C0B191B240B0B8FF15A7853
        365396362C68EC3033C7A52E9ADC7BEE1D73CFF98DEE77757FBF7B3AFD3E9D73
        2F817F12227A000B624190072DE46E64A41B08E9024A179C1313525C423E1084
        108F52534A7B581874902DEF4A6BD1E9D152649F85410559DD3E924F023C6981
        C061F1E17695110C1A8882506AA31814908094DFBEEB94DC60238E9FFDDF3032
        644F86D4A083D06990FF4EE0794C2EF11FE4CEE4B030284F444128350B83F21B
        8944B030E90FF76DF5ADCD5EB5FD8440D4106A184AA1A7A9AE52D2DA933B8485
        88C45042865808EE10BD0825CF29F91D99DDB78B7AD672831845807C9DC80048
        7A977381988DE002E181301DC20B612A8427C234086F84291011889843442162
        0A1189881944342226100C883F43B020FE04C184881A820D11150423C230042B
        C21004334237043B4217241E104C48BC20D8901DFB19DCBE1640181CCC9D0422
        34217437A90C6CB64B0851BF8CC9D1C408466843F69387E501273F0B2D0C0284
        3664CFBE26BF6EFC6AFC864182D084F4F75607C75B2E12325283DFCD9F184408
        55C8BCABB06F76BF68AE20FB19DCED2710813907FFEB1426842AC4355ABEB97E
        9DD7F0F15CEA7C0CCD761E6F00213E08857DA4F6ED4AF4D0BA216383153E7B62
        F8292BF5C53BECBA59163D64D490788C05C1160B822DEFAE3143425EF33B2D00
        00000049454E44AE426082}
    end
  end
  object dsEdit: TDataSource
    DataSet = DtmPrincipal.cdsEstoque
    Left = 416
    Top = 184
  end
end
