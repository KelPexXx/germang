object DtmAutenticacao: TDtmAutenticacao
  OldCreateOrder = False
  Height = 92
  Width = 364
  object sdsUsuarios: TSQLDataSet
    SchemaName = 'sysdba'
    CommandText = 
      'SELECT '#13#10#9'a.CODUSU, '#13#10#9'a.NOME, '#13#10#9'a.LOGIN, '#13#10#9'a.SENHA, '#13#10#9'a.NIVE' +
      'L'#13#10'FROM '#13#10#9'TB_USUARIOS a'#13#10'WHERE'#13#10#9'a.LOGIN = :pLogin AND a.SENHA ' +
      '= :pSenha'
    MaxBlobSize = -1
    Params = <
      item
        DataType = ftString
        Name = 'pLogin'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'pSenha'
        ParamType = ptInput
      end>
    SQLConnection = ConAutentica
    Left = 120
    Top = 32
    object sdsUsuariosCODUSU: TIntegerField
      FieldName = 'CODUSU'
      Required = True
    end
    object sdsUsuariosNOME: TStringField
      FieldName = 'NOME'
      Size = 64
    end
    object sdsUsuariosLOGIN: TStringField
      FieldName = 'LOGIN'
      Size = 32
    end
    object sdsUsuariosSENHA: TStringField
      FieldName = 'SENHA'
      Size = 512
    end
    object sdsUsuariosNIVEL: TIntegerField
      FieldName = 'NIVEL'
    end
  end
  object dsUsuarios: TDataSetProvider
    DataSet = sdsUsuarios
    Left = 200
    Top = 32
  end
  object cdsUsuarios: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftString
        Name = 'pLogin'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'pSenha'
        ParamType = ptInput
      end>
    ProviderName = 'dsUsuarios'
    Left = 288
    Top = 32
    object cdsUsuariosCODUSU: TIntegerField
      FieldName = 'CODUSU'
      Required = True
    end
    object cdsUsuariosNOME: TStringField
      FieldName = 'NOME'
      Size = 64
    end
    object cdsUsuariosLOGIN: TStringField
      FieldName = 'LOGIN'
      Size = 32
    end
    object cdsUsuariosSENHA: TStringField
      FieldName = 'SENHA'
      Size = 512
    end
    object cdsUsuariosNIVEL: TIntegerField
      FieldName = 'NIVEL'
    end
  end
  object ConAutentica: TSQLConnection
    ConnectionName = 'FBConnection'
    DriverName = 'Firebird'
    LoginPrompt = False
    Params.Strings = (
      'DriverName=Firebird'
      'Database=database.fdb'
      'RoleName=RoleName'
      'User_Name=sysdba'
      'Password=masterkey'
      'ServerCharSet=ISO8859_1'
      'SQLDialect=3'
      'ErrorResourceFile='
      'LocaleCode=0000'
      'BlobSize=-1'
      'CommitRetain=False'
      'WaitOnLocks=True'
      'IsolationLevel=ReadCommitted'
      'Trim Char=False')
    Left = 48
    Top = 32
  end
end
