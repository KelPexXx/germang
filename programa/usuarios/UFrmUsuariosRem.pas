unit UFrmUsuariosRem;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UFrmDelRegPadrao, Buttons, PngSpeedButton, StdCtrls, pngimage,
  ExtCtrls;

type
  TFrmUsuariosRem = class(TFrmDelRegPadrao)
    procedure btnRemoverClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmUsuariosRem: TFrmUsuariosRem;

implementation

uses UDtmPrincipal;

{$R *.dfm}

procedure TFrmUsuariosRem.btnCancelarClick(Sender: TObject);
begin
  inherited;
  FrmUsuariosRem.Close;
end;

procedure TFrmUsuariosRem.btnRemoverClick(Sender: TObject);
begin
  inherited;
  try
    if DtmPrincipal.cdsTesteTecnico.Active then
      begin
    DtmPrincipal.cdsTesteTecnico.Filtered := FALSE;
    DtmPrincipal.cdsTesteTecnico.Filter := 'CODTECNICO = ' + DtmPrincipal.cdsTesteTecnicoCODTECNICO.AsString;
    DtmPrincipal.cdsTesteTecnico.Filtered := TRUE;
    DtmPrincipal.cdsTesteTecnico.Delete;
    DtmPrincipal.cdsTesteTecnico.ApplyUpdates(0);
    DtmPrincipal.cdsTesteTecnico.Filtered := FALSE;
    DtmPrincipal.cdsTesteTecnico.Filter := '';
    DtmPrincipal.cdsTesteTecnico.Refresh;
    DtmPrincipal.cdsTesteTecnico.Refresh;
    FrmUsuariosRem.Close;
      end;
  except
   on e : Exception do
      begin
        DtmPrincipal.cdsTesteTecnico.Cancel;
        DtmPrincipal.cdsTesteTecnico.Open;
        MessageDlg('Erro: ' + E.Message, mtError, [mbOK], 0);
          MessageDlg('N�o foi possivel remover o item, j� existem registros no sistema.\n'
          +' Remova primeiro os registros de testes e certificados!\n'+'C�d Error:' + e.Message, mtWarning, mbOKCancel, 0);
      end;
  end;
end;

procedure TFrmUsuariosRem.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  FreeAndNil(FrmUsuariosRem);
end;

end.
