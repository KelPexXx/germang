unit UFrmPrincipal;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ToolWin, ImgList, PngImageList, jpeg, ExtCtrls, pngimage,
  StdCtrls, ActnList, PlatformDefaultStyleActnCtrls, ActnMan, ActnCtrls, Menus,IniFiles,
  System.Actions;

type
  TFrmPrincipal = class(TForm)
    tbMenuPrincipal: TToolBar;
    btnMClientes: TToolButton;
    btnMMangueiras: TToolButton;
    imgMenu50: TPngImageList;
    btnMTestes: TToolButton;
    sepM1: TToolButton;
    btnMCertificados: TToolButton;
    btnMAgendamentos: TToolButton;
    btnMUsuarios: TToolButton;
    btnMChat: TToolButton;
    btnMConfig: TToolButton;
    sepM2: TToolButton;
    btnMSair: TToolButton;
    pInfoUsu: TPanel;
    imgUser: TImage;
    lblMsg: TLabel;
    bsPrincipal: TStatusBar;
    sepM3: TToolButton;
    AcoesPrincipal: TActionManager;
    acAbreFrmCliente: TAction;
    acAbreFrmMangueiras: TAction;
    acAbreFrmTestes: TAction;
    acAbreFrmCertificados: TAction;
    acAbreFrmAgendamentos: TAction;
    acAbreFrmUsuarios: TAction;
    acAbreFrmChat: TAction;
    acAbreFrmRelatorios: TAction;
    acAbreFrmConfiguracoes: TAction;
    acSair: TAction;
    btnMPecas: TToolButton;
    acAbreFrmEstoque: TAction;
    lblStatusDesenvolvimento: TLabel;
    PngImageList1: TPngImageList;
    procedure FormCreate(Sender: TObject);
    procedure acAbreFrmClienteExecute(Sender: TObject);
    procedure acAbreFrmMangueirasExecute(Sender: TObject);
    procedure acAbreFrmTestesExecute(Sender: TObject);
    procedure acAbreFrmCertificadosExecute(Sender: TObject);
    procedure acAbreFrmAgendamentosExecute(Sender: TObject);
    procedure acAbreFrmUsuariosExecute(Sender: TObject);
    procedure acAbreFrmChatExecute(Sender: TObject);
    procedure acAbreFrmRelatoriosExecute(Sender: TObject);
    procedure acAbreFrmConfiguracoesExecute(Sender: TObject);
    procedure acSairExecute(Sender: TObject);
    procedure acAbreFrmEstoqueExecute(Sender: TObject);
  private
    procedure ScanOpen;
  public
    Banco:String;
    procedure TrimAppMemorySize;
  end;

var
  FrmPrincipal: TFrmPrincipal;

implementation

uses UFrmClientes, UDtmPrincipal, UFrmAutenticacao, UFrmMangueiras, UFrmTestes,
  UFrmCertificados, UFrmAgendamentos, UFrmChat, UFrmConfiguracoes,
  UFrmGerCertificado, UFrmLacre, UFrmUsuarios, UFrmRelatorios, UFrmEstoque,
  UDtmRelatorios;
{$R *.dfm}

procedure TFrmPrincipal.acAbreFrmAgendamentosExecute(Sender: TObject);
begin
  ScanOpen;
  if (not(assigned(FrmAgendamentos))) then
    Application.CreateForm(TFrmAgendamentos, FrmAgendamentos);
end;

procedure TFrmPrincipal.acAbreFrmCertificadosExecute(Sender: TObject);
begin
  ScanOpen;
  if (not(assigned(FrmCertificados))) then
    Application.CreateForm(TFrmCertificados, FrmCertificados);
end;

procedure TFrmPrincipal.acAbreFrmChatExecute(Sender: TObject);
begin
  if (not(assigned(FrmChat))) then
    Application.CreateForm(TFrmChat, FrmChat);
end;

procedure TFrmPrincipal.acAbreFrmClienteExecute(Sender: TObject);
begin
  ScanOpen;
  if (not(assigned(FrmClientes))) then
    Application.CreateForm(TFrmClientes, FrmClientes);
end;

procedure TFrmPrincipal.acAbreFrmConfiguracoesExecute(Sender: TObject);
begin
  ScanOpen;
  if (not(assigned(FrmConfiguracoes))) then
    Application.CreateForm(TFrmConfiguracoes, FrmConfiguracoes);
end;

procedure TFrmPrincipal.acAbreFrmMangueirasExecute(Sender: TObject);
begin
  ScanOpen;
  if (not(assigned(FrmMangueiras))) then
    Application.CreateForm(TFrmMangueiras, FrmMangueiras);
end;

procedure TFrmPrincipal.acAbreFrmRelatoriosExecute(Sender: TObject);
begin
  ScanOpen;
  if (not(assigned(FrmRelatorios))) then
    Application.CreateForm(TFrmRelatorios, FrmRelatorios);
end;

procedure TFrmPrincipal.acAbreFrmTestesExecute(Sender: TObject);
begin
  ScanOpen;
  if (not(assigned(FrmTestes))) then
    Application.CreateForm(TFrmTestes, FrmTestes);
end;

procedure TFrmPrincipal.acAbreFrmUsuariosExecute(Sender: TObject);
begin
  ScanOpen;
  if (not(assigned(FrmUsuarios))) then
    Application.CreateForm(TFrmUsuarios, FrmUsuarios);
  FrmUsuarios.Show;
end;

procedure TFrmPrincipal.acAbreFrmEstoqueExecute(Sender: TObject);
begin
  ScanOpen;
  if (not(assigned(FrmEstoque))) then
    Application.CreateForm(TFrmEstoque, FrmEstoque);
end;

procedure TFrmPrincipal.acSairExecute(Sender: TObject);
begin
  Close;
end;

procedure TFrmPrincipal.FormCreate(Sender: TObject);
var
  ConfFile:TiniFile;
begin
  try
  //criando o objeto do tipo TIniFile
    ConfFile := TIniFile.Create(ChangeFileExt(Application.ExeName,'.ini'));
  With ConfFile do
   begin
    Banco := ReadString('CONEXAO','BASE','c:\data.fdb');
   end;
  finally
   ConfFile.Free;
  end;
  if not assigned(FrmAutenticacao) then
    FrmAutenticacao := TFrmAutenticacao.Create(Self);
  FrmAutenticacao.Show;
  if not assigned(DtmRelatorios) then
    DtmRelatorios := TDtmRelatorios.Create(Self);
end;

procedure TFrmPrincipal.ScanOpen;
begin
  if FrmClientes <> nil then
    FrmClientes.Close;
  if FrmMangueiras <> nil then
    FrmMangueiras.Close;
  if FrmTestes <> nil then
    FrmTestes.Close;
  if FrmLacre <> nil then
    FrmLacre.Close;
  if FrmRelatorios <> nil then
    FrmRelatorios.Close;
  if FrmUsuarios <> nil then
    FrmUsuarios.Close;
  if FrmAgendamentos <> nil then
    FrmAgendamentos.Close;
  if FrmCertificados <> nil then
    FrmCertificados.Close;
  if FrmConfiguracoes <> nil then
    FrmConfiguracoes.Close;
  if FrmEstoque <> nil then
    FrmEstoque.Close;
  TrimAppMemorySize;
end;

procedure TFrmPrincipal.TrimAppMemorySize;
var
  MainHandle: THandle;
begin
  try
    MainHandle := OpenProcess(PROCESS_ALL_ACCESS, false, GetCurrentProcessID);
    SetProcessWorkingSetSize(MainHandle, $FFFFFFFF, $FFFFFFFF);
    CloseHandle(MainHandle);
  except
  end;
  Application.ProcessMessages;
end;

end.
