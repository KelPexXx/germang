unit UFrmUsuariosEdit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, StdCtrls, Buttons, PngBitBtn, Mask, DBCtrls, ExtCtrls;

type
  TFrmUsuariosEdit = class(TForm)
    pTop: TPanel;
    lblCodigo: TLabel;
    lblNome: TLabel;
    lblRg: TLabel;
    lblCpf: TLabel;
    lblObservacao: TLabel;
    dbCodigo: TDBEdit;
    dbNome: TDBEdit;
    dbRg: TDBEdit;
    dbCPF: TDBEdit;
    dbObserva��o: TDBEdit;
    pBottom: TPanel;
    btnSalvarCad: TPngBitBtn;
    btnEditar: TPngBitBtn;
    dsEdit: TDataSource;
    btnCancelar: TPngBitBtn;
    procedure FormShow(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnEditarClick(Sender: TObject);
    procedure btnSalvarCadClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    Procedure ModoEdit(modo:Boolean);
  public
    procedure Seleciona(cod:integer);
  end;

var
  FrmUsuariosEdit: TFrmUsuariosEdit;

implementation

uses UDtmPrincipal;

{$R *.dfm}

procedure TFrmUsuariosEdit.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  FreeAndNil(FrmUsuariosEdit);
end;

procedure TFrmUsuariosEdit.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
var
  Resposta: Integer;
begin
  if (dsEdit.DataSet <> nil) then
    begin
       if (DtmPrincipal.cdsTesteTecnico.Active) and (DtmPrincipal.cdsTesteTecnico.State in [db.dsEdit, db.dsInsert]) then
        begin
          Resposta := MessageDlg('O registro atual ainda n�o foi salvo! Deseja Salvar as altera��es antes de sair?',mtInformation, mbYesNoCancel, 0);
          case Resposta of
            mrYes:
              begin
                DtmPrincipal.cdsTesteTecnico.Post;
                DtmPrincipal.cdsTesteTecnico.ApplyUpdates(0);
                DtmPrincipal.cdsTesteTecnico.Filtered := FALSE;
                DtmPrincipal.cdsTesteTecnico.Filter := '';
                CanClose := True;
              end;
            mrNo:
              begin
                DtmPrincipal.cdsTesteTecnico.Cancel;
                DtmPrincipal.cdsTesteTecnico.Filtered := FALSE;
                DtmPrincipal.cdsTesteTecnico.Filter := '';
                CanClose := True;
              end;
            else
              begin
                CanClose := False;
                exit;
              end;
          end;
        end
      else
        begin
        CanClose := True;
        DtmPrincipal.cdsTesteTecnico.Filtered := FALSE;
        DtmPrincipal.cdsTesteTecnico.Filter := '';
        end;
    end;
end;

procedure TFrmUsuariosEdit.FormShow(Sender: TObject);
begin
  try


  except
    on E: Exception do
    begin
      DtmPrincipal.cdsTesteTecnico.Cancel;
      DtmPrincipal.cdsTesteTecnico.Open;
      MessageDlg('Erro: ' + E.Message, mtError, [mbOK], 0);
    end;
  end;
  ModoEdit(true);
end;

procedure TFrmUsuariosEdit.ModoEdit(modo:Boolean);
begin
  dbNome.ReadOnly := modo;
  dbCPF.ReadOnly := modo;
  dbRg.ReadOnly := modo;
  dbObserva��o.ReadOnly := modo;
  btnSalvarCad.Enabled := not modo;
  btnEditar.Enabled := modo;
end;

procedure TFrmUsuariosEdit.btnCancelarClick(Sender: TObject);
begin
  try
    FrmUsuariosEdit.Close;
  except on E: Exception do
    begin
      DtmPrincipal.cdsTesteTecnico.Cancel;
      DtmPrincipal.cdsTesteTecnico.Open;
      MessageDlg('Erro: '+E.Message, mtError, [mbOK], 0);
    end;
  end;
end;

procedure TFrmUsuariosEdit.btnEditarClick(Sender: TObject);
begin
  try
    DtmPrincipal.cdsTesteTecnico.Edit;
    ModoEdit(FALSE);
    dbNome.SetFocus;
  except
    on E: Exception do
      begin
        DtmPrincipal.cdsTesteTecnico.Cancel;
        DtmPrincipal.cdsTesteTecnico.Open;
        MessageDlg('Erro: ' + E.Message, mtError, [mbOK], 0);
      end;
  end;
end;

procedure TFrmUsuariosEdit.btnSalvarCadClick(Sender: TObject);
begin
  try
    if (DtmPrincipal.cdsTesteTecnico.State in [db.dsEdit, db.dsInsert]) then
    begin
      if MessageDlg(
        'Confirmar dados?', mtInformation, mbYesNo, 0) = mrYes then
      begin
        DtmPrincipal.cdsTesteTecnico.Post;
        DtmPrincipal.cdsTesteTecnico.ApplyUpdates(0);
        DtmPrincipal.cdsTesteTecnico.Refresh;
        DtmPrincipal.cdsTesteTecnico.Refresh;
        FrmUsuariosEdit.Close;
      end;
    end;
  except
    on E: Exception do
    begin
      DtmPrincipal.cdsTesteTecnico.Cancel;
      DtmPrincipal.cdsTesteTecnico.Open;
      MessageDlg('Erro: ' + E.Message, mtError, [mbOK], 0);
    end;
  end;
end;

procedure TFrmUsuariosEdit.Seleciona(cod: integer);
begin
  DtmPrincipal.cdsTesteTecnico.Open;
  DtmPrincipal.cdsTesteTecnico.Filtered := false;
  DtmPrincipal.cdsTesteTecnico.Filter := 'CODTECNICO = ' + IntToStr(cod);
  DtmPrincipal.cdsTesteTecnico.Filtered := true;
  DtmPrincipal.cdsTesteTecnico.First;
end;

end.
