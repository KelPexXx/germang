unit UFrmUsuarios;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ComCtrls, ImgList, PngImageList, ToolWin, ExtCtrls, Grids,
  DBGrids;

type
  TFrmUsuarios = class(TForm)
    Panel2: TPanel;
    ToolBar1: TToolBar;
    imgList50: TPngImageList;
    btnCadastrar: TToolButton;
    btnEditar: TToolButton;
    btnRemover: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    btnRelatorio: TToolButton;
    dsUsuarios: TDataSource;
    DBGrid1: TDBGrid;
    procedure FormCreate(Sender: TObject);
    procedure btnCadastrarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnEditarClick(Sender: TObject);
    procedure btnRemoverClick(Sender: TObject);
  private
    { Private declarations }
  public
    procedure ScanOpen;
  end;

var
  FrmUsuarios: TFrmUsuarios;

implementation

uses UDtmPrincipal, UFrmUsuariosCad, UFrmUsuariosEdit, UFrmUsuariosRem;

{$R *.dfm}

procedure TFrmUsuarios.btnEditarClick(Sender: TObject);
begin
  ScanOpen;
  if not Assigned(FrmUsuariosEdit) then
    FrmUsuariosEdit := TFrmUsuariosEdit.Create(self);
  FrmUsuariosEdit.Seleciona(DtmPrincipal.cdsTesteTecnicoCODTECNICO.AsInteger);
  FrmUsuariosEdit.Show;
end;

procedure TFrmUsuarios.btnRemoverClick(Sender: TObject);
begin
  ScanOpen;
  if not Assigned(FrmUsuariosRem) then
    Application.CreateForm(TFrmUsuariosRem,FrmUsuariosRem);
  FrmUsuariosRem.lblTextoMsg2.Caption:= FrmUsuariosRem.lblTextoMsg2.Caption + ' '
   + DtmPrincipal.cdsTesteTecnicoCODTECNICO.AsString + '?';
  FrmUsuariosRem.Show;
end;

procedure TFrmUsuarios.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  DtmPrincipal.cdsTesteTecnico.Close;
  FreeAndNil(FrmUsuarios);
end;

procedure TFrmUsuarios.FormCreate(Sender: TObject);
begin
  DtmPrincipal.cdsTesteTecnico.Open;
  DtmPrincipal.cdsTesteTecnico.Filtered := FALSE;
  DtmPrincipal.cdsTesteTecnico.Filter := '';
  DtmPrincipal.cdsTesteTecnico.Filtered := TRUE;
end;

procedure TFrmUsuarios.ScanOpen;
begin
  if FrmUsuariosCad <> nil then
    FrmUsuariosCad.Close;
  if FrmUsuariosEdit <> nil then
    FrmUsuariosEdit.Close;
end;

procedure TFrmUsuarios.btnCadastrarClick(Sender: TObject);
begin
  ScanOpen;
  if not Assigned(FrmUsuariosCad) then
    FrmUsuariosCad := TFrmUsuariosCad.Create(self);
  FrmUsuariosCad.Show;
end;

end.
