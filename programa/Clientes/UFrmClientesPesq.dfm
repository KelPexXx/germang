inherited FrmClientesPesq: TFrmClientesPesq
  Caption = 'Pesquisar Clientes'
  OnClose = FormClose
  ExplicitWidth = 910
  ExplicitHeight = 261
  PixelsPerInch = 96
  TextHeight = 19
  inherited pCentro: TPanel
    inherited rgCampoPesquisa: TRadioGroup
      Columns = 4
      ItemIndex = 0
      Items.Strings = (
        'C'#243'digo'
        'CNPJ'
        'Raz'#227'o Social'
        'Nome Fantasia'
        'Bairro'
        'Logradouro'
        'Contato')
    end
    inherited pBotoes: TPanel
      inherited btnPesqPrx: TPngSpeedButton
        OnClick = btnPesqPrxClick
      end
    end
  end
end
