inherited FrmEstoqueRemover: TFrmEstoqueRemover
  Caption = 'Remover Item do Estoque'
  OnClose = FormClose
  OnShow = FormShow
  ExplicitWidth = 463
  PixelsPerInch = 96
  TextHeight = 13
  inherited pTop: TPanel
    ExplicitLeft = 0
    ExplicitTop = 0
    ExplicitWidth = 457
    ExplicitHeight = 144
    inherited lblTextoMsg1: TLabel
      Width = 44
      Caption = 'Aten'#231#227'o!'
      ExplicitWidth = 44
    end
    inherited lblTextoMsg2: TLabel
      Width = 161
      Caption = 'Deseja remover o item de c'#243'digo:'
      ExplicitWidth = 161
    end
    inherited lblTextoMsg3: TLabel
      Width = 3
      Caption = ''
      ExplicitWidth = 3
    end
  end
  inherited pBottom: TPanel
    ExplicitLeft = 0
    ExplicitTop = 144
    inherited btnCancelar: TPngSpeedButton
      OnClick = btnCancelarClick
    end
    inherited btnRemover: TPngSpeedButton
      OnClick = btnRemoverClick
    end
  end
end
