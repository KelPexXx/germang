unit UFrmEstoque;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UFrmFormularioPadrao, DB, ImgList, PngImageList, ExtCtrls, Buttons,
  PngSpeedButton, Grids, DBGrids, ComCtrls, ToolWin, pngimage;

type
  TFrmEstoque = class(TFrmFormularioPadrao)
    procedure btnMaddClick(Sender: TObject);
    procedure btnMFiltroClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnMEditClick(Sender: TObject);
    procedure btnMOrderClick(Sender: TObject);
    procedure btnMExcluiClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    procedure ScanOpen;
  end;

var
  FrmEstoque: TFrmEstoque;

implementation

uses UDtmPrincipal, UFrmEstoqueCad, UFrmEstoqueFiltrar, UFrmEstoqueEdit,
  UFrmEstoqueOrdenar, UFrmEstoqueRemover;

{$R *.dfm}

{ TFrmPecas }

procedure TFrmEstoque.btnMaddClick(Sender: TObject);
begin
  inherited;
  ScanOpen;
  if not Assigned(FrmEstoqueCad) then
    Application.CreateForm(TFrmEstoqueCad,FrmEstoqueCad);
  FrmEstoqueCad.Show;
end;

procedure TFrmEstoque.btnMEditClick(Sender: TObject);
begin
  inherited;
  ScanOpen;
  if (not(assigned(FrmEstoqueEdit))) then
    Application.CreateForm(TFrmEstoqueEdit,FrmEstoqueEdit);
  FrmEstoqueEdit.Seleciona(DtmPrincipal.cdsEstoque_VCODESTOQUE.AsInteger);
  FrmEstoqueEdit.Show;
end;

procedure TFrmEstoque.btnMExcluiClick(Sender: TObject);
begin
  inherited;
  ScanOpen;
  if not Assigned(FrmEstoqueRemover) then
    Application.CreateForm(TFrmEstoqueRemover,FrmEstoqueRemover);
  FrmEstoqueRemover.lblTextoMsg2.Caption:= FrmEstoqueRemover.lblTextoMsg2.Caption + ' '
   + DtmPrincipal.cdsEstoque_VCODESTOQUE.AsString + '?';
  FrmEstoqueRemover.Show;
end;

procedure TFrmEstoque.btnMFiltroClick(Sender: TObject);
begin
  inherited;
  ScanOpen;
  if (not(assigned(FrmEstoqueFiltrar))) then
    Application.CreateForm(TFrmEstoqueFiltrar,FrmEstoqueFiltrar);
  FrmEstoqueFiltrar.Show;
end;

procedure TFrmEstoque.btnMOrderClick(Sender: TObject);
begin
  inherited;
  ScanOpen;
  if (not(assigned(FrmEstoqueOrdenar))) then
    Application.CreateForm(TFrmEstoqueOrdenar,FrmEstoqueOrdenar);
  FrmEstoqueOrdenar.Show;
end;

procedure TFrmEstoque.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  ScanOpen;
  DtmPrincipal.cdsEstoque_V.Close;
  DtmPrincipal.cdsEstoque_V.CommandText := 'SELECT * FROM TB_ESTOQUE_V';
  DtmPrincipal.cdsEstoque_V.Filtered := FALSE;
  DtmPrincipal.cdsEstoque.Filtered := FALSE;
  FreeAndNil(FrmEstoque);
end;

procedure TFrmEstoque.FormShow(Sender: TObject);
begin
  inherited;
  DtmPrincipal.cdsEstoque_V.Open;
end;

procedure TFrmEstoque.ScanOpen;
begin
  if FrmEstoqueCad <> nil then
    FrmEstoqueCad.Close;
  if FrmEstoqueFiltrar <> nil then
    FrmEstoqueFiltrar.Close;
  if FrmEstoqueRemover <> nil then
    FrmEstoqueRemover.Close;
  if FrmEstoqueOrdenar <> nil then
    FrmEstoqueOrdenar.Close;
  if FrmEstoqueEdit <> nil then
    FrmEstoqueEdit.Close;
end;

end.
