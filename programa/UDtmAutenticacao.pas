unit UDtmAutenticacao;

interface

uses
  SysUtils, Classes, WideStrings, DBXFirebird, FMTBcd, DB, DBClient, Provider,
  SqlExpr;

type
  TDtmAutenticacao = class(TDataModule)
    sdsUsuarios: TSQLDataSet;
    dsUsuarios: TDataSetProvider;
    cdsUsuarios: TClientDataSet;
    sdsUsuariosCODUSU: TIntegerField;
    sdsUsuariosNOME: TStringField;
    sdsUsuariosLOGIN: TStringField;
    sdsUsuariosSENHA: TStringField;
    sdsUsuariosNIVEL: TIntegerField;
    cdsUsuariosCODUSU: TIntegerField;
    cdsUsuariosNOME: TStringField;
    cdsUsuariosLOGIN: TStringField;
    cdsUsuariosSENHA: TStringField;
    cdsUsuariosNIVEL: TIntegerField;
    ConAutentica: TSQLConnection;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DtmAutenticacao: TDtmAutenticacao;

implementation

{$R *.dfm}

end.
