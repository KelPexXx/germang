unit UFrmFiltrarPadrao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Buttons, PngSpeedButton, StdCtrls, DB;

type
  TFrmFiltrarPadrao = class(TForm)
    pDireita: TPanel;
    pEsquerda: TPanel;
    pCentro: TPanel;
    pBotoes: TPanel;
    pTopo: TPanel;
    rgOpcoes: TRadioGroup;
    edtFiltro: TLabeledEdit;
    btnLimparFiltro: TPngSpeedButton;
    btnFiltrar: TPngSpeedButton;
    btnFechar: TPngSpeedButton;
    dsFiltro: TDataSource;
    cbTipoPesquisa: TComboBox;
    procedure btnFecharClick(Sender: TObject);
    procedure FormPaint(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmFiltrarPadrao: TFrmFiltrarPadrao;

implementation

uses UDtmPrincipal;

{$R *.dfm}

procedure TFrmFiltrarPadrao.FormPaint(Sender: TObject);
begin
  pEsquerda.Width := Trunc((Self.ClientWidth - 750)/ 2);
  pDireita.Width := Trunc((Self.ClientWidth - 750)/ 2);
end;

procedure TFrmFiltrarPadrao.FormShow(Sender: TObject);
begin
  Self.Refresh;
end;

procedure TFrmFiltrarPadrao.btnFecharClick(Sender: TObject);
begin
  Close;
end;

end.
