unit UFrmMangueirasBusCliente;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UFrmBuscarPadrao, DB, Grids, DBGrids, StdCtrls, ExtCtrls, Buttons,
  PngSpeedButton;

type
  TFrmMangueirasBusCliente = class(TFrmBuscarPadrao)
    procedure btnBuscarClick(Sender: TObject);
    procedure btnSelecionarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmMangueirasBusCliente: TFrmMangueirasBusCliente;

implementation

uses UDtmPrincipal, UFrmMangueirasCad;

{$R *.dfm}

procedure TFrmMangueirasBusCliente.btnBuscarClick(Sender: TObject);
begin
  inherited;
  if trim(edtBusca.Text) <> '' then
    begin
      case rgOpcoes.ItemIndex of
        0:begin
          DtmPrincipal.cdsClientes.Filtered := FALSE;
          DtmPrincipal.cdsClientes.Filter := 'CODCLI = ' + edtBusca.Text;
          DtmPrincipal.cdsClientes.Filtered := TRUE;
        end;
        1:begin
          DtmPrincipal.cdsClientes.Filtered := FALSE;
          DtmPrincipal.cdsClientes.Filter := 'CNPJ = ' + edtBusca.Text;
          DtmPrincipal.cdsClientes.Filtered := TRUE;
        end;
        2:begin
          DtmPrincipal.cdsClientes.Filtered := FALSE;
          DtmPrincipal.cdsClientes.Filter := 'RAZAO LIKE ' + QuotedStr('%'+edtBusca.Text+'%');
          DtmPrincipal.cdsClientes.Filtered := TRUE;
        end;
        3:begin
          DtmPrincipal.cdsClientes.Filtered := FALSE;
          DtmPrincipal.cdsClientes.Filter := 'FANTASIA LIKE ' + QuotedStr('%'+edtBusca.Text+'%');
          DtmPrincipal.cdsClientes.Filtered := TRUE;
        end;
      end;
    end
  else
    begin
      DtmPrincipal.cdsClientes.Filtered := FALSE;
      DtmPrincipal.cdsClientes.Filter := '';
      DtmPrincipal.cdsClientes.Filtered := TRUE;
    end;
end;

procedure TFrmMangueirasBusCliente.btnSelecionarClick(Sender: TObject);
begin
  inherited;
  FrmMangueirasCad.dbRazaoCliente.Visible := true;
  FrmMangueirasCad.dbFantasiaCliente.Visible := true;
  FrmMangueirasCad.dbCnpjCliente.Visible := true;
  FrmMangueirasCad.edtBuscaCliente.Text := DtmPrincipal.cdsClientesCODCLI.AsString;
  FrmMangueirasBusCliente.Close;
end;

procedure TFrmMangueirasBusCliente.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  FreeAndNil(FrmMangueirasBusCliente);
end;

end.
