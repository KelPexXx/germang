unit UFrmMangueiras;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UFrmFormularioPadrao, DB, ImgList, PngImageList, ExtCtrls, Buttons,
  PngSpeedButton, Grids, DBGrids, ComCtrls, ToolWin, pngimage;

type
  TFrmMangueiras = class(TFrmFormularioPadrao)
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure btnMaddClick(Sender: TObject);
    procedure btnMEditClick(Sender: TObject);
    procedure dgFormDblClick(Sender: TObject);
    procedure btnMFiltroClick(Sender: TObject);
    procedure btnMOrderClick(Sender: TObject);
    procedure btnMExcluiClick(Sender: TObject);
  private
    { Private declarations }
  public
    procedure ScanOpen;
  end;

var
  FrmMangueiras: TFrmMangueiras;

implementation

uses UDtmPrincipal, UFrmMangueirasCad, UFrmMangueirasEdit,
  UFrmMangueirasFiltrar, UFrmMangueirasOrdenar, UFrmMangueirasRemover;

{$R *.dfm}

procedure TFrmMangueiras.btnMaddClick(Sender: TObject);
begin
  inherited;
  if not Assigned(FrmMangueirasCad) then
    Application.CreateForm(TFrmMangueirasCad,FrmMangueirasCad);
  FrmMangueirasCad.Show;
end;

procedure TFrmMangueiras.btnMEditClick(Sender: TObject);
begin
  inherited;
  if not Assigned(FrmMangueirasEdit) then
    Application.CreateForm(TFrmMangueirasEdit,FrmMangueirasEdit);
  FrmMangueirasEdit.Show;
end;

procedure TFrmMangueiras.btnMExcluiClick(Sender: TObject);
begin
  inherited;
  ScanOpen;
  if not Assigned(FrmMangueirasRemover) then
    Application.CreateForm(TFrmMangueirasRemover,FrmMangueirasRemover);
  FrmMangueirasRemover.lblTextoMsg2.Caption:= FrmMangueirasRemover.lblTextoMsg2.Caption + ' '
   + DtmPrincipal.cdsMangueiras_VNUMERO.AsString + '?';
  FrmMangueirasRemover.Show;
end;

procedure TFrmMangueiras.btnMFiltroClick(Sender: TObject);
begin
  inherited;
  ScanOpen;
  if not Assigned(FrmMangueirasFiltrar) then
    Application.CreateForm(TFrmMangueirasFiltrar,FrmMangueirasFiltrar);
  FrmMangueirasFiltrar.Show;
end;

procedure TFrmMangueiras.btnMOrderClick(Sender: TObject);
begin
  inherited;
  ScanOpen;
  if not Assigned(FrmMangueirasOrdenar) then
    Application.CreateForm(TFrmMangueirasOrdenar,FrmMangueirasOrdenar);
  FrmMangueirasOrdenar.Show;
end;

procedure TFrmMangueiras.dgFormDblClick(Sender: TObject);
begin
  inherited;
  btnMEditClick(sender);
end;

procedure TFrmMangueiras.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  DtmPrincipal.cdsMangueiras_V.Close;
  FreeAndNil(FrmMangueiras);
end;

procedure TFrmMangueiras.FormCreate(Sender: TObject);
begin
  inherited;
  DtmPrincipal.cdsMangueiras_V.Open;
end;

procedure TFrmMangueiras.ScanOpen;
begin
  if FrmMangueirasCad <> nil then
    FrmMangueirasCad.Close;
  if FrmMangueirasEdit <> nil then
    FrmMangueirasEdit.Close;
  if FrmMangueirasOrdenar <> nil then
    FrmMangueirasOrdenar.Close;
  if FrmMangueirasFiltrar <> nil then
    FrmMangueirasFiltrar.Close;
end;

end.
