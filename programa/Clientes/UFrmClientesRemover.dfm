inherited FrmClientesRemover: TFrmClientesRemover
  Caption = 'Remover Cliente'
  ClientWidth = 425
  OnClose = FormClose
  OnShow = FormShow
  ExplicitWidth = 431
  PixelsPerInch = 96
  TextHeight = 13
  inherited pTop: TPanel
    Width = 425
    ExplicitLeft = 0
    ExplicitTop = 0
    ExplicitWidth = 425
    ExplicitHeight = 144
    inherited lblTextoMsg1: TLabel
      Width = 44
      Caption = 'Aten'#231#227'o!'
      ExplicitWidth = 44
    end
    inherited lblTextoMsg2: TLabel
      Width = 175
      Caption = 'Deseja remover o cliente de c'#243'digo: '
      ExplicitWidth = 175
    end
    inherited lblTextoMsg3: TLabel
      Width = 3
      Caption = ''
      ExplicitWidth = 3
    end
  end
  inherited pBottom: TPanel
    Width = 425
    ExplicitLeft = 0
    ExplicitTop = 144
    ExplicitWidth = 425
    inherited btnCancelar: TPngSpeedButton
      Left = 285
      OnClick = btnCancelarClick
      ExplicitLeft = 285
    end
    inherited btnRemover: TPngSpeedButton
      OnClick = btnRemoverClick
    end
  end
end
