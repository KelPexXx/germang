inherited FrmPecasFiltrar: TFrmPecasFiltrar
  Caption = 'Filtrar Pe'#231'as'
  PixelsPerInch = 96
  TextHeight = 13
  inherited pCentro: TPanel
    inherited pBotoes: TPanel
      inherited btnLimparFiltro: TPngSpeedButton
        OnClick = btnLimparFiltroClick
      end
      inherited btnFiltrar: TPngSpeedButton
        OnClick = btnFiltrarClick
      end
    end
    inherited pTopo: TPanel
      ExplicitLeft = -6
      ExplicitTop = -6
    end
    inherited rgOpcoes: TRadioGroup
      Columns = 4
      Items.Strings = (
        'C'#243'digo'
        'Descri'#231#227'o'
        'Fabricante'
        'N'#250'mero de S'#233'rie'
        'C'#243'digo de Barras'
        'Valor'
        'Quantidade'
        'Quantidade Minima')
      OnClick = rgOpcoesClick
    end
  end
  inherited dsFiltro: TDataSource
    DataSet = DtmPrincipal.cdsEstoque_V
  end
end
