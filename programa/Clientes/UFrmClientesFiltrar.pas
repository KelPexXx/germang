unit UFrmClientesFiltrar;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UFrmFiltrarPadrao, StdCtrls, ExtCtrls, Buttons, PngSpeedButton, DB;

type
  TFrmClientesFiltrar = class(TFrmFiltrarPadrao)
    procedure btnLimparFiltroClick(Sender: TObject);
    procedure btnFiltrarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    function convTipo(filtro:string):String;
  public
    { Public declarations }
  end;

var
  FrmClientesFiltrar: TFrmClientesFiltrar;

implementation

{$R *.dfm}

procedure TFrmClientesFiltrar.btnFiltrarClick(Sender: TObject);
begin
  inherited;
  if Trim(edtFiltro.Text) <> '' then
    begin
      dsFiltro.DataSet.Filtered := false;
      case rgOpcoes.ItemIndex of
        0:Begin
          dsFiltro.DataSet.Filter := 'CODCLI = ' + QuotedStr(edtFiltro.Text);
        End;
        1:Begin
          dsFiltro.DataSet.Filter := 'RAZAO like ' + QuotedStr((convTipo(edtFiltro.Text)));
        End;
        2:Begin
          dsFiltro.DataSet.Filter := 'FANTASIA like ' + QuotedStr((convTipo(edtFiltro.Text)));
        End;
        3:Begin
          dsFiltro.DataSet.Filter := 'CNPJ like ' + QuotedStr((convTipo(edtFiltro.Text)));
        End;
        4:Begin
          dsFiltro.DataSet.Filter := 'ENDLOGRAD like ' + QuotedStr((convTipo(edtFiltro.Text)));
        End;
        5:Begin
          dsFiltro.DataSet.Filter := 'ENDBAIRRO like ' + QuotedStr((convTipo(edtFiltro.Text)));
        End;
        6:Begin
          dsFiltro.DataSet.Filter := 'UF like ' + QuotedStr((convTipo(edtFiltro.Text)));
        End;
        7:Begin
          dsFiltro.DataSet.Filter := 'ESTADO like ' + QuotedStr((convTipo(edtFiltro.Text)));
        End;
      end;
      dsFiltro.DataSet.Filtered := true;
    end;
end;

procedure TFrmClientesFiltrar.btnLimparFiltroClick(Sender: TObject);
begin
  inherited;
  dsFiltro.DataSet.Filtered := FALSE;
  dsFiltro.DataSet.Filter := '';
end;

function TFrmClientesFiltrar.convTipo(filtro: string): String;
begin
  case cbTipoPesquisa.ItemIndex of
    0:begin
      result := '%'+edtFiltro.Text;
    end;
    1:begin
      result := '%'+edtFiltro.Text+'%';
    end;
  end;
end;

procedure TFrmClientesFiltrar.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  FreeAndNil(FrmClientesFiltrar);
end;

end.
