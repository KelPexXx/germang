unit UFrmMangueirasEdit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DBCtrls, JvExMask, JvToolEdit, JvMaskEdit,
  JvCheckedMaskEdit, JvDatePickerEdit, JvDBDatePickerEdit, JvExExtCtrls,
  JvExtComponent, JvDBRadioPanel, Mask, Buttons, PngSpeedButton, ComCtrls, DB,
  PngBitBtn, ExtCtrls;

type
  TFrmMangueirasEdit = class(TForm)
    pBottom: TPanel;
    btnSalvarCad: TPngBitBtn;
    btnCancelarCad: TPngBitBtn;
    dsClientes: TDataSource;
    dsLacre: TDataSource;
    dsMangTipo: TDataSource;
    dsMangueiras: TDataSource;
    pgPrincipal: TPageControl;
    tsInfoGer: TTabSheet;
    lblCodMang: TLabel;
    lblNumSerie: TLabel;
    lblTipo: TLabel;
    lblCompReal: TLabel;
    lblDiametro: TLabel;
    lblCompNominal: TLabel;
    lblFabricante: TLabel;
    lblMarcaduto: TLabel;
    lblMarcaUniao: TLabel;
    lblEstado: TLabel;
    lblCodCLi: TLabel;
    btnBuscarCodCliente: TPngSpeedButton;
    lblDtUltInsp: TLabel;
    lblDtProxManut: TLabel;
    lblDtProxInsp: TLabel;
    lblDtUltManut: TLabel;
    lblRevestimento: TLabel;
    lblDtFab: TLabel;
    lblCompLuva: TLabel;
    lblMCompNom: TLabel;
    lblMCompReal: TLabel;
    lblMMDiametro: TLabel;
    lblMMCompLuva: TLabel;
    dbCodigo: TDBEdit;
    dbNumSerie: TDBEdit;
    dbCompReal: TDBEdit;
    dbFab: TDBEdit;
    dbMarcUniao: TDBEdit;
    dbUso: TJvDBRadioPanel;
    edtBuscaCliente: TEdit;
    gbInfoCliente: TGroupBox;
    dbCnpjCliente: TDBText;
    lblCNPJCLI: TLabel;
    lblFantasiaCli: TLabel;
    lblRazaoCLi: TLabel;
    dbRazaoCliente: TDBText;
    dbFantasiaCliente: TDBText;
    dbTipo: TDBLookupComboBox;
    dbCompLuva: TDBEdit;
    dbDTProxManut: TJvDBDatePickerEdit;
    dbDtUltManut: TJvDBDatePickerEdit;
    dbDtUltInsp: TJvDBDatePickerEdit;
    dbDtProxInsp: TJvDBDatePickerEdit;
    dbDtFab: TJvDBDatePickerEdit;
    dbDiametro: TDBComboBox;
    dbCompNom: TDBComboBox;
    dbCarcaca: TDBComboBox;
    dbMarcDuto: TDBEdit;
    tsLacre: TTabSheet;
    lblCodLacre: TLabel;
    btnBuscaLacre: TPngSpeedButton;
    lblDtUsoLacre: TLabel;
    lblNumLacre: TLabel;
    dbNumLacre: TDBText;
    dbDataLacre: TDBText;
    dbCodLacre: TDBEdit;
    btnEditar: TPngBitBtn;
    procedure btnBuscaLacreClick(Sender: TObject);
    procedure dbCodLacreExit(Sender: TObject);
    procedure dbCompRealExit(Sender: TObject);
    procedure edtBuscaClienteExit(Sender: TObject);
    procedure btnBuscarCodClienteClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure btnEditarClick(Sender: TObject);
    procedure btnSalvarCadClick(Sender: TObject);
    procedure btnCancelarCadClick(Sender: TObject);
  private
    bModo:boolean;
    procedure AbrirDataSets;
    procedure FecharDataSets;
    function PesquisaCliente(codigo:integer;edit:boolean):boolean;
    function PesquisaLacre(codigo:integer;edit:boolean):boolean;
    function ValidaForm:Boolean;
    procedure AlteraUsoLacre;
    procedure ModoEdit(modo:boolean);
  public
    procedure Seleciona(codigo:integer);
    procedure ScanOpen;
  end;

var
  FrmMangueirasEdit: TFrmMangueirasEdit;

implementation

uses UFrmLacre, UDtmPrincipal, UFrmMangueirasBuscaCliEdit;

{$R *.dfm}

{ TFrmMangueirasEdit }

procedure TFrmMangueirasEdit.AbrirDataSets;
begin
  with DtmPrincipal do
    begin
      sdsMangueiras.Open;
      sdsClientes.Open;
      sdsMangTipo.Open;
      sdsMangLacre.Open;
      cdsMangueiras.Open;
      cdsClientes.Open;
      cdsMangTipo.Open;
      cdsMangLacre.Open;
    end;
end;

procedure TFrmMangueirasEdit.FecharDataSets;
begin
  with DtmPrincipal do
    begin
      cdsMangueiras.Close;
      cdsClientes.Close;
      cdsMangTipo.Close;
      cdsMangLacre.Close;
      sdsMangueiras.Close;
      sdsClientes.Close;
      sdsMangTipo.Close;
      sdsMangLacre.Close;
    end;
end;

procedure TFrmMangueirasEdit.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  FreeAndNil(FrmMangueirasEdit);
end;

procedure TFrmMangueirasEdit.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
var
  resposta:integer;
begin
   if (dsMangueiras.DataSet <> nil) then
    begin
      if (DtmPrincipal.cdsMangueiras.Active) and (DtmPrincipal.cdsMangueiras.State in [dsEdit, dsInsert]) then
        begin
          Resposta := MessageDlg('O registro atual ainda n�o foi salvo! Deseja Salvar as altera��es antes de sair?',mtInformation, mbYesNoCancel, 0);
          case Resposta of
            mrYes:
              begin
                if ValidaForm then
                  begin
                    if (dbCodLacre.Text = '') or (Length(dbCodLacre.Text)<1) then
                      dbCodLacre.Text := '0';
                    DtmPrincipal.cdsMangueiras.Post;
                    DtmPrincipal.cdsMangueiras.ApplyUpdates(0);
                    AlteraUsoLacre;
                    DtmPrincipal.cdsMangueiras_V.Refresh;
                    DtmPrincipal.cdsMangueiras_V.Refresh;
                    DtmPrincipal.cdsMangueiras.Refresh;
                    DtmPrincipal.cdsMangueiras.Refresh;
                    DtmPrincipal.cdsMangLacre.Refresh;
                    DtmPrincipal.cdsMangLacre.Refresh;
                    FecharDataSets;
                    CanClose := TRUE;
                  end
                else
                  CanClose := FALSE;
              end;
            mrNo:
              begin
                DtmPrincipal.cdsMangueiras.Cancel;
                FecharDataSets;
                CanCLose := TRUE;
              end;
            else
              begin
                CanClose := FALSE;
                exit;
              end;
          end;
        end;
    end;
end;

procedure TFrmMangueirasEdit.FormShow(Sender: TObject);
begin
  try
    AbrirDataSets;
    Seleciona(DtmPrincipal.cdsMangueiras_VCODMANG.AsInteger);
  except
    on E: Exception do
    begin
      DtmPrincipal.cdsMangueiras.Cancel;
      DtmPrincipal.cdsMangueiras.Open;
      MessageDlg('Erro: ' + E.Message, mtError, [mbOK], 0);
    end;
  end;
end;

procedure TFrmMangueirasEdit.ModoEdit(modo: boolean);
begin
  bModo := not modo;
  dbCodigo.ReadOnly := modo;
  dbNumSerie.ReadOnly := modo;
  dbCompReal.ReadOnly := modo;
  dbFab.ReadOnly := modo;
  dbMarcUniao.ReadOnly := modo;
  dbUso.ReadOnly := modo;
  dbTipo.ReadOnly := modo;
  dbCompLuva.ReadOnly := modo;
  dbDTProxManut.ReadOnly := modo;
  dbDtUltManut.ReadOnly := modo;
  dbDtUltInsp.ReadOnly := modo;
  dbDtProxInsp.ReadOnly := modo;
  dbDtFab.ReadOnly := modo;
  dbDiametro.ReadOnly := modo;
  dbCompNom.ReadOnly := modo;
  dbCarcaca.ReadOnly := modo;
  dbMarcDuto.ReadOnly := modo;
  dbCodLacre.ReadOnly := modo;
  edtBuscaCliente.ReadOnly := modo;
  btnBuscarCodCliente.Enabled := not modo;
  btnBuscaLacre.Enabled := not modo;
  btnSalvarCad.Enabled := not modo;
  btnEditar.Enabled := modo;
end;

procedure TFrmMangueirasEdit.AlteraUsoLacre;
begin
  if (trim(dbCodLacre.Text) <> '0') AND (Length(trim(dbCodLacre.Text)) <> 0) then
    begin
      DtmPrincipal.sdsAux.CommandText := 'UPDATE TB_MANG_LACRE SET ESTADO = 1, DATALACRE = CURRENT_TIMESTAMP WHERE CODLACRE = ' + dbCodLacre.Text;
      DtmPrincipal.sdsAux.ExecSQL(TRUE);
    end;
end;

procedure TFrmMangueirasEdit.btnBuscaLacreClick(Sender: TObject);
begin
  if (Length(dbCodLacre.Text) <> 0) AND (dbCodLacre.Text <> '')then
    begin
      if PesquisaLacre(strtoint(dbCodLacre.Text),bModo) then
      begin
        DtmPrincipal.cdsMangLacre.Refresh;
        dbNumLacre.Visible := true;
        dbDataLacre.Visible := true;
      end
    else
      begin
        dbNumLacre.Visible := false;
        dbDataLacre.Visible := false;
      end;
    end
  else
    begin
      if not Assigned(FrmLacre) then
        Application.CreateForm(TFrmLacre,FrmLacre);
      FrmLacre.Destino := 2;
      FrmLacre.Show;
    end;
end;

procedure TFrmMangueirasEdit.btnBuscarCodClienteClick(Sender: TObject);
begin
  if length(edtBuscaCliente.Text) <> 0  then
    if PesquisaCliente(strtoint(edtBuscaCliente.Text),bModo) then
      begin
        dbRazaoCliente.Visible := TRUE;
        dbFantasiaCliente.Visible := TRUE;
        dbCnpjCliente.Visible := TRUE;
      end
    else
      begin
        dbRazaoCliente.Visible := FALSE;
        dbFantasiaCliente.Visible := FALSE;
        dbCnpjCliente.Visible := FALSE;
      end
  else
    begin
      if not Assigned(FrmMangueirasBuscaCliEdit) then
        Application.CreateForm(TFrmMangueirasBuscaCliEdit,FrmMangueirasBuscaCliEdit);
      FrmMangueirasBuscaCliEdit.Show;
    end;
end;

procedure TFrmMangueirasEdit.btnCancelarCadClick(Sender: TObject);
begin
  try
    FrmMangueirasEdit.Close;
  except on E: Exception do
    begin
      DtmPrincipal.cdsMangueiras.Cancel;
      DtmPrincipal.cdsMangueiras.Open;
      MessageDlg('Erro: '+E.Message, mtError, [mbOK], 0);
    end;
  end;
end;

procedure TFrmMangueirasEdit.btnEditarClick(Sender: TObject);
begin
  try
    DtmPrincipal.cdsMangueiras.Edit;
    ModoEdit(FALSE);
    pgPrincipal.ActivePageIndex := 0;
    dbNumSerie.SetFocus;
    if dbCodLacre.Text = '0' then
      dbCodLacre.Text := '';
  except
    on E: Exception do
      begin
        DtmPrincipal.cdsMangueiras.Cancel;
        DtmPrincipal.cdsMangueiras.Open;
        MessageDlg('Erro: ' + E.Message, mtError, [mbOK], 0);
      end;
  end;
end;

procedure TFrmMangueirasEdit.btnSalvarCadClick(Sender: TObject);
begin
  try
    if (DtmPrincipal.cdsMangueiras.State in [dsEdit, dsInsert]) AND (ValidaForm) then
    begin
      if MessageDlg(
        'Confirmar dados?', mtInformation, mbYesNo, 0) = mrYes then
      begin
        if (dbCodLacre.Text = '') or (Length(dbCodLacre.Text)<1) then
          dbCodLacre.Text := '0';
        DtmPrincipal.cdsMangueiras.Post;
        DtmPrincipal.cdsMangueiras.ApplyUpdates(0);
        AlteraUsoLacre;
        DtmPrincipal.cdsMangueiras_V.Refresh;
        DtmPrincipal.cdsMangueiras_V.Refresh;
        DtmPrincipal.cdsMangueiras.Refresh;
        DtmPrincipal.cdsMangueiras.Refresh;
        DtmPrincipal.cdsMangLacre.Refresh;
        DtmPrincipal.cdsMangLacre.Refresh;
        FrmMangueirasEdit.Close;
      end;
    end;
  except
    on E: Exception do
    begin
      DtmPrincipal.cdsMangueiras.Cancel;
      DtmPrincipal.cdsMangueiras.Open;
      MessageDlg('Erro: ' + E.Message, mtError, [mbOK], 0);
    end;
  end;
end;

procedure TFrmMangueirasEdit.dbCodLacreExit(Sender: TObject);
begin
  if Length(dbCodLacre.Text) <> 0 then
    if PesquisaLacre(strtoint(dbCodLacre.Text),bModo) then
      begin
        DtmPrincipal.cdsMangLacre.Refresh;
        dbNumLacre.Visible := true;
        dbDataLacre.Visible := true;
      end
    else
      begin
        dbNumLacre.Visible := false;
        dbDataLacre.Visible := false;
      end;
end;

procedure TFrmMangueirasEdit.dbCompRealExit(Sender: TObject);
begin
  if dbCompReal.Text <> '' then
    begin
      if (strtoint(dbCompNom.Text)*0.98 > StrToCurr(dbCompReal.Text)) then
        Showmessage('Aten��o o comprimento real da mangueira ultrapassa o limite minimo definido pela norma!');
    end
  else
    begin
      Showmessage('Erro: Comprimento real n�o informado!');
    end;
end;

procedure TFrmMangueirasEdit.edtBuscaClienteExit(Sender: TObject);
begin
  btnBuscarCodClienteClick(sender);
end;

function TFrmMangueirasEdit.PesquisaCliente(codigo: integer;edit: boolean): boolean;
begin
  result := FALSE;
  DtmPrincipal.cdsClientes.Filtered := FALSE;
  DtmPrincipal.cdsClientes.Filter:= 'CODCLI = ' + inttostr(codigo);
  DtmPrincipal.cdsClientes.Filtered := TRUE;
  if DtmPrincipal.cdsClientes.RecordCount > 0 then
    begin
    if edit then
      DtmPrincipal.cdsMangueirasCODCLI.AsInteger := DtmPrincipal.cdsClientesCODCLI.AsInteger;
    result := TRUE;
    end
  else
    ShowMessage('Cliente n�o encontrado!');
end;

function TFrmMangueirasEdit.PesquisaLacre(codigo: integer;edit: boolean): boolean;
begin
  result := false;
  DtmPrincipal.cdsMangLacre.Filtered := FALSE;
  DtmPrincipal.cdsMangLacre.Filter := 'CODLACRE = ' + inttostr(codigo);
  DtmPrincipal.cdsMangLacre.Filtered := TRUE;
  if DtmPrincipal.cdsMangLacre.RecordCount > 0 then
    begin
      if edit then
        DtmPrincipal.cdsMangueirasCODLACRE.AsInteger := DtmPrincipal.cdsMangLacreCODLACRE.AsInteger;
      result := TRUE;
    end
  else
    Showmessage('Lacre j� em uso ou n�o encontrado!');
end;

procedure TFrmMangueirasEdit.ScanOpen;
begin
  if FrmMangueirasBuscaCliEdit <> nil then
    FrmMangueirasBuscaCliEdit.Close;
  if FrmLacre <> nil then
    FrmLacre.Close;
end;

procedure TFrmMangueirasEdit.Seleciona(codigo: integer);
begin
  DtmPrincipal.cdsMangueiras.Open;
  DtmPrincipal.cdsMangueiras.Filtered := false;
  DtmPrincipal.cdsMangueiras.Filter := 'CODMANG = ' + IntToStr(codigo);
  DtmPrincipal.cdsMangueiras.Filtered := true;
  DtmPrincipal.cdsMangueiras.First;
  edtBuscaCliente.Text := IntToStr(DtmPrincipal.cdsMangueirasCODCLI.AsInteger);
  btnBuscarCodCliente.Click;
  dbCodLacre.Text := IntToStr(DtmPrincipal.cdsMangueirasCODLACRE.AsInteger);
  btnBuscaLacre.Click;
end;

function TFrmMangueirasEdit.ValidaForm: Boolean;
begin
  result := TRUE;
  if edtBuscaCliente.Text = '' then
    begin
      Showmessage('� necessario selecionar um cliente para adicionar a mangueira');
      result := FALSE;
    end;
end;

end.
