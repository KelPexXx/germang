unit UFrmEstoqueOrdenar;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UFrmOrdenarPadrao, StdCtrls, ExtCtrls, Buttons, PngSpeedButton;

type
  TFrmEstoqueOrdenar = class(TFrmOrdenarPadrao)
    procedure FormCreate(Sender: TObject);
    procedure btnLimparOrdemClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnOrdenarClick(Sender: TObject);
  private
    { Private declarations }
  public
    sSelect:String;
  end;

var
  FrmEstoqueOrdenar: TFrmEstoqueOrdenar;

implementation

uses UDtmPrincipal;

{$R *.dfm}

procedure TFrmEstoqueOrdenar.btnLimparOrdemClick(Sender: TObject);
begin
  inherited;
  DtmPrincipal.cdsEstoque_V.Close;
  DtmPrincipal.sdsEstoque_V.Close;
  DtmPrincipal.sdsEstoque_V.CommandText := sSelect + ' ORDER BY CODESTOQUE';
  DtmPrincipal.sdsEstoque_V.Open;
  DtmPrincipal.cdsEstoque_V.Open;
end;

procedure TFrmEstoqueOrdenar.btnOrdenarClick(Sender: TObject);
var
  ordem,de:string;
begin
  ordem := '';
  DtmPrincipal.cdsEstoque_V.Close;
  DtmPrincipal.sdsEstoque_V.Close;
  case rgOpcoes.ItemIndex of
    0:begin ordem := 'CODESTOQUE'; end;
    1:begin ordem := 'DESCESTOQUE'; end;
    2:begin ordem := 'FABRICESTOQUE'; end;
    3:begin ordem := 'SERIALESTOQUE'; end;
    4:begin ordem := 'BARRASESTOQUE'; end;
    5:begin ordem := 'VALORESTOQUE'; end;
    6:begin ordem := 'QTDESTOQUE'; end;
    7:begin ordem := 'QTDMINESTOQUE'; end;
    8:begin ordem := 'ULTCOMPESTOQUE'; end;
  end;
  if rgOrdem.ItemIndex = 0 then
     de := ' ASC'
  else
    de := ' DESC';
  DtmPrincipal.sdsEstoque_V.CommandText := sSelect+' ORDER BY '+ordem+de;
  DtmPrincipal.sdsEstoque_V.Open;
  DtmPrincipal.cdsEstoque_V.Open;
end;

procedure TFrmEstoqueOrdenar.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  FreeAndNil(FrmEstoqueOrdenar);
end;

procedure TFrmEstoqueOrdenar.FormCreate(Sender: TObject);
begin
  inherited;
  sSelect := 'SELECT * FROM TB_ESTOQUE_V';
end;

end.
