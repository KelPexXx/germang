unit UFrmPecasCad;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, StdCtrls, Buttons, PngBitBtn, ExtCtrls, Mask, DBCtrls, ComCtrls,
  JvExMask, JvToolEdit, JvMaskEdit, JvCheckedMaskEdit, JvDatePickerEdit,
  JvDBDatePickerEdit;

type
  TFrmEstoqueCad = class(TForm)
    pgCadPeca: TPageControl;
    tbInfoGerais: TTabSheet;
    lblCod: TLabel;
    dbCodigo: TDBEdit;
    pBottom: TPanel;
    btnSalvarCad: TPngBitBtn;
    btnCancelarCad: TPngBitBtn;
    lblDesc: TLabel;
    dbDesc: TDBEdit;
    dsEstoque: TDataSource;
    dbFabricante: TDBEdit;
    lblFabricante: TLabel;
    dbQuantidade: TDBEdit;
    lblQuantidade: TLabel;
    dbValor: TDBEdit;
    lblValor: TLabel;
    dbSerie: TDBEdit;
    lblSerial: TLabel;
    dbBarras: TDBEdit;
    Label2: TLabel;
    dbQntMin: TDBEdit;
    lblQntMin: TLabel;
    dbUltCompra: TJvDBDatePickerEdit;
    lblUltCOmpra: TLabel;
    procedure FormShow(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnCancelarCadClick(Sender: TObject);
    procedure btnSalvarCadClick(Sender: TObject);
  private
    Procedure AbrirDataSets;
    procedure FecharDataSets;
    function ValidaForm:Boolean;
  public
    { Public declarations }
  end;

var
  FrmEstoqueCad: TFrmEstoqueCad;

implementation

uses UDtmPrincipal;

{$R *.dfm}

procedure TFrmEstoqueCad.AbrirDataSets;
begin
  with DtmPrincipal do
    begin
      sdsEstoque.Open;
      cdsEstoque.Open;
    end;
end;

procedure TFrmEstoqueCad.btnCancelarCadClick(Sender: TObject);
begin
  try
    if (DtmPrincipal.cdsEstoque.State in [dsEdit, dsInsert]) then
      begin
        FrmPecasCad.Close;
      end;
  except on E: Exception do
    begin
      DtmPrincipal.cdsEstoque.Cancel;
      DtmPrincipal.cdsEstoque.Open;
      MessageDlg('Erro: '+E.Message, mtError, [mbOK], 0);
    end;
  end;
end;

procedure TFrmEstoqueCad.btnSalvarCadClick(Sender: TObject);
begin
  try
    if (DtmPrincipal.cdsEstoque.State in [dsEdit, dsInsert]) AND (ValidaForm) then
    begin
      if MessageDlg(
        'Confirmar dados?', mtInformation, mbYesNo, 0) = mrYes then
      begin
        DtmPrincipal.cdsEstoque.Post;
        DtmPrincipal.cdsEstoque.ApplyUpdates(0);
        DtmPrincipal.cdsEstoque_V.Refresh;
        DtmPrincipal.cdsEstoque_V.Refresh;
        DtmPrincipal.cdsEstoque_V.Refresh;
        DtmPrincipal.cdsEstoque.Refresh;
        DtmPrincipal.cdsEstoque.Refresh;
        DtmPrincipal.cdsEstoque.Refresh;
        FrmPecasCad.Close;
      end;
    end;
  except
    on E: Exception do
    begin
      DtmPrincipal.cdsEstoque.Cancel;
      DtmPrincipal.cdsEstoque.Open;
      MessageDlg('Erro: ' + E.Message, mtError, [mbOK], 0);
    end;
  end;
end;

procedure TFrmEstoqueCad.FecharDataSets;
begin
  with DtmPrincipal do
    begin
      cdsEstoque.Open;
      sdsEstoque.Open;
    end;
end;

procedure TFrmEstoqueCad.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  FreeAndNil(FrmPecasCad);
end;

procedure TFrmEstoqueCad.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
var
  resposta:integer;
begin
   if (dsEstoque.DataSet <> nil) then
    begin
      if (DtmPrincipal.cdsEstoque.Active) and (DtmPrincipal.cdsEstoque.State in [dsEdit, dsInsert]) then
        begin
          Resposta := MessageDlg('O registro atual ainda n�o foi salvo! Deseja Salvar as altera��es antes de sair?',mtInformation, mbYesNoCancel, 0);
          case Resposta of
            mrYes:
              begin
                if ValidaForm then
                  begin
                    DtmPrincipal.cdsEstoque.Post;
                    DtmPrincipal.cdsEstoque.ApplyUpdates(0);
                    DtmPrincipal.cdsEstoque_V.Refresh;
                    DtmPrincipal.cdsEstoque_V.Refresh;
                    DtmPrincipal.cdsEstoque_V.Refresh;
                    DtmPrincipal.cdsEstoque.Refresh;
                    DtmPrincipal.cdsEstoque.Refresh;
                    DtmPrincipal.cdsEstoque.Refresh;
                    FecharDataSets;
                    CanClose := TRUE;
                  end
                else
                  CanClose := FALSE;
              end;
            mrNo:
              begin
                DtmPrincipal.cdsEstoque.Cancel;
                FecharDataSets;
                CanClose := TRUE;
              end;
            else
              begin
                CanClose := FALSE;
                exit;
              end;
          end;
        end;
    end;
end;

procedure TFrmEstoqueCad.FormShow(Sender: TObject);
begin
  try
    AbrirDataSets;
    DtmPrincipal.cdsEstoque.Append;
    dbUltCompra.Date := now;
    dbDesc.SetFocus;
  except
    on E: Exception do
    begin
      DtmPrincipal.cdsEstoque.Cancel;
      DtmPrincipal.cdsEstoque.Open;
      MessageDlg('Erro: ' + E.Message, mtError, [mbOK], 0);
    end;
  end;
end;

function TFrmEstoqueCad.ValidaForm: Boolean;
begin
  Result := TRUE;
  if Length(dbDesc.Text) < 1 then
    Begin
      Showmessage('Erro! � necessario descrever a pe�a!');
      Result := FALSE;
    End;
  if Length(dbQuantidade.Text) < 1 then
    begin
      Showmessage('Erro! � necessario informar a quantidade da pe�a no estoque');
      Result := FALSE;
    end;

end;

end.
