unit UFrmPesquisaPadrao;

interface

uses
  Windows, Messages, SysUtils, StrUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, PngSpeedButton, ExtCtrls, DB;

type
  TFrmPesquisaPadrao = class(TForm)
    pEsquerda: TPanel;
    pDireita: TPanel;
    pCentro: TPanel;
    edtPesquisa: TEdit;
    lblPesquisar: TLabel;
    rgCampoPesquisa: TRadioGroup;
    pBotoes: TPanel;
    btnPesqAnt: TPngSpeedButton;
    btnPesqPrx: TPngSpeedButton;
    btnFechar: TPngSpeedButton;
    dsPesquisa: TDataSource;
    cbTipoPesquisa: TComboBox;
    procedure FormPaint(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnFecharClick(Sender: TObject);
  private
    { Private declarations }
  public
    function Compara(valorA,valorB:string;tipoBusca:integer):boolean;
  end;

var
  FrmPesquisaPadrao: TFrmPesquisaPadrao;

implementation

uses UFrmPrincipal, UFrmClientes;

{$R *.dfm}

procedure TFrmPesquisaPadrao.btnFecharClick(Sender: TObject);
begin
  Close;
end;

function TFrmPesquisaPadrao.Compara(valorA, valorB: string;
  tipoBusca: integer): boolean;
begin
  result := FALSE;
  case tipoBusca of
    0:begin//Come�a com
        if AnsiStartsStr(UpperCase(valorA),UpperCase(valorB)) then
          result := TRUE
        else
          result := FALSE;
    end;
    1:begin//Identico
        if AnsiCompareText(valorA,valorB) = 0 then
          result := TRUE
        else
          result := FALSE;
    end;
    2:begin//Contem
        if AnsiContainsText(valorA,valorB) then
          result := TRUE
        else
          result := FALSE;
    end;
  end;
end;

procedure TFrmPesquisaPadrao.FormPaint(Sender: TObject);
begin
  pEsquerda.Width := Trunc((Self.ClientWidth - 750)/ 2);
  pDireita.Width := Trunc((Self.ClientWidth - 750)/ 2);
end;

procedure TFrmPesquisaPadrao.FormShow(Sender: TObject);
begin
  Self.Refresh;
end;

end.
