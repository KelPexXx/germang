unit UFrmMangueirasBuscaCliEdit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UFrmBuscarPadrao, DB, Grids, DBGrids, StdCtrls, ExtCtrls, Buttons,
  PngSpeedButton;

type
  TFrmMangueirasBuscaCliEdit = class(TFrmBuscarPadrao)
    procedure btnBuscarClick(Sender: TObject);
    procedure btnSelecionarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmMangueirasBuscaCliEdit: TFrmMangueirasBuscaCliEdit;

implementation

uses UDtmPrincipal, UFrmMangueirasEdit;

{$R *.dfm}

procedure TFrmMangueirasBuscaCliEdit.btnBuscarClick(Sender: TObject);
begin
  inherited;
  if trim(edtBusca.Text) <> '' then
    begin
      case rgOpcoes.ItemIndex of
        0:begin
          DtmPrincipal.cdsClientes.Filtered := FALSE;
          DtmPrincipal.cdsClientes.Filter := 'CODCLI = ' + edtBusca.Text;
          DtmPrincipal.cdsClientes.Filtered := TRUE;
        end;
        1:begin
          DtmPrincipal.cdsClientes.Filtered := FALSE;
          DtmPrincipal.cdsClientes.Filter := 'CNPJ = ' + edtBusca.Text;
          DtmPrincipal.cdsClientes.Filtered := TRUE;
        end;
        2:begin
          DtmPrincipal.cdsClientes.Filtered := FALSE;
          DtmPrincipal.cdsClientes.Filter := 'RAZAO LIKE ' + QuotedStr('%'+edtBusca.Text+'%');
          DtmPrincipal.cdsClientes.Filtered := TRUE;
        end;
        3:begin
          DtmPrincipal.cdsClientes.Filtered := FALSE;
          DtmPrincipal.cdsClientes.Filter := 'FANTASIA LIKE ' + QuotedStr('%'+edtBusca.Text+'%');
          DtmPrincipal.cdsClientes.Filtered := TRUE;
        end;
      end;
    end
  else
    begin
      DtmPrincipal.cdsClientes.Filtered := FALSE;
      DtmPrincipal.cdsClientes.Filter := '';
      DtmPrincipal.cdsClientes.Filtered := TRUE;
    end;
end;

procedure TFrmMangueirasBuscaCliEdit.btnSelecionarClick(Sender: TObject);
begin
  inherited;
  FrmMangueirasEdit.dbRazaoCliente.Visible := true;
  FrmMangueirasEdit.dbFantasiaCliente.Visible := true;
  FrmMangueirasEdit.dbCnpjCliente.Visible := true;
  FrmMangueirasEdit.edtBuscaCliente.Text := DtmPrincipal.cdsClientesCODCLI.AsString;
  FrmMangueirasBuscaCliEdit.Close;
end;

procedure TFrmMangueirasBuscaCliEdit.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  FreeAndNil(FrmMangueirasBuscaCliEdit);
end;

end.
