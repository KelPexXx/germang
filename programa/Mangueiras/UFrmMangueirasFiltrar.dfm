inherited FrmMangueirasFiltrar: TFrmMangueirasFiltrar
  Caption = 'Filtrar Mangueiras'
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  inherited pCentro: TPanel
    inherited pBotoes: TPanel
      inherited btnLimparFiltro: TPngSpeedButton
        OnClick = btnLimparFiltroClick
      end
      inherited btnFiltrar: TPngSpeedButton
        OnClick = btnFiltrarClick
      end
    end
    inherited pTopo: TPanel
      inherited cbTipoPesquisa: TComboBox
        Enabled = False
        ItemIndex = 1
        Text = 'Id'#234'ntico'
      end
    end
    inherited rgOpcoes: TRadioGroup
      Columns = 4
      ItemIndex = 0
      Items.Strings = (
        'Cliente'
        'N'#250'mero'
        'Fabricante'
        'Diametro'
        'Fabrica'#231#227'o'
        'Proxima Manuten'#231#227'o'
        'Ultima Manuten'#231#227'o'
        'Proxima Inspe'#231#227'o'
        'Ultima Inspe'#231#227'o'
        'Lacre'
        'Data do Lacre')
      OnClick = rgOpcoesClick
    end
  end
  inherited dsFiltro: TDataSource
    DataSet = DtmPrincipal.cdsMangueiras_V
  end
end
