unit UFrmPecaAdd;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, StdCtrls, Buttons, PngSpeedButton, ExtCtrls, DBCtrls, DB;

type
  TFrmPecaAdd = class(TForm)
    pTop: TPanel;
    pBottom: TPanel;
    edtQtd: TLabeledEdit;
    lblTexto: TLabel;
    btnAdicionar: TPngSpeedButton;
    btnCancelar: TPngSpeedButton;
    procedure btnCancelarClick(Sender: TObject);
    procedure btnAdicionarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmPecaAdd: TFrmPecaAdd;

implementation

uses UDtmPrincipal, UFrmTestesCad;

{$R *.dfm}

procedure TFrmPecaAdd.btnAdicionarClick(Sender: TObject);
begin
  if StrToInt(edtQtd.Text) > DtmPrincipal.cdsEstoque_VQTDESTOQUE.AsInteger  then
    MessageDlg('Erro! A quantidade requisitada � superior a dispon�vel no estoque!', mtError, [mbOK], 0)
  else
    if StrToInt(edtQtd.Text) = 0 then
      MessageDlg('Erro! N�o � poss�vel adicionar um item com a quantidade igual a zero!', mtError, [mbOK], 0)
    else
      begin
        try
          DtmPrincipal.cdsTesteMangPecasQUANTIDADE.AsInteger := StrToInt(edtQtd.Text);
          DtmPrincipal.cdsTesteMangPecas.Post;
          DtmPrincipal.cdsTesteMangPecas.ApplyUpdates(0);
          DtmPrincipal.sdsAux.CommandText :=
          'UPDATE tb_estoque SET tb_estoque.qtdestoque = tb_estoque.qtdestoque-' +
           edtQtd.Text + 'WHERE tb_estoque.codestoque = ' + DtmPrincipal.cdsEstoque_VCODESTOQUE.AsString;
          DtmPrincipal.sdsAux.ExecSQL(TRUE);
          DtmPrincipal.cdsEstoque_V.Refresh;
          DtmPrincipal.cdsEstoque_V.Refresh;
          DtmPrincipal.cdsTesteMangPecas.Refresh;
          DtmPrincipal.cdsTesteMangPecas.Refresh;
          DtmPrincipal.cdsTesteMangPecas_V.Refresh;
          DtmPrincipal.cdsTesteMangPecas_V.Refresh;
          if DtmPrincipal.cdsEstoque_VQTDESTOQUE.AsInteger <= DtmPrincipal.cdsEstoque_VQTDMINESTOQUE.AsInteger then
            MessageDlg('Aten��o! A quantidade do item abaixo est� dentro da quantidade minima do estoque! '+ #13 +
            DtmPrincipal.cdsEstoque_VDESCESTOQUE.AsString, mtWarning, [mbOK],0);
          FrmPecaAdd.Close;
        except
          on e:Exception do
            begin
              DtmPrincipal.cdsTesteMangPecas.Cancel;
              DtmPrincipal.cdsTesteMangPecas.Open;
              MessageDlg('Erro: ' + E.Message, mtError, [mbOK], 0);
            end;
        end;
      end;
end;

procedure TFrmPecaAdd.btnCancelarClick(Sender: TObject);
begin
  try
    DtmPrincipal.cdsTesteMangPecas.Cancel;
    FrmPecaAdd.Close;
  except
    on e:Exception do
      begin
        DtmPrincipal.cdsTesteMangPecas.Cancel;
        DtmPrincipal.cdsTesteMangPecas.Open;
        MessageDlg('Erro: ' + E.Message, mtError, [mbOK], 0);
      end;
  end;
end;

procedure TFrmPecaAdd.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  FreeAndNil(FrmPecaAdd);
end;

procedure TFrmPecaAdd.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  if (DtmPrincipal.cdsTesteMangPecas.Active) and (DtmPrincipal.cdsTesteMangPecas.State in [dsEdit, dsInsert]) then
    begin
      DtmPrincipal.cdsTesteMangPecas.Cancel;
      CanCLose := True;
    end;
end;

end.
