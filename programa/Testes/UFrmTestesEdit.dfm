﻿object FrmTestesEdit: TFrmTestesEdit
  Left = 0
  Top = 0
  BorderStyle = bsToolWindow
  Caption = 'Editar Teste'
  ClientHeight = 576
  ClientWidth = 1056
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pgTeste: TPageControl
    Left = 0
    Top = 0
    Width = 1056
    Height = 576
    ActivePage = tbRelatorio
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Images = icons
    ParentFont = False
    TabOrder = 0
    OnChanging = pgTesteChanging
    object tbTeste: TTabSheet
      Caption = 'Teste'
      object lblTecnico: TLabel
        Left = 191
        Top = 76
        Width = 62
        Height = 19
        Caption = 'T'#233'cnico'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblDtExec: TLabel
        Left = 20
        Top = 76
        Width = 145
        Height = 19
        Caption = 'Data da Execu'#231#227'o'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblObs: TLabel
        Left = 20
        Top = 134
        Width = 94
        Height = 19
        Caption = 'Observa'#231#227'o'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label1: TLabel
        Left = 20
        Top = 20
        Width = 112
        Height = 19
        Caption = 'Tipo do Teste'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object dbTecnico: TDBLookupComboBox
        Left = 207
        Top = 101
        Width = 421
        Height = 27
        DataField = 'CODTECNICO'
        DataSource = dsTestes
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = []
        KeyField = 'CODTECNICO'
        ListField = 'NOMECOMPLETO'
        ListSource = dsTecnicos
        ParentFont = False
        TabOrder = 2
      end
      object dbDtExec: TJvDBDatePickerEdit
        Left = 28
        Top = 101
        Width = 145
        Height = 27
        AllowNoDate = True
        DataField = 'DATAEXEC'
        DataSource = dsTestes
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
      end
      object pBottomTeste: TPanel
        Left = 0
        Top = 478
        Width = 1048
        Height = 60
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 3
        object btnStep1Prox: TPngSpeedButton
          Left = 898
          Top = 0
          Width = 150
          Height = 60
          Align = alRight
          Caption = 'Pr'#243'ximo'
          Flat = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Layout = blGlyphRight
          ParentFont = False
          OnClick = btnStep1ProxClick
          PngImage.Data = {
            89504E470D0A1A0A0000000D49484452000000320000003208060000001E3F88
            B10000015F4944415478DAEDD8CB5583501006E099B8D38576622A50582A2E92
            0E4207DA49EC2076309BEC49D240ECC01292BD320E81934D78AA9819CEFC3BCE
            817BF978FCDC03C24082E73E01873844791CA22D0ED11687FC26C164394E28DA
            9A87DC4D9709332C36142DCC4364E27B669EAFE9E9C53CE4B021776645516C1F
            9263B608976142E1CE36E488194D137AF8B00D3960788780E14F1A4D17A4C0A4
            308A37F448B621455286B84B3D9F40B28FD517E2759F1094DA9599C7C8507B29
            BBD4F3C9306DAED65F454E14101B1E8A96F5AC1F92631AEBD906E488A9AE673B
            90FC80CA7AB605293065F56C0F02B04F1967D6217BA9ECC0F8A3C5EFC81713E3
            2F7B86B80A4CD7AFECF3265FF759D358A54B944F486FFA044885CE65E25BCCA6
            AF5DA2C0EB9AA2E776639E21FFB268540029AD576B90CA7A3504A9AF572390E6
            7A550F695BAFAA215DEA552D6410BF4C07F313BB8F38445B1CA22D0ED1168768
            CB6020DFC540FE33396FC90C0000000049454E44AE426082}
          ExplicitLeft = 904
          ExplicitHeight = 48
        end
        object btnStep1Cancel: TPngSpeedButton
          Left = 0
          Top = 0
          Width = 150
          Height = 60
          Align = alLeft
          Caption = 'Cancelar'
          Flat = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          OnClick = btnStep1CancelClick
          PngImage.Data = {
            89504E470D0A1A0A0000000D49484452000000320000003208060000001E3F88
            B1000002E74944415478DAEDD9BD6ED3501400E0739C44A8C2167D842204B32B
            E1AC3809626168DEA0E9062B488C3465448215B6A64F403AB0209A9835416A66
            1052790282922A42697CB8D7F951451CFB5EFFC496C9912259B263DFCFF7D7F7
            20642430E9026C201B48CA2372C8D0347454708F1FDB083A026DF36302EC2B04
            3DE7D8A653D5EAF65207193CBC6FA28DFB045045C46D91FF10519F3DBC490A9D
            689FBF5A89421C00E121BB8D19AE186411D251185020C8C8D47726B9C27178C0
            322837191F6C59BD8BD821BC0F90026DD126244DE14DCE86926C1F92820CCB46
            0D108FE300B8880ED456B7113964AD88001821C8B4532BEDB522E631A15D9166
            E60BE11DFB4A299CC7D527FC82F799BC3DDEF51B007C21C38AD18E7E7492E658
            EA59B71418926893FA978276C96B9EF184C8D48672E72EFBDD83AB4F1F850A96
            7FF418EC1FDFD8EFBB28C5B35656422E2BC52A5B727C10456CBD7907A06AF0E7
            F52B5F0C47DC78F192BDA9018C9E3F15C678D5CA4A08AB8D063BBD2F83988717
            6681583C48064327AC566A529041D9F82532522D15CC032373ADBB032ED456E7
            B630842F432087E702AF48B880A111F35831AFB84206E5621D110EC5EFEE5D50
            1E912080CF2B70A4B53AF5D8205E18B708829087548A4D76624FFA298298A008
            070270AA9D75AA6290B261B18EFE20D0937C3061100E84E88BD6EA9AFF19243B
            4D2B2B9D3D2BC36F6626441E838AD147C05B5120825CEB1A043FD91265C7ED54
            348BC6B7EF016EAA42055BC25C0E61F4EC498C8B46898FAAEB18A965BC1422E0
            32DEC148CC27717F58AD9A3FC42059F9D475302167F948103EB5210471B68372
            859EC808160B02E8777E32D6436F07F1909D57228DA836E816982C6C99268289
            6B137B81E169851C5871F519DE27700266AC698579CCF6831B518F667C74CADB
            E3DA5A123DD783CF3360633D2C880340A1FADA536F6E2024AC39C950D126C716
            80804EEEB0917832D42DA6A939ACCEEEAEB3D73DDDE443ECC32C3D8D36355399
            9E4E436C20698B0D246DF117D2F102514C472D7D0000000049454E44AE426082}
          ExplicitHeight = 48
        end
      end
      object jgTipoTeste: TJvDBRadioPanel
        Left = 28
        Top = 45
        Width = 257
        Height = 25
        BevelOuter = bvNone
        Columns = 2
        DataField = 'TIPO'
        DataSource = dsTestes
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = []
        Items.Strings = (
          'Inspe'#231#227'o'
          'Manuten'#231#227'o')
        ParentFont = False
        TabOrder = 0
        Values.Strings = (
          'I'
          'M')
      end
      object dbObs: TDBMemo
        Left = 28
        Top = 159
        Width = 573
        Height = 106
        DataField = 'OBSERVACAO'
        DataSource = dsTestes
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 4
      end
    end
    object tbCliente: TTabSheet
      Caption = 'Cliente'
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ImageIndex = 1
      ParentFont = False
      object lblCnpj: TLabel
        Left = 38
        Top = 73
        Width = 42
        Height = 19
        Caption = 'CNPJ'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Visible = False
      end
      object lblRazao: TLabel
        Left = 38
        Top = 121
        Width = 104
        Height = 19
        Caption = 'Raz'#227'o Social'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Visible = False
      end
      object lblFantasia: TLabel
        Left = 38
        Top = 169
        Width = 121
        Height = 19
        Caption = 'Nome Fantasia'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Visible = False
      end
      object dbTCliCnpj: TDBText
        Left = 56
        Top = 98
        Width = 153
        Height = 17
        DataField = 'CNPJ'
        DataSource = dsClientes
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Visible = False
      end
      object btnProcurarCliente: TPngSpeedButton
        Left = 169
        Top = 40
        Width = 100
        Height = 30
        Caption = 'Procurar'
        Flat = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        OnClick = btnProcurarClienteClick
        PngImage.Data = {
          89504E470D0A1A0A0000000D4948445200000018000000180806000000E0773D
          F80000000473424954080808087C08648800000006624B474400FF00FF00FFA0
          BDA7930000000970485973000006EC000006EC011E7538350000001974455874
          536F667477617265007777772E696E6B73636170652E6F72679BEE3C1A000002
          2F4944415478DA63FCFFFF3F032D01E38059101F1FAFC6C2C2120594370661B0
          6246C6B320FCE7CF9F650B172EBC459605400318939292F281E26D0C4C2C9C8C
          FC520C0C827210C9F78F18FE7F7CC6C0F0EFCF77A0B2AA79F3E64DFC4F200850
          2C00199E9898B805C8F46294316260328A606060E346D5F1EB2BC3BF732B18FE
          3F3907E26D9B3F7FBE0F3E4B502C484E4E2E00F2FB4106332AD982C578D91819
          44B818C1EC37DFFE337CFE0551FFFFDE61B045403715CE9D3B7702410B4061CE
          CCCC7C01E8724E268B640666A099C612CC0CA2DC8C0CAFBE42D48801D9AF81EC
          B32FFE32FC050AFD3B3117E493EF7FFFFE35C01527700B804153CFC8CCDAC0EC
          D3060E16593E2606514E46868BAF20868100C8524371668637DFFF333CF8F80F
          1C5C7FB75431FCFFFBBB0118548D782D0046EC664641391F26E7726212071CFC
          DBDBC9F0FFFDA32DC008F725E483674CCAB6924C4691A459706E39C3BFBB879F
          037D2035B016D0238850229928404A24A32753A25C4F4A3205016C190D178065
          3420A805064F0B2E759416159F81F801103B032D794DD0029825C41476401E30
          B218C28058F7D5E7EF2FFFFFFD6FB775DD8A5B042D8001628A6BA043445F7CFC
          7AE5DDD79F62EC2CCC3F847958F5D7AD40B584E20AC723244AFBDBF71F677FFD
          FDC78ECD12AAD4683E41113A9F7EFE3A03B344808BCB60E3AAC537A966010878
          0784E97EFEFDE734C8120E5696F70736AF11A2AA0520E01912A5F7FDC7CF433C
          9CAC495B56AF5847750BB0019A5B0000C7DB86E012284D070000000049454E44
          AE426082}
      end
      object dbTCliRazao: TDBText
        Left = 56
        Top = 146
        Width = 321
        Height = 17
        DataField = 'RAZAO'
        DataSource = dsClientes
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Visible = False
      end
      object dbTCliFantasia: TDBText
        Left = 56
        Top = 194
        Width = 321
        Height = 17
        DataField = 'FANTASIA'
        DataSource = dsClientes
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Visible = False
      end
      object lblCliContato: TLabel
        Left = 38
        Top = 217
        Width = 65
        Height = 19
        Caption = 'Contato'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Visible = False
      end
      object dbTContato: TDBText
        Left = 56
        Top = 242
        Width = 289
        Height = 17
        DataField = 'CONTATO'
        DataSource = dsClientes
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Visible = False
      end
      object lblCodCliente: TLabel
        Left = 38
        Top = 15
        Width = 150
        Height = 19
        Caption = 'C'#243'digo do Cliente:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object edtCodCli: TLabeledEdit
        Left = 56
        Top = 40
        Width = 107
        Height = 27
        EditLabel.Width = 45
        EditLabel.Height = 19
        EditLabel.Caption = '         '
        EditLabel.Font.Charset = DEFAULT_CHARSET
        EditLabel.Font.Color = clWindowText
        EditLabel.Font.Height = -16
        EditLabel.Font.Name = 'Tahoma'
        EditLabel.Font.Style = [fsBold]
        EditLabel.ParentFont = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = []
        LabelSpacing = 5
        ParentFont = False
        TabOrder = 0
        OnKeyDown = edtCodCliKeyDown
      end
      object pBottomCliente: TPanel
        Left = 0
        Top = 478
        Width = 1048
        Height = 60
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 1
        object btnStep2Prox: TPngSpeedButton
          Left = 898
          Top = 0
          Width = 150
          Height = 60
          Align = alRight
          Caption = 'Pr'#243'ximo'
          Flat = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Layout = blGlyphRight
          ParentFont = False
          OnClick = btnStep2ProxClick
          PngImage.Data = {
            89504E470D0A1A0A0000000D49484452000000320000003208060000001E3F88
            B10000015F4944415478DAEDD8CB5583501006E099B8D38576622A50582A2E92
            0E4207DA49EC2076309BEC49D240ECC01292BD320E81934D78AA9819CEFC3BCE
            817BF978FCDC03C24082E73E01873844791CA22D0ED11687FC26C164394E28DA
            9A87DC4D9709332C36142DCC4364E27B669EAFE9E9C53CE4B021776645516C1F
            9263B608976142E1CE36E488194D137AF8B00D3960788780E14F1A4D17A4C0A4
            308A37F448B621455286B84B3D9F40B28FD517E2759F1094DA9599C7C8507B29
            BBD4F3C9306DAED65F454E14101B1E8A96F5AC1F92631AEBD906E488A9AE673B
            90FC80CA7AB605293065F56C0F02B04F1967D6217BA9ECC0F8A3C5EFC81713E3
            2F7B86B80A4CD7AFECF3265FF759D358A54B944F486FFA044885CE65E25BCCA6
            AF5DA2C0EB9AA2E776639E21FFB268540029AD576B90CA7A3504A9AF572390E6
            7A550F695BAFAA215DEA552D6410BF4C07F313BB8F38445B1CA22D0ED1168768
            CB6020DFC540FE33396FC90C0000000049454E44AE426082}
          ExplicitLeft = 939
          ExplicitHeight = 48
        end
        object btnStep2Cancel: TPngSpeedButton
          Left = 0
          Top = 0
          Width = 150
          Height = 60
          Align = alLeft
          Caption = 'Cancelar'
          Flat = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          OnClick = btnStep2CancelClick
          PngImage.Data = {
            89504E470D0A1A0A0000000D49484452000000320000003208060000001E3F88
            B1000002E74944415478DAEDD9BD6ED3501400E0739C44A8C2167D842204B32B
            E1AC3809626168DEA0E9062B488C3465448215B6A64F403AB0209A9835416A66
            1052790282922A42697CB8D7F951451CFB5EFFC496C9912259B263DFCFF7D7F7
            20642430E9026C201B48CA2372C8D0347454708F1FDB083A026DF36302EC2B04
            3DE7D8A653D5EAF65207193CBC6FA28DFB045045C46D91FF10519F3DBC490A9D
            689FBF5A89421C00E121BB8D19AE186411D251185020C8C8D47726B9C27178C0
            322837191F6C59BD8BD821BC0F90026DD126244DE14DCE86926C1F92820CCB46
            0D108FE300B8880ED456B7113964AD88001821C8B4532BEDB522E631A15D9166
            E60BE11DFB4A299CC7D527FC82F799BC3DDEF51B007C21C38AD18E7E7492E658
            EA59B71418926893FA978276C96B9EF184C8D48672E72EFBDD83AB4F1F850A96
            7FF418EC1FDFD8EFBB28C5B35656422E2BC52A5B727C10456CBD7907A06AF0E7
            F52B5F0C47DC78F192BDA9018C9E3F15C678D5CA4A08AB8D063BBD2F83988717
            6681583C48064327AC566A529041D9F82532522D15CC032373ADBB032ED456E7
            B630842F432087E702AF48B880A111F35831AFB84206E5621D110EC5EFEE5D50
            1E912080CF2B70A4B53AF5D8205E18B708829087548A4D76624FFA298298A008
            070270AA9D75AA6290B261B18EFE20D0937C3061100E84E88BD6EA9AFF19243B
            4D2B2B9D3D2BC36F6626441E838AD147C05B5120825CEB1A043FD91265C7ED54
            348BC6B7EF016EAA42055BC25C0E61F4EC498C8B46898FAAEB18A965BC1422E0
            32DEC148CC27717F58AD9A3FC42059F9D475302167F948103EB5210471B68372
            859EC808160B02E8777E32D6436F07F1909D57228DA836E816982C6C99268289
            6B137B81E169851C5871F519DE27700266AC698579CCF6831B518F667C74CADB
            E3DA5A123DD783CF3360633D2C880340A1FADA536F6E2024AC39C950D126C716
            80804EEEB0917832D42DA6A939ACCEEEAEB3D73DDDE443ECC32C3D8D36355399
            9E4E436C20698B0D246DF117D2F102514C472D7D0000000049454E44AE426082}
          ExplicitHeight = 48
        end
      end
    end
    object tbMangueiras: TTabSheet
      Caption = 'Mangueiras'
      Enabled = False
      ImageIndex = 2
      object gbMangSelect: TGroupBox
        AlignWithMargins = True
        Left = 511
        Top = 3
        Width = 534
        Height = 472
        Align = alRight
        Caption = 'Mangueiras em teste'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
        object dgMangTeste: TDBGrid
          AlignWithMargins = True
          Left = 5
          Top = 21
          Width = 524
          Height = 396
          Align = alClient
          DataSource = dsAvaliacao
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
          ParentFont = False
          ReadOnly = True
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = []
          Columns = <
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'CODTESTE'
              Title.Alignment = taCenter
              Title.Caption = 'Teste'
              Width = 47
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'CODAVALIACAO'
              Title.Alignment = taCenter
              Title.Caption = 'Avalia'#231#227'o'
              Width = 50
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'CODMANG'
              Title.Alignment = taCenter
              Title.Caption = 'C'#243'd Mang'
              Width = 55
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'NUMERO'
              Title.Alignment = taCenter
              Title.Caption = 'Numera'#231#227'o'
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'TIPO'
              Title.Alignment = taCenter
              Title.Caption = 'Tipo'
              Width = 54
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'COMPNOM'
              Title.Caption = 'Comp. Nominal'
              Width = 76
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'DATAULTISP'
              Title.Alignment = taCenter
              Title.Caption = 'Ult. Insp.'
              Width = 72
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'DATAULTMANUT'
              Title.Alignment = taCenter
              Title.Caption = 'Ult. Manut.'
              Width = 72
              Visible = True
            end>
        end
        object pTopMangSelect: TPanel
          Left = 2
          Top = 420
          Width = 530
          Height = 50
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 0
          object btnAtualizaListTeste: TPngSpeedButton
            AlignWithMargins = True
            Left = 424
            Top = 3
            Width = 103
            Height = 44
            Align = alRight
            Caption = 'Atualizar'
            Flat = True
            Layout = blGlyphRight
            OnClick = btnAtualizaListTesteClick
            PngImage.Data = {
              89504E470D0A1A0A0000000D4948445200000018000000180806000000E0773D
              F80000000473424954080808087C08648800000006624B474400FF00FF00FFA0
              BDA7930000000970485973000006EC000006EC011E7538350000001974455874
              536F667477617265007777772E696E6B73636170652E6F72679BEE3C1A000002
              A24944415478DACD954D4854511886DF33A34D469A15B46A1312B688C234CC34
              6C13056DDC046D33422AB3D1201083A4A01FA81C4B8C24715BADDAB4088A12CB
              1F100B24FAC3ADB68934295367EEE9B98EA3DC99D12E238107BE73BFFB9DFBBD
              EF77DEF3738DB556FFB399D5415063B215D24E055422A35C397AA756FB6AE504
              61534A7F0BDB8B8516E2560F21389539419D0929A82B005DE02D88CD62C3543F
              48EC17CF976AB1CF14CE3A095B232823B266846F466478CEC67AD466BFA72770
              C103EAC5DB834D63CD48D2A2BB763A4D21FB1408F47983F6B9266C95BAEC9FF4
              04F5E626555EC4FB08D131DDB11F96599B75CA094CE205E6C147012F4880A712
              C4357F8B4549295E167C2127F889BE10F02896857CFB91AF6F29821EFA0AAC51
              117BC3CF2242F008E07C90BA99F93522BDE496A712B85B3147F1E93A6CC5749A
              A76B75A692AC01322C99DF88E46B469BD46E7F7809EA4C111F0CE10D5141B12F
              F0E4566F5E435349810729B0DB4B1036EEBEEE40C30E34ACC988206CEED1D762
              E728B2CD4B506F4EC3DE8E779FC13319119C379D14580D4E3507B12B99A09C81
              37727751C4566428D17B307623F52E76E070F21AE431308E3789865BE8DD5BA7
              34A1E53F5B83C921EF27DE8C4695A72736E625886BE89EE032A09B39FA650C8F
              2B123BEE53FFABF49798C10BE43994087B091A4C09550CB861CCBD833E43B0C3
              87F6C514D58F174B3EA05E821366AD3698AF84B7CE471C4D39B97A607F2F53B9
              7BD33EC6B629CD015D2488833F2574D803E03865AC437F0AF059B35E6BB8B32C
              A052D69C34633A92D03E95A0D66C5676F0802C979554C04801EB806FAE2B12ED
              648754319FA3734B6F55C473BB9B8F4D614DDAA8565DB64E721DFE7F99893DBE
              D84621EAA5F626DDB65F964AF34F10D7BA90459C007890D33EE6276D95FCF457
              D0FE02C3232AE0687DC9FB0000000049454E44AE426082}
          end
          object btnRemListTeste: TPngSpeedButton
            AlignWithMargins = True
            Left = 3
            Top = 3
            Width = 126
            Height = 44
            Align = alLeft
            Caption = 'Remover'
            Flat = True
            OnClick = btnRemListTesteClick
            PngImage.Data = {
              89504E470D0A1A0A0000000D49484452000000320000003208060000001E3F88
              B1000001A04944415478DAEDD94D4EC3400C05603B6C53D11BC011BA811DA870
              0238299CA0AD60071B8E406F0022DBC64CAAA2FE109A8CFD0CA3325E45519597
              AFED249E19A60329FEEB1BC8900C49BC3224B5CA90D42A0A528DCF4654F01DD5
              725BCE9E5F3C6E489BD11BD204484153661E8AC81BD77485C658327A413603BE
              CEA131D68C7E90EBF3D7F0C993DDF3284C1B621D42F372F2748A813441473463
              E26334661F4248DE794163D82FE2854121A220680C12110D4161D00815C48AF1
              40A8215A8C17C20489C57822CC90BE98E5B1230202E9C6D047B85BE282065E08
              18A41B232188B7D290082824068346C0216B0C3F860B976D981059712D17E8CE
              D907C2FC40850CB8E5F2CD980990CBA4215B8FD8F0E50BAFFE4EDF30F8F90C76
              B0EF3E627F11837BFCFEF49E10AA5663BC6DCCC030981762C7CB6E79EC349F81
              4062DA0ECFC99909A2E99D3C31FA365ED93B796174132B6303E881899FEA82BA
              583446B54067457860D40B7456041A635BA00375B1FB30EE0B74E856DC9AA15A
              A0F3984F583334DB0AF754CB8DF3B64274C6FFDCE849B93224B5CA90D42A4352
              AB83817C02B56F43514D6E82350000000049454E44AE426082}
          end
        end
      end
      object gbMangDisp: TGroupBox
        AlignWithMargins = True
        Left = 3
        Top = 3
        Width = 502
        Height = 472
        Align = alLeft
        Caption = 'Mangueiras dispon'#237'veis'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
        object dgMangDisp: TDBGrid
          AlignWithMargins = True
          Left = 5
          Top = 21
          Width = 492
          Height = 396
          Align = alClient
          DataSource = dsMangueiras
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
          ParentFont = False
          ReadOnly = True
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = []
          Columns = <
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'CODMANG'
              Title.Alignment = taCenter
              Title.Caption = 'C'#243'd Mang'
              Width = 60
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'NUMERO'
              Title.Alignment = taCenter
              Title.Caption = 'Numera'#231#227'o'
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'LACRE'
              Title.Alignment = taCenter
              Title.Caption = 'Num Lacre Atual'
              Width = 94
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TIPO'
              Title.Alignment = taCenter
              Title.Caption = 'Tipo'
              Width = 47
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'COMPNOM'
              Title.Alignment = taCenter
              Title.Caption = 'Comp Nominal'
              Width = 72
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'DATAULTISP'
              Title.Alignment = taCenter
              Title.Caption = 'Ult. Insp.'
              Width = 65
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'DATAULTMANUT'
              Title.Alignment = taCenter
              Title.Caption = 'Ult. Manut.'
              Width = 65
              Visible = True
            end>
        end
        object pTopMangDisp: TPanel
          Left = 2
          Top = 420
          Width = 498
          Height = 50
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 0
          object btnAddListMang: TPngSpeedButton
            AlignWithMargins = True
            Left = 368
            Top = 3
            Width = 127
            Height = 44
            Align = alRight
            Caption = 'Adicionar'
            Flat = True
            Layout = blGlyphRight
            OnClick = btnAddListMangClick
            PngImage.Data = {
              89504E470D0A1A0A0000000D49484452000000320000003208060000001E3F88
              B1000001214944415478DAEDD9410AC2301005D0C4B5DEA1DE446FE2DE958711
              975EA937B047A82008228C55E84245C166FEFFA96436A55DE531696686C4F027
              11D50B289002C93C5C20D5F6B4EA1E75B39ED6A385DC1131DADECCDA10264B15
              2609D223FA77256630A4DA9DAB68D7C3EB7715C635234A8CDB3FA2C6B89D5A6A
              8C5B1D51635C0BA212E35ED95518488BA2C0C07A2D3606DA343231F0EE9785A1
              B4F10C0C6D1E4163A8831512439F105118C9A88BC0C866F6AF98CB6CDE6C623B
              6A4847399A4D16BF6625ABAD35142181201074080A4185201134081A41813010
              70080B0185301130081B01812810EE1015C215A244B841D40817480E8864482E
              8824C8A78B1E052209F2C0BC65448348863C63740817488F0963BF9ECE250A24
              B72890DCE2066D4B8942E091AE0C0000000049454E44AE426082}
          end
          object btnAtualizaListMang: TPngSpeedButton
            AlignWithMargins = True
            Left = 3
            Top = 3
            Width = 102
            Height = 44
            Align = alLeft
            Caption = 'Atualizar'
            Flat = True
            OnClick = btnAtualizaListMangClick
            PngImage.Data = {
              89504E470D0A1A0A0000000D4948445200000018000000180806000000E0773D
              F80000000473424954080808087C08648800000006624B474400FF00FF00FFA0
              BDA7930000000970485973000006EC000006EC011E7538350000001974455874
              536F667477617265007777772E696E6B73636170652E6F72679BEE3C1A000002
              A24944415478DACD954D4854511886DF33A34D469A15B46A1312B688C234CC34
              6C13056DDC046D33422AB3D1201083A4A01FA81C4B8C24715BADDAB4088A12CB
              1F100B24FAC3ADB68934295367EEE9B98EA3DC99D12E238107BE73BFFB9DFBBD
              EF77DEF3738DB556FFB399D5415063B215D24E055422A35C397AA756FB6AE504
              61534A7F0BDB8B8516E2560F21389539419D0929A82B005DE02D88CD62C3543F
              48EC17CF976AB1CF14CE3A095B232823B266846F466478CEC67AD466BFA72770
              C103EAC5DB834D63CD48D2A2BB763A4D21FB1408F47983F6B9266C95BAEC9FF4
              04F5E626555EC4FB08D131DDB11F96599B75CA094CE205E6C147012F4880A712
              C4357F8B4549295E167C2127F889BE10F02896857CFB91AF6F29821EFA0AAC51
              117BC3CF2242F008E07C90BA99F93522BDE496A712B85B3147F1E93A6CC5749A
              A76B75A692AC01322C99DF88E46B469BD46E7F7809EA4C111F0CE10D5141B12F
              F0E4566F5E435349810729B0DB4B1036EEBEEE40C30E34ACC988206CEED1D762
              E728B2CD4B506F4EC3DE8E779FC13319119C379D14580D4E3507B12B99A09C81
              37727751C4566428D17B307623F52E76E070F21AE431308E3789865BE8DD5BA7
              34A1E53F5B83C921EF27DE8C4695A72736E625886BE89EE032A09B39FA650C8F
              2B123BEE53FFABF49798C10BE43994087B091A4C09550CB861CCBD833E43B0C3
              87F6C514D58F174B3EA05E821366AD3698AF84B7CE471C4D39B97A607F2F53B9
              7BD33EC6B629CD015D2488833F2574D803E03865AC437F0AF059B35E6BB8B32C
              A052D69C34633A92D03E95A0D66C5676F0802C979554C04801EB806FAE2B12ED
              648754319FA3734B6F55C473BB9B8F4D614DDAA8565DB64E721DFE7F99893DBE
              D84621EAA5F626DDB65F964AF34F10D7BA90459C007890D33EE6276D95FCF457
              D0FE02C3232AE0687DC9FB0000000049454E44AE426082}
          end
        end
      end
      object pBottomMang: TPanel
        Left = 0
        Top = 478
        Width = 1048
        Height = 60
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 2
        object btnStep3Prox: TPngSpeedButton
          Left = 898
          Top = 0
          Width = 150
          Height = 60
          Align = alRight
          Caption = 'Pr'#243'ximo'
          Flat = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Layout = blGlyphRight
          ParentFont = False
          OnClick = btnStep3ProxClick
          PngImage.Data = {
            89504E470D0A1A0A0000000D49484452000000320000003208060000001E3F88
            B10000015F4944415478DAEDD8CB5583501006E099B8D38576622A50582A2E92
            0E4207DA49EC2076309BEC49D240ECC01292BD320E81934D78AA9819CEFC3BCE
            817BF978FCDC03C24082E73E01873844791CA22D0ED11687FC26C164394E28DA
            9A87DC4D9709332C36142DCC4364E27B669EAFE9E9C53CE4B021776645516C1F
            9263B608976142E1CE36E488194D137AF8B00D3960788780E14F1A4D17A4C0A4
            308A37F448B621455286B84B3D9F40B28FD517E2759F1094DA9599C7C8507B29
            BBD4F3C9306DAED65F454E14101B1E8A96F5AC1F92631AEBD906E488A9AE673B
            90FC80CA7AB605293065F56C0F02B04F1967D6217BA9ECC0F8A3C5EFC81713E3
            2F7B86B80A4CD7AFECF3265FF759D358A54B944F486FFA044885CE65E25BCCA6
            AF5DA2C0EB9AA2E776639E21FFB268540029AD576B90CA7A3504A9AF572390E6
            7A550F695BAFAA215DEA552D6410BF4C07F313BB8F38445B1CA22D0ED1168768
            CB6020DFC540FE33396FC90C0000000049454E44AE426082}
          ExplicitLeft = 895
          ExplicitTop = -2
        end
        object btnStep3Cancel: TPngSpeedButton
          Left = 0
          Top = 0
          Width = 150
          Height = 60
          Align = alLeft
          Caption = 'Cancelar'
          Flat = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          OnClick = btnStep3CancelClick
          PngImage.Data = {
            89504E470D0A1A0A0000000D49484452000000320000003208060000001E3F88
            B1000002E74944415478DAEDD9BD6ED3501400E0739C44A8C2167D842204B32B
            E1AC3809626168DEA0E9062B488C3465448215B6A64F403AB0209A9835416A66
            1052790282922A42697CB8D7F951451CFB5EFFC496C9912259B263DFCFF7D7F7
            20642430E9026C201B48CA2372C8D0347454708F1FDB083A026DF36302EC2B04
            3DE7D8A653D5EAF65207193CBC6FA28DFB045045C46D91FF10519F3DBC490A9D
            689FBF5A89421C00E121BB8D19AE186411D251185020C8C8D47726B9C27178C0
            322837191F6C59BD8BD821BC0F90026DD126244DE14DCE86926C1F92820CCB46
            0D108FE300B8880ED456B7113964AD88001821C8B4532BEDB522E631A15D9166
            E60BE11DFB4A299CC7D527FC82F799BC3DDEF51B007C21C38AD18E7E7492E658
            EA59B71418926893FA978276C96B9EF184C8D48672E72EFBDD83AB4F1F850A96
            7FF418EC1FDFD8EFBB28C5B35656422E2BC52A5B727C10456CBD7907A06AF0E7
            F52B5F0C47DC78F192BDA9018C9E3F15C678D5CA4A08AB8D063BBD2F83988717
            6681583C48064327AC566A529041D9F82532522D15CC032373ADBB032ED456E7
            B630842F432087E702AF48B880A111F35831AFB84206E5621D110EC5EFEE5D50
            1E912080CF2B70A4B53AF5D8205E18B708829087548A4D76624FFA298298A008
            070270AA9D75AA6290B261B18EFE20D0937C3061100E84E88BD6EA9AFF19243B
            4D2B2B9D3D2BC36F6626441E838AD147C05B5120825CEB1A043FD91265C7ED54
            348BC6B7EF016EAA42055BC25C0E61F4EC498C8B46898FAAEB18A965BC1422E0
            32DEC148CC27717F58AD9A3FC42059F9D475302167F948103EB5210471B68372
            859EC808160B02E8777E32D6436F07F1909D57228DA836E816982C6C99268289
            6B137B81E169851C5871F519DE27700266AC698579CCF6831B518F667C74CADB
            E3DA5A123DD783CF3360633D2C880340A1FADA536F6E2024AC39C950D126C716
            80804EEEB0917832D42DA6A939ACCEEEAEB3D73DDDE443ECC32C3D8D36355399
            9E4E436C20698B0D246DF117D2F102514C472D7D0000000049454E44AE426082}
          ExplicitHeight = 50
        end
      end
    end
    object tbAvaliacao: TTabSheet
      Caption = 'Avalia'#231#227'o'
      Enabled = False
      ImageIndex = 3
      object gbInfoMang: TGroupBox
        AlignWithMargins = True
        Left = 3
        Top = 3
        Width = 1042
        Height = 114
        Align = alTop
        Caption = 'Informa'#231#245'es sobre a mangueira'
        TabOrder = 0
        object lblCodMang: TLabel
          Left = 16
          Top = 16
          Width = 43
          Height = 16
          Caption = 'C'#243'digo'
        end
        object lblNumMang: TLabel
          Left = 107
          Top = 16
          Width = 106
          Height = 16
          Caption = 'N'#250'mero de S'#233'rie'
        end
        object lblMangDiametro: TLabel
          Left = 233
          Top = 16
          Width = 59
          Height = 16
          Caption = 'Di'#226'metro'
        end
        object lblMangCompNom: TLabel
          Left = 315
          Top = 16
          Width = 92
          Height = 16
          Caption = 'Comp. Nominal'
        end
        object lblMangFab: TLabel
          Left = 198
          Top = 62
          Width = 68
          Height = 16
          Caption = 'Fabricante'
        end
        object lblMangRevest: TLabel
          Left = 16
          Top = 62
          Width = 90
          Height = 16
          Caption = 'Revestimento'
        end
        object lblMangCompReal: TLabel
          Left = 411
          Top = 62
          Width = 71
          Height = 16
          Caption = 'Comp. Real'
        end
        object lblMangDtFab: TLabel
          Left = 427
          Top = 16
          Width = 124
          Height = 16
          Caption = 'Data de Fabrica'#231#227'o'
        end
        object lblMangLacreAt: TLabel
          Left = 506
          Top = 61
          Width = 75
          Height = 16
          Caption = 'Lacre Atual'
        end
        object dbMangCod: TDBText
          Left = 26
          Top = 38
          Width = 65
          Height = 17
          DataField = 'CODMANG'
          DataSource = dsEditMang
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object dbMangNumSer: TDBText
          Left = 117
          Top = 38
          Width = 96
          Height = 17
          DataField = 'NUMERO'
          DataSource = dsEditMang
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object dbMangDiam: TDBText
          Left = 243
          Top = 38
          Width = 58
          Height = 17
          DataField = 'DIAMETRO'
          DataSource = dsEditMang
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object dbMangCompNom: TDBText
          Left = 325
          Top = 38
          Width = 82
          Height = 17
          DataField = 'DIAMETRO'
          DataSource = dsEditMang
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object dbMangDtFab: TDBText
          Left = 437
          Top = 38
          Width = 114
          Height = 17
          DataField = 'DATAFAB'
          DataSource = dsEditMang
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object dbMangRevest: TDBText
          Left = 26
          Top = 84
          Width = 149
          Height = 17
          DataField = 'CARCACA'
          DataSource = dsEditMang
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object dbMangFab: TDBText
          Left = 208
          Top = 84
          Width = 197
          Height = 17
          DataField = 'FABRICANTE'
          DataSource = dsEditMang
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object dbMangCompReal: TDBText
          Left = 421
          Top = 84
          Width = 72
          Height = 17
          DataField = 'FABRICANTE'
          DataSource = dsEditMang
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object cbMangLacreAtual: TDBLookupComboBox
          Left = 519
          Top = 83
          Width = 134
          Height = 24
          DataField = 'CODLACRE'
          DataSource = dsEditMang
          DropDownRows = 0
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          KeyField = 'CODLACRE'
          ListField = 'NUMERO'
          ListSource = dsLacre
          ParentFont = False
          ReadOnly = True
          TabOrder = 0
        end
        object pLeftAval: TPanel
          AlignWithMargins = True
          Left = 659
          Top = 21
          Width = 378
          Height = 88
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 1
          object btnProxAval: TPngSpeedButton
            Left = 300
            Top = 0
            Width = 75
            Height = 88
            Align = alLeft
            Caption = 'Pr'#243'xima'
            Flat = True
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            Layout = blGlyphTop
            ParentFont = False
            OnClick = btnProxAvalClick
            PngImage.Data = {
              89504E470D0A1A0A0000000D4948445200000018000000180806000000E0773D
              F80000000473424954080808087C08648800000006624B474400FF00FF00FFA0
              BDA7930000000970485973000006EC000006EC011E7538350000001974455874
              536F667477617265007777772E696E6B73636170652E6F72679BEE3C1A000000
              844944415478DA63FCFFFF3F032D01E3A805A316E0050EC15B730FACF59E4C3B
              0B82B68174CF607ACF95BB7FBFC31F9A580065EEFBFBEB7FC8E12DDEEF696501
              08DC66FACBE4BB6FA3C74D9C16A06920077C6064FA1FBA7F8DF71E5A5900027F
              80A6E603237FDA90B4007F10910A488E640A2CA05D32A56D46A37551410C18B5
              600458000036859BD1ACC6ACF50000000049454E44AE426082}
            ExplicitLeft = 303
            ExplicitHeight = 82
          end
          object btnSalvarAval: TPngSpeedButton
            Left = 225
            Top = 0
            Width = 75
            Height = 88
            Align = alLeft
            Caption = 'Salvar'
            Enabled = False
            Flat = True
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            Layout = blGlyphTop
            ParentFont = False
            OnClick = btnSalvarAvalClick
            PngImage.Data = {
              89504E470D0A1A0A0000000D4948445200000018000000180806000000E0773D
              F80000000473424954080808087C08648800000006624B474400FF00FF00FFA0
              BDA7930000000970485973000006EC000006EC011E7538350000001974455874
              536F667477617265007777772E696E6B73636170652E6F72679BEE3C1A000001
              7F4944415478DA63FCFFFF3F032D01E3A805A31690049CE77B183132FD2FFDFB
              FF57E2FE84FD3FA86A01C8702626863D40D3041918190AF6C4ED9848350B9C16
              BA1B333330EE0619CEC8C030DFE681654A7D7DFD3FAA58806C3890BBC0F68165
              32CC708A2D0019CEC4C0B807C814C066384516B82D7633F9F78F6937D8F0FF8C
              0B6D1F5A24A11B4EB605C41A8E6281C92C13560176D1F9FFFFFDEFDB9BB8E31C
              2EC35D16789A0275ED8206CB2260B024E2321CC502D785EE85FF1918FB80A9E0
              FD5F86FFAEFBE2779EC56138C8E5FCC4188E6281E30147169647ECCB80DC5020
              F70313D33FD75DB1BBCEC0143A2FF23063FCCF0072393F2323C3629BFB960984
              0CC7880390254C0FD997027D1106B20418BE6E7B12B69F26D770AC911CB63A8C
              F9DDD74F4B81B9311CC8FDC8C0C858CEF0FF7F272458FE2FB17D60154FACE158
              2D805BF2EDE312A07404429474C3715A00B3E4FDB74F8B81B29140654B6D1F58
              C4916A385E0B6096BCFDFA2955989B6FF6AAD0557F49359CA005D40043DF0200
              9CB2F1D124E363870000000049454E44AE426082}
            ExplicitLeft = 227
            ExplicitHeight = 82
          end
          object btnCancelarAval: TPngSpeedButton
            Left = 75
            Top = 0
            Width = 75
            Height = 88
            Align = alLeft
            Caption = 'Cancelar'
            Enabled = False
            Flat = True
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            Layout = blGlyphTop
            ParentFont = False
            OnClick = btnCancelarAvalClick
            PngImage.Data = {
              89504E470D0A1A0A0000000D4948445200000018000000180806000000E0773D
              F80000000473424954080808087C08648800000006624B474400FF00FF00FFA0
              BDA7930000000970485973000006EC000006EC011E7538350000001974455874
              536F667477617265007777772E696E6B73636170652E6F72679BEE3C1A000001
              E94944415478DADD953D68535114C7CF69A808BADAA1999482DAA41DD4800D14
              523274B30E9D9C9CEC509C0A7E804E0A7E8053E9D04C9D3A7568BA75282D04D2
              42AD83A9B18552A774A8AB8244F4F8BB7A032FE1E5E52966682FFCB9E7BE7BEF
              FF7F3EEEBD4FCD4CBAD9F4F40BECA98EB0220F320C6FF8CF6F55641BAC5D31DB
              FC2781AAEA79BA37E05E07270B6066D0EC4B6C818FAAD7F8BA8479117CC59EC3
              DB7202AFC1F76F22A91E9171E6EE8373E013F39357CDDE7514F09EBFF7E4A51F
              227787CC0EC3BCC39134BB7730CF381130DC1A4998C0BC4F4B69502427663FC3
              C80F542FD4A901E610A87B910202536D055C41612BBBB4E0F9703BCF5BC82BA4
              E70E2C5B2E5DA42E1B2C7C9300213F61F40CBC4E993D8C438EDBF901B3CF44FE
              82F123C49E528BE7A1022C2AD2DD02B709B51897DCCDEDAAE6F07E1D7385BD13
              ED046A74FD9C94E465B3A3B8E481F963CC23049291026CEE0BD9DC96DCB57DD5
              7EEA56EB24F03B45147A2C6DB61197DCEF7569598E4C51A3C8982F59F4382EB9
              6B1F545F51E00791450E1E5316DE6466310E7945F552E2CFE58C3EA63ED4C645
              6B5C9E487251EDA98AB8748E4AA78BE605824F459D48AE13F26E84E70B9E3CDE
              53E16BD1F4D8815952B77A965473527A4186F92CE2D3F2B78F5D4B24DD79AE83
              AD6B3F9CFFD54EBEC02F8A1141E0D31A722F0000000049454E44AE426082}
            ExplicitLeft = 69
            ExplicitTop = 5
            ExplicitHeight = 91
          end
          object btnEditarAval: TPngSpeedButton
            Left = 150
            Top = 0
            Width = 75
            Height = 88
            Align = alLeft
            Caption = 'Editar'
            Flat = True
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            Layout = blGlyphTop
            ParentFont = False
            OnClick = btnEditarAvalClick
            PngImage.Data = {
              89504E470D0A1A0A0000000D4948445200000018000000180806000000E0773D
              F80000000473424954080808087C08648800000006624B474400FF00FF00FFA0
              BDA7930000000970485973000006EC000006EC011E7538350000001974455874
              536F667477617265007777772E696E6B73636170652E6F72679BEE3C1A000000
              B24944415478DA63FCFFFF3F032D0123BA05134EFD6AA0C4C002333614FD1816
              4C3CFD8B222FE59BB2318E5A30842C3872ED0D838D960898C607406A462D18B5
              60D482C16A013900AF05CECBBED5984832B95062C199E7FFF6EC8DE26AC165C1
              63202543890540F00468812C760B967E3BCAC0C8604591F1FF198EED8DE6B2C6
              6A81CBF2EF45407E2F25E633323216EF89E4ECC36A41D86A06E6B7BFBFA50199
              E2649AFF5298956BD6AA5086BF582DA00500006469EAD11D7100090000000049
              454E44AE426082}
            ExplicitLeft = 156
            ExplicitTop = 5
          end
          object btnAntAval: TPngSpeedButton
            Left = 0
            Top = 0
            Width = 75
            Height = 88
            Align = alLeft
            Caption = 'Anterior'
            Enabled = False
            Flat = True
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            Layout = blGlyphTop
            ParentFont = False
            OnClick = btnAntAvalClick
            PngImage.Data = {
              89504E470D0A1A0A0000000D4948445200000018000000180806000000E0773D
              F80000000473424954080808087C08648800000006624B474400FF00FF00FFA0
              BDA7930000000970485973000006EC000006EC011E7538350000001974455874
              536F667477617265007777772E696E6B73636170652E6F72679BEE3C1A000000
              924944415478DA63FCFFFF3F032D01E3A805A3163038046FCD3DB0D67B32D52D
              70743CC0F24FF0DB644606868C03EBBC18A96A81ADCF56416636C63540A61388
              4F550B9CFC77A8FF63FEB719C854858951CD02C790AD2EFFFF31AE063205C835
              10DD41700B809199C5F09F712290C94289E1036701CD830806681AC93040D364
              0A0F2E5A66346440B3A2825A60D482116001005E3F85D1CD9D2AA10000000049
              454E44AE426082}
            ExplicitLeft = -3
            ExplicitTop = 5
          end
        end
      end
      object gbAvaliacao: TGroupBox
        AlignWithMargins = True
        Left = 3
        Top = 123
        Width = 1042
        Height = 352
        Align = alClient
        Caption = 'Avalia'#231#227'o'
        TabOrder = 1
        object lblCodAval: TLabel
          Left = 19
          Top = 21
          Width = 63
          Height = 16
          Caption = 'Avalia'#231#227'o'
        end
        object lblCodMangAval: TLabel
          Left = 104
          Top = 21
          Width = 96
          Height = 16
          Caption = 'C'#243'd Mangueira'
        end
        object lblMarcAbnt: TLabel
          Left = 19
          Top = 71
          Width = 100
          Height = 16
          Caption = 'Marca'#231#227'o ABNT'
          OnClick = lblMarcAbntClick
        end
        object lblvazValvula: TLabel
          Left = 19
          Top = 121
          Width = 143
          Height = 16
          Caption = 'Vazamento na valv'#250'la'
          OnClick = lblvazValvulaClick
        end
        object lblAcuAguaCx: TLabel
          Left = 19
          Top = 171
          Width = 235
          Height = 16
          Caption = 'Ac'#250'mulo de '#193'gua na caixa do abrigo'
          OnClick = lblAcuAguaCxClick
        end
        object lblCondAduchAcondi: TLabel
          Left = 19
          Top = 221
          Width = 280
          Height = 16
          Caption = 'Cond. de aduchamento e Acondiconamento'
          OnClick = lblCondAduchAcondiClick
        end
        object lblPresMat: TLabel
          Left = 334
          Top = 21
          Width = 214
          Height = 16
          Caption = 'Presen'#231'a de materiais estranhos'
          OnClick = lblPresMatClick
        end
        object lblPresInset: TLabel
          Left = 334
          Top = 71
          Width = 187
          Height = 16
          Caption = 'Presen'#231'a de insetos/animais'
          OnClick = lblPresInsetClick
        end
        object lblAcoplaUnioes: TLabel
          Left = 334
          Top = 121
          Width = 158
          Height = 16
          Caption = 'Acoplamento das uni'#245'es'
          OnClick = lblAcoplaUnioesClick
        end
        object lblAnelVed: TLabel
          Left = 334
          Top = 171
          Width = 108
          Height = 16
          Caption = 'Anel de veda'#231#227'o'
          OnClick = lblAnelVedClick
        end
        object lblCompLuva: TLabel
          Left = 334
          Top = 221
          Width = 195
          Height = 16
          Caption = 'Comprimento da luva de uni'#227'o'
        end
        object lblEnsaioHidro: TLabel
          Left = 594
          Top = 21
          Width = 124
          Height = 16
          Caption = 'Ensaio Hidrost'#225'tico'
          OnClick = lblEnsaioHidroClick
        end
        object lblReempatacao: TLabel
          Left = 594
          Top = 71
          Width = 89
          Height = 16
          Caption = 'Reempata'#231#227'o'
          OnClick = lblReempatacaoClick
        end
        object lblCompReal: TLabel
          Left = 594
          Top = 121
          Width = 71
          Height = 16
          Caption = 'Comp. Real'
          FocusControl = dbCompReal
        end
        object lblSubstAneis: TLabel
          Left = 832
          Top = 21
          Width = 140
          Height = 16
          Caption = 'Substitui'#231#227'o de An'#233'is'
          OnClick = lblSubstAneisClick
        end
        object lblSubstUnioes: TLabel
          Left = 594
          Top = 171
          Width = 153
          Height = 16
          Caption = 'Substitui'#231#227'o das Uni'#245'es'
          OnClick = lblSubstUnioesClick
        end
        object lblSubstVed: TLabel
          Left = 594
          Top = 221
          Width = 173
          Height = 16
          Caption = 'Substitui'#231#227'o das veda'#231#245'es'
          OnClick = lblSubstVedClick
        end
        object lblNovoEnsaioHidro: TLabel
          Left = 832
          Top = 71
          Width = 160
          Height = 16
          Caption = 'Novo Ensaio Hidrost'#225'tico'
          OnClick = lblNovoEnsaioHidroClick
        end
        object lblLimpeza: TLabel
          Left = 832
          Top = 121
          Width = 52
          Height = 16
          Caption = 'Limpeza'
          OnClick = lblLimpezaClick
        end
        object lblSecagem: TLabel
          Left = 832
          Top = 171
          Width = 58
          Height = 16
          Caption = 'Secagem'
          OnClick = lblSecagemClick
        end
        object lblTesteAvalObs: TLabel
          Left = 19
          Top = 271
          Width = 209
          Height = 16
          Caption = 'Observa'#231#245'es Teste/Mangueira: '
        end
        object dbCodAval: TDBText
          Left = 29
          Top = 43
          Width = 69
          Height = 17
          DataField = 'CODAVALIACAO'
          DataSource = dsEditAvaliacao
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object dbCodMangAval: TDBText
          Left = 114
          Top = 43
          Width = 86
          Height = 17
          DataField = 'CODMANG'
          DataSource = dsEditAvaliacao
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object jgMarcAbnt: TJvDBRadioPanel
          Left = 29
          Top = 90
          Width = 185
          Height = 25
          BevelOuter = bvNone
          Columns = 2
          DataField = 'MARCABNT'
          DataSource = dsEditAvaliacao
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Items.Strings = (
            'Sim'
            'N'#227'o')
          ParentFont = False
          ReadOnly = True
          TabOrder = 0
          Values.Strings = (
            'S'
            'N')
        end
        object jgVazValvula: TJvDBRadioPanel
          Left = 29
          Top = 140
          Width = 185
          Height = 25
          BevelOuter = bvNone
          Columns = 2
          DataField = 'VAZVAL'
          DataSource = dsEditAvaliacao
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Items.Strings = (
            'Aprovado'
            'Reprovado')
          ParentFont = False
          ReadOnly = True
          TabOrder = 1
          Values.Strings = (
            'A'
            'R')
        end
        object jgAcuAguaCx: TJvDBRadioPanel
          Left = 29
          Top = 190
          Width = 185
          Height = 25
          BevelOuter = bvNone
          Columns = 2
          DataField = 'ACUMAGUA'
          DataSource = dsEditAvaliacao
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Items.Strings = (
            'Aprovado'
            'Reprovado')
          ParentFont = False
          ReadOnly = True
          TabOrder = 2
          Values.Strings = (
            'A'
            'R')
        end
        object jgPresMat: TJvDBRadioPanel
          Left = 344
          Top = 40
          Width = 174
          Height = 25
          BevelOuter = bvNone
          Columns = 2
          DataField = 'PRESMATER'
          DataSource = dsEditAvaliacao
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Items.Strings = (
            'Aprovado'
            'Reprovado')
          ParentFont = False
          ReadOnly = True
          TabOrder = 4
          Values.Strings = (
            'A'
            'R')
        end
        object jgPresInset: TJvDBRadioPanel
          Left = 344
          Top = 90
          Width = 185
          Height = 25
          BevelOuter = bvNone
          Columns = 2
          DataField = 'PRESANIM'
          DataSource = dsEditAvaliacao
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Items.Strings = (
            'Aprovado'
            'Reprovado')
          ParentFont = False
          ReadOnly = True
          TabOrder = 5
          Values.Strings = (
            'A'
            'R')
        end
        object jgAcoplaUnioes: TJvDBRadioPanel
          Left = 344
          Top = 140
          Width = 185
          Height = 25
          BevelOuter = bvNone
          Columns = 2
          DataField = 'ACOPUNI'
          DataSource = dsEditAvaliacao
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Items.Strings = (
            'Aprovado'
            'Reprovado')
          ParentFont = False
          ReadOnly = True
          TabOrder = 6
          Values.Strings = (
            'A'
            'R')
        end
        object jgAnelVed: TJvDBRadioPanel
          Left = 344
          Top = 190
          Width = 185
          Height = 25
          BevelOuter = bvNone
          Columns = 2
          DataField = 'ANELVED'
          DataSource = dsEditAvaliacao
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Items.Strings = (
            'Aprovado'
            'Reprovado')
          ParentFont = False
          ReadOnly = True
          TabOrder = 7
          Values.Strings = (
            'A'
            'R')
        end
        object dbCompLuva: TDBEdit
          Left = 344
          Top = 243
          Width = 71
          Height = 24
          DataField = 'COMPLUVA'
          DataSource = dsEditAvaliacao
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 8
        end
        object jgReempatacao: TJvDBRadioPanel
          Left = 604
          Top = 90
          Width = 185
          Height = 25
          BevelOuter = bvNone
          Columns = 2
          DataField = 'REEMPATE'
          DataSource = dsEditAvaliacao
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Items.Strings = (
            'Sim'
            'N'#227'o')
          ParentFont = False
          ReadOnly = True
          TabOrder = 10
          Values.Strings = (
            'S'
            'N')
        end
        object dbCompReal: TDBEdit
          Left = 604
          Top = 140
          Width = 74
          Height = 24
          DataField = 'COMPREAL'
          DataSource = dsEditAvaliacao
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 11
        end
        object jgNovoEnsaioHidro: TJvDBRadioPanel
          Left = 842
          Top = 90
          Width = 185
          Height = 25
          BevelOuter = bvNone
          Columns = 2
          DataField = 'NOVOENSAIOHIDRO'
          DataSource = dsEditAvaliacao
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Items.Strings = (
            'Aprovado'
            'Reprovado')
          ParentFont = False
          ReadOnly = True
          TabOrder = 15
          Values.Strings = (
            'A'
            'R')
        end
        object jgSubstUnioes: TJvDBRadioPanel
          Left = 604
          Top = 190
          Width = 185
          Height = 25
          BevelOuter = bvNone
          Columns = 2
          DataField = 'SUBUNI'
          DataSource = dsEditAvaliacao
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Items.Strings = (
            'Sim'
            'N'#227'o')
          ParentFont = False
          ReadOnly = True
          TabOrder = 12
          Values.Strings = (
            'S'
            'N')
        end
        object jgSubstVed: TJvDBRadioPanel
          Left = 604
          Top = 243
          Width = 185
          Height = 25
          BevelOuter = bvNone
          Columns = 2
          DataField = 'SUBVED'
          DataSource = dsEditAvaliacao
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Items.Strings = (
            'Sim'
            'N'#227'o')
          ParentFont = False
          ReadOnly = True
          TabOrder = 13
          Values.Strings = (
            'S'
            'N')
        end
        object jgSubstAneis: TJvDBRadioPanel
          Left = 842
          Top = 40
          Width = 185
          Height = 25
          BevelOuter = bvNone
          Columns = 2
          DataField = 'SUBANEL'
          DataSource = dsEditAvaliacao
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Items.Strings = (
            'Sim'
            'N'#227'o')
          ParentFont = False
          ReadOnly = True
          TabOrder = 14
          Values.Strings = (
            'S'
            'N')
        end
        object jgLimpeza: TJvDBRadioPanel
          Left = 842
          Top = 140
          Width = 185
          Height = 25
          BevelOuter = bvNone
          Columns = 2
          DataField = 'LIMPEZA'
          DataSource = dsEditAvaliacao
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Items.Strings = (
            'Sim'
            'N'#227'o')
          ParentFont = False
          ReadOnly = True
          TabOrder = 16
          Values.Strings = (
            'S'
            'N')
        end
        object jgSecagem: TJvDBRadioPanel
          Left = 842
          Top = 190
          Width = 185
          Height = 25
          BevelOuter = bvNone
          Columns = 2
          DataField = 'SECAGEM'
          DataSource = dsEditAvaliacao
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Items.Strings = (
            'Sim'
            'N'#227'o')
          ParentFont = False
          ReadOnly = True
          TabOrder = 17
          Values.Strings = (
            'S'
            'N')
        end
        object jgEnsaioHidro: TJvDBRadioPanel
          Left = 604
          Top = 40
          Width = 185
          Height = 25
          BevelOuter = bvNone
          Columns = 2
          DataField = 'ENSAIOHIDRO'
          DataSource = dsEditAvaliacao
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Items.Strings = (
            'Aprovado'
            'Reprovado')
          ParentFont = False
          ReadOnly = True
          TabOrder = 9
          Values.Strings = (
            'A'
            'R')
        end
        object jgCondAduchAcond: TJvDBRadioPanel
          Left = 29
          Top = 243
          Width = 185
          Height = 25
          BevelOuter = bvNone
          Columns = 2
          DataField = 'CONDADU'
          DataSource = dsEditAvaliacao
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Items.Strings = (
            'Aprovado'
            'Reprovado')
          ParentFont = False
          ReadOnly = True
          TabOrder = 3
          Values.Strings = (
            'A'
            'R')
        end
        object dbObsMang: TDBMemo
          Left = 16
          Top = 291
          Width = 697
          Height = 52
          DataField = 'OBSMANG'
          DataSource = dsEditAvaliacao
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 18
        end
        object gbResultadoFinal: TGroupBox
          Left = 719
          Top = 283
          Width = 311
          Height = 62
          Caption = ' Resultado Final: '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 19
          object jgResultFinal: TJvDBRadioPanel
            Left = 2
            Top = 18
            Width = 307
            Height = 42
            Align = alClient
            BevelOuter = bvNone
            Columns = 3
            DataField = 'RESULTADO'
            DataSource = dsEditAvaliacao
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            Items.Strings = (
              'Aprovado'
              'Manuten'#231#227'o'
              'Condenado')
            ParentFont = False
            ReadOnly = True
            TabOrder = 0
            Values.Strings = (
              'A'
              'M'
              'C')
            OnChange = jgResultFinalChange
          end
        end
      end
      object pBottomAval: TPanel
        Left = 0
        Top = 478
        Width = 1048
        Height = 60
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 2
        object btnStep4Prox: TPngSpeedButton
          Left = 898
          Top = 0
          Width = 150
          Height = 60
          Align = alRight
          Caption = 'Pr'#243'ximo'
          Flat = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Layout = blGlyphRight
          ParentFont = False
          OnClick = btnStep4ProxClick
          PngImage.Data = {
            89504E470D0A1A0A0000000D49484452000000320000003208060000001E3F88
            B10000015F4944415478DAEDD8CB5583501006E099B8D38576622A50582A2E92
            0E4207DA49EC2076309BEC49D240ECC01292BD320E81934D78AA9819CEFC3BCE
            817BF978FCDC03C24082E73E01873844791CA22D0ED11687FC26C164394E28DA
            9A87DC4D9709332C36142DCC4364E27B669EAFE9E9C53CE4B021776645516C1F
            9263B608976142E1CE36E488194D137AF8B00D3960788780E14F1A4D17A4C0A4
            308A37F448B621455286B84B3D9F40B28FD517E2759F1094DA9599C7C8507B29
            BBD4F3C9306DAED65F454E14101B1E8A96F5AC1F92631AEBD906E488A9AE673B
            90FC80CA7AB605293065F56C0F02B04F1967D6217BA9ECC0F8A3C5EFC81713E3
            2F7B86B80A4CD7AFECF3265FF759D358A54B944F486FFA044885CE65E25BCCA6
            AF5DA2C0EB9AA2E776639E21FFB268540029AD576B90CA7A3504A9AF572390E6
            7A550F695BAFAA215DEA552D6410BF4C07F313BB8F38445B1CA22D0ED1168768
            CB6020DFC540FE33396FC90C0000000049454E44AE426082}
          ExplicitLeft = 939
          ExplicitHeight = 50
        end
        object lblMangPos: TLabel
          Left = 467
          Top = 24
          Width = 114
          Height = 16
          Caption = 'Mangueira X de X'
        end
      end
    end
    object tbPecas: TTabSheet
      Caption = 'Pe'#231'as'
      Enabled = False
      ImageIndex = 4
      object gbPecasTeste: TGroupBox
        AlignWithMargins = True
        Left = 511
        Top = 3
        Width = 534
        Height = 472
        Align = alRight
        Caption = 'Pe'#231'as Utilizadas'
        TabOrder = 1
        object dgPecasUtilizadas: TDBGrid
          AlignWithMargins = True
          Left = 5
          Top = 21
          Width = 524
          Height = 397
          Align = alClient
          DataSource = dsTestesMangPecas
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
          ParentFont = False
          ReadOnly = True
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -13
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = []
          Columns = <
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'CODPECA'
              Title.Alignment = taCenter
              Title.Caption = 'C'#243'digo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DESCESTOQUE'
              Title.Caption = 'Descri'#231#227'o'
              Width = 308
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'QUANTIDADE'
              Title.Alignment = taCenter
              Title.Caption = 'Qnt.'
              Width = 41
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'VALOR'
              Title.Alignment = taCenter
              Title.Caption = 'Pre'#231'o'
              Width = 75
              Visible = True
            end>
        end
        object pBottomPecasUtil: TPanel
          Left = 2
          Top = 421
          Width = 530
          Height = 49
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 1
          object btnRemoverPeca: TPngSpeedButton
            AlignWithMargins = True
            Left = 3
            Top = 3
            Width = 126
            Height = 43
            Align = alLeft
            Caption = 'Remover'
            Flat = True
            OnClick = btnRemoverPecaClick
            PngImage.Data = {
              89504E470D0A1A0A0000000D49484452000000320000003208060000001E3F88
              B1000001A04944415478DAEDD94D4EC3400C05603B6C53D11BC011BA811DA870
              0238299CA0AD60071B8E406F0022DBC64CAAA2FE109A8CFD0CA3325E45519597
              AFED249E19A60329FEEB1BC8900C49BC3224B5CA90D42A0A528DCF4654F01DD5
              725BCE9E5F3C6E489BD11BD204484153661E8AC81BD77485C658327A413603BE
              CEA131D68C7E90EBF3D7F0C993DDF3284C1B621D42F372F2748A813441473463
              E26334661F4248DE794163D82FE2854121A220680C12110D4161D00815C48AF1
              40A8215A8C17C20489C57822CC90BE98E5B1230202E9C6D047B85BE282065E08
              18A41B232188B7D290082824068346C0216B0C3F860B976D981059712D17E8CE
              D907C2FC40850CB8E5F2CD980990CBA4215B8FD8F0E50BAFFE4EDF30F8F90C76
              B0EF3E627F11837BFCFEF49E10AA5663BC6DCCC030981762C7CB6E79EC349F81
              4062DA0ECFC99909A2E99D3C31FA365ED93B796174132B6303E881899FEA82BA
              583446B54067457860D40B7456041A635BA00375B1FB30EE0B74E856DC9AA15A
              A0F3984F583334DB0AF754CB8DF3B64274C6FFDCE849B93224B5CA90D42A4352
              AB83817C02B56F43514D6E82350000000049454E44AE426082}
          end
          object lblTotalPecas: TLabel
            Left = 408
            Top = 6
            Width = 37
            Height = 16
            Caption = 'Total:'
          end
          object dbTotalPreco: TDBEdit
            Left = 445
            Top = 3
            Width = 74
            Height = 24
            DataField = 'TotalPreco'
            DataSource = dsTestesMangPecas
            TabOrder = 0
          end
        end
      end
      object pBottomPecas: TPanel
        Left = 0
        Top = 478
        Width = 1048
        Height = 60
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 2
        object btnStep5Prox: TPngSpeedButton
          Left = 898
          Top = 0
          Width = 150
          Height = 60
          Align = alRight
          Caption = 'Pr'#243'ximo'
          Flat = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Layout = blGlyphRight
          ParentFont = False
          OnClick = btnStep5ProxClick
          PngImage.Data = {
            89504E470D0A1A0A0000000D49484452000000320000003208060000001E3F88
            B10000015F4944415478DAEDD8CB5583501006E099B8D38576622A50582A2E92
            0E4207DA49EC2076309BEC49D240ECC01292BD320E81934D78AA9819CEFC3BCE
            817BF978FCDC03C24082E73E01873844791CA22D0ED11687FC26C164394E28DA
            9A87DC4D9709332C36142DCC4364E27B669EAFE9E9C53CE4B021776645516C1F
            9263B608976142E1CE36E488194D137AF8B00D3960788780E14F1A4D17A4C0A4
            308A37F448B621455286B84B3D9F40B28FD517E2759F1094DA9599C7C8507B29
            BBD4F3C9306DAED65F454E14101B1E8A96F5AC1F92631AEBD906E488A9AE673B
            90FC80CA7AB605293065F56C0F02B04F1967D6217BA9ECC0F8A3C5EFC81713E3
            2F7B86B80A4CD7AFECF3265FF759D358A54B944F486FFA044885CE65E25BCCA6
            AF5DA2C0EB9AA2E776639E21FFB268540029AD576B90CA7A3504A9AF572390E6
            7A550F695BAFAA215DEA552D6410BF4C07F313BB8F38445B1CA22D0ED1168768
            CB6020DFC540FE33396FC90C0000000049454E44AE426082}
          ExplicitLeft = 939
          ExplicitHeight = 50
        end
        object btnStep5Voltar: TPngSpeedButton
          Left = 0
          Top = 0
          Width = 150
          Height = 60
          Align = alLeft
          Caption = 'Voltar'
          Flat = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          OnClick = btnStep5VoltarClick
          PngImage.Data = {
            89504E470D0A1A0A0000000D49484452000000320000003208060000001E3F88
            B10000014A4944415478DAEDD8CD6DC2401005E019933B74103A8006887CE7B2
            1DA4054A48072921740007C815920AEC4EC82139B2998DB5E058B048889F79D6
            BC031A632CED2733B318A69684EFBD00831844790CA22D06D11683688B41B4E5
            2290DC2D875BA697CFD9D8C14246EEDD65B47DF3CCA5407248C8C82D2619F36B
            A83DD10724E4C92DA6CCFC1C8FE120B95BF53CFDACE4CA61FD7D2848686A59F0
            4CAEEA37CFC14062531373EFD0790848BDA99BABF7BCAB0B995C936B2EB6E3FD
            D77A3E2ECE82349BFA9F43207CC32D3575D78F2EE358534341524D0D0339D5D4
            5810F65329BBD09090BFAF16D35CCA476848859166E7EFB57C64000D89811FBF
            F52437C4F05295A5D457DD101F28DB9CBD21EE31E92100F11325263504A02015
            E6F0108083C4C03F58D5D38A47DD3DA61A020229A02121ADF83B484B0CA22D06
            D11683688B41B4C520DA62106DF905F9ABBE336465879B0000000049454E44AE
            426082}
          ExplicitHeight = 50
        end
      end
      object gbPeçasDisponiveis: TGroupBox
        AlignWithMargins = True
        Left = 3
        Top = 3
        Width = 502
        Height = 472
        Align = alLeft
        Caption = 'Pe'#231'as Disponiveis'
        TabOrder = 0
        object pTopPecasDisp: TPanel
          Left = 2
          Top = 18
          Width = 498
          Height = 34
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object edtPesquisaPecas: TEdit
            AlignWithMargins = True
            Left = 3
            Top = 3
            Width = 295
            Height = 23
            Margins.Bottom = 8
            Align = alClient
            TabOrder = 0
            ExplicitHeight = 24
          end
          object btnPesquisaPecas: TButton
            AlignWithMargins = True
            Left = 420
            Top = 3
            Width = 75
            Height = 23
            Margins.Bottom = 8
            Align = alRight
            Caption = 'Pesquisar'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
            OnClick = btnPesquisaPecasClick
          end
          object cbPesquisaPecas: TComboBox
            AlignWithMargins = True
            Left = 304
            Top = 3
            Width = 110
            Height = 24
            Margins.Bottom = 0
            Align = alRight
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ItemIndex = 0
            ParentFont = False
            TabOrder = 2
            Text = 'Come'#231'a com'
            Items.Strings = (
              'Come'#231'a com'
              'Id'#234'ntico'
              'Cont'#233'm')
          end
        end
        object pBottomPecasDisp: TPanel
          Left = 2
          Top = 421
          Width = 498
          Height = 49
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 1
          object btnAdicionarPecas: TPngSpeedButton
            AlignWithMargins = True
            Left = 360
            Top = 3
            Width = 135
            Height = 43
            Align = alRight
            Caption = 'Adicionar'
            Flat = True
            Layout = blGlyphRight
            OnClick = btnAdicionarPecasClick
            PngImage.Data = {
              89504E470D0A1A0A0000000D49484452000000320000003208060000001E3F88
              B1000001214944415478DAEDD9410AC2301005D0C4B5DEA1DE446FE2DE958711
              975EA937B047A82008228C55E84245C166FEFFA96436A55DE531696686C4F027
              11D50B289002C93C5C20D5F6B4EA1E75B39ED6A385DC1131DADECCDA10264B15
              2609D223FA77256630A4DA9DAB68D7C3EB7715C635234A8CDB3FA2C6B89D5A6A
              8C5B1D51635C0BA212E35ED95518488BA2C0C07A2D3606DA343231F0EE9785A1
              B4F10C0C6D1E4163A8831512439F105118C9A88BC0C866F6AF98CB6CDE6C623B
              6A4847399A4D16BF6625ABAD35142181201074080A4185201134081A41813010
              70080B0185301130081B01812810EE1015C215A244B841D40817480E8864482E
              8824C8A78B1E052209F2C0BC65448348863C63740817488F0963BF9ECE250A24
              B72890DCE2066D4B8942E091AE0C0000000049454E44AE426082}
            ExplicitLeft = 365
          end
        end
        object dgPecasDisp: TDBGrid
          AlignWithMargins = True
          Left = 5
          Top = 55
          Width = 492
          Height = 363
          Align = alClient
          DataSource = dsPecas
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
          ParentFont = False
          TabOrder = 2
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -13
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = []
          OnTitleClick = dgPecasDispTitleClick
          Columns = <
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'CODESTOQUE'
              Title.Alignment = taCenter
              Title.Caption = 'C'#243'digo'
              Title.Color = clSilver
              Width = 49
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DESCESTOQUE'
              Title.Caption = 'Descri'#231#227'o'
              Width = 292
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'QTDESTOQUE'
              Title.Alignment = taCenter
              Title.Caption = 'Qnt.'
              Width = 41
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'VALORESTOQUE'
              Title.Alignment = taCenter
              Title.Caption = 'Pre'#231'o'
              Width = 81
              Visible = True
            end>
        end
      end
    end
    object tbLacres: TTabSheet
      Caption = 'Lacres'
      Enabled = False
      ImageIndex = 5
      object pBottomLacres: TPanel
        Left = 0
        Top = 478
        Width = 1048
        Height = 60
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 2
        object btnStep6Prox: TPngSpeedButton
          Left = 893
          Top = 0
          Width = 155
          Height = 60
          Align = alRight
          Caption = 'Pr'#243'ximo'
          Flat = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Layout = blGlyphRight
          ParentFont = False
          OnClick = btnStep6ProxClick
          PngImage.Data = {
            89504E470D0A1A0A0000000D49484452000000320000003208060000001E3F88
            B10000015F4944415478DAEDD8CB5583501006E099B8D38576622A50582A2E92
            0E4207DA49EC2076309BEC49D240ECC01292BD320E81934D78AA9819CEFC3BCE
            817BF978FCDC03C24082E73E01873844791CA22D0ED11687FC26C164394E28DA
            9A87DC4D9709332C36142DCC4364E27B669EAFE9E9C53CE4B021776645516C1F
            9263B608976142E1CE36E488194D137AF8B00D3960788780E14F1A4D17A4C0A4
            308A37F448B621455286B84B3D9F40B28FD517E2759F1094DA9599C7C8507B29
            BBD4F3C9306DAED65F454E14101B1E8A96F5AC1F92631AEBD906E488A9AE673B
            90FC80CA7AB605293065F56C0F02B04F1967D6217BA9ECC0F8A3C5EFC81713E3
            2F7B86B80A4CD7AFECF3265FF759D358A54B944F486FFA044885CE65E25BCCA6
            AF5DA2C0EB9AA2E776639E21FFB268540029AD576B90CA7A3504A9AF572390E6
            7A550F695BAFAA215DEA552D6410BF4C07F313BB8F38445B1CA22D0ED1168768
            CB6020DFC540FE33396FC90C0000000049454E44AE426082}
          ExplicitLeft = 939
        end
        object PngSpeedButton1: TPngSpeedButton
          Left = 0
          Top = 0
          Width = 150
          Height = 60
          Align = alLeft
          Caption = 'Voltar'
          Enabled = False
          Flat = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          OnClick = btnStep5VoltarClick
          PngImage.Data = {
            89504E470D0A1A0A0000000D49484452000000320000003208060000001E3F88
            B10000014A4944415478DAEDD8CD6DC2401005E019933B74103A8006887CE7B2
            1DA4054A48072921740007C815920AEC4EC82139B2998DB5E058B048889F79D6
            BC031A632CED2733B318A69684EFBD00831844790CA22D06D11683688B41B4E5
            2290DC2D875BA697CFD9D8C14246EEDD65B47DF3CCA5407248C8C82D2619F36B
            A83DD10724E4C92DA6CCFC1C8FE120B95BF53CFDACE4CA61FD7D2848686A59F0
            4CAEEA37CFC14062531373EFD0790848BDA99BABF7BCAB0B995C936B2EB6E3FD
            D77A3E2ECE82349BFA9F43207CC32D3575D78F2EE358534341524D0D0339D5D4
            5810F65329BBD09090BFAF16D35CCA476848859166E7EFB57C64000D89811FBF
            F52437C4F05295A5D457DD101F28DB9CBD21EE31E92100F11325263504A02015
            E6F0108083C4C03F58D5D38A47DD3DA61A020229A02121ADF83B484B0CA22D06
            D11683688B41B4C520DA62106DF905F9ABBE336465879B0000000049454E44AE
            426082}
          ExplicitHeight = 50
        end
      end
      object gbInfoMangLacre: TGroupBox
        AlignWithMargins = True
        Left = 3
        Top = 3
        Width = 1042
        Height = 122
        Align = alTop
        Caption = 'Informa'#231#245'es sobre a mangueira'
        TabOrder = 0
        object lblCodMangLacre: TLabel
          Left = 16
          Top = 16
          Width = 43
          Height = 16
          Caption = 'C'#243'digo'
        end
        object lblNumSerieLacre: TLabel
          Left = 103
          Top = 16
          Width = 106
          Height = 16
          Caption = 'N'#250'mero de S'#233'rie'
        end
        object lblDiametroLacre: TLabel
          Left = 246
          Top = 16
          Width = 59
          Height = 16
          Caption = 'Di'#226'metro'
        end
        object lblCompNomLacre: TLabel
          Left = 338
          Top = 16
          Width = 92
          Height = 16
          Caption = 'Comp. Nominal'
        end
        object lblFabricante: TLabel
          Left = 181
          Top = 61
          Width = 68
          Height = 16
          Caption = 'Fabricante'
        end
        object lblRevestLacre: TLabel
          Left = 16
          Top = 62
          Width = 90
          Height = 16
          Caption = 'Revestimento'
        end
        object lblCompRealLacre: TLabel
          Left = 378
          Top = 61
          Width = 71
          Height = 16
          Caption = 'Comp. Real'
        end
        object lblDtFabLacre: TLabel
          Left = 449
          Top = 16
          Width = 124
          Height = 16
          Caption = 'Data de Fabrica'#231#227'o'
        end
        object lblLacreAnterior: TLabel
          Left = 490
          Top = 61
          Width = 95
          Height = 16
          Caption = 'Lacre Anterior'
        end
        object dbCodMangLacre: TDBText
          Left = 26
          Top = 38
          Width = 55
          Height = 17
          DataField = 'CODMANG'
          DataSource = dsMangueiras
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object dbRevestLacre: TDBText
          Left = 26
          Top = 84
          Width = 159
          Height = 17
          DataField = 'CARCACA'
          DataSource = dsMangueiras
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object dbNumSerieLacre: TDBText
          Left = 113
          Top = 38
          Width = 106
          Height = 17
          DataField = 'NUMERO'
          DataSource = dsMangueiras
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object dbDiamLacre: TDBText
          Left = 256
          Top = 38
          Width = 65
          Height = 17
          DataField = 'DIAMETRO'
          DataSource = dsMangueiras
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object dbCompNomLacre: TDBText
          Left = 348
          Top = 38
          Width = 55
          Height = 17
          DataField = 'COMPNOM'
          DataSource = dsMangueiras
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object dbDtFabLacre: TDBText
          Left = 459
          Top = 38
          Width = 55
          Height = 17
          DataField = 'DATAFAB'
          DataSource = dsMangueiras
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object dbFabricante: TDBText
          Left = 191
          Top = 83
          Width = 181
          Height = 17
          DataField = 'FABRICANTE'
          DataSource = dsMangueiras
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object dbCompRealLacre: TDBText
          Left = 388
          Top = 83
          Width = 55
          Height = 17
          DataField = 'COMPREAL'
          DataSource = dsMangueiras
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object dbLacreAtual: TDBText
          Left = 500
          Top = 83
          Width = 55
          Height = 17
          DataField = 'LACRE'
          DataSource = dsMangueiras
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object pLeftLacre: TPanel
          Left = 659
          Top = 18
          Width = 381
          Height = 102
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 0
          object btnAntMngLacre: TPngSpeedButton
            Left = 0
            Top = 0
            Width = 75
            Height = 102
            Align = alLeft
            Caption = 'Anterior'
            Enabled = False
            Flat = True
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            Layout = blGlyphTop
            ParentFont = False
            OnClick = btnAntMngLacreClick
            PngImage.Data = {
              89504E470D0A1A0A0000000D4948445200000018000000180806000000E0773D
              F80000000473424954080808087C08648800000006624B474400FF00FF00FFA0
              BDA7930000000970485973000006EC000006EC011E7538350000001974455874
              536F667477617265007777772E696E6B73636170652E6F72679BEE3C1A000000
              924944415478DA63FCFFFF3F032D01E3A805A3163038046FCD3DB0D67B32D52D
              70743CC0F24FF0DB644606868C03EBBC18A96A81ADCF56416636C63540A61388
              4F550B9CFC77A8FF63FEB719C854858951CD02C790AD2EFFFF31AE063205C835
              10DD41700B809199C5F09F712290C94289E1036701CD830806681AC93040D364
              0A0F2E5A66346440B3A2825A60D482116001005E3F85D1CD9D2AA10000000049
              454E44AE426082}
            ExplicitHeight = 82
          end
          object btnCancelarMngLacre: TPngSpeedButton
            Left = 75
            Top = 0
            Width = 75
            Height = 102
            Align = alLeft
            Caption = 'Cancelar'
            Enabled = False
            Flat = True
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            Layout = blGlyphTop
            ParentFont = False
            OnClick = btnCancelarMngLacreClick
            PngImage.Data = {
              89504E470D0A1A0A0000000D4948445200000018000000180806000000E0773D
              F80000000473424954080808087C08648800000006624B474400FF00FF00FFA0
              BDA7930000000970485973000006EC000006EC011E7538350000001974455874
              536F667477617265007777772E696E6B73636170652E6F72679BEE3C1A000001
              E94944415478DADD953D68535114C7CF69A808BADAA1999482DAA41DD4800D14
              523274B30E9D9C9CEC509C0A7E804E0A7E8053E9D04C9D3A7568BA75282D04D2
              42AD83A9B18552A774A8AB8244F4F8BB7A032FE1E5E52966682FFCB9E7BE7BEF
              FF7F3EEEBD4FCD4CBAD9F4F40BECA98EB0220F320C6FF8CF6F55641BAC5D31DB
              FC2781AAEA79BA37E05E07270B6066D0EC4B6C818FAAD7F8BA8479117CC59EC3
              DB7202AFC1F76F22A91E9171E6EE8373E013F39357CDDE7514F09EBFF7E4A51F
              227787CC0EC3BCC39134BB7730CF381130DC1A4998C0BC4F4B69502427663FC3
              C80F542FD4A901E610A87B910202536D055C41612BBBB4E0F9703BCF5BC82BA4
              E70E2C5B2E5DA42E1B2C7C9300213F61F40CBC4E993D8C438EDBF901B3CF44FE
              82F123C49E528BE7A1022C2AD2DD02B709B51897DCCDEDAAE6F07E1D7385BD13
              ED046A74FD9C94E465B3A3B8E481F963CC23049291026CEE0BD9DC96DCB57DD5
              7EEA56EB24F03B45147A2C6DB61197DCEF7569598E4C51A3C8982F59F4382EB9
              6B1F545F51E00791450E1E5316DE6466310E7945F552E2CFE58C3EA63ED4C645
              6B5C9E487251EDA98AB8748E4AA78BE605824F459D48AE13F26E84E70B9E3CDE
              53E16BD1F4D8815952B77A965473527A4186F92CE2D3F2B78F5D4B24DD79AE83
              AD6B3F9CFFD54EBEC02F8A1141E0D31A722F0000000049454E44AE426082}
            ExplicitHeight = 82
          end
          object btnEditarMngLacre: TPngSpeedButton
            Left = 150
            Top = 0
            Width = 75
            Height = 102
            Align = alLeft
            Caption = 'Editar'
            Flat = True
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            Layout = blGlyphTop
            ParentFont = False
            OnClick = btnEditarMngLacreClick
            PngImage.Data = {
              89504E470D0A1A0A0000000D4948445200000018000000180806000000E0773D
              F80000000473424954080808087C08648800000006624B474400FF00FF00FFA0
              BDA7930000000970485973000006EC000006EC011E7538350000001974455874
              536F667477617265007777772E696E6B73636170652E6F72679BEE3C1A000000
              B24944415478DA63FCFFFF3F032D0123BA05134EFD6AA0C4C002333614FD1816
              4C3CFD8B222FE59BB2318E5A30842C3872ED0D838D960898C607406A462D18B5
              60D482C16A013900AF05CECBBED5984832B95062C199E7FFF6EC8DE26AC165C1
              63202543890540F00468812C760B967E3BCAC0C8604591F1FF198EED8DE6B2C6
              6A81CBF2EF45407E2F25E633323216EF89E4ECC36A41D86A06E6B7BFBFA50199
              E2649AFF5298956BD6AA5086BF582DA00500006469EAD11D7100090000000049
              454E44AE426082}
            ExplicitLeft = 151
            ExplicitHeight = 82
          end
          object btnSalvarMngLacre: TPngSpeedButton
            Left = 225
            Top = 0
            Width = 75
            Height = 102
            Align = alLeft
            Caption = 'Salvar'
            Enabled = False
            Flat = True
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            Layout = blGlyphTop
            ParentFont = False
            OnClick = btnSalvarMngLacreClick
            PngImage.Data = {
              89504E470D0A1A0A0000000D4948445200000018000000180806000000E0773D
              F80000000473424954080808087C08648800000006624B474400FF00FF00FFA0
              BDA7930000000970485973000006EC000006EC011E7538350000001974455874
              536F667477617265007777772E696E6B73636170652E6F72679BEE3C1A000001
              7F4944415478DA63FCFFFF3F032D01E3A805A31690049CE77B183132FD2FFDFB
              FF57E2FE84FD3FA86A01C8702626863D40D3041918190AF6C4ED9848350B9C16
              BA1B333330EE0619CEC8C030DFE681654A7D7DFD3FAA58806C3890BBC0F68165
              32CC708A2D0019CEC4C0B807C814C066384516B82D7633F9F78F6937D8F0FF8C
              0B6D1F5A24A11B4EB605C41A8E6281C92C13560176D1F9FFFFFDEFDB9BB8E31C
              2EC35D16789A0275ED8206CB2260B024E2321CC502D785EE85FF1918FB80A9E0
              FD5F86FFAEFBE2779EC56138C8E5FCC4188E6281E30147169647ECCB80DC5020
              F70313D33FD75DB1BBCEC0143A2FF23063FCCF0072393F2323C3629BFB960984
              0CC7880390254C0FD997027D1106B20418BE6E7B12B69F26D770AC911CB63A8C
              F9DDD74F4B81B9311CC8FDC8C0C858CEF0FF7F272458FE2FB17D60154FACE158
              2D805BF2EDE312A07404429474C3715A00B3E4FDB74F8B81B29140654B6D1F58
              C4916A385E0B6096BCFDFA2955989B6FF6AAD0557F49359CA005D40043DF0200
              9CB2F1D124E363870000000049454E44AE426082}
            ExplicitLeft = 227
            ExplicitHeight = 82
          end
          object btnProxMngLacre: TPngSpeedButton
            Left = 300
            Top = 0
            Width = 75
            Height = 102
            Align = alLeft
            Caption = 'Pr'#243'xima'
            Flat = True
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            Layout = blGlyphTop
            ParentFont = False
            OnClick = btnProxMngLacreClick
            PngImage.Data = {
              89504E470D0A1A0A0000000D4948445200000018000000180806000000E0773D
              F80000000473424954080808087C08648800000006624B474400FF00FF00FFA0
              BDA7930000000970485973000006EC000006EC011E7538350000001974455874
              536F667477617265007777772E696E6B73636170652E6F72679BEE3C1A000000
              844944415478DA63FCFFFF3F032D01E3A805A316E0050EC15B730FACF59E4C3B
              0B82B68174CF607ACF95BB7FBFC31F9A580065EEFBFBEB7FC8E12DDEEF696501
              08DC66FACBE4BB6FA3C74D9C16A06920077C6064FA1FBA7F8DF71E5A5900027F
              80A6E603237FDA90B4007F10910A488E640A2CA05D32A56D46A37551410C18B5
              600458000036859BD1ACC6ACF50000000049454E44AE426082}
            ExplicitLeft = 303
            ExplicitHeight = 82
          end
        end
      end
      object gbLacre: TGroupBox
        AlignWithMargins = True
        Left = 3
        Top = 131
        Width = 1042
        Height = 174
        Align = alTop
        Caption = 'Novo Lacre'
        TabOrder = 1
        object lblCodLacre: TLabel
          Left = 16
          Top = 17
          Width = 103
          Height = 16
          Caption = 'C'#243'digo do Lacre'
        end
        object lblNumLacre: TLabel
          Left = 16
          Top = 69
          Width = 132
          Height = 16
          Caption = 'Numera'#231#227'o do Lacre'
        end
        object lblDataCadLacre: TLabel
          Left = 16
          Top = 115
          Width = 174
          Height = 16
          Caption = 'Data do Cadastro do Lacre'
        end
        object dbNumLacreNovo: TDBText
          Left = 26
          Top = 91
          Width = 223
          Height = 17
          DataField = 'NUMERO'
          DataSource = dsLacre
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object dbDataCadLacreNovo: TDBText
          Left = 26
          Top = 137
          Width = 223
          Height = 17
          DataField = 'DATACADLACRE'
          DataSource = dsLacre
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object btnPesquisarLacre: TPngSpeedButton
          Left = 109
          Top = 39
          Width = 172
          Height = 24
          Caption = 'Pesquisar/Adicionar'
          Flat = True
          OnClick = btnPesquisarLacreClick
          PngImage.Data = {
            89504E470D0A1A0A0000000D4948445200000018000000180806000000E0773D
            F80000000473424954080808087C08648800000006624B474400FF00FF00FFA0
            BDA7930000000970485973000006EC000006EC011E7538350000001974455874
            536F667477617265007777772E696E6B73636170652E6F72679BEE3C1A000002
            2F4944415478DA63FCFFFF3F032D01E38059101F1FAFC6C2C2120594370661B0
            6246C6B320FCE7CF9F650B172EBC459605400318939292F281E26D0C4C2C9C8C
            FC520C0C827210C9F78F18FE7F7CC6C0F0EFCF77A0B2AA79F3E64DFC4F200850
            2C00199E9898B805C8F46294316260328A606060E346D5F1EB2BC3BF732B18FE
            3F3907E26D9B3F7FBE0F3E4B502C484E4E2E00F2FB4106332AD982C578D91819
            44B818C1EC37DFFE337CFE0551FFFFDE61B045403715CE9D3B7702410B4061CE
            CCCC7C01E8724E268B640666A099C612CC0CA2DC8C0CAFBE42D48801D9AF81EC
            B32FFE32FC050AFD3B3117E493EF7FFFFE35C01527700B804153CFC8CCDAC0EC
            D3060E16593E2606514E46868BAF20868100C8524371668637DFFF333CF8F80F
            1C5C7FB75431FCFFFBBB0118548D782D0046EC664641391F26E7726212071CFC
            DBDBC9F0FFFDA32DC008F725E483674CCAB6924C4691A459706E39C3BFBB879F
            037D2035B016D0238850229928404A24A32753A25C4F4A3205016C190D178065
            3420A805064F0B2E759416159F81F801103B032D794DD0029825C41476401E30
            B218C28058F7D5E7EF2FFFFFFD6FB775DD8A5B042D8001628A6BA043445F7CFC
            7AE5DDD79F62EC2CCC3F847958F5D7AD40B584E20AC723244AFBDBF71F677FFD
            FDC78ECD12AAD4683E41113A9F7EFE3A03B344808BCB60E3AAC537A966010878
            0784E97EFEFDE734C8120E5696F70736AF11A2AA0520E01912A5F7FDC7CF433C
            9CAC495B56AF5847750BB0019A5B0000C7DB86E012284D070000000049454E44
            AE426082}
        end
        object edtCodLacre: TEdit
          Left = 26
          Top = 39
          Width = 77
          Height = 24
          ReadOnly = True
          TabOrder = 0
        end
      end
    end
    object tbRelatorio: TTabSheet
      Caption = 'Relat'#243'rio'
      ImageIndex = 6
      object btnGerRelatInsp: TPngSpeedButton
        AlignWithMargins = True
        Left = 150
        Top = 150
        Width = 306
        Height = 178
        Margins.Left = 150
        Margins.Top = 150
        Margins.Right = 150
        Margins.Bottom = 150
        Align = alLeft
        Caption = 'Gerar Relat'#243'rio de Inspe'#231#227'o'
        Flat = True
        Layout = blGlyphTop
        OnClick = btnGerRelatInspClick
        PngImage.Data = {
          89504E470D0A1A0A0000000D49484452000000320000003208060000001E3F88
          B1000002DA4944415478DAEDD9CF5312611807F0074165011D24154114B164B2
          435213DAD82F6B9A693AE92D3B3575EBBFE9D6ADA65376CB53D334D338353911
          FD300ED2680906822401C34F1194F67D6919D03576A165DF68BF330CB3EBB23C
          1F79F77D970719344964621720412408E19120A4A539210FE756EED14F636217
          C535B767AD9759210F1EAF2CC8647049EC0279404AF5578518FB54A052CA452D
          38BDBD0B81CD747D90A949035022433234646131583BE484550B669346540493
          757F12965762FC2068380D0F764087A655ECFA2B9248E660ED7BA234CCAA4226
          4EF7824EDB2676DDAC89C476C0F1F1477D9048340B3F63D986147C44DB0EBAAE
          7661205F3D7158A51F8DC888A5138ED10F09F25F40368229F0070F2E4C42C464
          5041BF412D0C84844810D222416A0D9A18BCBE14C4933B78BB53D306E6413598
          FAD4759DB7A110D77214363653AC7F4333D1C9D12EF221CC5AA350B4E0B540DF
          4DE1FDA17006EFCFE7F70E5D238882BC7815C0C58E8E686168A0F22B80D79704
          F76A0C5A69E4D58B467221E87A78F3AEF826D7AF98588F79F6D28F9FCF8DF7E2
          EB864808BA3B767CDAE2049938D5C37A774B04A4BC50B6FF38974F8C08887369
          0B5CEE1818F51428E42D707E5C0F14252F211C1FC290DFDD83402843CF5C5AB0
          DB7AC88320C493790F9EAD26ED7AFA822E9E9AF95498F524972FC0A233842784
          1BD316DE1841210C8289B25D811B175DFBCE11A58B70B92318C1842F4630C87E
          04933363DD307DCD0C99ED3CDEA6940A987FBE0EEF3F870F1CCB0723080415F9
          FA6DE8F71B644B4522C4ECCC30EB6BE69EAE551CA7D31667AE0B67F518FB5721
          FDF4971ACB00BF76D0376F1CEE3FFAF247C47ECCDD5BC7E1E810F7151EB5833C
          BE047D1FC7B11DC46CF369D0218873295C15518EB1DBBA39436A6AD095EFE3DA
          3245C38CCBD0A8E53575B74C51D0305351E2761CD3995C6938D50C21398742D0
          0F3D8502D8C42E906BEEDCB44EB142FEE54810D22241488B04212D4D03F90586
          7916515B5236DE0000000049454E44AE426082}
        ExplicitTop = 147
      end
      object btnGerCertManut: TPngSpeedButton
        AlignWithMargins = True
        Left = 600
        Top = 150
        Width = 298
        Height = 178
        Margins.Left = 150
        Margins.Top = 150
        Margins.Right = 150
        Margins.Bottom = 150
        Align = alRight
        Caption = 'Gerar Certificado Manuten'#231#227'o'
        Enabled = False
        Flat = True
        Layout = blGlyphTop
        OnClick = btnGerCertManutClick
        PngImage.Data = {
          89504E470D0A1A0A0000000D49484452000000320000003208060000001E3F88
          B1000002FC4944415478DAED984B4C13511486CF60B196B674A8829456AD09D4
          B489F85840048D5456C6A51B5F89BA33B8758FEC5DB891B89298A8B871695C18
          852898D0051A4C6852486C425F82C0943EC63E60BC776C9B693AC1CE30CCDCE0
          FCC9E466CE34F79E2F77CE3FBD87823D224AEB0474101D8470ED4D90B1D7A1C7
          6838A57552F5EAEE358F5F14E4D9786892A2E0A2D6094A00A9E4FF4F109F8706
          AB65BFA609A7D279980F31F2410C060AFC7D1DFCA8A5D8DF9B30154840B1C8C9
          03E9F6DAC1E968D214A2AC683C0B73C13569209D6E2BB83A2C603AB04FEBFCAB
          847726124BC36238551F48EFD936B0D3B575B1B69E835526A74AD2076923D85B
          8CB539307998995DDE19C8E28F0D5840971AEA3ADE0C9DE8D241FE0B90683C03
          11E41E6AC885DCD2E930EF0E08095204846537219B2BAA927093D1002653ADFD
          EB3522945E230A4A07214DAA83E07A0A2F6560039D25B09AD199E6D85133B8DA
          CD3B9A575590B9F975882632A2CF7001777B5BC807115A34B650676907309830
          2E66AD4481BCFF144327B92DF076D1E03E62A97A165E4A437081E13F7403E71C
          E482E07A980EFC5DE4F22597E86FDE7D8CF0230611FB721301820F5E335F57EA
          02E93DD32A7A7022024498687F4F1BEF5442D5B363C480941D0B439CF4D11518
          0C31FB7D95FFE3890DA0DB27CFB9540329A0429F0EFCE41B055842102CDCD0E8
          EF390C8D8606B241CA306FDE86C166AD9E2399CAC3D52B6ED9109241702FABBD
          D5046D874CB2177C301200DA6644308D25880230C91C3C1AEE913DE7F22F1612
          2B2CDFE3AA0BA47CEF7659C0EBA1658388492E4830C4403892AE8A49EAFD0EF4
          396435EA9404C1F536F9255E139704825FB32653A3E4C59F3E0F8AC6EFDDF64A
          9E2BCB162AAF936C10B9FAF039261A1FBCD0A1C4F4DB838C8D87EE00706E4540
          A6E2C3786CA038BEC7BAC551FC677CF0BC63443190EB271E8A8228A95BF72739
          A42487CAACB410DA6DCAF6E2C9C0AEACB96B20378726180CF16AD4FF0DDFDF18
          9A388D615E8EFAE5D9A0562038F132C47631E241D4960E429A7410D2A48390A6
          3FE80C1551D0726E4B0000000049454E44AE426082}
        ExplicitLeft = 639
        ExplicitTop = 120
        ExplicitHeight = 185
      end
      object Panel1: TPanel
        Left = 0
        Top = 478
        Width = 1048
        Height = 60
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 0
        object btnConcluido: TPngSpeedButton
          Left = 898
          Top = 0
          Width = 150
          Height = 60
          Align = alRight
          Caption = 'Concluido'
          Flat = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          OnClick = btnConcluidoClick
          PngImage.Data = {
            89504E470D0A1A0A0000000D49484452000000320000003208060000001E3F88
            B1000001374944415478DAEDD6BB0DC2301080613B053523B0012B981440C908
            6402D884111236A00421A18CC008B0010C408EA48814204FDBF15DD0FD95BBBB
            4F6E4E8A3F49622FC01086108F21D46208B518422D86B84E85CB491C1C6F8386
            F8D12214025609C02C0ECED741423244BAE43A7B838047158634A488C80310F7
            4B709A0C0652864815CF970035981FE98A2009D1419083E82248414C106420A6
            0812101B0874882D042AC426020D621B8102E903D108F1A3F92E3DD2F626035C
            206A21F9D0BA8B930AA212F23DD414D337A214A24235F6C42896524E3FE66A62
            5C204A213631AE1095101B1897885A8809C635A211A283C140B48274C160215A
            43DA603CE96DB0109D207598D21C223A435A631C23B4208D18048436A4128384
            3082FC601011C69002E69008D86221AC40A8C4106A31845A0CA1D61BC4179142
            2AC25C470000000049454E44AE426082}
          ExplicitLeft = 919
          ExplicitHeight = 55
        end
      end
    end
  end
  object dsTestes: TDataSource
    DataSet = DtmPrincipal.cdsTestes
    Left = 80
    Top = 472
  end
  object dsTecnicos: TDataSource
    DataSet = DtmPrincipal.cdsTesteTecnico
    Left = 128
    Top = 472
  end
  object dsPecas: TDataSource
    DataSet = DtmPrincipal.cdsEstoque_V
    Left = 496
    Top = 472
  end
  object dsAvaliacao: TDataSource
    DataSet = DtmPrincipal.cdsAvaliacao_V
    Left = 320
    Top = 472
  end
  object dsTestesMangPecas: TDataSource
    DataSet = DtmPrincipal.cdsTesteMangPecas_V
    Left = 560
    Top = 472
  end
  object dsClientes: TDataSource
    DataSet = DtmPrincipal.cdsClientes_V
    Left = 184
    Top = 472
  end
  object dsMangueiras: TDataSource
    DataSet = DtmPrincipal.cdsMangueiras_V
    Left = 248
    Top = 472
  end
  object dsEditMang: TDataSource
    DataSet = DtmPrincipal.cdsMangueiras
    Left = 376
    Top = 472
  end
  object dsLacre: TDataSource
    DataSet = DtmPrincipal.cdsMangLacre
    Left = 640
    Top = 472
  end
  object icons: TPngImageList
    Height = 25
    Width = 25
    PngImages = <
      item
        Background = clWindow
        Name = 'Industrial Scales-50'
        PngImage.Data = {
          89504E470D0A1A0A0000000D4948445200000019000000190806000000C4E985
          63000000097048597300000B1300000B1301009A9C1800000A4F694343505068
          6F746F73686F70204943432070726F66696C65000078DA9D53675453E9163DF7
          DEF4424B8880944B6F5215082052428B801491262A2109104A8821A1D91551C1
          114545041BC8A088038E8E808C15512C0C8A0AD807E421A28E83A3888ACAFBE1
          7BA36BD6BCF7E6CDFEB5D73EE7ACF39DB3CF07C0080C9648335135800CA9421E
          11E083C7C4C6E1E42E40810A2470001008B3642173FD230100F87E3C3C2B22C0
          07BE000178D30B0800C04D9BC0301C87FF0FEA42995C01808401C07491384B08
          801400407A8E42A600404601809D98265300A0040060CB6362E300502D006027
          7FE6D300809DF8997B01005B94211501A09100201365884400683B00ACCF568A
          450058300014664BC43900D82D00304957664800B0B700C0CE100BB200080C00
          305188852900047B0060C8232378008499001446F2573CF12BAE10E72A000078
          99B23CB9243945815B082D710757572E1E28CE49172B14366102619A402EC279
          99193281340FE0F3CC0000A0911511E083F3FD78CE0EAECECE368EB60E5F2DEA
          BF06FF226262E3FEE5CFAB70400000E1747ED1FE2C2FB31A803B06806DFEA225
          EE04685E0BA075F78B66B20F40B500A0E9DA57F370F87E3C3C45A190B9D9D9E5
          E4E4D84AC4425B61CA577DFE67C25FC057FD6CF97E3CFCF7F5E0BEE22481325D
          814704F8E0C2CCF44CA51CCF92098462DCE68F47FCB70BFFFC1DD322C44962B9
          582A14E35112718E449A8CF332A52289429229C525D2FF64E2DF2CFB033EDF35
          00B06A3E017B912DA85D6303F64B27105874C0E2F70000F2BB6FC1D428080380
          6883E1CF77FFEF3FFD47A02500806649927100005E44242E54CAB33FC7080000
          44A0812AB0411BF4C1182CC0061CC105DCC10BFC6036844224C4C24210420A64
          801C726029AC82422886CDB01D2A602FD4401D34C051688693700E2EC255B80E
          3D700FFA61089EC128BC81090441C808136121DA8801628A58238E08179985F8
          21C14804128B2420C9881451224B91354831528A542055481DF23D720239875C
          46BA913BC8003282FC86BC47319481B2513DD40CB543B9A8371A8446A20BD064
          74319A8F16A09BD072B41A3D8C36A1E7D0AB680FDA8F3E43C730C0E8180733C4
          6C302EC6C342B1382C099363CBB122AC0CABC61AB056AC03BB89F563CFB17704
          128145C0093604774220611E4148584C584ED848A8201C243411DA0937090384
          51C2272293A84BB426BA11F9C4186232318758482C23D6128F132F107B8843C4
          37241289433227B9900249B1A454D212D246D26E5223E92CA99B34481A2393C9
          DA646BB20739942C202BC885E49DE4C3E433E41BE421F25B0A9D624071A4F853
          E22852CA6A4A19E510E534E5066598324155A39A52DDA8A15411358F5A42ADA1
          B652AF5187A81334759A39CD8316494BA5ADA295D31A681768F769AFE874BA11
          DD951E4E97D057D2CBE947E897E803F4770C0D861583C7886728199B18071867
          197718AF984CA619D38B19C754303731EB98E7990F996F55582AB62A7C1591CA
          0A954A9526951B2A2F54A9AAA6AADEAA0B55F355CB548FA95E537DAE46553353
          E3A909D496AB55AA9D50EB531B5367A93BA887AA67A86F543FA47E59FD890659
          C34CC34F43A451A0B15FE3BCC6200B6319B3782C216B0DAB86758135C426B1CD
          D97C762ABB98FD1DBB8B3DAAA9A13943334A3357B352F394663F07E39871F89C
          744E09E728A797F37E8ADE14EF29E2291BA6344CB931655C6BAA96979658AB48
          AB51AB47EBBD36AEEDA79DA6BD45BB59FB810E41C74A275C2747678FCE059DE7
          53D953DDA70AA7164D3D3AF5AE2EAA6BA51BA1BB4477BF6EA7EE989EBE5E809E
          4C6FA7DE79BDE7FA1C7D2FFD54FD6DFAA7F5470C5806B30C2406DB0CCE183CC5
          35716F3C1D2FC7DBF151435DC34043A561956197E18491B9D13CA3D5468D460F
          8C69C65CE324E36DC66DC6A326062621264B4DEA4DEE9A524DB9A629A63B4C3B
          4CC7CDCCCDA2CDD699359B3D31D732E79BE79BD79BDFB7605A785A2CB6A8B6B8
          6549B2E45AA659EEB6BC6E855A3959A558555A5DB346AD9DAD25D6BBADBBA711
          A7B94E934EAB9ED667C3B0F1B6C9B6A9B719B0E5D806DBAEB66DB67D61676217
          67B7C5AEC3EE93BD937DBA7D8DFD3D070D87D90EAB1D5A1D7E73B472143A563A
          DE9ACE9CEE3F7DC5F496E92F6758CF10CFD833E3B613CB29C4699D539BD34767
          1767B97383F3888B894B82CB2E973E2E9B1BC6DDC8BDE44A74F5715DE17AD2F5
          9D9BB39BC2EDA8DBAFEE36EE69EE87DC9FCC349F299E593373D0C3C843E051E5
          D13F0B9F95306BDFAC7E4F434F8167B5E7232F632F9157ADD7B0B7A577AAF761
          EF173EF63E729FE33EE33C37DE32DE595FCC37C0B7C8B7CB4FC36F9E5F85DF43
          7F23FF64FF7AFFD100A78025016703898141815B02FBF87A7C21BF8E3F3ADB65
          F6B2D9ED418CA0B94115418F82AD82E5C1AD2168C8EC90AD21F7E798CE91CE69
          0E85507EE8D6D00761E6618BC37E0C2785878557863F8E7088581AD131973577
          D1DC4373DF44FA449644DE9B67314F39AF2D4A352A3EAA2E6A3CDA37BA34BA3F
          C62E6659CCD5589D58496C4B1C392E2AAE366E6CBEDFFCEDF387E29DE20BE37B
          17982FC85D7079A1CEC2F485A716A92E122C3A96404C884E3894F041102AA816
          8C25F21377258E0A79C21DC267222FD136D188D8435C2A1E4EF2482A4D7A92EC
          91BC357924C533A52CE5B98427A990BC4C0D4CDD9B3A9E169A76206D323D3ABD
          31839291907142AA214D93B667EA67E66676CBAC6585B2FEC56E8BB72F1E9507
          C96BB390AC05592D0AB642A6E8545A28D72A07B267655766BFCD89CA3996AB9E
          2BCDEDCCB3CADB90379CEF9FFFED12C212E192B6A5864B572D1D58E6BDAC6A39
          B23C7179DB0AE315052B865606AC3CB88AB62A6DD54FABED5797AE7EBD267A4D
          6B815EC1CA82C1B5016BEB0B550AE5857DEBDCD7ED5D4F582F59DFB561FA869D
          1B3E15898AAE14DB1797157FD828DC78E51B876FCABF99DC94B4A9ABC4B964CF
          66D266E9E6DE2D9E5B0E96AA97E6970E6E0DD9DAB40DDF56B4EDF5F645DB2F97
          CD28DBBB83B643B9A3BF3CB8BC65A7C9CECD3B3F54A454F454FA5436EED2DDB5
          61D7F86ED1EE1B7BBCF634ECD5DB5BBCF7FD3EC9BEDB5501554DD566D565FB49
          FBB3F73FAE89AAE9F896FB6D5DAD4E6D71EDC703D203FD07230EB6D7B9D4D51D
          D23D54528FD62BEB470EC71FBEFE9DEF772D0D360D558D9CC6E223704479E4E9
          F709DFF71E0D3ADA768C7BACE107D31F761D671D2F6A429AF29A469B539AFB5B
          625BBA4FCC3ED1D6EADE7AFC47DB1F0F9C343C59794AF354C969DAE982D39367
          F2CF8C9D959D7D7E2EF9DC60DBA2B67BE763CEDF6A0F6FEFBA1074E1D245FF8B
          E73BBC3BCE5CF2B874F2B2DBE51357B8579AAF3A5F6DEA74EA3CFE93D34FC7BB
          9CBB9AAEB95C6BB9EE7ABDB57B66F7E91B9E37CEDDF4BD79F116FFD6D59E393D
          DDBDF37A6FF7C5F7F5DF16DD7E7227FDCECBBBD97727EEADBC4FBC5FF440ED41
          D943DD87D53F5BFEDCD8EFDC7F6AC077A0F3D1DC47F7068583CFFE91F58F0F43
          058F998FCB860D86EB9E383E3939E23F72FDE9FCA743CF64CF269E17FEA2FECB
          AE17162F7EF8D5EBD7CED198D1A197F29793BF6D7CA5FDEAC0EB19AFDBC6C2C6
          1EBEC97833315EF456FBEDC177DC771DEFA3DF0F4FE47C207F28FF68F9B1F553
          D0A7FB93199393FF040398F3FC63332DDB000003D34944415478DADD955D6C14
          5514C7CF9DB9333BDB5DB7D80A8B947457366917B6B195446B9BDAA814798010
          892F6A30211A9E7C26A104EC168CE5DDF8E4474C8CF2443458354851531BB092
          D82E7661695CDDD522160885A5FB3133F7CEF5DC598A4267A56AF0C1934C6692
          73EEF9DD73FEE7DE214208B8DB46FE73C893CFBC78AB175D44817B81AAAF95CA
          34C839B1FC0607031F47101D8498E7A6B307E3E680DCBAF48BC3EF7843363EB7
          73D12E2C4B59D7D068A5BB3BAF402864C3446A199C9EAA87608055F7E140025F
          676E5F77ECD09B3520CF2F86948A34D1DC5C9ADAB13D0FF7874D387C64150C7F
          BA121A1BAD2A44401BBED28B201FD482785442087460C404632A98A60D9C9721
          18D441A33AB64CC84A1EC2B0C9A557E201415B4114E5EDC2F532090683F14873
          53ECC2855FB2D7E78B993ABF5F385CBC843117970CE979FA8505BD41C1120CC3
          078AA240A95202EE38B0A9AF6F773C1E1FCA6432FD4747460EAAE8AB33EAC041
          5FA562BA952DE83FF6D17BDE902D3B5E76DF0A8E94CD18CC5EBCE42AEB0FF861
          63DF068846A27B0B85C2815028B42F97CFBD7A6CE438948B657704C32B96630B
          29821C37C7F0BB6F7843C6CF4DBB2250DC8FB0181CF9FC2B3893FD0912F11688
          46A3808024C60F1042061194CCE57290CE4CC3BAD803B0F5A9C781E81498EC03
          E6EC6C6DF1869C9BFDCD85089B832E08F80C03BE4D4F43F6E7F3408974912486
          0DE03388EB920C97C69A9BE091440B98950A5804DBA5A92EA435BCD21B32893B
          9310608E5B89E1939A1038993A0B33B397A03E1848A27F00930C5E9B2F265787
          974357FB5AD44440C534DD4A802A2EA4032BBF2304B01AE93174DD15FF442A0D
          F95F677707FCFEA162B9DC1F59153ED8DD9EA88A6E5955C165157F1382D210BF
          F41B3EDD1D86A3274EF54FE7CEF7B7449B8636753F3C2445AE98966CA34C5C46
          08BB33247F13720F58FC7BFC6EA89E6A013E5D936DF1619B746C9B856D344DCB
          AE02AA41574057DB1032EF4222B520D91B10CE0FE12C3EFBE7C325E3A8AABA63
          2AC79B71FE0760C114F23EA8EA761712AB014965F320187F14834EDE7EAB2EC9
          642A42BA0855BF698F45BC21A7F379506C91C1CFD67F80B879121C8DC41F8CD4
          808C4DA6F76BAABAEFDFFCC8640B6DCE0FF474245EF1847C787C8CA9683E4D03
          CBB665F0E2BE7B750973E0E640C775A62D6F6ACEB76DE8A19E90E1D1F18F3973
          9EB87CED6AE0BEFAD09C4AE98C6D33FA97FAE0724DA33807ACE9F25CA1A17159
          3DD3A8FAD9E6DECEAD9E904FBE1E87D1EF525D991F675E8FAF89EEEC5DDF3621
          6F5F79E06A993CA8F2361E9D98EA38FB43EEADB5B1D5BB7AD7B77FB9F9B14E6F
          4DEE96FD7F20BF030C5403ED46284D3A0000000049454E44AE426082}
      end
      item
        Background = clWindow
        Name = 'Conference-50'
        PngImage.Data = {
          89504E470D0A1A0A0000000D4948445200000019000000190806000000C4E985
          63000000097048597300000B1300000B1301009A9C1800000A4F694343505068
          6F746F73686F70204943432070726F66696C65000078DA9D53675453E9163DF7
          DEF4424B8880944B6F5215082052428B801491262A2109104A8821A1D91551C1
          114545041BC8A088038E8E808C15512C0C8A0AD807E421A28E83A3888ACAFBE1
          7BA36BD6BCF7E6CDFEB5D73EE7ACF39DB3CF07C0080C9648335135800CA9421E
          11E083C7C4C6E1E42E40810A2470001008B3642173FD230100F87E3C3C2B22C0
          07BE000178D30B0800C04D9BC0301C87FF0FEA42995C01808401C07491384B08
          801400407A8E42A600404601809D98265300A0040060CB6362E300502D006027
          7FE6D300809DF8997B01005B94211501A09100201365884400683B00ACCF568A
          450058300014664BC43900D82D00304957664800B0B700C0CE100BB200080C00
          305188852900047B0060C8232378008499001446F2573CF12BAE10E72A000078
          99B23CB9243945815B082D710757572E1E28CE49172B14366102619A402EC279
          99193281340FE0F3CC0000A0911511E083F3FD78CE0EAECECE368EB60E5F2DEA
          BF06FF226262E3FEE5CFAB70400000E1747ED1FE2C2FB31A803B06806DFEA225
          EE04685E0BA075F78B66B20F40B500A0E9DA57F370F87E3C3C45A190B9D9D9E5
          E4E4D84AC4425B61CA577DFE67C25FC057FD6CF97E3CFCF7F5E0BEE22481325D
          814704F8E0C2CCF44CA51CCF92098462DCE68F47FCB70BFFFC1DD322C44962B9
          582A14E35112718E449A8CF332A52289429229C525D2FF64E2DF2CFB033EDF35
          00B06A3E017B912DA85D6303F64B27105874C0E2F70000F2BB6FC1D428080380
          6883E1CF77FFEF3FFD47A02500806649927100005E44242E54CAB33FC7080000
          44A0812AB0411BF4C1182CC0061CC105DCC10BFC6036844224C4C24210420A64
          801C726029AC82422886CDB01D2A602FD4401D34C051688693700E2EC255B80E
          3D700FFA61089EC128BC81090441C808136121DA8801628A58238E08179985F8
          21C14804128B2420C9881451224B91354831528A542055481DF23D720239875C
          46BA913BC8003282FC86BC47319481B2513DD40CB543B9A8371A8446A20BD064
          74319A8F16A09BD072B41A3D8C36A1E7D0AB680FDA8F3E43C730C0E8180733C4
          6C302EC6C342B1382C099363CBB122AC0CABC61AB056AC03BB89F563CFB17704
          128145C0093604774220611E4148584C584ED848A8201C243411DA0937090384
          51C2272293A84BB426BA11F9C4186232318758482C23D6128F132F107B8843C4
          37241289433227B9900249B1A454D212D246D26E5223E92CA99B34481A2393C9
          DA646BB20739942C202BC885E49DE4C3E433E41BE421F25B0A9D624071A4F853
          E22852CA6A4A19E510E534E5066598324155A39A52DDA8A15411358F5A42ADA1
          B652AF5187A81334759A39CD8316494BA5ADA295D31A681768F769AFE874BA11
          DD951E4E97D057D2CBE947E897E803F4770C0D861583C7886728199B18071867
          197718AF984CA619D38B19C754303731EB98E7990F996F55582AB62A7C1591CA
          0A954A9526951B2A2F54A9AAA6AADEAA0B55F355CB548FA95E537DAE46553353
          E3A909D496AB55AA9D50EB531B5367A93BA887AA67A86F543FA47E59FD890659
          C34CC34F43A451A0B15FE3BCC6200B6319B3782C216B0DAB86758135C426B1CD
          D97C762ABB98FD1DBB8B3DAAA9A13943334A3357B352F394663F07E39871F89C
          744E09E728A797F37E8ADE14EF29E2291BA6344CB931655C6BAA96979658AB48
          AB51AB47EBBD36AEEDA79DA6BD45BB59FB810E41C74A275C2747678FCE059DE7
          53D953DDA70AA7164D3D3AF5AE2EAA6BA51BA1BB4477BF6EA7EE989EBE5E809E
          4C6FA7DE79BDE7FA1C7D2FFD54FD6DFAA7F5470C5806B30C2406DB0CCE183CC5
          35716F3C1D2FC7DBF151435DC34043A561956197E18491B9D13CA3D5468D460F
          8C69C65CE324E36DC66DC6A326062621264B4DEA4DEE9A524DB9A629A63B4C3B
          4CC7CDCCCDA2CDD699359B3D31D732E79BE79BD79BDFB7605A785A2CB6A8B6B8
          6549B2E45AA659EEB6BC6E855A3959A558555A5DB346AD9DAD25D6BBADBBA711
          A7B94E934EAB9ED667C3B0F1B6C9B6A9B719B0E5D806DBAEB66DB67D61676217
          67B7C5AEC3EE93BD937DBA7D8DFD3D070D87D90EAB1D5A1D7E73B472143A563A
          DE9ACE9CEE3F7DC5F496E92F6758CF10CFD833E3B613CB29C4699D539BD34767
          1767B97383F3888B894B82CB2E973E2E9B1BC6DDC8BDE44A74F5715DE17AD2F5
          9D9BB39BC2EDA8DBAFEE36EE69EE87DC9FCC349F299E593373D0C3C843E051E5
          D13F0B9F95306BDFAC7E4F434F8167B5E7232F632F9157ADD7B0B7A577AAF761
          EF173EF63E729FE33EE33C37DE32DE595FCC37C0B7C8B7CB4FC36F9E5F85DF43
          7F23FF64FF7AFFD100A78025016703898141815B02FBF87A7C21BF8E3F3ADB65
          F6B2D9ED418CA0B94115418F82AD82E5C1AD2168C8EC90AD21F7E798CE91CE69
          0E85507EE8D6D00761E6618BC37E0C2785878557863F8E7088581AD131973577
          D1DC4373DF44FA449644DE9B67314F39AF2D4A352A3EAA2E6A3CDA37BA34BA3F
          C62E6659CCD5589D58496C4B1C392E2AAE366E6CBEDFFCEDF387E29DE20BE37B
          17982FC85D7079A1CEC2F485A716A92E122C3A96404C884E3894F041102AA816
          8C25F21377258E0A79C21DC267222FD136D188D8435C2A1E4EF2482A4D7A92EC
          91BC357924C533A52CE5B98427A990BC4C0D4CDD9B3A9E169A76206D323D3ABD
          31839291907142AA214D93B667EA67E66676CBAC6585B2FEC56E8BB72F1E9507
          C96BB390AC05592D0AB642A6E8545A28D72A07B267655766BFCD89CA3996AB9E
          2BCDEDCCB3CADB90379CEF9FFFED12C212E192B6A5864B572D1D58E6BDAC6A39
          B23C7179DB0AE315052B865606AC3CB88AB62A6DD54FABED5797AE7EBD267A4D
          6B815EC1CA82C1B5016BEB0B550AE5857DEBDCD7ED5D4F582F59DFB561FA869D
          1B3E15898AAE14DB1797157FD828DC78E51B876FCABF99DC94B4A9ABC4B964CF
          66D266E9E6DE2D9E5B0E96AA97E6970E6E0DD9DAB40DDF56B4EDF5F645DB2F97
          CD28DBBB83B643B9A3BF3CB8BC65A7C9CECD3B3F54A454F454FA5436EED2DDB5
          61D7F86ED1EE1B7BBCF634ECD5DB5BBCF7FD3EC9BEDB5501554DD566D565FB49
          FBB3F73FAE89AAE9F896FB6D5DAD4E6D71EDC703D203FD07230EB6D7B9D4D51D
          D23D54528FD62BEB470EC71FBEFE9DEF772D0D360D558D9CC6E223704479E4E9
          F709DFF71E0D3ADA768C7BACE107D31F761D671D2F6A429AF29A469B539AFB5B
          625BBA4FCC3ED1D6EADE7AFC47DB1F0F9C343C59794AF354C969DAE982D39367
          F2CF8C9D959D7D7E2EF9DC60DBA2B67BE763CEDF6A0F6FEFBA1074E1D245FF8B
          E73BBC3BCE5CF2B874F2B2DBE51357B8579AAF3A5F6DEA74EA3CFE93D34FC7BB
          9CBB9AAEB95C6BB9EE7ABDB57B66F7E91B9E37CEDDF4BD79F116FFD6D59E393D
          DDBDF37A6FF7C5F7F5DF16DD7E7227FDCECBBBD97727EEADBC4FBC5FF440ED41
          D943DD87D53F5BFEDCD8EFDC7F6AC077A0F3D1DC47F7068583CFFE91F58F0F43
          058F998FCB860D86EB9E383E3939E23F72FDE9FCA743CF64CF269E17FEA2FECB
          AE17162F7EF8D5EBD7CED198D1A197F29793BF6D7CA5FDEAC0EB19AFDBC6C2C6
          1EBEC97833315EF456FBEDC177DC771DEFA3DF0F4FE47C207F28FF68F9B1F553
          D0A7FB93199393FF040398F3FC63332DDB000004A04944415478DAED957B4CD5
          6518C7BFBFDBE1FC0E070E20E8814445A44692034A117543C0D0D65836FFE0D6
          DCCC48A6A44D5BCDD8529AB1E5865A0D6FE70F57CD23B175D9B2CD7082B88E89
          0B8A929949083B2917E5720EE7C239BFCBDB730E6AACDA22B7FE719DB377EFEF
          7D9FDFF37CDEE7F23E3F8E3186FFFAC7FD0FF9D790FCFCFCA905C7E1EFA03CCF
          87F68B495E42CB15340668BD9666CFF4F7EFE94FB7D3DADA3A63C8AB341D2431
          140DF04E320802FAA28DA85135667F20C8F499EC66690C9DBE2030EA6530491C
          16CFE7E1F100CE711DF191C8D619BEFFB3DE8C2177F71A7C416EABD50C2C4D13
          919B26202F53426F9F862D1FFAE10BE8EFCB12763C902792C0E01CE371CBC5DB
          F73DC797556F208A89FCF2904BBC04CCE6F179E304CA6D9A3D2541AF9817A323
          A8CD1C62A139F18E57FCB92273145E456B5CF7E4829224F310C64781CCBCF9F8
          AEA51FB299E176D08AF35D7D8D465128B3FF104BA1D31692BE9BF4EFFC05B29A
          207446D01936FB83D8EFF2B138A74B3CFCEEB323DBAA4B7CD55E57FA0737FB46
          A0F1407AB615573B06C1938DB90B6621D272B5BAA1C9D4B0BF2DFED338595D3F
          E1634E4B245E8F35A149D3A741F2561740D591E199C44F49160ECF6489D05560
          DF69F5EDAFB64B7B7272252F3C0653E8187053B8A20D21BF01B3E2EDB81C3017
          1D5476CE8D61F5D92922D63C21E2FC1515EDBDEAAA18198E0B6DD320BA8EDD55
          051175C54B254427D191030C056F7ABB9F4E6719BBABE42A0C471D099319D530
          27505E44CACB4455BDCD7FACA993EB3EB2D1F078F622DA4B1670CD11C40BC77D
          35B2C4D55D686B9982D4EF580B9EC39E578A0C7BC57832E0D6C1540E2D573CFD
          373C310B72E7B9B0D8F2CB29443C5A0A910A40A5FA0D5C6BEC763D56E6E8B720
          2D7ABC2F3F276A3EA8D641211A1BD3F1D137C1BDAACED5EE3A74E66E5BE95A47
          4ADC32B8C576E854393A85420A85C5536FFF71F96B073AE7E534A5D61C586819
          5B019D42C507D1E38ABB58F2EB3B3B7765F7B7972FB9540FB7792794908CECF1
          E4B139F81422F40E64354F418ED7956359F22432122737890A3B0A9119C0944F
          DEBBBEB2D43190503BA8CC7E6B83E56BECB09E08A725948E43032FE2337711AC
          D270EDCAC4DB7BB7A75D3CC5716229542EA848DCA6EEDF0CF6CB3765BC5C736A
          0A92BC662B2C466091D5C77F916B1F4282127FCCB1AAF0A8F7A59C257163751C
          C5C04F1E14479D438AECC40D7F324E4F14C2481E31F0E81A897D639BD9D659B9
          CA71164386DBCF5F2A4FBC3E286BAE0007E7D98629C8BA8A4AC113E40EBB14E3
          C62DA9DF1A534DC33832B81EE6C43418B9497A4987C2248C6A1688D0A042409C
          E082C4296054043EDD0875F82A365BBFC4F58939B0F52EF75BA4C98FCD06B6F5
          CC499B16861455549EA3C417500FC2484086A232A4A53E8284581981A072BF51
          D23D229F78FAEBF4F447238D8890303CEA474FCF4D48D4DB6645F8438504B2D7
          D27CD256780F12D6085D48BA2F212112E726C314150D4D53FFF17B2108227C1E
          37069CCEB07191C7FD2310849B8294571EA575240D8585CB8DB1D839497172A4
          59D6540A8E248E0B82304122715A5B57354D8BD21435461045F8BD1EFFD8D0AD
          D1B024D49FA83E69789BEDB6AA87ECF3FB50407E07D7207CED8840B0F4000000
          0049454E44AE426082}
      end
      item
        Background = clWindow
        Name = 'Plumbing-50'
        PngImage.Data = {
          89504E470D0A1A0A0000000D4948445200000019000000190806000000C4E985
          63000000097048597300000B1300000B1301009A9C1800000A4F694343505068
          6F746F73686F70204943432070726F66696C65000078DA9D53675453E9163DF7
          DEF4424B8880944B6F5215082052428B801491262A2109104A8821A1D91551C1
          114545041BC8A088038E8E808C15512C0C8A0AD807E421A28E83A3888ACAFBE1
          7BA36BD6BCF7E6CDFEB5D73EE7ACF39DB3CF07C0080C9648335135800CA9421E
          11E083C7C4C6E1E42E40810A2470001008B3642173FD230100F87E3C3C2B22C0
          07BE000178D30B0800C04D9BC0301C87FF0FEA42995C01808401C07491384B08
          801400407A8E42A600404601809D98265300A0040060CB6362E300502D006027
          7FE6D300809DF8997B01005B94211501A09100201365884400683B00ACCF568A
          450058300014664BC43900D82D00304957664800B0B700C0CE100BB200080C00
          305188852900047B0060C8232378008499001446F2573CF12BAE10E72A000078
          99B23CB9243945815B082D710757572E1E28CE49172B14366102619A402EC279
          99193281340FE0F3CC0000A0911511E083F3FD78CE0EAECECE368EB60E5F2DEA
          BF06FF226262E3FEE5CFAB70400000E1747ED1FE2C2FB31A803B06806DFEA225
          EE04685E0BA075F78B66B20F40B500A0E9DA57F370F87E3C3C45A190B9D9D9E5
          E4E4D84AC4425B61CA577DFE67C25FC057FD6CF97E3CFCF7F5E0BEE22481325D
          814704F8E0C2CCF44CA51CCF92098462DCE68F47FCB70BFFFC1DD322C44962B9
          582A14E35112718E449A8CF332A52289429229C525D2FF64E2DF2CFB033EDF35
          00B06A3E017B912DA85D6303F64B27105874C0E2F70000F2BB6FC1D428080380
          6883E1CF77FFEF3FFD47A02500806649927100005E44242E54CAB33FC7080000
          44A0812AB0411BF4C1182CC0061CC105DCC10BFC6036844224C4C24210420A64
          801C726029AC82422886CDB01D2A602FD4401D34C051688693700E2EC255B80E
          3D700FFA61089EC128BC81090441C808136121DA8801628A58238E08179985F8
          21C14804128B2420C9881451224B91354831528A542055481DF23D720239875C
          46BA913BC8003282FC86BC47319481B2513DD40CB543B9A8371A8446A20BD064
          74319A8F16A09BD072B41A3D8C36A1E7D0AB680FDA8F3E43C730C0E8180733C4
          6C302EC6C342B1382C099363CBB122AC0CABC61AB056AC03BB89F563CFB17704
          128145C0093604774220611E4148584C584ED848A8201C243411DA0937090384
          51C2272293A84BB426BA11F9C4186232318758482C23D6128F132F107B8843C4
          37241289433227B9900249B1A454D212D246D26E5223E92CA99B34481A2393C9
          DA646BB20739942C202BC885E49DE4C3E433E41BE421F25B0A9D624071A4F853
          E22852CA6A4A19E510E534E5066598324155A39A52DDA8A15411358F5A42ADA1
          B652AF5187A81334759A39CD8316494BA5ADA295D31A681768F769AFE874BA11
          DD951E4E97D057D2CBE947E897E803F4770C0D861583C7886728199B18071867
          197718AF984CA619D38B19C754303731EB98E7990F996F55582AB62A7C1591CA
          0A954A9526951B2A2F54A9AAA6AADEAA0B55F355CB548FA95E537DAE46553353
          E3A909D496AB55AA9D50EB531B5367A93BA887AA67A86F543FA47E59FD890659
          C34CC34F43A451A0B15FE3BCC6200B6319B3782C216B0DAB86758135C426B1CD
          D97C762ABB98FD1DBB8B3DAAA9A13943334A3357B352F394663F07E39871F89C
          744E09E728A797F37E8ADE14EF29E2291BA6344CB931655C6BAA96979658AB48
          AB51AB47EBBD36AEEDA79DA6BD45BB59FB810E41C74A275C2747678FCE059DE7
          53D953DDA70AA7164D3D3AF5AE2EAA6BA51BA1BB4477BF6EA7EE989EBE5E809E
          4C6FA7DE79BDE7FA1C7D2FFD54FD6DFAA7F5470C5806B30C2406DB0CCE183CC5
          35716F3C1D2FC7DBF151435DC34043A561956197E18491B9D13CA3D5468D460F
          8C69C65CE324E36DC66DC6A326062621264B4DEA4DEE9A524DB9A629A63B4C3B
          4CC7CDCCCDA2CDD699359B3D31D732E79BE79BD79BDFB7605A785A2CB6A8B6B8
          6549B2E45AA659EEB6BC6E855A3959A558555A5DB346AD9DAD25D6BBADBBA711
          A7B94E934EAB9ED667C3B0F1B6C9B6A9B719B0E5D806DBAEB66DB67D61676217
          67B7C5AEC3EE93BD937DBA7D8DFD3D070D87D90EAB1D5A1D7E73B472143A563A
          DE9ACE9CEE3F7DC5F496E92F6758CF10CFD833E3B613CB29C4699D539BD34767
          1767B97383F3888B894B82CB2E973E2E9B1BC6DDC8BDE44A74F5715DE17AD2F5
          9D9BB39BC2EDA8DBAFEE36EE69EE87DC9FCC349F299E593373D0C3C843E051E5
          D13F0B9F95306BDFAC7E4F434F8167B5E7232F632F9157ADD7B0B7A577AAF761
          EF173EF63E729FE33EE33C37DE32DE595FCC37C0B7C8B7CB4FC36F9E5F85DF43
          7F23FF64FF7AFFD100A78025016703898141815B02FBF87A7C21BF8E3F3ADB65
          F6B2D9ED418CA0B94115418F82AD82E5C1AD2168C8EC90AD21F7E798CE91CE69
          0E85507EE8D6D00761E6618BC37E0C2785878557863F8E7088581AD131973577
          D1DC4373DF44FA449644DE9B67314F39AF2D4A352A3EAA2E6A3CDA37BA34BA3F
          C62E6659CCD5589D58496C4B1C392E2AAE366E6CBEDFFCEDF387E29DE20BE37B
          17982FC85D7079A1CEC2F485A716A92E122C3A96404C884E3894F041102AA816
          8C25F21377258E0A79C21DC267222FD136D188D8435C2A1E4EF2482A4D7A92EC
          91BC357924C533A52CE5B98427A990BC4C0D4CDD9B3A9E169A76206D323D3ABD
          31839291907142AA214D93B667EA67E66676CBAC6585B2FEC56E8BB72F1E9507
          C96BB390AC05592D0AB642A6E8545A28D72A07B267655766BFCD89CA3996AB9E
          2BCDEDCCB3CADB90379CEF9FFFED12C212E192B6A5864B572D1D58E6BDAC6A39
          B23C7179DB0AE315052B865606AC3CB88AB62A6DD54FABED5797AE7EBD267A4D
          6B815EC1CA82C1B5016BEB0B550AE5857DEBDCD7ED5D4F582F59DFB561FA869D
          1B3E15898AAE14DB1797157FD828DC78E51B876FCABF99DC94B4A9ABC4B964CF
          66D266E9E6DE2D9E5B0E96AA97E6970E6E0DD9DAB40DDF56B4EDF5F645DB2F97
          CD28DBBB83B643B9A3BF3CB8BC65A7C9CECD3B3F54A454F454FA5436EED2DDB5
          61D7F86ED1EE1B7BBCF634ECD5DB5BBCF7FD3EC9BEDB5501554DD566D565FB49
          FBB3F73FAE89AAE9F896FB6D5DAD4E6D71EDC703D203FD07230EB6D7B9D4D51D
          D23D54528FD62BEB470EC71FBEFE9DEF772D0D360D558D9CC6E223704479E4E9
          F709DFF71E0D3ADA768C7BACE107D31F761D671D2F6A429AF29A469B539AFB5B
          625BBA4FCC3ED1D6EADE7AFC47DB1F0F9C343C59794AF354C969DAE982D39367
          F2CF8C9D959D7D7E2EF9DC60DBA2B67BE763CEDF6A0F6FEFBA1074E1D245FF8B
          E73BBC3BCE5CF2B874F2B2DBE51357B8579AAF3A5F6DEA74EA3CFE93D34FC7BB
          9CBB9AAEB95C6BB9EE7ABDB57B66F7E91B9E37CEDDF4BD79F116FFD6D59E393D
          DDBDF37A6FF7C5F7F5DF16DD7E7227FDCECBBBD97727EEADBC4FBC5FF440ED41
          D943DD87D53F5BFEDCD8EFDC7F6AC077A0F3D1DC47F7068583CFFE91F58F0F43
          058F998FCB860D86EB9E383E3939E23F72FDE9FCA743CF64CF269E17FEA2FECB
          AE17162F7EF8D5EBD7CED198D1A197F29793BF6D7CA5FDEAC0EB19AFDBC6C2C6
          1EBEC97833315EF456FBEDC177DC771DEFA3DF0F4FE47C207F28FF68F9B1F553
          D0A7FB93199393FF040398F3FC63332DDB000003ED4944415478DADD955D6C54
          4514C7FF3373F7EE37E917D86E57DBDD52BAA54483D2684040493406488CBCF8
          91F8595F342410688C318A0986F8A29144B131E21331C477AB89B14949281A25
          28A1515A97A52A696DDD52BAECEEBD7BEFCE1DCFBD5D4A8DDB5A8CBC30C9C9DC
          3973E6FC66CE99339729A570B31BBB35210D1F28D4FBA630D2D10868A4286B9B
          21B80EC586102C9BD604C7D6FEA387E1307662C78B7BF46609185A00CCD904C9
          68A006E79D3D605587B4F61531254378BEEE331C493C4110F11D94D60D8529F8
          593FB2E6EFF974F0008D11491A87D018684249EDA4A5AB484E13A47BDED99652
          7548B22F0F13014C581A06DB77606BED172761FB3781D3A44392B101933E187D
          FB4899F4D18969E0783E4E1264F33220858F39939D178A2BBE3ADCF2FAC13DC9
          430328609B173A77CD2F9803B00A34411271C3EA2D1F22B91FBECA7CB75AF424
          33D4D54C5AE16F76D57DBEF1FDC40B8D3E69DF012E0A904E687CB8E6C3742EBE
          810B85DBD464BABD73B2072B7D595884E2F2B2ADF4F4A7D33D28D821BCF2F81B
          8B427EA2AE93839DCACAF0B33191BDCC856D50E2050FFAF3D974F69D89F4F47E
          AE09AC88F0E3CDEB624F315D449574288ECA9474E4F3C64AA048A3D7B0348451
          661D70755586D9B5391E8465FF3A2E64E667A1188788B74A5F5B42A91279765D
          288C53EE3E8A70F3202FF991D9CB968654BBEB3C108439968179F102ADE2D09B
          E308B6AD86B2DD43A86BDE2857EC6B65451EBAB817FF1364753B94655D87781E
          E9469403BB33BBF523FF06A1C272235B696423C21198BF8D458DD1F344141E24
          B42675D5313CB3108958B0A7C1CC4B91079784906A80313C366FC4A92648618C
          8EECB7A7FFDC07A1B9E0E3C18E54AF5EDF00C7B21E25B3630B20A304E9580C52
          F076A5F023E9D733EE9980691AA469A078F62C4DA9281302E55C2E1F686955A1
          541764B1702FEDE1DB05901182A4162BC6B748D342EA1F94E2EF79061463F724
          8E6DA178EE9CD7BB10592C229068F3C4318D4708F2A5EB8ACD5DAA0C41DAAA42
          127D85B9C479E1D5E60CA89C19677F83787A5D4768ED3A70DD0F25E576ABACFA
          4D1B792AA148506797C65E8EDCBE0C88A840E43F20E66C014E3C097FAA13329F
          77CDEA574545D7C635DAD3E33372F84C467E3FDD1B3DF59F2157B239D4D506D1
          FDF09DF037D4A15430A10986B5310DB15A76A5545647872FC9DE4FB6F8714310
          A171CCCC18981E18C2DDF73561D79377A126AAC129D9F4E230B7DA51B4D4DB34
          7CD5BD88E1003AF6257DA33704A178235EEB20A6B2B867431CE1880FB3B3959F
          5225D354EC67A85B5F71F5CC812EDFB165432CBB8C80E6E0B9ED51C49A803FA6
          486729707EFD6D729F765676B6C128BF4B1369C5B59E3753C85585DCAC76EB40
          FE02B9E70FED62E525C50000000049454E44AE426082}
      end
      item
        Background = clWindow
        Name = 'Todo List-50'
        PngImage.Data = {
          89504E470D0A1A0A0000000D4948445200000019000000190806000000C4E985
          63000000097048597300000B1300000B1301009A9C1800000A4F694343505068
          6F746F73686F70204943432070726F66696C65000078DA9D53675453E9163DF7
          DEF4424B8880944B6F5215082052428B801491262A2109104A8821A1D91551C1
          114545041BC8A088038E8E808C15512C0C8A0AD807E421A28E83A3888ACAFBE1
          7BA36BD6BCF7E6CDFEB5D73EE7ACF39DB3CF07C0080C9648335135800CA9421E
          11E083C7C4C6E1E42E40810A2470001008B3642173FD230100F87E3C3C2B22C0
          07BE000178D30B0800C04D9BC0301C87FF0FEA42995C01808401C07491384B08
          801400407A8E42A600404601809D98265300A0040060CB6362E300502D006027
          7FE6D300809DF8997B01005B94211501A09100201365884400683B00ACCF568A
          450058300014664BC43900D82D00304957664800B0B700C0CE100BB200080C00
          305188852900047B0060C8232378008499001446F2573CF12BAE10E72A000078
          99B23CB9243945815B082D710757572E1E28CE49172B14366102619A402EC279
          99193281340FE0F3CC0000A0911511E083F3FD78CE0EAECECE368EB60E5F2DEA
          BF06FF226262E3FEE5CFAB70400000E1747ED1FE2C2FB31A803B06806DFEA225
          EE04685E0BA075F78B66B20F40B500A0E9DA57F370F87E3C3C45A190B9D9D9E5
          E4E4D84AC4425B61CA577DFE67C25FC057FD6CF97E3CFCF7F5E0BEE22481325D
          814704F8E0C2CCF44CA51CCF92098462DCE68F47FCB70BFFFC1DD322C44962B9
          582A14E35112718E449A8CF332A52289429229C525D2FF64E2DF2CFB033EDF35
          00B06A3E017B912DA85D6303F64B27105874C0E2F70000F2BB6FC1D428080380
          6883E1CF77FFEF3FFD47A02500806649927100005E44242E54CAB33FC7080000
          44A0812AB0411BF4C1182CC0061CC105DCC10BFC6036844224C4C24210420A64
          801C726029AC82422886CDB01D2A602FD4401D34C051688693700E2EC255B80E
          3D700FFA61089EC128BC81090441C808136121DA8801628A58238E08179985F8
          21C14804128B2420C9881451224B91354831528A542055481DF23D720239875C
          46BA913BC8003282FC86BC47319481B2513DD40CB543B9A8371A8446A20BD064
          74319A8F16A09BD072B41A3D8C36A1E7D0AB680FDA8F3E43C730C0E8180733C4
          6C302EC6C342B1382C099363CBB122AC0CABC61AB056AC03BB89F563CFB17704
          128145C0093604774220611E4148584C584ED848A8201C243411DA0937090384
          51C2272293A84BB426BA11F9C4186232318758482C23D6128F132F107B8843C4
          37241289433227B9900249B1A454D212D246D26E5223E92CA99B34481A2393C9
          DA646BB20739942C202BC885E49DE4C3E433E41BE421F25B0A9D624071A4F853
          E22852CA6A4A19E510E534E5066598324155A39A52DDA8A15411358F5A42ADA1
          B652AF5187A81334759A39CD8316494BA5ADA295D31A681768F769AFE874BA11
          DD951E4E97D057D2CBE947E897E803F4770C0D861583C7886728199B18071867
          197718AF984CA619D38B19C754303731EB98E7990F996F55582AB62A7C1591CA
          0A954A9526951B2A2F54A9AAA6AADEAA0B55F355CB548FA95E537DAE46553353
          E3A909D496AB55AA9D50EB531B5367A93BA887AA67A86F543FA47E59FD890659
          C34CC34F43A451A0B15FE3BCC6200B6319B3782C216B0DAB86758135C426B1CD
          D97C762ABB98FD1DBB8B3DAAA9A13943334A3357B352F394663F07E39871F89C
          744E09E728A797F37E8ADE14EF29E2291BA6344CB931655C6BAA96979658AB48
          AB51AB47EBBD36AEEDA79DA6BD45BB59FB810E41C74A275C2747678FCE059DE7
          53D953DDA70AA7164D3D3AF5AE2EAA6BA51BA1BB4477BF6EA7EE989EBE5E809E
          4C6FA7DE79BDE7FA1C7D2FFD54FD6DFAA7F5470C5806B30C2406DB0CCE183CC5
          35716F3C1D2FC7DBF151435DC34043A561956197E18491B9D13CA3D5468D460F
          8C69C65CE324E36DC66DC6A326062621264B4DEA4DEE9A524DB9A629A63B4C3B
          4CC7CDCCCDA2CDD699359B3D31D732E79BE79BD79BDFB7605A785A2CB6A8B6B8
          6549B2E45AA659EEB6BC6E855A3959A558555A5DB346AD9DAD25D6BBADBBA711
          A7B94E934EAB9ED667C3B0F1B6C9B6A9B719B0E5D806DBAEB66DB67D61676217
          67B7C5AEC3EE93BD937DBA7D8DFD3D070D87D90EAB1D5A1D7E73B472143A563A
          DE9ACE9CEE3F7DC5F496E92F6758CF10CFD833E3B613CB29C4699D539BD34767
          1767B97383F3888B894B82CB2E973E2E9B1BC6DDC8BDE44A74F5715DE17AD2F5
          9D9BB39BC2EDA8DBAFEE36EE69EE87DC9FCC349F299E593373D0C3C843E051E5
          D13F0B9F95306BDFAC7E4F434F8167B5E7232F632F9157ADD7B0B7A577AAF761
          EF173EF63E729FE33EE33C37DE32DE595FCC37C0B7C8B7CB4FC36F9E5F85DF43
          7F23FF64FF7AFFD100A78025016703898141815B02FBF87A7C21BF8E3F3ADB65
          F6B2D9ED418CA0B94115418F82AD82E5C1AD2168C8EC90AD21F7E798CE91CE69
          0E85507EE8D6D00761E6618BC37E0C2785878557863F8E7088581AD131973577
          D1DC4373DF44FA449644DE9B67314F39AF2D4A352A3EAA2E6A3CDA37BA34BA3F
          C62E6659CCD5589D58496C4B1C392E2AAE366E6CBEDFFCEDF387E29DE20BE37B
          17982FC85D7079A1CEC2F485A716A92E122C3A96404C884E3894F041102AA816
          8C25F21377258E0A79C21DC267222FD136D188D8435C2A1E4EF2482A4D7A92EC
          91BC357924C533A52CE5B98427A990BC4C0D4CDD9B3A9E169A76206D323D3ABD
          31839291907142AA214D93B667EA67E66676CBAC6585B2FEC56E8BB72F1E9507
          C96BB390AC05592D0AB642A6E8545A28D72A07B267655766BFCD89CA3996AB9E
          2BCDEDCCB3CADB90379CEF9FFFED12C212E192B6A5864B572D1D58E6BDAC6A39
          B23C7179DB0AE315052B865606AC3CB88AB62A6DD54FABED5797AE7EBD267A4D
          6B815EC1CA82C1B5016BEB0B550AE5857DEBDCD7ED5D4F582F59DFB561FA869D
          1B3E15898AAE14DB1797157FD828DC78E51B876FCABF99DC94B4A9ABC4B964CF
          66D266E9E6DE2D9E5B0E96AA97E6970E6E0DD9DAB40DDF56B4EDF5F645DB2F97
          CD28DBBB83B643B9A3BF3CB8BC65A7C9CECD3B3F54A454F454FA5436EED2DDB5
          61D7F86ED1EE1B7BBCF634ECD5DB5BBCF7FD3EC9BEDB5501554DD566D565FB49
          FBB3F73FAE89AAE9F896FB6D5DAD4E6D71EDC703D203FD07230EB6D7B9D4D51D
          D23D54528FD62BEB470EC71FBEFE9DEF772D0D360D558D9CC6E223704479E4E9
          F709DFF71E0D3ADA768C7BACE107D31F761D671D2F6A429AF29A469B539AFB5B
          625BBA4FCC3ED1D6EADE7AFC47DB1F0F9C343C59794AF354C969DAE982D39367
          F2CF8C9D959D7D7E2EF9DC60DBA2B67BE763CEDF6A0F6FEFBA1074E1D245FF8B
          E73BBC3BCE5CF2B874F2B2DBE51357B8579AAF3A5F6DEA74EA3CFE93D34FC7BB
          9CBB9AAEB95C6BB9EE7ABDB57B66F7E91B9E37CEDDF4BD79F116FFD6D59E393D
          DDBDF37A6FF7C5F7F5DF16DD7E7227FDCECBBBD97727EEADBC4FBC5FF440ED41
          D943DD87D53F5BFEDCD8EFDC7F6AC077A0F3D1DC47F7068583CFFE91F58F0F43
          058F998FCB860D86EB9E383E3939E23F72FDE9FCA743CF64CF269E17FEA2FECB
          AE17162F7EF8D5EBD7CED198D1A197F29793BF6D7CA5FDEAC0EB19AFDBC6C2C6
          1EBEC97833315EF456FBEDC177DC771DEFA3DF0F4FE47C207F28FF68F9B1F553
          D0A7FB93199393FF040398F3FC63332DDB000003AA4944415478DABD956D6853
          5718C7FFE726B96689AB1F9C525FD639DAAEDB7063D6A6B5839AB49DD3BAE1B8
          290E36A712C1E1A490F8C2A6329808FBB475EC8B0CC610C64661433798120ACA
          5E1883ED93207E1019FDE6EAEC48933637B9B96FC7FFBD5A21B52651A6379C7B
          EF799E93F33BCFF3FCCFB9424A89877D89470A896B59AF0B17128A6FB9035F41
          7B8F6DDA3F05428AFBFA8101343D118659B66B4E2C3957BA2BD41844087CC3C7
          3B866EB7469684275ECBC411695261190F06E963B78390AFEE40043E17906921
          42170A93F65B31ADE3BFEEE1B6563D67BEC001A51A8C10217942FEA88624B33F
          73F27E1718A1EDA422C428E01E1408FE6095D5E1C89245184AAF47B849FDC22C
          D9FB1A2885938EA9C12AC8462DFB8C2230CED7A769F99389DBE00120A3C3F9EB
          B388EF5E8B58B20537262A29A188F738AE5C2B12A6F96F42765541FAB473CC8E
          B294A04B8C602510FC51C86812B0512AE4B1E6A5660CEEEB85557160EA3608AA
          2329814C6C5E4D12DA7916DD62259CB68008ED5564F40309AFB8BA3F617EB288
          B69EA7104F75C1A5D9B1DDFB870C68BF1262FB8D6942804DA2E80D59C6D6EB54
          9CB38EE3C86D4736A169790466C9AA5B14A6AB1AD2AFFDC2BBB73A95931B0499
          9E5991425CE1A2DA4B85CA73E1C5EA15EDE820D4487D092F0861BAD2024E0FB0
          F86D10C2BA0620027FD1BBDE71E5877AC1F86C30D5536EDFB02A512E588906D4
          3545C8C96A4872FC7BC0DC2E10FE1A521C81A89CE1B67C99DEC3C5E9F2687BAC
          059BF7C750CCD9A75DDB1D86A84F2144CC4BD76FEC95BF93B0DE04029E3C1FF3
          00748F4EFF53C0D0481F3AB7AEC6E45523A10495078BA45FFB1DB77679294B55
          0D318AF7E9FAC4F31933069E5CDB8C2DFB7B615B1215DDAA2FE17BD4841950BD
          2CA80EF45729817373C78B50144A78C697F0C01E4AD8A144EA4878C1B38B3BDE
          97AEE0CFDB1F924ABBBDD63E21C41BB6E99E30746326796C0B96AE8EFAD1DC37
          24AE8DFB0ED684A77000B7E48C0E022E3335C1D91BC5CE55CF375FDC3CD20BD9
          C0665C1892CC7EC947972BF10A513946D14EC045DAA270E54E3D57FA762893C0
          9A75CB76CF4E55DE25D8A8C108123241486A3EE463D6E0187B975D298FF2141E
          A3F971C27694A6CB63ADDD2D18D81BE339669F92AE9B6A405D774BD8AB09233C
          44D0A773833C00E31ECBFF3B8B8D3BBBD0B9AD15B96B4627234CD05DA8317F98
          FFBE9E89A967EE86C03FD73EE2E4C7D97611E07D153133554477F2451EF5CFF2
          BDD24810FE1233F3253C07616F11CBBE83119D9AFBCAFF6F9087793D12C84D07
          E1C8DE8B9BE9420000000049454E44AE426082}
      end
      item
        Background = clWindow
        Name = 'Piping-50'
        PngImage.Data = {
          89504E470D0A1A0A0000000D4948445200000019000000190806000000C4E985
          63000000097048597300000B1300000B1301009A9C1800000A4F694343505068
          6F746F73686F70204943432070726F66696C65000078DA9D53675453E9163DF7
          DEF4424B8880944B6F5215082052428B801491262A2109104A8821A1D91551C1
          114545041BC8A088038E8E808C15512C0C8A0AD807E421A28E83A3888ACAFBE1
          7BA36BD6BCF7E6CDFEB5D73EE7ACF39DB3CF07C0080C9648335135800CA9421E
          11E083C7C4C6E1E42E40810A2470001008B3642173FD230100F87E3C3C2B22C0
          07BE000178D30B0800C04D9BC0301C87FF0FEA42995C01808401C07491384B08
          801400407A8E42A600404601809D98265300A0040060CB6362E300502D006027
          7FE6D300809DF8997B01005B94211501A09100201365884400683B00ACCF568A
          450058300014664BC43900D82D00304957664800B0B700C0CE100BB200080C00
          305188852900047B0060C8232378008499001446F2573CF12BAE10E72A000078
          99B23CB9243945815B082D710757572E1E28CE49172B14366102619A402EC279
          99193281340FE0F3CC0000A0911511E083F3FD78CE0EAECECE368EB60E5F2DEA
          BF06FF226262E3FEE5CFAB70400000E1747ED1FE2C2FB31A803B06806DFEA225
          EE04685E0BA075F78B66B20F40B500A0E9DA57F370F87E3C3C45A190B9D9D9E5
          E4E4D84AC4425B61CA577DFE67C25FC057FD6CF97E3CFCF7F5E0BEE22481325D
          814704F8E0C2CCF44CA51CCF92098462DCE68F47FCB70BFFFC1DD322C44962B9
          582A14E35112718E449A8CF332A52289429229C525D2FF64E2DF2CFB033EDF35
          00B06A3E017B912DA85D6303F64B27105874C0E2F70000F2BB6FC1D428080380
          6883E1CF77FFEF3FFD47A02500806649927100005E44242E54CAB33FC7080000
          44A0812AB0411BF4C1182CC0061CC105DCC10BFC6036844224C4C24210420A64
          801C726029AC82422886CDB01D2A602FD4401D34C051688693700E2EC255B80E
          3D700FFA61089EC128BC81090441C808136121DA8801628A58238E08179985F8
          21C14804128B2420C9881451224B91354831528A542055481DF23D720239875C
          46BA913BC8003282FC86BC47319481B2513DD40CB543B9A8371A8446A20BD064
          74319A8F16A09BD072B41A3D8C36A1E7D0AB680FDA8F3E43C730C0E8180733C4
          6C302EC6C342B1382C099363CBB122AC0CABC61AB056AC03BB89F563CFB17704
          128145C0093604774220611E4148584C584ED848A8201C243411DA0937090384
          51C2272293A84BB426BA11F9C4186232318758482C23D6128F132F107B8843C4
          37241289433227B9900249B1A454D212D246D26E5223E92CA99B34481A2393C9
          DA646BB20739942C202BC885E49DE4C3E433E41BE421F25B0A9D624071A4F853
          E22852CA6A4A19E510E534E5066598324155A39A52DDA8A15411358F5A42ADA1
          B652AF5187A81334759A39CD8316494BA5ADA295D31A681768F769AFE874BA11
          DD951E4E97D057D2CBE947E897E803F4770C0D861583C7886728199B18071867
          197718AF984CA619D38B19C754303731EB98E7990F996F55582AB62A7C1591CA
          0A954A9526951B2A2F54A9AAA6AADEAA0B55F355CB548FA95E537DAE46553353
          E3A909D496AB55AA9D50EB531B5367A93BA887AA67A86F543FA47E59FD890659
          C34CC34F43A451A0B15FE3BCC6200B6319B3782C216B0DAB86758135C426B1CD
          D97C762ABB98FD1DBB8B3DAAA9A13943334A3357B352F394663F07E39871F89C
          744E09E728A797F37E8ADE14EF29E2291BA6344CB931655C6BAA96979658AB48
          AB51AB47EBBD36AEEDA79DA6BD45BB59FB810E41C74A275C2747678FCE059DE7
          53D953DDA70AA7164D3D3AF5AE2EAA6BA51BA1BB4477BF6EA7EE989EBE5E809E
          4C6FA7DE79BDE7FA1C7D2FFD54FD6DFAA7F5470C5806B30C2406DB0CCE183CC5
          35716F3C1D2FC7DBF151435DC34043A561956197E18491B9D13CA3D5468D460F
          8C69C65CE324E36DC66DC6A326062621264B4DEA4DEE9A524DB9A629A63B4C3B
          4CC7CDCCCDA2CDD699359B3D31D732E79BE79BD79BDFB7605A785A2CB6A8B6B8
          6549B2E45AA659EEB6BC6E855A3959A558555A5DB346AD9DAD25D6BBADBBA711
          A7B94E934EAB9ED667C3B0F1B6C9B6A9B719B0E5D806DBAEB66DB67D61676217
          67B7C5AEC3EE93BD937DBA7D8DFD3D070D87D90EAB1D5A1D7E73B472143A563A
          DE9ACE9CEE3F7DC5F496E92F6758CF10CFD833E3B613CB29C4699D539BD34767
          1767B97383F3888B894B82CB2E973E2E9B1BC6DDC8BDE44A74F5715DE17AD2F5
          9D9BB39BC2EDA8DBAFEE36EE69EE87DC9FCC349F299E593373D0C3C843E051E5
          D13F0B9F95306BDFAC7E4F434F8167B5E7232F632F9157ADD7B0B7A577AAF761
          EF173EF63E729FE33EE33C37DE32DE595FCC37C0B7C8B7CB4FC36F9E5F85DF43
          7F23FF64FF7AFFD100A78025016703898141815B02FBF87A7C21BF8E3F3ADB65
          F6B2D9ED418CA0B94115418F82AD82E5C1AD2168C8EC90AD21F7E798CE91CE69
          0E85507EE8D6D00761E6618BC37E0C2785878557863F8E7088581AD131973577
          D1DC4373DF44FA449644DE9B67314F39AF2D4A352A3EAA2E6A3CDA37BA34BA3F
          C62E6659CCD5589D58496C4B1C392E2AAE366E6CBEDFFCEDF387E29DE20BE37B
          17982FC85D7079A1CEC2F485A716A92E122C3A96404C884E3894F041102AA816
          8C25F21377258E0A79C21DC267222FD136D188D8435C2A1E4EF2482A4D7A92EC
          91BC357924C533A52CE5B98427A990BC4C0D4CDD9B3A9E169A76206D323D3ABD
          31839291907142AA214D93B667EA67E66676CBAC6585B2FEC56E8BB72F1E9507
          C96BB390AC05592D0AB642A6E8545A28D72A07B267655766BFCD89CA3996AB9E
          2BCDEDCCB3CADB90379CEF9FFFED12C212E192B6A5864B572D1D58E6BDAC6A39
          B23C7179DB0AE315052B865606AC3CB88AB62A6DD54FABED5797AE7EBD267A4D
          6B815EC1CA82C1B5016BEB0B550AE5857DEBDCD7ED5D4F582F59DFB561FA869D
          1B3E15898AAE14DB1797157FD828DC78E51B876FCABF99DC94B4A9ABC4B964CF
          66D266E9E6DE2D9E5B0E96AA97E6970E6E0DD9DAB40DDF56B4EDF5F645DB2F97
          CD28DBBB83B643B9A3BF3CB8BC65A7C9CECD3B3F54A454F454FA5436EED2DDB5
          61D7F86ED1EE1B7BBCF634ECD5DB5BBCF7FD3EC9BEDB5501554DD566D565FB49
          FBB3F73FAE89AAE9F896FB6D5DAD4E6D71EDC703D203FD07230EB6D7B9D4D51D
          D23D54528FD62BEB470EC71FBEFE9DEF772D0D360D558D9CC6E223704479E4E9
          F709DFF71E0D3ADA768C7BACE107D31F761D671D2F6A429AF29A469B539AFB5B
          625BBA4FCC3ED1D6EADE7AFC47DB1F0F9C343C59794AF354C969DAE982D39367
          F2CF8C9D959D7D7E2EF9DC60DBA2B67BE763CEDF6A0F6FEFBA1074E1D245FF8B
          E73BBC3BCE5CF2B874F2B2DBE51357B8579AAF3A5F6DEA74EA3CFE93D34FC7BB
          9CBB9AAEB95C6BB9EE7ABDB57B66F7E91B9E37CEDDF4BD79F116FFD6D59E393D
          DDBDF37A6FF7C5F7F5DF16DD7E7227FDCECBBBD97727EEADBC4FBC5FF440ED41
          D943DD87D53F5BFEDCD8EFDC7F6AC077A0F3D1DC47F7068583CFFE91F58F0F43
          058F998FCB860D86EB9E383E3939E23F72FDE9FCA743CF64CF269E17FEA2FECB
          AE17162F7EF8D5EBD7CED198D1A197F29793BF6D7CA5FDEAC0EB19AFDBC6C2C6
          1EBEC97833315EF456FBEDC177DC771DEFA3DF0F4FE47C207F28FF68F9B1F553
          D0A7FB93199393FF040398F3FC63332DDB0000030B4944415478DADD954B6813
          4118C7FFB3C9C6F47950A18D28420F4AD347D2422982568ACF0A8A15AB27B162
          45532F1EC49B374F7AF2E003ADD4E2C91788C5FA06BD0B7AF0DC1C6C8D360F63
          B3316993ECF89FDD3EB6B54A52E8C585D96F76BF6FBEDFCC7CDF3723A49458E9
          47FC7F90ED87462E0BE02CBB636C0536A5A861FBC8CE2E652385803B9BE5A002
          C6DB3B91F6AD81DB4C0126951A5BDE0BE4747AB59D87CFFD097948DDE1252632
          468B3ACA9C28981C2F11D91244AA66033C93396055B6784847F7C83A4EF422F5
          671C80CFA6C431CA4F6A61BA9141ACC58F89363FBC91BC6DE1C994040121BDD4
          0F3A20C3841CB02D35420C421A106FDE043D2E4B8770BB943849FD8003F28AFA
          3D021E0BA2190924020D8805084960D9903EEA6FCF21249E9B30F7B950611BA7
          633624A856B27C4888FAEB8E95BC659AED644EA9801FD18DD4F178C01F890536
          FBF49FB9218E7C008DB1513E4A80F4083570FE494988FB12E6243D1DF41AD9BA
          68A01EB1663FF4B4314AFD0B488D5ECD6F52E02972DE0FC8FF03B2AD7B58895A
          97D0C2FCED5DAAA87423CD78F86D08FB8B2A8ED5E5BE29A7CB4256DF8288C5D9
          F5CCFE21C4156ECDF9B9E9140B9909222BF62ADFD61AC2FD950B219DDDEF1CB3
          9A1A94C8F72A90981F5E04C4B2CC494855BC63E150F562C8FB19238DC93245A7
          99AD2EE1EAA2766D0185BC06B1773626512B26A951CEE00963524DE3A31C5835
          BF1EA94E8EC7E150D5DF56320BC9821014249398D9E51282D9956476354662C1
          7A9F9E9C1E62B0995D390853BC21708763D34E53DC0A872A8B8514F8A7DC5619
          13F8116C42BCA51E7A4CDA7BE8F945271A8F0B743920A728064A82089459B111
          46148959485C55209B7B5A45EE253F763B207D14779605D15213ACF846C45A9B
          A02779FA0AD37229548D00FB1D9013CCB2BBE1FE8AD221EEF838A26DAD88079A
          ACC3D24A73812053FE1E3F1A1D901B545C0A872ABE160F11E5BC3B52486E5C8D
          687B9B3DFB8269950E31AAF2D72F51318FB85D3D4543DC59818CAF165F3A9AA1
          99F6EDA86E49D86B794DD1C2F6DDFE844B41E9F51A2117164056F2F97F20BF01
          8962CBDEE641F02E0000000049454E44AE426082}
      end
      item
        Background = clWindow
        Name = 'Ticket-50'
        PngImage.Data = {
          89504E470D0A1A0A0000000D4948445200000019000000190806000000C4E985
          63000000097048597300000B1300000B1301009A9C1800000A4F694343505068
          6F746F73686F70204943432070726F66696C65000078DA9D53675453E9163DF7
          DEF4424B8880944B6F5215082052428B801491262A2109104A8821A1D91551C1
          114545041BC8A088038E8E808C15512C0C8A0AD807E421A28E83A3888ACAFBE1
          7BA36BD6BCF7E6CDFEB5D73EE7ACF39DB3CF07C0080C9648335135800CA9421E
          11E083C7C4C6E1E42E40810A2470001008B3642173FD230100F87E3C3C2B22C0
          07BE000178D30B0800C04D9BC0301C87FF0FEA42995C01808401C07491384B08
          801400407A8E42A600404601809D98265300A0040060CB6362E300502D006027
          7FE6D300809DF8997B01005B94211501A09100201365884400683B00ACCF568A
          450058300014664BC43900D82D00304957664800B0B700C0CE100BB200080C00
          305188852900047B0060C8232378008499001446F2573CF12BAE10E72A000078
          99B23CB9243945815B082D710757572E1E28CE49172B14366102619A402EC279
          99193281340FE0F3CC0000A0911511E083F3FD78CE0EAECECE368EB60E5F2DEA
          BF06FF226262E3FEE5CFAB70400000E1747ED1FE2C2FB31A803B06806DFEA225
          EE04685E0BA075F78B66B20F40B500A0E9DA57F370F87E3C3C45A190B9D9D9E5
          E4E4D84AC4425B61CA577DFE67C25FC057FD6CF97E3CFCF7F5E0BEE22481325D
          814704F8E0C2CCF44CA51CCF92098462DCE68F47FCB70BFFFC1DD322C44962B9
          582A14E35112718E449A8CF332A52289429229C525D2FF64E2DF2CFB033EDF35
          00B06A3E017B912DA85D6303F64B27105874C0E2F70000F2BB6FC1D428080380
          6883E1CF77FFEF3FFD47A02500806649927100005E44242E54CAB33FC7080000
          44A0812AB0411BF4C1182CC0061CC105DCC10BFC6036844224C4C24210420A64
          801C726029AC82422886CDB01D2A602FD4401D34C051688693700E2EC255B80E
          3D700FFA61089EC128BC81090441C808136121DA8801628A58238E08179985F8
          21C14804128B2420C9881451224B91354831528A542055481DF23D720239875C
          46BA913BC8003282FC86BC47319481B2513DD40CB543B9A8371A8446A20BD064
          74319A8F16A09BD072B41A3D8C36A1E7D0AB680FDA8F3E43C730C0E8180733C4
          6C302EC6C342B1382C099363CBB122AC0CABC61AB056AC03BB89F563CFB17704
          128145C0093604774220611E4148584C584ED848A8201C243411DA0937090384
          51C2272293A84BB426BA11F9C4186232318758482C23D6128F132F107B8843C4
          37241289433227B9900249B1A454D212D246D26E5223E92CA99B34481A2393C9
          DA646BB20739942C202BC885E49DE4C3E433E41BE421F25B0A9D624071A4F853
          E22852CA6A4A19E510E534E5066598324155A39A52DDA8A15411358F5A42ADA1
          B652AF5187A81334759A39CD8316494BA5ADA295D31A681768F769AFE874BA11
          DD951E4E97D057D2CBE947E897E803F4770C0D861583C7886728199B18071867
          197718AF984CA619D38B19C754303731EB98E7990F996F55582AB62A7C1591CA
          0A954A9526951B2A2F54A9AAA6AADEAA0B55F355CB548FA95E537DAE46553353
          E3A909D496AB55AA9D50EB531B5367A93BA887AA67A86F543FA47E59FD890659
          C34CC34F43A451A0B15FE3BCC6200B6319B3782C216B0DAB86758135C426B1CD
          D97C762ABB98FD1DBB8B3DAAA9A13943334A3357B352F394663F07E39871F89C
          744E09E728A797F37E8ADE14EF29E2291BA6344CB931655C6BAA96979658AB48
          AB51AB47EBBD36AEEDA79DA6BD45BB59FB810E41C74A275C2747678FCE059DE7
          53D953DDA70AA7164D3D3AF5AE2EAA6BA51BA1BB4477BF6EA7EE989EBE5E809E
          4C6FA7DE79BDE7FA1C7D2FFD54FD6DFAA7F5470C5806B30C2406DB0CCE183CC5
          35716F3C1D2FC7DBF151435DC34043A561956197E18491B9D13CA3D5468D460F
          8C69C65CE324E36DC66DC6A326062621264B4DEA4DEE9A524DB9A629A63B4C3B
          4CC7CDCCCDA2CDD699359B3D31D732E79BE79BD79BDFB7605A785A2CB6A8B6B8
          6549B2E45AA659EEB6BC6E855A3959A558555A5DB346AD9DAD25D6BBADBBA711
          A7B94E934EAB9ED667C3B0F1B6C9B6A9B719B0E5D806DBAEB66DB67D61676217
          67B7C5AEC3EE93BD937DBA7D8DFD3D070D87D90EAB1D5A1D7E73B472143A563A
          DE9ACE9CEE3F7DC5F496E92F6758CF10CFD833E3B613CB29C4699D539BD34767
          1767B97383F3888B894B82CB2E973E2E9B1BC6DDC8BDE44A74F5715DE17AD2F5
          9D9BB39BC2EDA8DBAFEE36EE69EE87DC9FCC349F299E593373D0C3C843E051E5
          D13F0B9F95306BDFAC7E4F434F8167B5E7232F632F9157ADD7B0B7A577AAF761
          EF173EF63E729FE33EE33C37DE32DE595FCC37C0B7C8B7CB4FC36F9E5F85DF43
          7F23FF64FF7AFFD100A78025016703898141815B02FBF87A7C21BF8E3F3ADB65
          F6B2D9ED418CA0B94115418F82AD82E5C1AD2168C8EC90AD21F7E798CE91CE69
          0E85507EE8D6D00761E6618BC37E0C2785878557863F8E7088581AD131973577
          D1DC4373DF44FA449644DE9B67314F39AF2D4A352A3EAA2E6A3CDA37BA34BA3F
          C62E6659CCD5589D58496C4B1C392E2AAE366E6CBEDFFCEDF387E29DE20BE37B
          17982FC85D7079A1CEC2F485A716A92E122C3A96404C884E3894F041102AA816
          8C25F21377258E0A79C21DC267222FD136D188D8435C2A1E4EF2482A4D7A92EC
          91BC357924C533A52CE5B98427A990BC4C0D4CDD9B3A9E169A76206D323D3ABD
          31839291907142AA214D93B667EA67E66676CBAC6585B2FEC56E8BB72F1E9507
          C96BB390AC05592D0AB642A6E8545A28D72A07B267655766BFCD89CA3996AB9E
          2BCDEDCCB3CADB90379CEF9FFFED12C212E192B6A5864B572D1D58E6BDAC6A39
          B23C7179DB0AE315052B865606AC3CB88AB62A6DD54FABED5797AE7EBD267A4D
          6B815EC1CA82C1B5016BEB0B550AE5857DEBDCD7ED5D4F582F59DFB561FA869D
          1B3E15898AAE14DB1797157FD828DC78E51B876FCABF99DC94B4A9ABC4B964CF
          66D266E9E6DE2D9E5B0E96AA97E6970E6E0DD9DAB40DDF56B4EDF5F645DB2F97
          CD28DBBB83B643B9A3BF3CB8BC65A7C9CECD3B3F54A454F454FA5436EED2DDB5
          61D7F86ED1EE1B7BBCF634ECD5DB5BBCF7FD3EC9BEDB5501554DD566D565FB49
          FBB3F73FAE89AAE9F896FB6D5DAD4E6D71EDC703D203FD07230EB6D7B9D4D51D
          D23D54528FD62BEB470EC71FBEFE9DEF772D0D360D558D9CC6E223704479E4E9
          F709DFF71E0D3ADA768C7BACE107D31F761D671D2F6A429AF29A469B539AFB5B
          625BBA4FCC3ED1D6EADE7AFC47DB1F0F9C343C59794AF354C969DAE982D39367
          F2CF8C9D959D7D7E2EF9DC60DBA2B67BE763CEDF6A0F6FEFBA1074E1D245FF8B
          E73BBC3BCE5CF2B874F2B2DBE51357B8579AAF3A5F6DEA74EA3CFE93D34FC7BB
          9CBB9AAEB95C6BB9EE7ABDB57B66F7E91B9E37CEDDF4BD79F116FFD6D59E393D
          DDBDF37A6FF7C5F7F5DF16DD7E7227FDCECBBBD97727EEADBC4FBC5FF440ED41
          D943DD87D53F5BFEDCD8EFDC7F6AC077A0F3D1DC47F7068583CFFE91F58F0F43
          058F998FCB860D86EB9E383E3939E23F72FDE9FCA743CF64CF269E17FEA2FECB
          AE17162F7EF8D5EBD7CED198D1A197F29793BF6D7CA5FDEAC0EB19AFDBC6C2C6
          1EBEC97833315EF456FBEDC177DC771DEFA3DF0F4FE47C207F28FF68F9B1F553
          D0A7FB93199393FF040398F3FC63332DDB000001ED4944415478DAED953B4B03
          411485CFEC43A3365A8A1004353E0A5BD11F616391D88A8288606DA141EC1541
          050B1B0B5F58E89F3028289AC267041F58D8682A9398CC8E674914770C985DD0
          CA81197267F7DE2F67EEBDB3422985DF1EE21F120CB2D96AD21C01CC6640652A
          F1E50CC1707660CB53E4AC492861437042DE32C60A6229A5412263349702FCCD
          47E2EEE0883EEF03358AD8D58A0669BFE31A0E701A4750EA960A06B4FD07C42E
          C33AE49A6BAB6F84C21044610DB072B4CC2F4F5284B4E99033AE9D0120C5F488
          6F05744E489706E9C8D2A3DA1F80C14D19E771EDC2318FC9FAA244E410BB0869
          90C8389D66F962BD4FC836D524E01873F415257569469E62E217BD902DA6C374
          BA51B00FE09666E523CDE9967C63C9CEC2CAF7401A494453D0202D404D1E780D
          3137A2B2DCB8AE961CA6A2550655AC306E38E7A8CD7621C37689DE68900D422C
          D50DE95BC9734949D3A712934A0A2289C16F90C804CD384FB5C107C08DB04E45
          87FC310F7CE6E485CB0C06AF16BC90CD8E373EB07D018A11A6A19C3D26F4189E
          3E117956579506F9933E09D0F1AEABA18621E41AA495F1F649F98EF77F771521
          278C72CFEAEA2F65E46394BBBB02DEC2504F0C7EC332AEE416FEF89E18BD28EE
          C91FA2BB89A88370965055D8C79BBD4C65B49970A804FBA5CCF7E417C73FC4D7
          7807F3C100DEAE30DC560000000049454E44AE426082}
      end
      item
        Background = clWindow
        Name = 'Diploma-50'
        PngImage.Data = {
          89504E470D0A1A0A0000000D4948445200000019000000190806000000C4E985
          63000000097048597300000B1300000B1301009A9C1800000A4F694343505068
          6F746F73686F70204943432070726F66696C65000078DA9D53675453E9163DF7
          DEF4424B8880944B6F5215082052428B801491262A2109104A8821A1D91551C1
          114545041BC8A088038E8E808C15512C0C8A0AD807E421A28E83A3888ACAFBE1
          7BA36BD6BCF7E6CDFEB5D73EE7ACF39DB3CF07C0080C9648335135800CA9421E
          11E083C7C4C6E1E42E40810A2470001008B3642173FD230100F87E3C3C2B22C0
          07BE000178D30B0800C04D9BC0301C87FF0FEA42995C01808401C07491384B08
          801400407A8E42A600404601809D98265300A0040060CB6362E300502D006027
          7FE6D300809DF8997B01005B94211501A09100201365884400683B00ACCF568A
          450058300014664BC43900D82D00304957664800B0B700C0CE100BB200080C00
          305188852900047B0060C8232378008499001446F2573CF12BAE10E72A000078
          99B23CB9243945815B082D710757572E1E28CE49172B14366102619A402EC279
          99193281340FE0F3CC0000A0911511E083F3FD78CE0EAECECE368EB60E5F2DEA
          BF06FF226262E3FEE5CFAB70400000E1747ED1FE2C2FB31A803B06806DFEA225
          EE04685E0BA075F78B66B20F40B500A0E9DA57F370F87E3C3C45A190B9D9D9E5
          E4E4D84AC4425B61CA577DFE67C25FC057FD6CF97E3CFCF7F5E0BEE22481325D
          814704F8E0C2CCF44CA51CCF92098462DCE68F47FCB70BFFFC1DD322C44962B9
          582A14E35112718E449A8CF332A52289429229C525D2FF64E2DF2CFB033EDF35
          00B06A3E017B912DA85D6303F64B27105874C0E2F70000F2BB6FC1D428080380
          6883E1CF77FFEF3FFD47A02500806649927100005E44242E54CAB33FC7080000
          44A0812AB0411BF4C1182CC0061CC105DCC10BFC6036844224C4C24210420A64
          801C726029AC82422886CDB01D2A602FD4401D34C051688693700E2EC255B80E
          3D700FFA61089EC128BC81090441C808136121DA8801628A58238E08179985F8
          21C14804128B2420C9881451224B91354831528A542055481DF23D720239875C
          46BA913BC8003282FC86BC47319481B2513DD40CB543B9A8371A8446A20BD064
          74319A8F16A09BD072B41A3D8C36A1E7D0AB680FDA8F3E43C730C0E8180733C4
          6C302EC6C342B1382C099363CBB122AC0CABC61AB056AC03BB89F563CFB17704
          128145C0093604774220611E4148584C584ED848A8201C243411DA0937090384
          51C2272293A84BB426BA11F9C4186232318758482C23D6128F132F107B8843C4
          37241289433227B9900249B1A454D212D246D26E5223E92CA99B34481A2393C9
          DA646BB20739942C202BC885E49DE4C3E433E41BE421F25B0A9D624071A4F853
          E22852CA6A4A19E510E534E5066598324155A39A52DDA8A15411358F5A42ADA1
          B652AF5187A81334759A39CD8316494BA5ADA295D31A681768F769AFE874BA11
          DD951E4E97D057D2CBE947E897E803F4770C0D861583C7886728199B18071867
          197718AF984CA619D38B19C754303731EB98E7990F996F55582AB62A7C1591CA
          0A954A9526951B2A2F54A9AAA6AADEAA0B55F355CB548FA95E537DAE46553353
          E3A909D496AB55AA9D50EB531B5367A93BA887AA67A86F543FA47E59FD890659
          C34CC34F43A451A0B15FE3BCC6200B6319B3782C216B0DAB86758135C426B1CD
          D97C762ABB98FD1DBB8B3DAAA9A13943334A3357B352F394663F07E39871F89C
          744E09E728A797F37E8ADE14EF29E2291BA6344CB931655C6BAA96979658AB48
          AB51AB47EBBD36AEEDA79DA6BD45BB59FB810E41C74A275C2747678FCE059DE7
          53D953DDA70AA7164D3D3AF5AE2EAA6BA51BA1BB4477BF6EA7EE989EBE5E809E
          4C6FA7DE79BDE7FA1C7D2FFD54FD6DFAA7F5470C5806B30C2406DB0CCE183CC5
          35716F3C1D2FC7DBF151435DC34043A561956197E18491B9D13CA3D5468D460F
          8C69C65CE324E36DC66DC6A326062621264B4DEA4DEE9A524DB9A629A63B4C3B
          4CC7CDCCCDA2CDD699359B3D31D732E79BE79BD79BDFB7605A785A2CB6A8B6B8
          6549B2E45AA659EEB6BC6E855A3959A558555A5DB346AD9DAD25D6BBADBBA711
          A7B94E934EAB9ED667C3B0F1B6C9B6A9B719B0E5D806DBAEB66DB67D61676217
          67B7C5AEC3EE93BD937DBA7D8DFD3D070D87D90EAB1D5A1D7E73B472143A563A
          DE9ACE9CEE3F7DC5F496E92F6758CF10CFD833E3B613CB29C4699D539BD34767
          1767B97383F3888B894B82CB2E973E2E9B1BC6DDC8BDE44A74F5715DE17AD2F5
          9D9BB39BC2EDA8DBAFEE36EE69EE87DC9FCC349F299E593373D0C3C843E051E5
          D13F0B9F95306BDFAC7E4F434F8167B5E7232F632F9157ADD7B0B7A577AAF761
          EF173EF63E729FE33EE33C37DE32DE595FCC37C0B7C8B7CB4FC36F9E5F85DF43
          7F23FF64FF7AFFD100A78025016703898141815B02FBF87A7C21BF8E3F3ADB65
          F6B2D9ED418CA0B94115418F82AD82E5C1AD2168C8EC90AD21F7E798CE91CE69
          0E85507EE8D6D00761E6618BC37E0C2785878557863F8E7088581AD131973577
          D1DC4373DF44FA449644DE9B67314F39AF2D4A352A3EAA2E6A3CDA37BA34BA3F
          C62E6659CCD5589D58496C4B1C392E2AAE366E6CBEDFFCEDF387E29DE20BE37B
          17982FC85D7079A1CEC2F485A716A92E122C3A96404C884E3894F041102AA816
          8C25F21377258E0A79C21DC267222FD136D188D8435C2A1E4EF2482A4D7A92EC
          91BC357924C533A52CE5B98427A990BC4C0D4CDD9B3A9E169A76206D323D3ABD
          31839291907142AA214D93B667EA67E66676CBAC6585B2FEC56E8BB72F1E9507
          C96BB390AC05592D0AB642A6E8545A28D72A07B267655766BFCD89CA3996AB9E
          2BCDEDCCB3CADB90379CEF9FFFED12C212E192B6A5864B572D1D58E6BDAC6A39
          B23C7179DB0AE315052B865606AC3CB88AB62A6DD54FABED5797AE7EBD267A4D
          6B815EC1CA82C1B5016BEB0B550AE5857DEBDCD7ED5D4F582F59DFB561FA869D
          1B3E15898AAE14DB1797157FD828DC78E51B876FCABF99DC94B4A9ABC4B964CF
          66D266E9E6DE2D9E5B0E96AA97E6970E6E0DD9DAB40DDF56B4EDF5F645DB2F97
          CD28DBBB83B643B9A3BF3CB8BC65A7C9CECD3B3F54A454F454FA5436EED2DDB5
          61D7F86ED1EE1B7BBCF634ECD5DB5BBCF7FD3EC9BEDB5501554DD566D565FB49
          FBB3F73FAE89AAE9F896FB6D5DAD4E6D71EDC703D203FD07230EB6D7B9D4D51D
          D23D54528FD62BEB470EC71FBEFE9DEF772D0D360D558D9CC6E223704479E4E9
          F709DFF71E0D3ADA768C7BACE107D31F761D671D2F6A429AF29A469B539AFB5B
          625BBA4FCC3ED1D6EADE7AFC47DB1F0F9C343C59794AF354C969DAE982D39367
          F2CF8C9D959D7D7E2EF9DC60DBA2B67BE763CEDF6A0F6FEFBA1074E1D245FF8B
          E73BBC3BCE5CF2B874F2B2DBE51357B8579AAF3A5F6DEA74EA3CFE93D34FC7BB
          9CBB9AAEB95C6BB9EE7ABDB57B66F7E91B9E37CEDDF4BD79F116FFD6D59E393D
          DDBDF37A6FF7C5F7F5DF16DD7E7227FDCECBBBD97727EEADBC4FBC5FF440ED41
          D943DD87D53F5BFEDCD8EFDC7F6AC077A0F3D1DC47F7068583CFFE91F58F0F43
          058F998FCB860D86EB9E383E3939E23F72FDE9FCA743CF64CF269E17FEA2FECB
          AE17162F7EF8D5EBD7CED198D1A197F29793BF6D7CA5FDEAC0EB19AFDBC6C2C6
          1EBEC97833315EF456FBEDC177DC771DEFA3DF0F4FE47C207F28FF68F9B1F553
          D0A7FB93199393FF040398F3FC63332DDB000003834944415478DADD954B6813
          511486FF642669129B474D5B8305FB4CAD56C167141FC585BA71E7C2AA506D10
          5470E74A50104171E75611292A28BAD19D0BA9A05551FB50C12A317DD8165AFB
          4A9BB4318F6666E23937C64EABD456E9C6BB993039F77CE7FCF7FC770CE9741A
          8BBD0CFF17E4E6FD8E8B06034EA96A7A98DEA99264C0DFB2352DB3D1683478E9
          71BBBED67B2C0BB91B8F2B8724D9889AAD1ED86C323475E1142E2E1A53D0F4EA
          ABD84F791A8F1EF0EE11901B77820D168BE45F5B9587F2623BF89DC168583024
          CD5DD0B6EEDE283E04C6914CAA8DC70E576620576F051A56795DFEEDBE42C4A8
          92A191383E05C32009B9ED7949C4F2AEAE746159815528F1B2791881CE70E3C9
          235519C8B5DB81066FA9D3BF797D3E386757CF241E3FED170096E04F8BCE5280
          F6EE2A4279891DDC50CBBB5174F6441A4FD4E92015A50EBF6F5D3E52290DB184
          8A70642A3319F3502D3B242EA719368B0493C988E6F7A354ECC4EF212A953136
          3E85EEBE49EA84217FA6700E4D03CA56D8B134CF0C8914981B42AD7317BDFD51
          01D033A41FF2718CAAA56774C2798A8B7245371C3327244E52719095DACECAC0
          4F4B8E8494A261329A823DD7242449506CB6087EF25E2E80F7CE094924D55F8C
          C880B17012AFDB46289102AB55C6D60D0558EACA11F1FAC5308E5F1084BBE2C9
          79FC6C4024DDB4D68DD60F218C4792D85B53447ECA4CD73F417268C3C0600CAF
          5A87B07F5F299C0E332626A6F0E0510FB66D2A84A7D086E4943A3F88B7CCF9E3
          4C94191099AE1A722E9EBC18403599AD9A6E857672F3C7CF61ECDEB95C14A1D0
          59CD805864B412A4E38BCE27ECF895154EFF0EDF3251A542EDEBA7CA4667D0DD
          37215C5CE0B66078340E8E2D2B76201657664C994CF23AEC66BC681942B02B32
          EDF81B77830D6693D15F55E144F54A9708D674236AA5CAA2DF52387BB90DA363
          09E413E8D2998DC85D62129D6717DF105C1C7719E88CB0B1A7EF2EBA85EF9124
          B51CE8A3A95962358971D54F178FEE95EBEDE27C967B6C387D7C8D1865FD7499
          48DA6FF1149ADF8E64F659A4E95BF8E6BD8ED354419D9A4E8754455367BB5C96
          8C5CAEA5E9CD600DC9293B1C66A5668BA789DE2514559367BB9F3F1964DC12FA
          F990BE2767B210FE5B04E965FA39616609FD8331EFDBF6D045CA729E34B9B061
          8DFB5C91C7D6A19F2CBD6CD9557FB012BF4066AFAC4F9E370FBBC91B6E727390
          9C5D99E7CC09EDF4158638A13AC707EE2764B1D7FF03F90E422040EDE5CD30CF
          0000000049454E44AE426082}
      end>
    Left = 32
    Top = 472
    Bitmap = {}
  end
  object dsEditAvaliacao: TDataSource
    DataSet = DtmPrincipal.cdsTesteAvaliacao
    Left = 448
    Top = 472
  end
  object dsLacreNovo: TDataSource
    DataSet = DtmPrincipal.cdsMangLacre
    Left = 696
    Top = 472
  end
end
