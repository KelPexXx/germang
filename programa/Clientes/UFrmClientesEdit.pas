unit UFrmClientesEdit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, JvExExtCtrls, JvExtComponent, JvDBRadioPanel, StdCtrls, DBCtrls,
  Mask, ComCtrls, Buttons, PngBitBtn, ExtCtrls;

type
  TFrmClientesEdit = class(TForm)
    pBottom: TPanel;
    btnSalvarCad: TPngBitBtn;
    btnCancelarCad: TPngBitBtn;
    pgCadCliente: TPageControl;
    tbInfoGerais: TTabSheet;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label9: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    dbCodigo: TDBEdit;
    dbRazaoSocial: TDBEdit;
    dbNomeFantasia: TDBEdit;
    dbCnpj: TDBEdit;
    dbContato: TDBEdit;
    dbCEP: TDBEdit;
    dbTipoEnd: TDBLookupComboBox;
    dbLogradouro: TDBEdit;
    dbNumEnd: TDBEdit;
    dbComplemento: TDBEdit;
    dbBairro: TDBEdit;
    dbCidade: TDBLookupComboBox;
    dbEstado: TComboBox;
    dbFoneCom: TDBEdit;
    dbFoneCell: TDBEdit;
    dbFoneFax: TDBEdit;
    dbEmailCont: TDBEdit;
    dbEmailNFE: TDBEdit;
    dbObs: TDBEdit;
    tbInfoEspec: TTabSheet;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    dbAtuacao: TDBLookupComboBox;
    dbRisco: TDBLookupComboBox;
    dbSisPrev: TDBLookupComboBox;
    dbDataInstSisPrev: TDBEdit;
    dbResponsavel: TDBEdit;
    dbBrigada: TJvDBRadioPanel;
    dsEndTipo: TDataSource;
    dsSisPrev: TDataSource;
    dsRisco: TDataSource;
    dsAtuacao: TDataSource;
    dsCidade: TDataSource;
    dsEstado: TDataSource;
    dsCad: TDataSource;
    btnEditar: TPngBitBtn;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure btnEditarClick(Sender: TObject);
    procedure btnSalvarCadClick(Sender: TObject);
    procedure btnCancelarCadClick(Sender: TObject);
    procedure dbEstadoChange(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
  private
    procedure AbrirDataSets;
    procedure FecharDataSets;
    procedure ModoEdit(modo:boolean);
    function getCodUF(Cod:integer):integer;
  public
    procedure Seleciona(cod:integer);
  end;

var
  FrmClientesEdit: TFrmClientesEdit;

implementation

uses UDtmPrincipal;

{$R *.dfm}

procedure TFrmClientesEdit.AbrirDataSets;
begin
  with DtmPrincipal do
    begin
      sdsTipoEnd.Open;
      sdsRisco.Open;
      sdsAtuacao.Open;
      sdsSisPrev.Open;
      sdsEstado.Open;
      sdsCidade.Open;
      sdsClientes.Open;
      cdsTipoEnd.Open;
      cdsRisco.Open;
      cdsSisPrev.Open;
      cdsAtuacao.Open;
      cdsEstado.Open;
      cdsCidade.Open;
      cdsClientes.Open;
    end
end;

procedure TFrmClientesEdit.FecharDataSets;
begin
  with DtmPrincipal do
    begin
      cdsClientes.Close;
      cdsEstado.Close;
      cdsCidade.Close;
      cdsRisco.Close;
      cdsAtuacao.Close;
      cdsSisPrev.Close;
      cdsTipoEnd.Close;
      sdsClientes.Close;
      sdsEstado.Close;
      sdsCidade.Close;
      sdsRisco.Close;
      sdsAtuacao.Close;
      sdsSisPrev.Close;
      sdsTipoEnd.Close;
    end;
end;

function TFrmClientesEdit.getCodUF(Cod:integer): integer;
begin
  DtmPrincipal.cdsCidade.open;
  DtmPrincipal.cdsCidade.Filtered := false;
  DtmPrincipal.cdsCidade.Filter := 'CODCIDADE = ' + IntToStr(Cod);
  DtmPrincipal.cdsCidade.Filtered := true;
  DtmPrincipal.cdsCidade.First;
  Result := DtmPrincipal.cdsCidadeCODESTADO.AsInteger;
  DtmPrincipal.cdsCidade.Filtered := false;
  DtmPrincipal.cdsCidade.Filter := '';
end;

procedure TFrmClientesEdit.ModoEdit(modo: boolean);
begin
  dbBrigada.ReadOnly := modo;
  dbResponsavel.ReadOnly := modo;
  dbDataInstSisPrev.ReadOnly := modo;
  dbSisPrev.ReadOnly := modo;
  dbRisco.ReadOnly := modo;
  dbAtuacao.ReadOnly := modo;
  dbObs.ReadOnly := modo;
  dbEmailNFE.ReadOnly := modo;
  dbEmailCont.ReadOnly := modo;
  dbFoneFax.ReadOnly := modo;
  dbFoneCell.ReadOnly := modo;
  dbFoneCom.ReadOnly := modo;
  dbEstado.Enabled := not modo;
  dbCidade.ReadOnly := modo;
  dbBairro.ReadOnly := modo;
  dbComplemento.ReadOnly := modo;
  dbNumEnd.ReadOnly := modo;
  dbLogradouro.ReadOnly := modo;
  dbTipoEnd.ReadOnly := modo;
  dbCEP.ReadOnly := modo;
  dbContato.ReadOnly := modo;
  dbCnpj.ReadOnly := modo;
  dbRazaoSocial.ReadOnly := modo;
  dbNomeFantasia.ReadOnly := modo;
  btnEditar.Enabled := modo;
  btnSalvarCad.Enabled := not modo;
end;

procedure TFrmClientesEdit.Seleciona(cod: integer);
begin
  DtmPrincipal.cdsClientes.Open;
  DtmPrincipal.cdsClientes.Filtered := false;
  DtmPrincipal.cdsClientes.Filter := 'CODCLI = ' + IntToStr(cod);
  DtmPrincipal.cdsClientes.Filtered := true;
  DtmPrincipal.cdsClientes.First;
  dbEstado.ItemIndex := getCodUF(DtmPrincipal.cdsClientesCODCIDADE.AsInteger);
end;

procedure TFrmClientesEdit.dbEstadoChange(Sender: TObject);
begin
  if dbEstado.ItemIndex > 0 then
    begin
      DtmPrincipal.cdsCidade.Filtered := false;
      DtmPrincipal.cdsCidade.Filter := 'CODESTADO = ' + inttostr(dbEstado.ItemIndex);
      DtmPrincipal.cdsCidade.Filtered := true;
      dbCidade.Enabled := true;
    end
  else
    dbCidade.Enabled := false;
end;

procedure TFrmClientesEdit.FormShow(Sender: TObject);
begin
  try
    AbrirDataSets;
    Seleciona(DtmPrincipal.cdsClientes_VCODCLI.AsInteger);
  except
    on E: Exception do
    begin
      DtmPrincipal.cdsClientes.Cancel;
      DtmPrincipal.cdsClientes.Open;
      MessageDlg('Erro: ' + E.Message, mtError, [mbOK], 0);
    end;
  end;
  ModoEdit(true);
end;

procedure TFrmClientesEdit.FormClose(Sender: TObject; var Action: TCloseAction);
Begin
  FreeAndNil(FrmClientesEdit);
end;

procedure TFrmClientesEdit.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
var
  Resposta: Integer;
begin
  if (dsCad.DataSet <> nil) then
    begin
      if (DtmPrincipal.cdsClientes.Active) and (DtmPrincipal.cdsClientes.State in [dsEdit, dsInsert]) then
        begin
          Resposta := MessageDlg('O registro atual ainda n�o foi salvo! Deseja Salvar as altera��es antes de sair?',mtInformation, mbYesNoCancel, 0);
          case Resposta of
            mrYes:
              begin
                DtmPrincipal.cdsClientes.Post;
                DtmPrincipal.cdsClientes.ApplyUpdates(0);
                FecharDataSets;
                CanClose := True;
              end;
            mrNo:
              begin
                DtmPrincipal.cdsClientes.Cancel;
                FecharDataSets;
                CanClose := True;
              end;
            else
              begin
                CanClose := False;
                exit;
              end;
          end;
        end
      else
        CanClose := True;
    end;
end;

procedure TFrmClientesEdit.btnCancelarCadClick(Sender: TObject);
begin
  try
    FrmClientesEdit.Close;
  except on E: Exception do
    begin
      DtmPrincipal.cdsClientes.Cancel;
      DtmPrincipal.cdsClientes.Open;
      MessageDlg('Erro: '+E.Message, mtError, [mbOK], 0);
    end;
  end;
end;

procedure TFrmClientesEdit.btnEditarClick(Sender: TObject);
begin
  try
    DtmPrincipal.cdsClientes.Edit;
    ModoEdit(false);
    dbCnpj.SetFocus;
  except
    on E: Exception do
      begin
        DtmPrincipal.cdsClientes.Cancel;
        DtmPrincipal.cdsClientes.Open;
        MessageDlg('Erro: ' + E.Message, mtError, [mbOK], 0);
      end;
  end;
end;

procedure TFrmClientesEdit.btnSalvarCadClick(Sender: TObject);
begin
  try
    if (DtmPrincipal.cdsClientes.State in [dsEdit, dsInsert]) then
    begin
      if MessageDlg(
        'Confirmar dados?', mtInformation, mbYesNo, 0) = mrYes then
      begin
        DtmPrincipal.cdsClientes.Post;
        DtmPrincipal.cdsClientes.ApplyUpdates(0);
        DtmPrincipal.cdsClientes_V.Refresh;
        DtmPrincipal.cdsClientes_V.Refresh;
        DtmPrincipal.cdsClientes.Refresh;
        DtmPrincipal.cdsClientes.Refresh;
        FrmClientesEdit.Close;
      end;
    end;
  except
    on E: Exception do
    begin
      DtmPrincipal.cdsClientes.Cancel;
      DtmPrincipal.cdsClientes.Open;
      MessageDlg('Erro: ' + E.Message, mtError, [mbOK], 0);
    end;
  end;
end;
end.
