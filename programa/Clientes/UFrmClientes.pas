unit UFrmClientes;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ImgList, PngImageList, DBGrids,
  ExtCtrls, ComCtrls, ToolWin, Buttons, PngSpeedButton, pngimage,
  UFrmFormularioPadrao, Grids;

type
  TFrmClientes = class(TFrmFormularioPadrao)
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure btnMaddClick(Sender: TObject);
    procedure btnMEditClick(Sender: TObject);
    procedure dgFormDblClick(Sender: TObject);
    procedure btnMPesqClick(Sender: TObject);
    procedure btnMFiltroClick(Sender: TObject);
    procedure btnMOrderClick(Sender: TObject);
    procedure btnMExcluiClick(Sender: TObject);
  private

  public
    procedure ScanOpen;
    procedure MudaPesquisa(status:boolean);
  end;

var
  FrmClientes: TFrmClientes;

implementation

uses UDtmPrincipal, UFrmClientesCad, UFrmClientesEdit, UFrmClientesPesq,
  UFrmClientesFiltrar, UFrmClientesOrdenar, UFrmClientesRemover;

{$R *.dfm}

procedure TFrmClientes.btnMaddClick(Sender: TObject);
begin
  inherited;
  ScanOpen;
  if not Assigned(FrmClientesCad) then
    FrmClientesCad := TFrmClientesCad.Create(Self);
  FrmClientesCad.Show;

end;

procedure TFrmClientes.btnMEditClick(Sender: TObject);
begin
  inherited;
  ScanOpen;
  if not Assigned(FrmClientesEdit) then
    Application.CreateForm(TFrmClientesEdit,FrmClientesEdit);
  FrmClientesEdit.Show;

end;

procedure TFrmClientes.btnMExcluiClick(Sender: TObject);
begin
  inherited;
  ScanOpen;
  if not Assigned(FrmClientesRemover) then
    Application.CreateForm(TFrmClientesRemover,FrmClientesRemover);
  FrmClientesRemover.lblTextoMsg2.Caption:= FrmClientesRemover.lblTextoMsg2.Caption + ' '
   + DtmPrincipal.cdsClientes_VCODCLI.AsString + '?';
  FrmClientesRemover.Show;
end;

procedure TFrmClientes.btnMFiltroClick(Sender: TObject);
begin
  inherited;
  ScanOpen;
  if not Assigned(FrmClientesFiltrar) then
    Application.CreateForm(TFrmClientesFiltrar,FrmClientesFiltrar);
  FrmClientesFiltrar.Show;
end;

procedure TFrmClientes.btnMOrderClick(Sender: TObject);
begin
  inherited;
  ScanOpen;
  if not Assigned(FrmClientesOrdenar) then
    Application.CreateForm(TFrmClientesOrdenar,FrmClientesOrdenar);
  FrmClientesOrdenar.Show;

end;

procedure TFrmClientes.btnMPesqClick(Sender: TObject);
begin
  inherited;
  ScanOpen;
  if not Assigned(FrmClientesPesq) then
    Application.CreateForm(TFrmClientesPesq,FrmClientesPesq);
  FrmClientesPesq.Show;
end;

procedure TFrmClientes.dgFormDblClick(Sender: TObject);
begin
  inherited;
  ScanOpen;
  if not Assigned(FrmClientesEdit) then
    Application.CreateForm(TFrmClientesEdit,FrmClientesEdit);
  FrmClientesEdit.Seleciona(DtmPrincipal.cdsClientes_VCODCLI.AsInteger);
  FrmClientesEdit.Show;
end;

procedure TFrmClientes.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  ScanOpen;
  DtmPrincipal.cdsClientes_V.Close;
  FreeAndNil(FrmClientes);
end;

procedure TFrmClientes.FormCreate(Sender: TObject);
begin
  inherited;
  DtmPrincipal.cdsClientes_V.Open;
end;

procedure TFrmClientes.MudaPesquisa(status: boolean);
begin
  if status then
    dgForm.Options:= dgForm.Options-[dgRowSelect]
  else
    dgForm.Options:= dgForm.Options+[dgRowSelect];
end;

procedure TFrmClientes.ScanOpen;
begin
  if FrmClientesCad <> nil then
    FrmClientesCad.Close;
  if FrmClientesEdit <> nil then
    FrmClientesEdit.Close;
  if FrmClientesFiltrar <> nil then
    FrmClientesFiltrar.Close;
  if FrmClientesOrdenar <> nil then
    FrmClientesOrdenar.Close;
  if FrmClientesRemover <> nil then
    FrmClientesRemover.Close;
end;

end.
