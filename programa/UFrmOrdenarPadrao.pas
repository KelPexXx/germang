unit UFrmOrdenarPadrao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Buttons, PngSpeedButton, StdCtrls;

type
  TFrmOrdenarPadrao = class(TForm)
    pEsquerda: TPanel;
    pCentro: TPanel;
    pDireita: TPanel;
    pBotoes: TPanel;
    rgOpcoes: TRadioGroup;
    rgOrdem: TRadioGroup;
    btnOrdenar: TPngSpeedButton;
    btnLimparOrdem: TPngSpeedButton;
    btnFechar: TPngSpeedButton;
    procedure FormPaint(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnFecharClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmOrdenarPadrao: TFrmOrdenarPadrao;

implementation

{$R *.dfm}

procedure TFrmOrdenarPadrao.btnFecharClick(Sender: TObject);
begin
  Close;
end;

procedure TFrmOrdenarPadrao.FormPaint(Sender: TObject);
begin
  pEsquerda.Width := Trunc((Self.ClientWidth - 530)/ 2);
  pDireita.Width := Trunc((Self.ClientWidth - 530)/ 2);
end;

procedure TFrmOrdenarPadrao.FormShow(Sender: TObject);
begin
  Self.Refresh;
end;

end.
