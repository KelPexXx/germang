inherited FrmTestesBuscaCliCad: TFrmTestesBuscaCliCad
  Caption = ' Buscar Cliente'
  ClientHeight = 264
  ClientWidth = 533
  OnClose = FormClose
  ExplicitWidth = 539
  ExplicitHeight = 293
  PixelsPerInch = 96
  TextHeight = 13
  inherited pTopo: TPanel
    Width = 533
    Height = 97
    ExplicitWidth = 533
    ExplicitHeight = 97
    inherited btnBuscar: TPngSpeedButton
      OnClick = btnBuscarClick
    end
    inherited rgOpcoes: TRadioGroup
      Width = 525
      Height = 42
      Columns = 4
      ItemIndex = 0
      Items.Strings = (
        'C'#243'digo'
        'CNPJ'
        'Raz'#227'o Social'
        'Nome Fantasia')
      ExplicitWidth = 525
      ExplicitHeight = 42
    end
  end
  inherited dgBusca: TDBGrid
    Top = 97
    Width = 533
    Height = 113
    Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
    ReadOnly = True
    OnDblClick = dgBuscaDblClick
    Columns = <
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'CODCLI'
        Title.Alignment = taCenter
        Title.Caption = 'C'#243'digo'
        Width = 47
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'FANTASIA'
        Title.Caption = 'Nome Fantasia'
        Width = 166
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'RAZAO'
        Title.Caption = 'Raz'#227'o Social'
        Width = 182
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CNPJ'
        Visible = True
      end>
  end
  inherited pBaixo: TPanel
    Top = 210
    Width = 533
    ExplicitTop = 210
    ExplicitWidth = 533
    inherited btnSelecionar: TPngSpeedButton
      Left = 409
      OnClick = btnSelecionarClick
      ExplicitLeft = 528
    end
  end
  inherited dsBusca: TDataSource
    DataSet = DtmPrincipal.cdsClientes_V
    Left = 152
    Top = 168
  end
end
