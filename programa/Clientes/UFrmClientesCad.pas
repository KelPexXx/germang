unit UFrmClientesCad;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, StdCtrls, Buttons, PngBitBtn, ExtCtrls, DB, Mask, DBCtrls,
  DBClient, JvExControls, JvDBLookup, JvExExtCtrls, JvExtComponent,
  JvDBRadioPanel, JvExMask, JvToolEdit, JvDBControls;

type
  TFrmClientesCad = class(TForm)
    pgCadCliente: TPageControl;
    tbInfoGerais: TTabSheet;
    tbInfoEspec: TTabSheet;
    Label1: TLabel;
    dbCodigo: TDBEdit;
    dsCad: TDataSource;
    Label2: TLabel;
    dbRazaoSocial: TDBEdit;
    pBottom: TPanel;
    btnSalvarCad: TPngBitBtn;
    btnCancelarCad: TPngBitBtn;
    Label3: TLabel;
    dbNomeFantasia: TDBEdit;
    Label4: TLabel;
    dbCnpj: TDBEdit;
    Label5: TLabel;
    dbContato: TDBEdit;
    Label6: TLabel;
    dbCEP: TDBEdit;
    dsEndTipo: TDataSource;
    dbTipoEnd: TDBLookupComboBox;
    Label7: TLabel;
    dbLogradouro: TDBEdit;
    Label8: TLabel;
    Label10: TLabel;
    dbNumEnd: TDBEdit;
    Label11: TLabel;
    dbComplemento: TDBEdit;
    Label12: TLabel;
    dbBairro: TDBEdit;
    dbCidade: TDBLookupComboBox;
    Label13: TLabel;
    dsEstado: TDataSource;
    dsCidade: TDataSource;
    Label14: TLabel;
    dbEstado: TComboBox;
    Label9: TLabel;
    dbFoneCom: TDBEdit;
    dbFoneCell: TDBEdit;
    Label15: TLabel;
    Label16: TLabel;
    dbFoneFax: TDBEdit;
    Label17: TLabel;
    dbEmailCont: TDBEdit;
    Label18: TLabel;
    dbEmailNFE: TDBEdit;
    Label19: TLabel;
    dbObs: TDBEdit;
    Label20: TLabel;
    dsAtuacao: TDataSource;
    dsRisco: TDataSource;
    dsSisPrev: TDataSource;
    Label21: TLabel;
    dbAtuacao: TDBLookupComboBox;
    dbRisco: TDBLookupComboBox;
    Label22: TLabel;
    dbSisPrev: TDBLookupComboBox;
    Label23: TLabel;
    dbDataInstSisPrev: TDBEdit;
    Label24: TLabel;
    Label25: TLabel;
    dbResponsavel: TDBEdit;
    dbBrigada: TJvDBRadioPanel;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnSalvarCadClick(Sender: TObject);
    procedure btnCancelarCadClick(Sender: TObject);
    procedure dbEstadoChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
  private
    procedure AbrirDataSets;
    procedure FecharDataSets;
  public
    { Public declarations }
  end;

var
  FrmClientesCad: TFrmClientesCad;

implementation

uses UDtmPrincipal;

{$R *.dfm}

procedure TFrmClientesCad.AbrirDataSets;
begin
  with DtmPrincipal do
    begin
      sdsTipoEnd.Open;
      sdsRisco.Open;
      sdsAtuacao.Open;
      sdsSisPrev.Open;
      sdsEstado.Open;
      sdsCidade.Open;
      sdsClientes.Open;
      cdsTipoEnd.Open;
      cdsRisco.Open;
      cdsSisPrev.Open;
      cdsAtuacao.Open;
      cdsEstado.Open;
      cdsCidade.Open;
      cdsClientes.Open;
    end
end;

procedure TFrmClientesCad.FecharDataSets;
begin
  with DtmPrincipal do
    begin
      cdsClientes.Close;
      cdsEstado.Close;
      cdsCidade.Close;
      cdsRisco.Close;
      cdsAtuacao.Close;
      cdsSisPrev.Close;
      cdsTipoEnd.Close;
      sdsClientes.Close;
      sdsEstado.Close;
      sdsCidade.Close;
      sdsRisco.Close;
      sdsAtuacao.Close;
      sdsSisPrev.Close;
      sdsTipoEnd.Close;
    end;
end;

procedure TFrmClientesCad.dbEstadoChange(Sender: TObject);
begin
  if dbEstado.ItemIndex > 0 then
    begin
      DtmPrincipal.cdsCidade.Filtered := false;
      DtmPrincipal.cdsCidade.Filter := 'CODESTADO = ' + inttostr(dbEstado.ItemIndex);
      DtmPrincipal.cdsCidade.Filtered := true;
      dbCidade.Enabled := true;
    end
  else
    dbCidade.Enabled := false;
end;

procedure TFrmClientesCad.FormShow(Sender: TObject);
begin
  try
    AbrirDataSets;
    DtmPrincipal.cdsClientes.Append;
    DtmPrincipal.cdsClientesDATACAD.AsDateTime := Now;
  except
    on E: Exception do
    begin
      DtmPrincipal.cdsClientes.Cancel;
      DtmPrincipal.cdsClientes.Open;
      MessageDlg('Erro: ' + E.Message, mtError, [mbOK], 0);
    end;
  end;
end;

procedure TFrmClientesCad.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  FreeAndNil(FrmClientesCad);
end;

procedure TFrmClientesCad.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
var
  Resposta: Integer;
begin
  if (dsCad.DataSet <> nil) then
    begin
      if (DtmPrincipal.cdsClientes.Active) and (DtmPrincipal.cdsClientes.State in [dsEdit, dsInsert]) then
        begin
          Resposta := MessageDlg('O registro atual ainda n�o foi salvo! Deseja Salvar as altera��es antes de sair?',mtInformation, mbYesNoCancel, 0);
          case Resposta of
            mrYes:
              begin
                DtmPrincipal.cdsClientes.Post;
                DtmPrincipal.cdsClientes.ApplyUpdates(0);
                DtmPrincipal.cdsClientes_V.Refresh;
                DtmPrincipal.cdsClientes_V.Refresh;
                DtmPrincipal.cdsClientes.Refresh;
                DtmPrincipal.cdsClientes.Refresh;
                FecharDataSets;
                CanClose := True;
              end;
            mrNo:
              begin
                DtmPrincipal.cdsClientes.Cancel;
                FecharDataSets;
                CanCLose := True;
              end;
            else
              begin
                CanClose := False;
                exit;
              end;
          end;
        end;
    end;
end;

procedure TFrmClientesCad.btnCancelarCadClick(Sender: TObject);
begin
  try
    if (DtmPrincipal.cdsClientes.State in [dsEdit, dsInsert]) then
      begin
        FrmClientesCad.Close;
      end;
  except on E: Exception do
    begin
      DtmPrincipal.cdsClientes.Cancel;
      DtmPrincipal.cdsClientes.Open;
      MessageDlg('Erro: '+E.Message, mtError, [mbOK], 0);
    end;
  end;
end;

procedure TFrmClientesCad.btnSalvarCadClick(Sender: TObject);
begin
 try
    if (DtmPrincipal.cdsClientes.State in [dsEdit, dsInsert]) then
    begin
      if MessageDlg(
        'Confirmar dados?', mtInformation, mbYesNo, 0) = mrYes then
      begin
        DtmPrincipal.cdsClientes.Post;
        DtmPrincipal.cdsClientes.ApplyUpdates(0);
        DtmPrincipal.cdsClientes_V.Refresh;
        DtmPrincipal.cdsClientes_V.Refresh;
        DtmPrincipal.cdsClientes.Refresh;
        DtmPrincipal.cdsClientes.Refresh;
        FrmClientesCad.Close;
      end;
    end;
  except
    on E: Exception do
    begin
      DtmPrincipal.cdsClientes.Cancel;
      DtmPrincipal.cdsClientes.Open;
      MessageDlg('Erro: ' + E.Message, mtError, [mbOK], 0);
    end;
  end;
end;

end.
