unit UFrmMangueirasRemover;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UFrmDelRegPadrao, Buttons, PngSpeedButton, StdCtrls, pngimage,
  ExtCtrls, DB;

type
  TFrmMangueirasRemover = class(TFrmDelRegPadrao)
    procedure btnCancelarClick(Sender: TObject);
    procedure btnRemoverClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmMangueirasRemover: TFrmMangueirasRemover;

implementation

uses UDtmPrincipal;

{$R *.dfm}

procedure TFrmMangueirasRemover.btnCancelarClick(Sender: TObject);
begin
  inherited;
  FrmMangueirasRemover.Close;
end;

procedure TFrmMangueirasRemover.btnRemoverClick(Sender: TObject);
begin
  inherited;
 try
    if DtmPrincipal.cdsMangueiras.Active then
      begin
    DtmPrincipal.cdsMangueiras.Filtered := FALSE;
    DtmPrincipal.cdsMangueiras.Filter := 'CODMANG = ' + DtmPrincipal.cdsMangueiras_VCODMANG.AsString;
    DtmPrincipal.cdsMangueiras.Filtered := TRUE;
    DtmPrincipal.cdsMangueiras.Delete;
    DtmPrincipal.cdsMangueiras.ApplyUpdates(0);
    DtmPrincipal.cdsMangueiras.Filtered := FALSE;
    DtmPrincipal.cdsMangueiras.Filter := '';
    DtmPrincipal.cdsMangueiras_V.Refresh;
    DtmPrincipal.cdsMangueiras_V.Refresh;
    FrmMangueirasRemover.Close;
      end;
  except
   on e : Exception do
      begin
      DtmPrincipal.cdsMangueiras.Cancel;
      DtmPrincipal.cdsMangueiras.Open;
      MessageDlg('Erro: ' + E.Message, mtError, [mbOK], 0);
        MessageDlg('N�o foi possivel remover a mangueira, j� existem registros no sistema.\n'
        +' Remova primeiro os registros de testes e lacres!\n'+' C�d Error:' + e.Message, mtWarning, mbOKCancel, 0);
      end;
    on e : EDatabaseError do
      showmessage(e.Message);
  end;
end;

procedure TFrmMangueirasRemover.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  DtmPrincipal.cdsMangueiras.Close;
  DtmPrincipal.sdsMangueiras.Close;
  FreeAndNil(FrmMangueirasRemover);
end;

procedure TFrmMangueirasRemover.FormShow(Sender: TObject);
begin
  inherited;
  DtmPrincipal.sdsMangueiras.Open;
  DtmPrincipal.cdsMangueiras.Open;
end;

end.
