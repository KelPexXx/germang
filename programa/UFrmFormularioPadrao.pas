unit UFrmFormularioPadrao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, ComCtrls, ToolWin, ImgList, PngImageList, Grids, DBGrids,
  DB, Buttons, PngSpeedButton;

type
  TFrmFormularioPadrao = class(TForm)
    tbMenuForm: TToolBar;
    btnMadd: TToolButton;
    btnMEdit: TToolButton;
    imgList50: TPngImageList;
    btnMPesq: TToolButton;
    btnMFiltro: TToolButton;
    btnMExclui: TToolButton;
    btnMOrder: TToolButton;
    sepM2: TToolButton;
    btnMRelatorios: TToolButton;
    pPrincipal: TPanel;
    dgForm: TDBGrid;
    sepM1: TToolButton;
    dsForm: TDataSource;
    pTitle: TPanel;
    btnFechar: TPngSpeedButton;
    imgIcon: TImage;
    procedure imgIconClick(Sender: TObject);
    procedure btnFecharClick(Sender: TObject);
  private
    { Private declarations }
  public

  end;

var
  FrmFormularioPadrao: TFrmFormularioPadrao;

implementation

uses UDtmPrincipal;

{$R *.dfm}

{ TFrmFormularioPadrao }

procedure TFrmFormularioPadrao.btnFecharClick(Sender: TObject);
begin
  Close;
end;

procedure TFrmFormularioPadrao.imgIconClick(Sender: TObject);
begin
  dsForm.DataSet.Refresh;
  dsForm.DataSet.Refresh;
end;

end.
