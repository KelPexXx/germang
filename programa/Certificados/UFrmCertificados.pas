unit UFrmCertificados;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UFrmFormularioPadrao, DB, ImgList, PngImageList, ExtCtrls, Buttons,
  PngSpeedButton, Grids, DBGrids, ComCtrls, ToolWin, pngimage;

type
  TFrmCertificados = class(TFrmFormularioPadrao)
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmCertificados: TFrmCertificados;

implementation

uses UDtmPrincipal;

{$R *.dfm}

procedure TFrmCertificados.FormCreate(Sender: TObject);
begin
  inherited;
  DtmPrincipal.cdsTestes_V.Open;
end;

end.
