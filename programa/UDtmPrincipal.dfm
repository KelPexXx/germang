object DtmPrincipal: TDtmPrincipal
  OldCreateOrder = False
  Height = 523
  Width = 1052
  object sdsClientes: TSQLDataSet
    SchemaName = 'sysdba'
    CommandText = 
      'SELECT a.CODCLI, a.CNPJ, a.RAZAO, a.FANTASIA, a.DATACAD, a.ENDCE' +
      'P, a.ENDNUMERO, a.ENDLOGRAD, a.ENDBAIRRO, a.ENDCOMPLE, a.CONTATO' +
      ', a.FONECOM, a.FONECEL, a.FONEFAX, a.EMAILCONT, a.EMAILNFE, a.OB' +
      'SERVACAO, a.DATAINSSISPREV, a.BRIGADA, a.RESPBRIG, a.CODRISCO, a' +
      '.CODCIDADE, a.CODSISPREV, a.CODENDTIPO, a.CODATUA'#13#10'FROM TB_CLIEN' +
      'TE a'
    MaxBlobSize = -1
    Params = <>
    SQLConnection = conexaoDB
    Left = 400
    Top = 64
    object sdsClientesCODCLI: TSmallintField
      FieldName = 'CODCLI'
    end
    object sdsClientesCNPJ: TStringField
      FieldName = 'CNPJ'
      Size = 18
    end
    object sdsClientesRAZAO: TStringField
      FieldName = 'RAZAO'
      Size = 256
    end
    object sdsClientesFANTASIA: TStringField
      FieldName = 'FANTASIA'
      Size = 256
    end
    object sdsClientesDATACAD: TDateField
      FieldName = 'DATACAD'
    end
    object sdsClientesENDCEP: TStringField
      FieldName = 'ENDCEP'
      Size = 16
    end
    object sdsClientesENDNUMERO: TStringField
      FieldName = 'ENDNUMERO'
      Size = 16
    end
    object sdsClientesENDLOGRAD: TStringField
      FieldName = 'ENDLOGRAD'
      Size = 64
    end
    object sdsClientesENDBAIRRO: TStringField
      FieldName = 'ENDBAIRRO'
      Size = 32
    end
    object sdsClientesENDCOMPLE: TStringField
      FieldName = 'ENDCOMPLE'
      Size = 32
    end
    object sdsClientesCONTATO: TStringField
      FieldName = 'CONTATO'
      Size = 64
    end
    object sdsClientesFONECOM: TStringField
      FieldName = 'FONECOM'
      Size = 16
    end
    object sdsClientesFONECEL: TStringField
      FieldName = 'FONECEL'
      Size = 16
    end
    object sdsClientesFONEFAX: TStringField
      FieldName = 'FONEFAX'
      Size = 16
    end
    object sdsClientesEMAILCONT: TStringField
      FieldName = 'EMAILCONT'
      Size = 64
    end
    object sdsClientesEMAILNFE: TStringField
      FieldName = 'EMAILNFE'
      Size = 64
    end
    object sdsClientesOBSERVACAO: TStringField
      FieldName = 'OBSERVACAO'
      Size = 128
    end
    object sdsClientesDATAINSSISPREV: TIntegerField
      FieldName = 'DATAINSSISPREV'
    end
    object sdsClientesBRIGADA: TIntegerField
      FieldName = 'BRIGADA'
    end
    object sdsClientesRESPBRIG: TStringField
      FieldName = 'RESPBRIG'
      Size = 32
    end
    object sdsClientesCODRISCO: TIntegerField
      FieldName = 'CODRISCO'
    end
    object sdsClientesCODCIDADE: TSmallintField
      FieldName = 'CODCIDADE'
    end
    object sdsClientesCODSISPREV: TSmallintField
      FieldName = 'CODSISPREV'
    end
    object sdsClientesCODENDTIPO: TSmallintField
      FieldName = 'CODENDTIPO'
    end
    object sdsClientesCODATUA: TSmallintField
      FieldName = 'CODATUA'
    end
  end
  object dspClientes: TDataSetProvider
    DataSet = sdsClientes
    Left = 512
    Top = 64
  end
  object cdsClientes: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dspClientes'
    OnReconcileError = cdsClientesReconcileError
    Left = 624
    Top = 64
    object cdsClientesCODCLI: TSmallintField
      FieldName = 'CODCLI'
    end
    object cdsClientesCNPJ: TStringField
      FieldName = 'CNPJ'
      Size = 18
    end
    object cdsClientesRAZAO: TStringField
      FieldName = 'RAZAO'
      Size = 256
    end
    object cdsClientesFANTASIA: TStringField
      FieldName = 'FANTASIA'
      Size = 256
    end
    object cdsClientesDATACAD: TDateField
      FieldName = 'DATACAD'
    end
    object cdsClientesENDCEP: TStringField
      FieldName = 'ENDCEP'
      Size = 16
    end
    object cdsClientesENDNUMERO: TStringField
      FieldName = 'ENDNUMERO'
      Size = 16
    end
    object cdsClientesENDLOGRAD: TStringField
      FieldName = 'ENDLOGRAD'
      Size = 64
    end
    object cdsClientesENDBAIRRO: TStringField
      FieldName = 'ENDBAIRRO'
      Size = 32
    end
    object cdsClientesENDCOMPLE: TStringField
      FieldName = 'ENDCOMPLE'
      Size = 32
    end
    object cdsClientesCONTATO: TStringField
      FieldName = 'CONTATO'
      Size = 64
    end
    object cdsClientesFONECOM: TStringField
      FieldName = 'FONECOM'
      Size = 16
    end
    object cdsClientesFONECEL: TStringField
      FieldName = 'FONECEL'
      Size = 16
    end
    object cdsClientesFONEFAX: TStringField
      FieldName = 'FONEFAX'
      Size = 16
    end
    object cdsClientesEMAILCONT: TStringField
      FieldName = 'EMAILCONT'
      Size = 64
    end
    object cdsClientesEMAILNFE: TStringField
      FieldName = 'EMAILNFE'
      Size = 64
    end
    object cdsClientesOBSERVACAO: TStringField
      FieldName = 'OBSERVACAO'
      Size = 128
    end
    object cdsClientesDATAINSSISPREV: TIntegerField
      FieldName = 'DATAINSSISPREV'
    end
    object cdsClientesBRIGADA: TIntegerField
      FieldName = 'BRIGADA'
    end
    object cdsClientesRESPBRIG: TStringField
      FieldName = 'RESPBRIG'
      Size = 32
    end
    object cdsClientesCODRISCO: TIntegerField
      FieldName = 'CODRISCO'
    end
    object cdsClientesCODCIDADE: TSmallintField
      FieldName = 'CODCIDADE'
    end
    object cdsClientesCODSISPREV: TSmallintField
      FieldName = 'CODSISPREV'
    end
    object cdsClientesCODENDTIPO: TSmallintField
      FieldName = 'CODENDTIPO'
    end
    object cdsClientesCODATUA: TSmallintField
      FieldName = 'CODATUA'
    end
  end
  object sdsClientes_V: TSQLDataSet
    SchemaName = 'sysdba'
    CommandText = 'SELECT * FROM TB_CLIENTE_V ORDER BY CODCLI'
    MaxBlobSize = -1
    Params = <>
    SQLConnection = conexaoDB
    Left = 40
    Top = 112
    object sdsClientes_VCODCLI: TSmallintField
      FieldName = 'CODCLI'
    end
    object sdsClientes_VCNPJ: TStringField
      FieldName = 'CNPJ'
      Size = 18
    end
    object sdsClientes_VRAZAO: TStringField
      FieldName = 'RAZAO'
      Size = 256
    end
    object sdsClientes_VFANTASIA: TStringField
      FieldName = 'FANTASIA'
      Size = 256
    end
    object sdsClientes_VDATACAD: TDateField
      FieldName = 'DATACAD'
    end
    object sdsClientes_VENDCEP: TStringField
      FieldName = 'ENDCEP'
      Size = 16
    end
    object sdsClientes_VENDNUMERO: TStringField
      FieldName = 'ENDNUMERO'
      Size = 16
    end
    object sdsClientes_VENDLOGRAD: TStringField
      FieldName = 'ENDLOGRAD'
      Size = 64
    end
    object sdsClientes_VENDBAIRRO: TStringField
      FieldName = 'ENDBAIRRO'
      Size = 32
    end
    object sdsClientes_VENDCOMPLE: TStringField
      FieldName = 'ENDCOMPLE'
      Size = 32
    end
    object sdsClientes_VCONTATO: TStringField
      FieldName = 'CONTATO'
      Size = 64
    end
    object sdsClientes_VFONECOM: TStringField
      FieldName = 'FONECOM'
      Size = 16
    end
    object sdsClientes_VFONECEL: TStringField
      FieldName = 'FONECEL'
      Size = 16
    end
    object sdsClientes_VFONEFAX: TStringField
      FieldName = 'FONEFAX'
      Size = 16
    end
    object sdsClientes_VEMAILCONT: TStringField
      FieldName = 'EMAILCONT'
      Size = 64
    end
    object sdsClientes_VEMAILNFE: TStringField
      FieldName = 'EMAILNFE'
      Size = 64
    end
    object sdsClientes_VOBSERVACAO: TStringField
      FieldName = 'OBSERVACAO'
      Size = 128
    end
    object sdsClientes_VDATAINSSISPREV: TIntegerField
      FieldName = 'DATAINSSISPREV'
    end
    object sdsClientes_VBRIGADA: TStringField
      FieldName = 'BRIGADA'
      FixedChar = True
      Size = 3
    end
    object sdsClientes_VRESPBRIG: TStringField
      FieldName = 'RESPBRIG'
      Size = 32
    end
    object sdsClientes_VTIPO: TStringField
      FieldName = 'TIPO'
      Size = 32
    end
    object sdsClientes_VUF: TStringField
      FieldName = 'UF'
      FixedChar = True
      Size = 2
    end
    object sdsClientes_VESTADO: TStringField
      FieldName = 'ESTADO'
      Size = 128
    end
    object sdsClientes_VCIDADE: TStringField
      FieldName = 'CIDADE'
      Size = 128
    end
    object sdsClientes_VDESC_ATUA: TStringField
      FieldName = 'DESC_ATUA'
      Size = 64
    end
    object sdsClientes_VGRAU: TIntegerField
      FieldName = 'GRAU'
    end
    object sdsClientes_VDESC_RISCO: TStringField
      FieldName = 'DESC_RISCO'
      Size = 32
    end
    object sdsClientes_VDESC_SISPREV: TStringField
      FieldName = 'DESC_SISPREV'
      Size = 64
    end
  end
  object dsClientes_V: TDataSetProvider
    DataSet = sdsClientes_V
    Left = 152
    Top = 112
  end
  object cdsClientes_V: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dsClientes_V'
    Left = 264
    Top = 112
    object cdsClientes_VCODCLI: TSmallintField
      FieldName = 'CODCLI'
    end
    object cdsClientes_VCNPJ: TStringField
      FieldName = 'CNPJ'
      Size = 18
    end
    object cdsClientes_VRAZAO: TStringField
      FieldName = 'RAZAO'
      Size = 256
    end
    object cdsClientes_VFANTASIA: TStringField
      FieldName = 'FANTASIA'
      Size = 256
    end
    object cdsClientes_VDATACAD: TDateField
      FieldName = 'DATACAD'
    end
    object cdsClientes_VENDCEP: TStringField
      FieldName = 'ENDCEP'
      Size = 16
    end
    object cdsClientes_VENDNUMERO: TStringField
      FieldName = 'ENDNUMERO'
      Size = 16
    end
    object cdsClientes_VENDLOGRAD: TStringField
      FieldName = 'ENDLOGRAD'
      Size = 64
    end
    object cdsClientes_VENDBAIRRO: TStringField
      FieldName = 'ENDBAIRRO'
      Size = 32
    end
    object cdsClientes_VENDCOMPLE: TStringField
      FieldName = 'ENDCOMPLE'
      Size = 32
    end
    object cdsClientes_VCONTATO: TStringField
      FieldName = 'CONTATO'
      Size = 64
    end
    object cdsClientes_VFONECOM: TStringField
      FieldName = 'FONECOM'
      Size = 16
    end
    object cdsClientes_VFONECEL: TStringField
      FieldName = 'FONECEL'
      Size = 16
    end
    object cdsClientes_VFONEFAX: TStringField
      FieldName = 'FONEFAX'
      Size = 16
    end
    object cdsClientes_VEMAILCONT: TStringField
      FieldName = 'EMAILCONT'
      Size = 64
    end
    object cdsClientes_VEMAILNFE: TStringField
      FieldName = 'EMAILNFE'
      Size = 64
    end
    object cdsClientes_VOBSERVACAO: TStringField
      FieldName = 'OBSERVACAO'
      Size = 128
    end
    object cdsClientes_VDATAINSSISPREV: TIntegerField
      FieldName = 'DATAINSSISPREV'
    end
    object cdsClientes_VBRIGADA: TStringField
      FieldName = 'BRIGADA'
      FixedChar = True
      Size = 3
    end
    object cdsClientes_VRESPBRIG: TStringField
      FieldName = 'RESPBRIG'
      Size = 32
    end
    object cdsClientes_VTIPO: TStringField
      FieldName = 'TIPO'
      Size = 32
    end
    object cdsClientes_VUF: TStringField
      FieldName = 'UF'
      FixedChar = True
      Size = 2
    end
    object cdsClientes_VESTADO: TStringField
      FieldName = 'ESTADO'
      Size = 128
    end
    object cdsClientes_VCIDADE: TStringField
      FieldName = 'CIDADE'
      Size = 128
    end
    object cdsClientes_VDESC_ATUA: TStringField
      FieldName = 'DESC_ATUA'
      Size = 64
    end
    object cdsClientes_VGRAU: TIntegerField
      FieldName = 'GRAU'
    end
    object cdsClientes_VDESC_RISCO: TStringField
      FieldName = 'DESC_RISCO'
      Size = 32
    end
    object cdsClientes_VDESC_SISPREV: TStringField
      FieldName = 'DESC_SISPREV'
      Size = 64
    end
  end
  object sdsTipoEnd: TSQLDataSet
    SchemaName = 'sysdba'
    CommandText = 'SELECT a.CODTIPOEND, a.TIPO'#13#10'FROM TB_CLI_TIPO_END a'
    MaxBlobSize = -1
    Params = <>
    SQLConnection = conexaoDB
    Left = 752
    Top = 64
    object sdsTipoEndCODTIPOEND: TIntegerField
      FieldName = 'CODTIPOEND'
    end
    object sdsTipoEndTIPO: TStringField
      FieldName = 'TIPO'
      Size = 32
    end
  end
  object dsTipoEnd: TDataSetProvider
    DataSet = sdsTipoEnd
    Left = 864
    Top = 64
  end
  object cdsTipoEnd: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dsTipoEnd'
    Left = 976
    Top = 64
    object cdsTipoEndCODTIPOEND: TIntegerField
      FieldName = 'CODTIPOEND'
    end
    object cdsTipoEndTIPO: TStringField
      FieldName = 'TIPO'
      Size = 32
    end
  end
  object sdsSisPrev: TSQLDataSet
    SchemaName = 'sysdba'
    CommandText = 'SELECT a.CODSISPREV, a.DESCRICAO'#13#10'FROM TB_CLI_SISPREV a'
    MaxBlobSize = -1
    Params = <>
    SQLConnection = conexaoDB
    Left = 752
    Top = 120
    object sdsSisPrevCODSISPREV: TIntegerField
      FieldName = 'CODSISPREV'
      Required = True
    end
    object sdsSisPrevDESCRICAO: TStringField
      FieldName = 'DESCRICAO'
      Size = 64
    end
  end
  object dsSisPrev: TDataSetProvider
    DataSet = sdsSisPrev
    Left = 864
    Top = 120
  end
  object cdsSisPrev: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dsSisPrev'
    Left = 976
    Top = 120
    object cdsSisPrevCODSISPREV: TIntegerField
      FieldName = 'CODSISPREV'
    end
    object cdsSisPrevDESCRICAO: TStringField
      FieldName = 'DESCRICAO'
      Size = 64
    end
  end
  object sdsEstoque_V: TSQLDataSet
    SchemaName = 'sysdba'
    CommandText = 
      'SELECT a.CODESTOQUE, a.DESCESTOQUE, a.VALORESTOQUE, a.QTDESTOQUE' +
      ', a.QTDMINESTOQUE, a.BARRASESTOQUE, a.SERIALESTOQUE, a.ULTCOMPES' +
      'TOQUE, a.FABRICESTOQUE'#13#10'FROM TB_ESTOQUE_V a'
    MaxBlobSize = -1
    Params = <>
    SQLConnection = conexaoDB
    Left = 40
    Top = 328
    object sdsEstoque_VCODESTOQUE: TIntegerField
      FieldName = 'CODESTOQUE'
    end
    object sdsEstoque_VDESCESTOQUE: TStringField
      FieldName = 'DESCESTOQUE'
      Size = 64
    end
    object sdsEstoque_VVALORESTOQUE: TFMTBCDField
      FieldName = 'VALORESTOQUE'
      Precision = 15
      Size = 4
    end
    object sdsEstoque_VQTDESTOQUE: TIntegerField
      FieldName = 'QTDESTOQUE'
    end
    object sdsEstoque_VQTDMINESTOQUE: TIntegerField
      FieldName = 'QTDMINESTOQUE'
    end
    object sdsEstoque_VBARRASESTOQUE: TStringField
      FieldName = 'BARRASESTOQUE'
      Size = 64
    end
    object sdsEstoque_VSERIALESTOQUE: TStringField
      FieldName = 'SERIALESTOQUE'
      Size = 64
    end
    object sdsEstoque_VULTCOMPESTOQUE: TDateField
      FieldName = 'ULTCOMPESTOQUE'
    end
    object sdsEstoque_VFABRICESTOQUE: TStringField
      FieldName = 'FABRICESTOQUE'
      Size = 64
    end
  end
  object dsEstoque_V: TDataSetProvider
    DataSet = sdsEstoque_V
    Left = 152
    Top = 328
  end
  object cdsEstoque_V: TClientDataSet
    Aggregates = <>
    FilterOptions = [foCaseInsensitive]
    Params = <>
    ProviderName = 'dsEstoque_V'
    Left = 264
    Top = 328
    object cdsEstoque_VCODESTOQUE: TIntegerField
      FieldName = 'CODESTOQUE'
    end
    object cdsEstoque_VDESCESTOQUE: TStringField
      FieldName = 'DESCESTOQUE'
      Size = 64
    end
    object cdsEstoque_VVALORESTOQUE: TFMTBCDField
      FieldName = 'VALORESTOQUE'
      Precision = 15
      Size = 4
    end
    object cdsEstoque_VQTDESTOQUE: TIntegerField
      FieldName = 'QTDESTOQUE'
    end
    object cdsEstoque_VQTDMINESTOQUE: TIntegerField
      FieldName = 'QTDMINESTOQUE'
    end
    object cdsEstoque_VBARRASESTOQUE: TStringField
      FieldName = 'BARRASESTOQUE'
      Size = 64
    end
    object cdsEstoque_VSERIALESTOQUE: TStringField
      FieldName = 'SERIALESTOQUE'
      Size = 64
    end
    object cdsEstoque_VULTCOMPESTOQUE: TDateField
      FieldName = 'ULTCOMPESTOQUE'
    end
    object cdsEstoque_VFABRICESTOQUE: TStringField
      FieldName = 'FABRICESTOQUE'
      Size = 64
    end
  end
  object sdsCidade: TSQLDataSet
    SchemaName = 'sysdba'
    CommandText = 'SELECT a.CODCIDADE, a.NOME, a.CODESTADO'#13#10'FROM TB_CLI_CIDADE a'
    MaxBlobSize = -1
    Params = <>
    SQLConnection = conexaoDB
    Left = 752
    Top = 176
    object sdsCidadeCODCIDADE: TIntegerField
      FieldName = 'CODCIDADE'
      Required = True
    end
    object sdsCidadeNOME: TStringField
      FieldName = 'NOME'
      Size = 128
    end
    object sdsCidadeCODESTADO: TSmallintField
      FieldName = 'CODESTADO'
    end
  end
  object dsCidade: TDataSetProvider
    DataSet = sdsCidade
    Left = 864
    Top = 176
  end
  object cdsCidade: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dsCidade'
    Left = 976
    Top = 176
    object cdsCidadeCODCIDADE: TIntegerField
      FieldName = 'CODCIDADE'
    end
    object cdsCidadeNOME: TStringField
      FieldName = 'NOME'
      Size = 128
    end
    object cdsCidadeCODESTADO: TSmallintField
      FieldName = 'CODESTADO'
    end
  end
  object sdsEstado: TSQLDataSet
    SchemaName = 'sysdba'
    CommandText = 'SELECT a.CODESTADO, a.NOME, a.UF'#13#10'FROM TB_CLI_ESTADO a'
    MaxBlobSize = -1
    Params = <>
    SQLConnection = conexaoDB
    Left = 752
    Top = 232
    object sdsEstadoCODESTADO: TIntegerField
      FieldName = 'CODESTADO'
      Required = True
    end
    object sdsEstadoNOME: TStringField
      FieldName = 'NOME'
      Size = 128
    end
    object sdsEstadoUF: TStringField
      FieldName = 'UF'
      FixedChar = True
      Size = 2
    end
  end
  object dsEstado: TDataSetProvider
    DataSet = sdsEstado
    Left = 864
    Top = 232
  end
  object cdsEstado: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dsEstado'
    Left = 976
    Top = 232
    object cdsEstadoCODESTADO: TIntegerField
      FieldName = 'CODESTADO'
    end
    object cdsEstadoNOME: TStringField
      FieldName = 'NOME'
      Size = 128
    end
    object cdsEstadoUF: TStringField
      FieldName = 'UF'
      FixedChar = True
      Size = 2
    end
  end
  object sdsAtuacao: TSQLDataSet
    SchemaName = 'sysdba'
    CommandText = 'SELECT a.CODATUA, a.DESCRICAO'#13#10'FROM TB_CLI_ATUACAO a'
    MaxBlobSize = -1
    Params = <>
    SQLConnection = conexaoDB
    Left = 752
    Top = 288
    object sdsAtuacaoCODATUA: TIntegerField
      FieldName = 'CODATUA'
      Required = True
    end
    object sdsAtuacaoDESCRICAO: TStringField
      FieldName = 'DESCRICAO'
      Size = 64
    end
  end
  object dsAtuacao: TDataSetProvider
    DataSet = sdsAtuacao
    Left = 864
    Top = 288
  end
  object cdsAtuacao: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dsAtuacao'
    Left = 976
    Top = 288
    object cdsAtuacaoCODATUA: TIntegerField
      FieldName = 'CODATUA'
      Required = True
    end
    object cdsAtuacaoDESCRICAO: TStringField
      FieldName = 'DESCRICAO'
      Size = 64
    end
  end
  object sdsRisco: TSQLDataSet
    SchemaName = 'sysdba'
    CommandText = 'SELECT a.CODRISCO, a.GRAU, a.DESCRICAO'#13#10'FROM TB_CLI_RISCO a'
    MaxBlobSize = -1
    Params = <>
    SQLConnection = conexaoDB
    Left = 752
    Top = 344
    object sdsRiscoCODRISCO: TIntegerField
      FieldName = 'CODRISCO'
      Required = True
    end
    object sdsRiscoGRAU: TIntegerField
      FieldName = 'GRAU'
    end
    object sdsRiscoDESCRICAO: TStringField
      FieldName = 'DESCRICAO'
      Size = 32
    end
  end
  object dsRisco: TDataSetProvider
    DataSet = sdsRisco
    Left = 864
    Top = 344
  end
  object cdsRisco: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dsRisco'
    Left = 976
    Top = 344
    object cdsRiscoCODRISCO: TIntegerField
      FieldName = 'CODRISCO'
    end
    object cdsRiscoGRAU: TIntegerField
      FieldName = 'GRAU'
    end
    object cdsRiscoDESCRICAO: TStringField
      FieldName = 'DESCRICAO'
      Size = 32
    end
  end
  object sdsMangueiras: TSQLDataSet
    SchemaName = 'sysdba'
    CommandText = 
      'SELECT a.CODMANG, a.NUMERO, a.DIAMETRO, a.COMPREAL, a.COMPNOM, a' +
      '.CODTIPO, a.MARCADUTO, a.MARCAUNIAO, a.CARCACA, a.COMPLUVA, a.DA' +
      'TAFAB, a.DATAULTMANUT, a.DATAPRXMANUT, a.DATAULTISP, a.DATAPRXIS' +
      'P, a.FABRICANTE, a.CODLACRE, a.CODCLI, a.USO'#13#10'FROM TB_MANGUEIRA ' +
      'a'
    MaxBlobSize = -1
    Params = <>
    SQLConnection = conexaoDB
    Left = 400
    Top = 120
    object sdsMangueirasCODMANG: TIntegerField
      FieldName = 'CODMANG'
    end
    object sdsMangueirasNUMERO: TIntegerField
      FieldName = 'NUMERO'
    end
    object sdsMangueirasDIAMETRO: TFMTBCDField
      FieldName = 'DIAMETRO'
      Precision = 15
      Size = 4
    end
    object sdsMangueirasCOMPREAL: TFMTBCDField
      FieldName = 'COMPREAL'
      DisplayFormat = '##0.00'
      Precision = 15
      Size = 4
    end
    object sdsMangueirasCOMPNOM: TFMTBCDField
      FieldName = 'COMPNOM'
      DisplayFormat = '##0.00'
      Precision = 15
      Size = 4
    end
    object sdsMangueirasCODTIPO: TIntegerField
      FieldName = 'CODTIPO'
    end
    object sdsMangueirasMARCADUTO: TStringField
      FieldName = 'MARCADUTO'
      Size = 32
    end
    object sdsMangueirasMARCAUNIAO: TStringField
      FieldName = 'MARCAUNIAO'
      Size = 32
    end
    object sdsMangueirasCARCACA: TStringField
      FieldName = 'CARCACA'
      Size = 32
    end
    object sdsMangueirasCOMPLUVA: TFMTBCDField
      FieldName = 'COMPLUVA'
      DisplayFormat = '##0.00'
      Precision = 15
      Size = 4
    end
    object sdsMangueirasDATAFAB: TDateField
      FieldName = 'DATAFAB'
    end
    object sdsMangueirasDATAULTMANUT: TDateField
      FieldName = 'DATAULTMANUT'
    end
    object sdsMangueirasDATAPRXMANUT: TDateField
      FieldName = 'DATAPRXMANUT'
    end
    object sdsMangueirasDATAULTISP: TDateField
      FieldName = 'DATAULTISP'
    end
    object sdsMangueirasDATAPRXISP: TDateField
      FieldName = 'DATAPRXISP'
    end
    object sdsMangueirasFABRICANTE: TStringField
      FieldName = 'FABRICANTE'
      Size = 64
    end
    object sdsMangueirasCODLACRE: TIntegerField
      FieldName = 'CODLACRE'
    end
    object sdsMangueirasCODCLI: TSmallintField
      FieldName = 'CODCLI'
    end
    object sdsMangueirasUSO: TIntegerField
      FieldName = 'USO'
    end
  end
  object dsMangueiras: TDataSetProvider
    DataSet = sdsMangueiras
    Left = 512
    Top = 120
  end
  object cdsMangueiras: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dsMangueiras'
    OnReconcileError = cdsMangueirasReconcileError
    Left = 624
    Top = 120
    object cdsMangueirasCODMANG: TIntegerField
      FieldName = 'CODMANG'
    end
    object cdsMangueirasNUMERO: TIntegerField
      FieldName = 'NUMERO'
    end
    object cdsMangueirasDIAMETRO: TFMTBCDField
      FieldName = 'DIAMETRO'
      Precision = 15
      Size = 4
    end
    object cdsMangueirasCOMPREAL: TFMTBCDField
      FieldName = 'COMPREAL'
      DisplayFormat = '##0.00'
      Precision = 15
      Size = 4
    end
    object cdsMangueirasCOMPNOM: TFMTBCDField
      FieldName = 'COMPNOM'
      DisplayFormat = '##0.00'
      Precision = 15
      Size = 4
    end
    object cdsMangueirasCODTIPO: TIntegerField
      FieldName = 'CODTIPO'
    end
    object cdsMangueirasMARCADUTO: TStringField
      FieldName = 'MARCADUTO'
      Size = 32
    end
    object cdsMangueirasMARCAUNIAO: TStringField
      FieldName = 'MARCAUNIAO'
      Size = 32
    end
    object cdsMangueirasCARCACA: TStringField
      FieldName = 'CARCACA'
      Size = 32
    end
    object cdsMangueirasCOMPLUVA: TFMTBCDField
      FieldName = 'COMPLUVA'
      DisplayFormat = '##0.00'
      Precision = 15
      Size = 4
    end
    object cdsMangueirasDATAFAB: TDateField
      FieldName = 'DATAFAB'
    end
    object cdsMangueirasDATAULTMANUT: TDateField
      FieldName = 'DATAULTMANUT'
    end
    object cdsMangueirasDATAPRXMANUT: TDateField
      FieldName = 'DATAPRXMANUT'
    end
    object cdsMangueirasDATAULTISP: TDateField
      FieldName = 'DATAULTISP'
    end
    object cdsMangueirasDATAPRXISP: TDateField
      FieldName = 'DATAPRXISP'
    end
    object cdsMangueirasFABRICANTE: TStringField
      FieldName = 'FABRICANTE'
      Size = 64
    end
    object cdsMangueirasCODLACRE: TIntegerField
      FieldName = 'CODLACRE'
    end
    object cdsMangueirasCODCLI: TSmallintField
      FieldName = 'CODCLI'
    end
    object cdsMangueirasUSO: TIntegerField
      FieldName = 'USO'
    end
  end
  object sdsMangueiras_V: TSQLDataSet
    SchemaName = 'sysdba'
    CommandText = 'SELECT * FROM TB_MANGUEIRAS_V a'
    MaxBlobSize = -1
    Params = <>
    SQLConnection = conexaoDB
    Left = 40
    Top = 160
    object sdsMangueiras_VRAZAO: TStringField
      FieldName = 'RAZAO'
      Size = 256
    end
    object sdsMangueiras_VCNPJ: TStringField
      FieldName = 'CNPJ'
      Size = 18
    end
    object sdsMangueiras_VCODMANG: TIntegerField
      FieldName = 'CODMANG'
    end
    object sdsMangueiras_VNUMERO: TIntegerField
      FieldName = 'NUMERO'
    end
    object sdsMangueiras_VDIAMETRO: TFMTBCDField
      FieldName = 'DIAMETRO'
      Precision = 15
      Size = 4
    end
    object sdsMangueiras_VCOMPREAL: TFMTBCDField
      FieldName = 'COMPREAL'
      Precision = 15
      Size = 4
    end
    object sdsMangueiras_VCOMPNOM: TFMTBCDField
      FieldName = 'COMPNOM'
      Precision = 15
      Size = 4
    end
    object sdsMangueiras_VCODTIPO: TIntegerField
      FieldName = 'CODTIPO'
    end
    object sdsMangueiras_VMARCADUTO: TStringField
      FieldName = 'MARCADUTO'
      Size = 32
    end
    object sdsMangueiras_VMARCAUNIAO: TStringField
      FieldName = 'MARCAUNIAO'
      Size = 32
    end
    object sdsMangueiras_VCARCACA: TStringField
      FieldName = 'CARCACA'
      Size = 32
    end
    object sdsMangueiras_VCOMPLUVA: TFMTBCDField
      FieldName = 'COMPLUVA'
      Precision = 15
      Size = 4
    end
    object sdsMangueiras_VDATAFAB: TDateField
      FieldName = 'DATAFAB'
    end
    object sdsMangueiras_VDATAPRXMANUT: TDateField
      FieldName = 'DATAPRXMANUT'
    end
    object sdsMangueiras_VDATAULTMANUT: TDateField
      FieldName = 'DATAULTMANUT'
    end
    object sdsMangueiras_VDATAULTISP: TDateField
      FieldName = 'DATAULTISP'
    end
    object sdsMangueiras_VDATAPRXISP: TDateField
      FieldName = 'DATAPRXISP'
    end
    object sdsMangueiras_VCODLACRE: TIntegerField
      FieldName = 'CODLACRE'
    end
    object sdsMangueiras_VFABRICANTE: TStringField
      FieldName = 'FABRICANTE'
      Size = 64
    end
    object sdsMangueiras_VCODCLI: TSmallintField
      FieldName = 'CODCLI'
    end
    object sdsMangueiras_VUSO: TStringField
      FieldName = 'USO'
      FixedChar = True
      Size = 3
    end
    object sdsMangueiras_VTIPO: TStringField
      FieldName = 'TIPO'
      Size = 16
    end
    object sdsMangueiras_VLACRE: TIntegerField
      FieldName = 'LACRE'
    end
    object sdsMangueiras_VDATA_LACRE: TDateField
      FieldName = 'DATA_LACRE'
    end
  end
  object dsMangueiras_V: TDataSetProvider
    DataSet = sdsMangueiras_V
    Left = 152
    Top = 160
  end
  object cdsMangueiras_V: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dsMangueiras_V'
    Left = 264
    Top = 160
    object cdsMangueiras_VRAZAO: TStringField
      FieldName = 'RAZAO'
      Size = 256
    end
    object cdsMangueiras_VCNPJ: TStringField
      FieldName = 'CNPJ'
      Size = 18
    end
    object cdsMangueiras_VCODMANG: TIntegerField
      FieldName = 'CODMANG'
    end
    object cdsMangueiras_VNUMERO: TIntegerField
      FieldName = 'NUMERO'
    end
    object cdsMangueiras_VDIAMETRO: TFMTBCDField
      FieldName = 'DIAMETRO'
      Precision = 15
      Size = 4
    end
    object cdsMangueiras_VCOMPREAL: TFMTBCDField
      FieldName = 'COMPREAL'
      Precision = 15
      Size = 4
    end
    object cdsMangueiras_VCOMPNOM: TFMTBCDField
      FieldName = 'COMPNOM'
      Precision = 15
      Size = 4
    end
    object cdsMangueiras_VCODTIPO: TIntegerField
      FieldName = 'CODTIPO'
    end
    object cdsMangueiras_VMARCADUTO: TStringField
      FieldName = 'MARCADUTO'
      Size = 32
    end
    object cdsMangueiras_VMARCAUNIAO: TStringField
      FieldName = 'MARCAUNIAO'
      Size = 32
    end
    object cdsMangueiras_VCARCACA: TStringField
      FieldName = 'CARCACA'
      Size = 32
    end
    object cdsMangueiras_VCOMPLUVA: TFMTBCDField
      FieldName = 'COMPLUVA'
      Precision = 15
      Size = 4
    end
    object cdsMangueiras_VDATAFAB: TDateField
      FieldName = 'DATAFAB'
    end
    object cdsMangueiras_VDATAPRXMANUT: TDateField
      FieldName = 'DATAPRXMANUT'
    end
    object cdsMangueiras_VDATAULTMANUT: TDateField
      FieldName = 'DATAULTMANUT'
    end
    object cdsMangueiras_VDATAULTISP: TDateField
      FieldName = 'DATAULTISP'
    end
    object cdsMangueiras_VDATAPRXISP: TDateField
      FieldName = 'DATAPRXISP'
    end
    object cdsMangueiras_VCODLACRE: TIntegerField
      FieldName = 'CODLACRE'
    end
    object cdsMangueiras_VFABRICANTE: TStringField
      FieldName = 'FABRICANTE'
      Size = 64
    end
    object cdsMangueiras_VCODCLI: TSmallintField
      FieldName = 'CODCLI'
    end
    object cdsMangueiras_VUSO: TStringField
      FieldName = 'USO'
      FixedChar = True
      Size = 3
    end
    object cdsMangueiras_VTIPO: TStringField
      FieldName = 'TIPO'
      Size = 16
    end
    object cdsMangueiras_VLACRE: TIntegerField
      FieldName = 'LACRE'
    end
    object cdsMangueiras_VDATA_LACRE: TDateField
      FieldName = 'DATA_LACRE'
    end
  end
  object sdsMangLacre: TSQLDataSet
    SchemaName = 'sysdba'
    CommandText = 'SELECT * FROM TB_MANG_LACRE'
    MaxBlobSize = -1
    Params = <>
    SQLConnection = conexaoDB
    Left = 400
    Top = 176
    object sdsMangLacreCODLACRE: TIntegerField
      FieldName = 'CODLACRE'
    end
    object sdsMangLacreNUMERO: TIntegerField
      FieldName = 'NUMERO'
    end
    object sdsMangLacreDATALACRE: TDateField
      FieldName = 'DATALACRE'
    end
    object sdsMangLacreESTADO: TSmallintField
      FieldName = 'ESTADO'
    end
    object sdsMangLacreDATACADLACRE: TDateField
      FieldName = 'DATACADLACRE'
    end
    object sdsMangLacreMANGNUM: TIntegerField
      FieldName = 'MANGNUM'
    end
  end
  object dsMangLacre: TDataSetProvider
    DataSet = sdsMangLacre
    Left = 512
    Top = 176
  end
  object cdsMangLacre: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dsMangLacre'
    Left = 624
    Top = 176
    object cdsMangLacreCODLACRE: TIntegerField
      FieldName = 'CODLACRE'
    end
    object cdsMangLacreNUMERO: TIntegerField
      FieldName = 'NUMERO'
    end
    object cdsMangLacreDATALACRE: TDateField
      FieldName = 'DATALACRE'
    end
    object cdsMangLacreESTADO: TSmallintField
      FieldName = 'ESTADO'
    end
    object cdsMangLacreDATACADLACRE: TDateField
      FieldName = 'DATACADLACRE'
    end
    object cdsMangLacreMANGNUM: TIntegerField
      FieldName = 'MANGNUM'
    end
  end
  object sdsMangTipo: TSQLDataSet
    SchemaName = 'sysdba'
    CommandText = 'SELECT a.CODTIPO, a.DESCRICAO'#13#10'FROM TB_MANG_TIPO a'
    MaxBlobSize = -1
    Params = <>
    SQLConnection = conexaoDB
    Left = 400
    Top = 232
    object sdsMangTipoCODTIPO: TIntegerField
      FieldName = 'CODTIPO'
    end
    object sdsMangTipoDESCRICAO: TStringField
      FieldName = 'DESCRICAO'
      Size = 16
    end
  end
  object dsMangTipo: TDataSetProvider
    DataSet = sdsMangTipo
    Left = 512
    Top = 232
  end
  object cdsMangTipo: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dsMangTipo'
    Left = 624
    Top = 232
    object cdsMangTipoCODTIPO: TIntegerField
      FieldName = 'CODTIPO'
    end
    object cdsMangTipoDESCRICAO: TStringField
      FieldName = 'DESCRICAO'
      Size = 16
    end
  end
  object sdsAux: TSQLDataSet
    SchemaName = 'sysdba'
    MaxBlobSize = -1
    Params = <>
    Left = 104
    Top = 16
  end
  object sdsTestes_V: TSQLDataSet
    SchemaName = 'sysdba'
    CommandText = 'SELECT * FROM TB_TESTES_V a'
    MaxBlobSize = 1
    Params = <>
    SQLConnection = conexaoDB
    Left = 40
    Top = 216
    object sdsTestes_VCODTESTE: TIntegerField
      FieldName = 'CODTESTE'
    end
    object sdsTestes_VCODTECNICO: TIntegerField
      FieldName = 'CODTECNICO'
    end
    object sdsTestes_VDATAEXEC: TDateField
      FieldName = 'DATAEXEC'
    end
    object sdsTestes_VOBSTEC: TStringField
      FieldName = 'OBSTEC'
      Size = 128
    end
    object sdsTestes_VNOMECOMPLETO: TStringField
      FieldName = 'NOMECOMPLETO'
      Size = 128
    end
    object sdsTestes_VCNPJ: TStringField
      FieldName = 'CNPJ'
      Size = 18
    end
    object sdsTestes_VRAZAO: TStringField
      FieldName = 'RAZAO'
      Size = 256
    end
    object sdsTestes_VFANTASIA: TStringField
      FieldName = 'FANTASIA'
      Size = 256
    end
    object sdsTestes_VQNTMANG: TIntegerField
      FieldName = 'QNTMANG'
    end
    object sdsTestes_VCODCLI: TSmallintField
      FieldName = 'CODCLI'
    end
    object sdsTestes_VTIPO: TStringField
      FieldName = 'TIPO'
      FixedChar = True
      Size = 10
    end
    object sdsTestes_VOBSERVACAO: TMemoField
      FieldName = 'OBSERVACAO'
      BlobType = ftMemo
      Size = 1
    end
    object sdsTestes_VQNTPECA: TIntegerField
      FieldName = 'QNTPECA'
    end
  end
  object dsTestes_V: TDataSetProvider
    DataSet = sdsTestes_V
    Left = 152
    Top = 216
  end
  object cdsTestes_V: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dsTestes_V'
    Left = 264
    Top = 216
    object cdsTestes_VCODTESTE: TIntegerField
      FieldName = 'CODTESTE'
    end
    object cdsTestes_VCODTECNICO: TIntegerField
      FieldName = 'CODTECNICO'
    end
    object cdsTestes_VDATAEXEC: TDateField
      FieldName = 'DATAEXEC'
    end
    object cdsTestes_VOBSTEC: TStringField
      FieldName = 'OBSTEC'
      Size = 128
    end
    object cdsTestes_VNOMECOMPLETO: TStringField
      FieldName = 'NOMECOMPLETO'
      Size = 128
    end
    object cdsTestes_VCNPJ: TStringField
      FieldName = 'CNPJ'
      Size = 18
    end
    object cdsTestes_VRAZAO: TStringField
      FieldName = 'RAZAO'
      Size = 256
    end
    object cdsTestes_VFANTASIA: TStringField
      FieldName = 'FANTASIA'
      Size = 256
    end
    object cdsTestes_VQNTMANG: TIntegerField
      FieldName = 'QNTMANG'
    end
    object cdsTestes_VCODCLI: TSmallintField
      FieldName = 'CODCLI'
    end
    object cdsTestes_VTIPO: TStringField
      FieldName = 'TIPO'
      FixedChar = True
      Size = 10
    end
    object cdsTestes_VOBSERVACAO: TMemoField
      FieldName = 'OBSERVACAO'
      BlobType = ftMemo
      Size = 1
    end
    object cdsTestes_VQNTPECA: TIntegerField
      FieldName = 'QNTPECA'
    end
  end
  object sdsTestes: TSQLDataSet
    SchemaName = 'sysdba'
    CommandText = 'SELECT * FROM TB_TESTE a'
    MaxBlobSize = 1
    Params = <>
    SQLConnection = conexaoDB
    Left = 400
    Top = 288
    object sdsTestesCODTESTE: TIntegerField
      FieldName = 'CODTESTE'
    end
    object sdsTestesCODTECNICO: TIntegerField
      FieldName = 'CODTECNICO'
    end
    object sdsTestesDATAEXEC: TDateField
      FieldName = 'DATAEXEC'
    end
    object sdsTestesCODCLI: TSmallintField
      FieldName = 'CODCLI'
    end
    object sdsTestesTIPO: TStringField
      FieldName = 'TIPO'
      Required = True
      FixedChar = True
      Size = 1
    end
    object sdsTestesOBSERVACAO: TMemoField
      FieldName = 'OBSERVACAO'
      BlobType = ftMemo
      Size = 1
    end
  end
  object dsTestes: TDataSetProvider
    DataSet = sdsTestes
    Left = 512
    Top = 288
  end
  object cdsTestes: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dsTestes'
    Left = 624
    Top = 288
    object cdsTestesCODTESTE: TIntegerField
      FieldName = 'CODTESTE'
    end
    object cdsTestesCODTECNICO: TIntegerField
      FieldName = 'CODTECNICO'
    end
    object cdsTestesDATAEXEC: TDateField
      FieldName = 'DATAEXEC'
    end
    object cdsTestesCODCLI: TSmallintField
      FieldName = 'CODCLI'
    end
    object cdsTestesTIPO: TStringField
      FieldName = 'TIPO'
      Required = True
      FixedChar = True
      Size = 1
    end
    object cdsTestesOBSERVACAO: TMemoField
      FieldName = 'OBSERVACAO'
      BlobType = ftMemo
      Size = 1
    end
  end
  object sdsTesteAvaliacao: TSQLDataSet
    SchemaName = 'sysdba'
    CommandText = 'SELECT * FROM TB_TESTE_AVALIACAO'
    MaxBlobSize = 1
    Params = <>
    SQLConnection = conexaoDB
    Left = 400
    Top = 344
    object sdsTesteAvaliacaoCODAVALIACAO: TIntegerField
      FieldName = 'CODAVALIACAO'
    end
    object sdsTesteAvaliacaoCODTESTE: TIntegerField
      FieldName = 'CODTESTE'
    end
    object sdsTesteAvaliacaoCODMANG: TIntegerField
      FieldName = 'CODMANG'
    end
    object sdsTesteAvaliacaoMARCABNT: TStringField
      FieldName = 'MARCABNT'
      FixedChar = True
      Size = 1
    end
    object sdsTesteAvaliacaoVAZVAL: TStringField
      FieldName = 'VAZVAL'
      FixedChar = True
      Size = 1
    end
    object sdsTesteAvaliacaoACUMAGUA: TStringField
      FieldName = 'ACUMAGUA'
      FixedChar = True
      Size = 1
    end
    object sdsTesteAvaliacaoCONDADU: TStringField
      FieldName = 'CONDADU'
      FixedChar = True
      Size = 1
    end
    object sdsTesteAvaliacaoPRESMATER: TStringField
      FieldName = 'PRESMATER'
      FixedChar = True
      Size = 1
    end
    object sdsTesteAvaliacaoPRESANIM: TStringField
      FieldName = 'PRESANIM'
      FixedChar = True
      Size = 1
    end
    object sdsTesteAvaliacaoACOPUNI: TStringField
      FieldName = 'ACOPUNI'
      FixedChar = True
      Size = 1
    end
    object sdsTesteAvaliacaoANELVED: TStringField
      FieldName = 'ANELVED'
      FixedChar = True
      Size = 1
    end
    object sdsTesteAvaliacaoENSAIOHIDRO: TStringField
      FieldName = 'ENSAIOHIDRO'
      FixedChar = True
      Size = 1
    end
    object sdsTesteAvaliacaoREEMPATE: TStringField
      FieldName = 'REEMPATE'
      FixedChar = True
      Size = 1
    end
    object sdsTesteAvaliacaoSUBUNI: TStringField
      FieldName = 'SUBUNI'
      FixedChar = True
      Size = 1
    end
    object sdsTesteAvaliacaoSUBVED: TStringField
      FieldName = 'SUBVED'
      FixedChar = True
      Size = 1
    end
    object sdsTesteAvaliacaoSUBANEL: TStringField
      FieldName = 'SUBANEL'
      FixedChar = True
      Size = 1
    end
    object sdsTesteAvaliacaoNOVOENSAIOHIDRO: TStringField
      FieldName = 'NOVOENSAIOHIDRO'
      FixedChar = True
      Size = 1
    end
    object sdsTesteAvaliacaoLIMPEZA: TStringField
      FieldName = 'LIMPEZA'
      FixedChar = True
      Size = 1
    end
    object sdsTesteAvaliacaoSECAGEM: TStringField
      FieldName = 'SECAGEM'
      FixedChar = True
      Size = 1
    end
    object sdsTesteAvaliacaoRESULTADO: TStringField
      FieldName = 'RESULTADO'
      FixedChar = True
      Size = 1
    end
    object sdsTesteAvaliacaoDATAEXEC: TDateField
      FieldName = 'DATAEXEC'
    end
    object sdsTesteAvaliacaoCOMPREAL: TFMTBCDField
      FieldName = 'COMPREAL'
      DisplayFormat = '##0.00'
      Precision = 15
      Size = 4
    end
    object sdsTesteAvaliacaoCOMPLUVA: TFMTBCDField
      FieldName = 'COMPLUVA'
      DisplayFormat = '##0.00'
      Precision = 15
      Size = 4
    end
    object sdsTesteAvaliacaoCODLACRE: TIntegerField
      FieldName = 'CODLACRE'
    end
    object sdsTesteAvaliacaoCODLACREOLD: TIntegerField
      FieldName = 'CODLACREOLD'
    end
    object sdsTesteAvaliacaoOBSMANG: TMemoField
      FieldName = 'OBSMANG'
      BlobType = ftMemo
      Size = 1
    end
  end
  object dsTesteAvaliacao: TDataSetProvider
    DataSet = sdsTesteAvaliacao
    Left = 512
    Top = 344
  end
  object cdsTesteAvaliacao: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dsTesteAvaliacao'
    Left = 624
    Top = 344
    object cdsTesteAvaliacaoCODAVALIACAO: TIntegerField
      FieldName = 'CODAVALIACAO'
    end
    object cdsTesteAvaliacaoCODTESTE: TIntegerField
      FieldName = 'CODTESTE'
    end
    object cdsTesteAvaliacaoCODMANG: TIntegerField
      FieldName = 'CODMANG'
    end
    object cdsTesteAvaliacaoMARCABNT: TStringField
      FieldName = 'MARCABNT'
      FixedChar = True
      Size = 1
    end
    object cdsTesteAvaliacaoVAZVAL: TStringField
      FieldName = 'VAZVAL'
      FixedChar = True
      Size = 1
    end
    object cdsTesteAvaliacaoACUMAGUA: TStringField
      FieldName = 'ACUMAGUA'
      FixedChar = True
      Size = 1
    end
    object cdsTesteAvaliacaoCONDADU: TStringField
      FieldName = 'CONDADU'
      FixedChar = True
      Size = 1
    end
    object cdsTesteAvaliacaoPRESMATER: TStringField
      FieldName = 'PRESMATER'
      FixedChar = True
      Size = 1
    end
    object cdsTesteAvaliacaoPRESANIM: TStringField
      FieldName = 'PRESANIM'
      FixedChar = True
      Size = 1
    end
    object cdsTesteAvaliacaoACOPUNI: TStringField
      FieldName = 'ACOPUNI'
      FixedChar = True
      Size = 1
    end
    object cdsTesteAvaliacaoANELVED: TStringField
      FieldName = 'ANELVED'
      FixedChar = True
      Size = 1
    end
    object cdsTesteAvaliacaoENSAIOHIDRO: TStringField
      FieldName = 'ENSAIOHIDRO'
      FixedChar = True
      Size = 1
    end
    object cdsTesteAvaliacaoREEMPATE: TStringField
      FieldName = 'REEMPATE'
      FixedChar = True
      Size = 1
    end
    object cdsTesteAvaliacaoSUBUNI: TStringField
      FieldName = 'SUBUNI'
      FixedChar = True
      Size = 1
    end
    object cdsTesteAvaliacaoSUBVED: TStringField
      FieldName = 'SUBVED'
      FixedChar = True
      Size = 1
    end
    object cdsTesteAvaliacaoSUBANEL: TStringField
      FieldName = 'SUBANEL'
      FixedChar = True
      Size = 1
    end
    object cdsTesteAvaliacaoNOVOENSAIOHIDRO: TStringField
      FieldName = 'NOVOENSAIOHIDRO'
      FixedChar = True
      Size = 1
    end
    object cdsTesteAvaliacaoLIMPEZA: TStringField
      FieldName = 'LIMPEZA'
      FixedChar = True
      Size = 1
    end
    object cdsTesteAvaliacaoSECAGEM: TStringField
      FieldName = 'SECAGEM'
      FixedChar = True
      Size = 1
    end
    object cdsTesteAvaliacaoRESULTADO: TStringField
      FieldName = 'RESULTADO'
      FixedChar = True
      Size = 1
    end
    object cdsTesteAvaliacaoDATAEXEC: TDateField
      FieldName = 'DATAEXEC'
    end
    object cdsTesteAvaliacaoCOMPREAL: TFMTBCDField
      FieldName = 'COMPREAL'
      DisplayFormat = '##0.00'
      Precision = 15
      Size = 4
    end
    object cdsTesteAvaliacaoCOMPLUVA: TFMTBCDField
      FieldName = 'COMPLUVA'
      DisplayFormat = '##0.00'
      Precision = 15
      Size = 4
    end
    object cdsTesteAvaliacaoCODLACRE: TIntegerField
      FieldName = 'CODLACRE'
    end
    object cdsTesteAvaliacaoCODLACREOLD: TIntegerField
      FieldName = 'CODLACREOLD'
    end
    object cdsTesteAvaliacaoOBSMANG: TMemoField
      FieldName = 'OBSMANG'
      BlobType = ftMemo
      Size = 1
    end
  end
  object sdsTesteTecnico: TSQLDataSet
    SchemaName = 'sysdba'
    CommandText = 
      'SELECT a.CODTECNICO, a.NOMECOMPLETO, a.RG, a.CPF, a.OBSERVACAO'#13#10 +
      'FROM TB_TESTE_TECNICOS a'
    MaxBlobSize = -1
    Params = <>
    SQLConnection = conexaoDB
    Left = 400
    Top = 400
    object sdsTesteTecnicoCODTECNICO: TIntegerField
      FieldName = 'CODTECNICO'
    end
    object sdsTesteTecnicoNOMECOMPLETO: TStringField
      FieldName = 'NOMECOMPLETO'
      Size = 128
    end
    object sdsTesteTecnicoRG: TIntegerField
      FieldName = 'RG'
    end
    object sdsTesteTecnicoCPF: TStringField
      FieldName = 'CPF'
      Size = 14
    end
    object sdsTesteTecnicoOBSERVACAO: TStringField
      FieldName = 'OBSERVACAO'
      Size = 128
    end
  end
  object dsTesteTecnico: TDataSetProvider
    DataSet = sdsTesteTecnico
    Left = 512
    Top = 400
  end
  object cdsTesteTecnico: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dsTesteTecnico'
    OnReconcileError = cdsTesteTecnicoReconcileError
    Left = 624
    Top = 400
    object cdsTesteTecnicoCODTECNICO: TIntegerField
      FieldName = 'CODTECNICO'
    end
    object cdsTesteTecnicoNOMECOMPLETO: TStringField
      FieldName = 'NOMECOMPLETO'
      Size = 128
    end
    object cdsTesteTecnicoRG: TIntegerField
      FieldName = 'RG'
    end
    object cdsTesteTecnicoCPF: TStringField
      FieldName = 'CPF'
      Size = 14
    end
    object cdsTesteTecnicoOBSERVACAO: TStringField
      FieldName = 'OBSERVACAO'
      Size = 128
    end
  end
  object sdsTesteMangPecas: TSQLDataSet
    SchemaName = 'sysdba'
    CommandText = 
      'SELECT a.CODUSOPECAS, a.CODPECA, a.CODTESTE, a.QUANTIDADE, a.DAT' +
      'A'#13#10'FROM TB_TESTES_MANG_PECAS a'
    MaxBlobSize = -1
    Params = <>
    SQLConnection = conexaoDB
    Left = 752
    Top = 8
    object sdsTesteMangPecasCODUSOPECAS: TIntegerField
      FieldName = 'CODUSOPECAS'
    end
    object sdsTesteMangPecasCODPECA: TIntegerField
      FieldName = 'CODPECA'
    end
    object sdsTesteMangPecasCODTESTE: TIntegerField
      FieldName = 'CODTESTE'
    end
    object sdsTesteMangPecasQUANTIDADE: TIntegerField
      FieldName = 'QUANTIDADE'
    end
    object sdsTesteMangPecasDATA: TDateField
      FieldName = 'DATA'
    end
  end
  object dsTesteMangPecas: TDataSetProvider
    DataSet = sdsTesteMangPecas
    Left = 864
    Top = 8
  end
  object cdsTesteMangPecas: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dsTesteMangPecas'
    Left = 976
    Top = 8
    object cdsTesteMangPecasCODUSOPECAS: TIntegerField
      FieldName = 'CODUSOPECAS'
    end
    object cdsTesteMangPecasCODPECA: TIntegerField
      FieldName = 'CODPECA'
    end
    object cdsTesteMangPecasCODTESTE: TIntegerField
      FieldName = 'CODTESTE'
    end
    object cdsTesteMangPecasQUANTIDADE: TIntegerField
      FieldName = 'QUANTIDADE'
    end
    object cdsTesteMangPecasDATA: TDateField
      FieldName = 'DATA'
    end
  end
  object sdsPecas: TSQLDataSet
    SchemaName = 'sysdba'
    CommandText = 'SELECT a.CODPECA, a.DESCRICAO, a.PRECO'#13#10'FROM TB_TESTE_PECAS a'
    MaxBlobSize = -1
    Params = <>
    SQLConnection = conexaoDB
    Left = 400
    Top = 456
    object sdsPecasCODPECA: TIntegerField
      FieldName = 'CODPECA'
    end
    object sdsPecasDESCRICAO: TStringField
      FieldName = 'DESCRICAO'
      Size = 64
    end
    object sdsPecasPRECO: TFMTBCDField
      FieldName = 'PRECO'
      currency = True
      Precision = 15
      Size = 2
    end
  end
  object dsPecas: TDataSetProvider
    DataSet = sdsPecas
    Left = 512
    Top = 456
  end
  object cdsPecas: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dsPecas'
    Left = 624
    Top = 456
    object cdsPecasCODPECA: TIntegerField
      FieldName = 'CODPECA'
    end
    object cdsPecasDESCRICAO: TStringField
      FieldName = 'DESCRICAO'
      Size = 64
    end
    object cdsPecasPRECO: TFMTBCDField
      FieldName = 'PRECO'
      currency = True
      Precision = 15
      Size = 2
    end
  end
  object sdsAvaliacao_V: TSQLDataSet
    SchemaName = 'sysdba'
    CommandText = 'SELECT * FROM TB_AVALIACAO_V a'
    MaxBlobSize = 1
    Params = <>
    SQLConnection = conexaoDB
    Left = 40
    Top = 272
    object sdsAvaliacao_VCODMANG: TIntegerField
      FieldName = 'CODMANG'
    end
    object sdsAvaliacao_VCODTESTE: TIntegerField
      FieldName = 'CODTESTE'
    end
    object sdsAvaliacao_VCODAVALIACAO: TIntegerField
      FieldName = 'CODAVALIACAO'
    end
    object sdsAvaliacao_VFABRICANTE: TStringField
      FieldName = 'FABRICANTE'
      Size = 64
    end
    object sdsAvaliacao_VMARCABNT: TStringField
      FieldName = 'MARCABNT'
      FixedChar = True
      Size = 1
    end
    object sdsAvaliacao_VCODTIPO: TIntegerField
      FieldName = 'CODTIPO'
    end
    object sdsAvaliacao_VTIPO: TStringField
      FieldName = 'TIPO'
      Size = 16
    end
    object sdsAvaliacao_VDIAMETRO: TFMTBCDField
      FieldName = 'DIAMETRO'
      Precision = 15
      Size = 4
    end
    object sdsAvaliacao_VCOMPNOM: TFMTBCDField
      FieldName = 'COMPNOM'
      Precision = 15
      Size = 4
    end
    object sdsAvaliacao_VDATAFAB: TDateField
      FieldName = 'DATAFAB'
    end
    object sdsAvaliacao_VDATAULTMANUT: TDateField
      FieldName = 'DATAULTMANUT'
    end
    object sdsAvaliacao_VDATAPRXMANUT: TDateField
      FieldName = 'DATAPRXMANUT'
    end
    object sdsAvaliacao_VVAZVAL: TStringField
      FieldName = 'VAZVAL'
      FixedChar = True
      Size = 1
    end
    object sdsAvaliacao_VACUMAGUA: TStringField
      FieldName = 'ACUMAGUA'
      FixedChar = True
      Size = 1
    end
    object sdsAvaliacao_VPRESMATER: TStringField
      FieldName = 'PRESMATER'
      FixedChar = True
      Size = 1
    end
    object sdsAvaliacao_VCONDADU: TStringField
      FieldName = 'CONDADU'
      FixedChar = True
      Size = 1
    end
    object sdsAvaliacao_VPRESANIM: TStringField
      FieldName = 'PRESANIM'
      FixedChar = True
      Size = 1
    end
    object sdsAvaliacao_VACOPUNI: TStringField
      FieldName = 'ACOPUNI'
      FixedChar = True
      Size = 1
    end
    object sdsAvaliacao_VANELVED: TStringField
      FieldName = 'ANELVED'
      FixedChar = True
      Size = 1
    end
    object sdsAvaliacao_VENSAIOHIDRO: TStringField
      FieldName = 'ENSAIOHIDRO'
      FixedChar = True
      Size = 1
    end
    object sdsAvaliacao_VREEMPATE: TStringField
      FieldName = 'REEMPATE'
      FixedChar = True
      Size = 1
    end
    object sdsAvaliacao_VSUBUNI: TStringField
      FieldName = 'SUBUNI'
      FixedChar = True
      Size = 1
    end
    object sdsAvaliacao_VSUBVED: TStringField
      FieldName = 'SUBVED'
      FixedChar = True
      Size = 1
    end
    object sdsAvaliacao_VSUBANEL: TStringField
      FieldName = 'SUBANEL'
      FixedChar = True
      Size = 1
    end
    object sdsAvaliacao_VNOVOENSAIOHIDRO: TStringField
      FieldName = 'NOVOENSAIOHIDRO'
      FixedChar = True
      Size = 1
    end
    object sdsAvaliacao_VLIMPEZA: TStringField
      FieldName = 'LIMPEZA'
      FixedChar = True
      Size = 1
    end
    object sdsAvaliacao_VSECAGEM: TStringField
      FieldName = 'SECAGEM'
      FixedChar = True
      Size = 1
    end
    object sdsAvaliacao_VRESULTADO: TStringField
      FieldName = 'RESULTADO'
      FixedChar = True
      Size = 1
    end
    object sdsAvaliacao_VCOMPREAL: TFMTBCDField
      FieldName = 'COMPREAL'
      Precision = 15
      Size = 4
    end
    object sdsAvaliacao_VCOMPLUVA: TFMTBCDField
      FieldName = 'COMPLUVA'
      Precision = 15
      Size = 4
    end
    object sdsAvaliacao_VDATAEXEC: TDateField
      FieldName = 'DATAEXEC'
    end
    object sdsAvaliacao_VCODLACRE: TIntegerField
      FieldName = 'CODLACRE'
    end
    object sdsAvaliacao_VNUMERO: TIntegerField
      FieldName = 'NUMERO'
    end
    object sdsAvaliacao_VNUMMANG: TIntegerField
      FieldName = 'NUMMANG'
    end
    object sdsAvaliacao_VCODLACREOLD: TIntegerField
      FieldName = 'CODLACREOLD'
    end
    object sdsAvaliacao_VNUMEROOLD: TIntegerField
      FieldName = 'NUMEROOLD'
    end
    object sdsAvaliacao_VOBSMANG: TMemoField
      FieldName = 'OBSMANG'
      BlobType = ftMemo
      Size = 1
    end
    object sdsAvaliacao_VDATAULTISP: TDateField
      FieldName = 'DATAULTISP'
    end
    object sdsAvaliacao_VDATAPRXISP: TDateField
      FieldName = 'DATAPRXISP'
    end
  end
  object dsAvaliacao_V: TDataSetProvider
    DataSet = sdsAvaliacao_V
    Left = 152
    Top = 272
  end
  object cdsAvaliacao_V: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dsAvaliacao_V'
    Left = 264
    Top = 272
    object cdsAvaliacao_VCODMANG: TIntegerField
      FieldName = 'CODMANG'
    end
    object cdsAvaliacao_VCODTESTE: TIntegerField
      FieldName = 'CODTESTE'
    end
    object cdsAvaliacao_VCODAVALIACAO: TIntegerField
      FieldName = 'CODAVALIACAO'
    end
    object cdsAvaliacao_VFABRICANTE: TStringField
      FieldName = 'FABRICANTE'
      Size = 64
    end
    object cdsAvaliacao_VMARCABNT: TStringField
      FieldName = 'MARCABNT'
      FixedChar = True
      Size = 1
    end
    object cdsAvaliacao_VCODTIPO: TIntegerField
      FieldName = 'CODTIPO'
    end
    object cdsAvaliacao_VTIPO: TStringField
      FieldName = 'TIPO'
      Size = 16
    end
    object cdsAvaliacao_VDIAMETRO: TFMTBCDField
      FieldName = 'DIAMETRO'
      Precision = 15
      Size = 4
    end
    object cdsAvaliacao_VCOMPNOM: TFMTBCDField
      FieldName = 'COMPNOM'
      Precision = 15
      Size = 4
    end
    object cdsAvaliacao_VDATAFAB: TDateField
      FieldName = 'DATAFAB'
    end
    object cdsAvaliacao_VDATAULTMANUT: TDateField
      FieldName = 'DATAULTMANUT'
    end
    object cdsAvaliacao_VDATAPRXMANUT: TDateField
      FieldName = 'DATAPRXMANUT'
    end
    object cdsAvaliacao_VVAZVAL: TStringField
      FieldName = 'VAZVAL'
      FixedChar = True
      Size = 1
    end
    object cdsAvaliacao_VACUMAGUA: TStringField
      FieldName = 'ACUMAGUA'
      FixedChar = True
      Size = 1
    end
    object cdsAvaliacao_VPRESMATER: TStringField
      FieldName = 'PRESMATER'
      FixedChar = True
      Size = 1
    end
    object cdsAvaliacao_VCONDADU: TStringField
      FieldName = 'CONDADU'
      FixedChar = True
      Size = 1
    end
    object cdsAvaliacao_VPRESANIM: TStringField
      FieldName = 'PRESANIM'
      FixedChar = True
      Size = 1
    end
    object cdsAvaliacao_VACOPUNI: TStringField
      FieldName = 'ACOPUNI'
      FixedChar = True
      Size = 1
    end
    object cdsAvaliacao_VANELVED: TStringField
      FieldName = 'ANELVED'
      FixedChar = True
      Size = 1
    end
    object cdsAvaliacao_VENSAIOHIDRO: TStringField
      FieldName = 'ENSAIOHIDRO'
      FixedChar = True
      Size = 1
    end
    object cdsAvaliacao_VREEMPATE: TStringField
      FieldName = 'REEMPATE'
      FixedChar = True
      Size = 1
    end
    object cdsAvaliacao_VSUBUNI: TStringField
      FieldName = 'SUBUNI'
      FixedChar = True
      Size = 1
    end
    object cdsAvaliacao_VSUBVED: TStringField
      FieldName = 'SUBVED'
      FixedChar = True
      Size = 1
    end
    object cdsAvaliacao_VSUBANEL: TStringField
      FieldName = 'SUBANEL'
      FixedChar = True
      Size = 1
    end
    object cdsAvaliacao_VNOVOENSAIOHIDRO: TStringField
      FieldName = 'NOVOENSAIOHIDRO'
      FixedChar = True
      Size = 1
    end
    object cdsAvaliacao_VLIMPEZA: TStringField
      FieldName = 'LIMPEZA'
      FixedChar = True
      Size = 1
    end
    object cdsAvaliacao_VSECAGEM: TStringField
      FieldName = 'SECAGEM'
      FixedChar = True
      Size = 1
    end
    object cdsAvaliacao_VRESULTADO: TStringField
      FieldName = 'RESULTADO'
      FixedChar = True
      Size = 1
    end
    object cdsAvaliacao_VCOMPREAL: TFMTBCDField
      FieldName = 'COMPREAL'
      DisplayFormat = '##0.00'
      Precision = 15
      Size = 4
    end
    object cdsAvaliacao_VCOMPLUVA: TFMTBCDField
      FieldName = 'COMPLUVA'
      DisplayFormat = '##0.00'
      Precision = 15
      Size = 4
    end
    object cdsAvaliacao_VDATAEXEC: TDateField
      FieldName = 'DATAEXEC'
    end
    object cdsAvaliacao_VCODLACRE: TIntegerField
      FieldName = 'CODLACRE'
    end
    object cdsAvaliacao_VNUMERO: TIntegerField
      FieldName = 'NUMERO'
    end
    object cdsAvaliacao_VNUMMANG: TIntegerField
      FieldName = 'NUMMANG'
    end
    object cdsAvaliacao_VCODLACREOLD: TIntegerField
      FieldName = 'CODLACREOLD'
    end
    object cdsAvaliacao_VNUMEROOLD: TIntegerField
      FieldName = 'NUMEROOLD'
    end
    object cdsAvaliacao_VOBSMANG: TMemoField
      FieldName = 'OBSMANG'
      BlobType = ftMemo
      Size = 1
    end
    object cdsAvaliacao_VDATAULTISP: TDateField
      FieldName = 'DATAULTISP'
    end
    object cdsAvaliacao_VDATAPRXISP: TDateField
      FieldName = 'DATAPRXISP'
    end
  end
  object sdsEstoque: TSQLDataSet
    SchemaName = 'sysdba'
    CommandText = 
      'SELECT a.CODESTOQUE, a.DESCESTOQUE, a.VALORESTOQUE, a.QTDESTOQUE' +
      ', a.QTDMINESTOQUE, a.BARRASESTOQUE, a.SERIALESTOQUE, a.ULTCOMPES' +
      'TOQUE, a.FABRICESTOQUE'#13#10'FROM TB_ESTOQUE a'
    MaxBlobSize = -1
    Params = <>
    SQLConnection = conexaoDB
    Left = 400
    Top = 8
    object sdsEstoqueCODESTOQUE: TIntegerField
      FieldName = 'CODESTOQUE'
    end
    object sdsEstoqueDESCESTOQUE: TStringField
      FieldName = 'DESCESTOQUE'
      Size = 64
    end
    object sdsEstoqueVALORESTOQUE: TFMTBCDField
      FieldName = 'VALORESTOQUE'
      currency = True
      Precision = 15
      Size = 4
    end
    object sdsEstoqueQTDESTOQUE: TIntegerField
      FieldName = 'QTDESTOQUE'
    end
    object sdsEstoqueQTDMINESTOQUE: TIntegerField
      FieldName = 'QTDMINESTOQUE'
    end
    object sdsEstoqueBARRASESTOQUE: TStringField
      FieldName = 'BARRASESTOQUE'
      Size = 64
    end
    object sdsEstoqueSERIALESTOQUE: TStringField
      FieldName = 'SERIALESTOQUE'
      Size = 64
    end
    object sdsEstoqueULTCOMPESTOQUE: TDateField
      FieldName = 'ULTCOMPESTOQUE'
    end
    object sdsEstoqueFABRICESTOQUE: TStringField
      FieldName = 'FABRICESTOQUE'
      Size = 64
    end
  end
  object dsEstoque: TDataSetProvider
    DataSet = sdsEstoque
    Left = 512
    Top = 8
  end
  object cdsEstoque: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dsEstoque'
    OnReconcileError = cdsEstoqueReconcileError
    Left = 624
    Top = 8
    object cdsEstoqueCODESTOQUE: TIntegerField
      FieldName = 'CODESTOQUE'
    end
    object cdsEstoqueDESCESTOQUE: TStringField
      FieldName = 'DESCESTOQUE'
      Size = 64
    end
    object cdsEstoqueVALORESTOQUE: TFMTBCDField
      FieldName = 'VALORESTOQUE'
      currency = True
      Precision = 15
      Size = 4
    end
    object cdsEstoqueQTDESTOQUE: TIntegerField
      FieldName = 'QTDESTOQUE'
    end
    object cdsEstoqueQTDMINESTOQUE: TIntegerField
      FieldName = 'QTDMINESTOQUE'
    end
    object cdsEstoqueBARRASESTOQUE: TStringField
      FieldName = 'BARRASESTOQUE'
      Size = 64
    end
    object cdsEstoqueSERIALESTOQUE: TStringField
      FieldName = 'SERIALESTOQUE'
      Size = 64
    end
    object cdsEstoqueULTCOMPESTOQUE: TDateField
      FieldName = 'ULTCOMPESTOQUE'
    end
    object cdsEstoqueFABRICESTOQUE: TStringField
      FieldName = 'FABRICESTOQUE'
      Size = 64
    end
  end
  object sdsTesteMangPecas_V: TSQLDataSet
    SchemaName = 'sysdba'
    CommandText = 'SELECT * FROM TB_TESTES_MANG_PECAS_V'
    MaxBlobSize = -1
    Params = <>
    SQLConnection = conexaoDB
    Left = 40
    Top = 384
    object sdsTesteMangPecas_VCODUSOPECAS: TIntegerField
      FieldName = 'CODUSOPECAS'
    end
    object sdsTesteMangPecas_VCODPECA: TIntegerField
      FieldName = 'CODPECA'
    end
    object sdsTesteMangPecas_VDESCESTOQUE: TStringField
      FieldName = 'DESCESTOQUE'
      Size = 64
    end
    object sdsTesteMangPecas_VCODTESTE: TIntegerField
      FieldName = 'CODTESTE'
    end
    object sdsTesteMangPecas_VVALOR: TFMTBCDField
      FieldName = 'VALOR'
      Precision = 15
      Size = 4
    end
    object sdsTesteMangPecas_VQUANTIDADE: TIntegerField
      FieldName = 'QUANTIDADE'
    end
  end
  object dsTesteMangPecas_V: TDataSetProvider
    DataSet = sdsTesteMangPecas_V
    Left = 152
    Top = 384
  end
  object cdsTesteMangPecas_V: TClientDataSet
    Aggregates = <>
    AggregatesActive = True
    Params = <>
    ProviderName = 'dsTesteMangPecas_V'
    Left = 264
    Top = 384
    object cdsTesteMangPecas_VCODUSOPECAS: TIntegerField
      FieldName = 'CODUSOPECAS'
    end
    object cdsTesteMangPecas_VCODPECA: TIntegerField
      FieldName = 'CODPECA'
    end
    object cdsTesteMangPecas_VDESCESTOQUE: TStringField
      FieldName = 'DESCESTOQUE'
      Size = 64
    end
    object cdsTesteMangPecas_VCODTESTE: TIntegerField
      FieldName = 'CODTESTE'
    end
    object cdsTesteMangPecas_VVALOR: TFMTBCDField
      FieldName = 'VALOR'
      Precision = 15
      Size = 4
    end
    object cdsTesteMangPecas_VQUANTIDADE: TIntegerField
      FieldName = 'QUANTIDADE'
    end
    object cdsTesteMangPecas_VTotalPreco: TAggregateField
      FieldName = 'TotalPreco'
      Active = True
      currency = True
      DisplayName = ''
      Expression = 'SUM(VALOR)'
    end
  end
  object sdsDoc: TSQLDataSet
    SchemaName = 'sysdba'
    MaxBlobSize = -1
    Params = <>
    SQLConnection = conexaoDB
    Left = 752
    Top = 399
  end
  object dsDoc: TDataSetProvider
    DataSet = sdsDoc
    Left = 864
    Top = 399
  end
  object cdsDoc: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dsDoc'
    Left = 976
    Top = 399
  end
  object sdsDocMang: TSQLDataSet
    SchemaName = 'sysdba'
    MaxBlobSize = -1
    Params = <>
    SQLConnection = conexaoDB
    Left = 752
    Top = 455
  end
  object dsDocMang: TDataSetProvider
    DataSet = sdsDocMang
    Left = 864
    Top = 455
  end
  object cdsDocMang: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dsDocMang'
    Left = 976
    Top = 455
  end
  object conexaoDB: TSQLConnection
    ConnectionName = 'FBConnection'
    DriverName = 'Firebird'
    LoginPrompt = False
    Params.Strings = (
      'DriverName=Firebird'
      'Database=database.fdb'
      'RoleName=RoleName'
      'User_Name=sysdba'
      'Password=masterkey'
      'ServerCharSet=ISO8859_1'
      'SQLDialect=3'
      'ErrorResourceFile='
      'LocaleCode=0000'
      'BlobSize=-1'
      'CommitRetain=False'
      'WaitOnLocks=True'
      'IsolationLevel=ReadCommitted'
      'Trim Char=False')
    Left = 40
    Top = 16
  end
end
