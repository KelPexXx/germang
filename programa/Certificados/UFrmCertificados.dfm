inherited FrmCertificados: TFrmCertificados
  Caption = 'Certificados'
  OnCreate = FormCreate
  ExplicitWidth = 1014
  ExplicitHeight = 523
  PixelsPerInch = 96
  TextHeight = 13
  inherited tbMenuForm: TToolBar
    inherited btnMadd: TToolButton
      Enabled = False
    end
    inherited btnMEdit: TToolButton
      Enabled = False
    end
    inherited btnMPesq: TToolButton
      Enabled = False
    end
    inherited btnMFiltro: TToolButton
      Enabled = False
    end
    inherited btnMOrder: TToolButton
      Enabled = False
    end
    inherited btnMExclui: TToolButton
      Enabled = False
    end
  end
  inherited pPrincipal: TPanel
    inherited dgForm: TDBGrid
      Columns = <
        item
          Expanded = False
          FieldName = 'CODTESTE'
          Title.Caption = 'C'#243'digo'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'RAZAO'
          Title.Caption = 'Cliente'
          Width = 200
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMECOMPLETO'
          Title.Caption = 'Tecnico'
          Width = 133
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DATAEXEC'
          Title.Caption = 'Data da Execu'#231#227'o'
          Visible = True
        end>
    end
  end
  inherited pTitle: TPanel
    Caption = 'Certificados'
    inherited imgIcon: TImage
      Center = True
      Picture.Data = {
        0954506E67496D61676589504E470D0A1A0A0000000D49484452000000180000
        00180806000000E0773DF80000000473424954080808087C0864880000000662
        4B474400FF00FF00FFA0BDA7930000000970485973000006EC000006EC011E75
        38350000001974455874536F667477617265007777772E696E6B73636170652E
        6F72679BEE3C1A000001504944415478DA63FCFFFF3F032D01E3A8054459B060
        C5AD060646C67AAA9AFCFF7F6342845A038A05FCBC6C0C56A66214997BECF42B
        868F9F7F615A2026CA59AFA321C8C0CEC64C91053F7FFD65B872E33DC3ABD7DF
        512D5051E2AF5755E4032B7AF7FE27C3DB0F3F49325858809D4148901DCCBE7D
        FF13C39D7B1F715B005600C4A40015A05E14FDF82CA0B90F2805645900CA2EAF
        DFFE00B345853980898E8A16800C3F7AEA250307072485FDF8F197C1DA4C1CA7
        25245BF0EACD0F86474FBF3098E88B80F9672EBE619093E6611013E1A08E058F
        9E7E6578F2EC2B3C138232938C1437D0126ECA2DF8F2ED0FC3CC85371814E578
        1824C4B8C0622F5E7D63B8FFE80B437ABC06030F170BF916C00C7F0E34501268
        78A89F22587CF5A6FB70316C9660B5005F51D13BFD0A8A41308B8B337530D4E2
        2C2AF015762003D15D894D0C163F580B3B9A16D7B404A316100400411E54E074
        918AEE0000000049454E44AE426082}
      Proportional = True
    end
  end
  inherited dsForm: TDataSource
    DataSet = DtmPrincipal.cdsTestes_V
  end
end
