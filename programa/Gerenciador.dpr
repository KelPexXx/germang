program Gerenciador;

uses
  Forms,
  UFrmPrincipal in 'UFrmPrincipal.pas' {FrmPrincipal},
  UFrmFormularioPadrao in 'UFrmFormularioPadrao.pas' {FrmFormularioPadrao},
  UDtmPrincipal in 'UDtmPrincipal.pas' {DtmPrincipal: TDataModule},
  UFrmClientes in 'Clientes\UFrmClientes.pas' {FrmClientes},
  UFrmClientesCad in 'Clientes\UFrmClientesCad.pas' {FrmClientesCad},
  UFrmAutenticacao in 'UFrmAutenticacao.pas' {FrmAutenticacao},
  UDtmAutenticacao in 'UDtmAutenticacao.pas' {DtmAutenticacao: TDataModule},
  UFrmClientesEdit in 'Clientes\UFrmClientesEdit.pas' {FrmClientesEdit},
  UFrmMangueiras in 'Mangueiras\UFrmMangueiras.pas' {FrmMangueiras},
  UFrmMangueirasCad in 'Mangueiras\UFrmMangueirasCad.pas' {FrmMangueirasCad},
  UFrmLacre in 'Lacres\UFrmLacre.pas' {FrmLacre},
  UFrmLacreCad in 'Lacres\UFrmLacreCad.pas' {FrmLacreCad},
  UFrmTestesCad in 'Testes\UFrmTestesCad.pas' {FrmTestesCad},
  UFrmTestes in 'Testes\UFrmTestes.pas' {FrmTestes},
  UFrmCertificados in 'Certificados\UFrmCertificados.pas' {FrmCertificados},
  UFrmGerCertificado in 'Certificados\UFrmGerCertificado.pas' {FrmGerCertificado},
  UFrmConfiguracoes in 'Configuracoes\UFrmConfiguracoes.pas' {FrmConfiguracoes},
  UFrmAgendamentos in 'Agendamentos\UFrmAgendamentos.pas' {FrmAgendamentos},
  UFrmChat in 'Chat\UFrmChat.pas' {FrmChat},
  UFrmRelatorios in 'Relatorios\UFrmRelatorios.pas' {FrmRelatorios},
  UFrmPesquisaPadrao in 'UFrmPesquisaPadrao.pas' {FrmPesquisaPadrao},
  UFrmClientesPesq in 'Clientes\UFrmClientesPesq.pas' {FrmClientesPesq},
  UFrmFiltrarPadrao in 'UFrmFiltrarPadrao.pas' {FrmFiltrarPadrao},
  UFrmClientesFiltrar in 'Clientes\UFrmClientesFiltrar.pas' {FrmClientesFiltrar},
  UFrmOrdenarPadrao in 'UFrmOrdenarPadrao.pas' {FrmOrdenarPadrao},
  UFrmClientesOrdenar in 'Clientes\UFrmClientesOrdenar.pas' {FrmClientesOrdenar},
  UFrmDelRegPadrao in 'UFrmDelRegPadrao.pas' {FrmDelRegPadrao},
  UFrmClientesRemover in 'Clientes\UFrmClientesRemover.pas' {FrmClientesRemover},
  UFrmBuscarPadrao in 'UFrmBuscarPadrao.pas' {FrmBuscarPadrao},
  UFrmMangueirasBuscaCliCad in 'Mangueiras\UFrmMangueirasBuscaCliCad.pas' {FrmMangueirasBuscaCliCad},
  UFrmLacreEdit in 'Lacres\UFrmLacreEdit.pas' {FrmLacreEdit},
  UFrmMangueirasEdit in 'Mangueiras\UFrmMangueirasEdit.pas' {FrmMangueirasEdit},
  UFrmMangueirasBuscaCliEdit in 'Mangueiras\UFrmMangueirasBuscaCliEdit.pas' {FrmMangueirasBuscaCliEdit},
  UFrmMangueirasFiltrar in 'Mangueiras\UFrmMangueirasFiltrar.pas' {FrmMangueirasFiltrar},
  UFrmMangueirasOrdenar in 'Mangueiras\UFrmMangueirasOrdenar.pas' {FrmMangueirasOrdenar},
  UFrmMangueirasRemover in 'Mangueiras\UFrmMangueirasRemover.pas' {FrmMangueirasRemover},
  UFrmEstoque in 'Estoque\UFrmEstoque.pas' {FrmEstoque},
  UFrmEstoqueCad in 'Estoque\UFrmEstoqueCad.pas' {FrmEstoqueCad},
  UFrmEstoqueFiltrar in 'Estoque\UFrmEstoqueFiltrar.pas' {FrmEstoqueFiltrar},
  UFrmEstoqueOrdenar in 'Estoque\UFrmEstoqueOrdenar.pas' {FrmEstoqueOrdenar},
  UFrmEstoqueEdit in 'Estoque\UFrmEstoqueEdit.pas' {FrmEstoqueEdit},
  UFrmEstoqueRemover in 'Estoque\UFrmEstoqueRemover.pas' {FrmEstoqueRemover},
  UFrmTestesBuscaCliCad in 'Testes\UFrmTestesBuscaCliCad.pas' {FrmTestesBuscaCliCad},
  UFrmPecaAdd in 'Testes\UFrmPecaAdd.pas' {FrmPecaAdd},
  UDtmRelatorios in 'UDtmRelatorios.pas' {DtmRelatorios: TDataModule},
  UFrmUsuarios in 'Usuarios\UFrmUsuarios.pas' {FrmUsuarios},
  UFrmUsuariosCad in 'Usuarios\UFrmUsuariosCad.pas' {FrmUsuariosCad},
  UFrmUsuariosEdit in 'Usuarios\UFrmUsuariosEdit.pas' {FrmUsuariosEdit},
  UFrmUsuariosRem in 'Usuarios\UFrmUsuariosRem.pas' {FrmUsuariosRem},
  UFrmTestesEdit in 'Testes\UFrmTestesEdit.pas' {FrmTestesEdit};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.Title := 'Gerenciador Vizun de Mangueiras';
  Application.CreateForm(TFrmPrincipal, FrmPrincipal);
  Application.CreateForm(TFrmTestesEdit, FrmTestesEdit);
  Application.ShowMainForm := false;
  Application.Run;
end.
