unit UFrmTestes;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UFrmFormularioPadrao, DB, ImgList, PngImageList, ExtCtrls, Buttons,
  PngSpeedButton, Grids, DBGrids, ComCtrls, ToolWin, pngimage, StdCtrls, Menus;

type
  TFrmTestes = class(TFrmFormularioPadrao)
    menuRelarorios: TPopupMenu;
    mGerCertManut: TMenuItem;
    mGerRelatInsp: TMenuItem;
    imgList24: TPngImageList;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure btnMaddClick(Sender: TObject);
    procedure mGerCertManutClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure dsFormDataChange(Sender: TObject; Field: TField);
    procedure mGerRelatInspClick(Sender: TObject);
  private
    procedure LimparFiltros;
    procedure AtualizaMenuRelatorios;
  public
    procedure ScanOpen;
  end;

var
  FrmTestes: TFrmTestes;

implementation

uses UDtmPrincipal, UFrmTestesCad, UDtmRelatorios;
{$R *.dfm}

procedure TFrmTestes.AtualizaMenuRelatorios;
var
  tipo:char;
begin
if DtmPrincipal.cdsTestes_V.RecordCount > 0 then
  begin
    tipo := DtmPrincipal.cdsTestes_VTIPO.AsString[1];
    case tipo of
      'I':
      begin
        mGerRelatInsp.Enabled := TRUE;
        mGerCertManut.Enabled := FALSE;
      end;
      'M':
      begin
        mGerRelatInsp.Enabled := FALSE;
        mGerCertManut.Enabled := TRUE;
      end;
    end;
  end;
end;

procedure TFrmTestes.btnMaddClick(Sender: TObject);
begin
  ScanOpen;
  if not Assigned(FrmTestesCad) then
    FrmTestesCad := TFrmTestesCad.Create(self);
  FrmTestesCad.Show;
end;

procedure TFrmTestes.dsFormDataChange(Sender: TObject; Field: TField);
begin
  inherited;
  AtualizaMenuRelatorios;
end;

procedure TFrmTestes.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  DtmPrincipal.cdsTestes_V.Close;
  DtmPrincipal.cdsTestes_V.Filtered := FALSE;
  DtmPrincipal.cdsTestes_V.Filter := '';
  FreeAndNil(FrmTestes);
end;

procedure TFrmTestes.FormCreate(Sender: TObject);
begin
  inherited;
  DtmPrincipal.cdsTestes_V.Open;
  DtmPrincipal.cdsTestes_V.Filtered := FALSE;
  DtmPrincipal.cdsTestes_V.Filter := '';
  DtmPrincipal.cdsTestes_V.Filtered := TRUE;
end;

procedure TFrmTestes.FormShow(Sender: TObject);
begin
  inherited;
  AtualizaMenuRelatorios;
end;

procedure TFrmTestes.LimparFiltros;
begin
  with DtmPrincipal do
  begin
    cdsMangueiras_V.Filtered := FALSE;
    cdsMangueiras_V.Filter := '';
    cdsClientes_V.Filtered := FALSE;
    cdsClientes_V.Filter := '';
    cdsClientes_V.Open;
    cdsClientes_V.Filtered := FALSE;
    cdsClientes_V.Filter := '';
    cdsTestes_V.Open;
    cdsTestes_V.Filtered := FALSE;
    cdsTestes_V.Filter := '';
    cdsAvaliacao_V.Open;
    cdsAvaliacao_V.Filtered := FALSE;
    cdsAvaliacao_V.Filter := '';
    cdsTesteMangPecas_V.Open;
    cdsTesteMangPecas_V.Filtered := FALSE;
    cdsTesteMangPecas_V.Filter := '';
  end;
end;

procedure TFrmTestes.mGerCertManutClick(Sender: TObject);
var
  cod:string;
begin
  inherited;

  cod :=  DtmPrincipal.cdsTestes_VCODTESTE.AsString;
  DtmPrincipal.cdsClientes_V.Open;
  DtmPrincipal.cdsClientes_V.Filtered := FALSE;
  DtmPrincipal.cdsClientes_V.Filter := 'CODCLI = ' + DtmPrincipal.cdsTestes_VCODCLI.AsString;
  DtmPrincipal.cdsClientes_V.Filtered := TRUE;
  DtmPrincipal.cdsTestes_V.Open;
  DtmPrincipal.cdsTestes_V.Filtered := FALSE;
  DtmPrincipal.cdsTestes_V.Filter := 'CODTESTE = ' + cod;
  DtmPrincipal.cdsTestes_V.Filtered := TRUE;
  DtmPrincipal.cdsAvaliacao_V.Open;
  DtmPrincipal.cdsAvaliacao_V.Filtered := FALSE;
  DtmPrincipal.cdsAvaliacao_V.Filter := 'CODTESTE = ' + cod;
  DtmPrincipal.cdsAvaliacao_V.Filtered := TRUE;
  DtmPrincipal.cdsTesteMangPecas_V.Open;
  DtmPrincipal.cdsTesteMangPecas_V.Filtered := FALSE;
  DtmPrincipal.cdsTesteMangPecas_V.Filter := 'CODTESTE = ' + cod;
  DtmPrincipal.cdsTesteMangPecas_V.Filtered := TRUE;
  DtmRelatorios.rpCertificado.Open;
  //showmessage(inttostr(dtmPrincipal.cdstestemangpecas_v.recordcount));
  if DtmPrincipal.cdsTesteMangPecas_V.RecordCount > 0 then
    DtmRelatorios.rpCertificado.SetParam('bSemPecas',' ')
  else
   DtmRelatorios.rpCertificado.SetParam('bSemPecas','Sem pe�as utilizadas!');
  DtmRelatorios.rpCertificado.Execute;
  DtmRelatorios.rpCertificado.ClearParams;
  DtmRelatorios.rpCertificado.Close;
  LimparFiltros;
  DtmPrincipal.cdsClientes_V.Close;
  DtmPrincipal.cdsAvaliacao_V.Close;
  DtmPrincipal.cdsTesteMangPecas_V.Close;
  DtmPrincipal.cdsTestes_V.Close;
  DtmPrincipal.cdsTestes_V.Open;
end;

procedure TFrmTestes.mGerRelatInspClick(Sender: TObject);
var
  cod:String;
begin
  inherited;
  cod :=  DtmPrincipal.cdsTestes_VCODTESTE.AsString;
  DtmPrincipal.cdsClientes_V.Open;
  DtmPrincipal.cdsClientes_V.Filtered := FALSE;
  DtmPrincipal.cdsClientes_V.Filter := 'CODCLI = ' + DtmPrincipal.cdsTestes_VCODCLI.AsString;
  DtmPrincipal.cdsClientes_V.Filtered := TRUE;
  DtmPrincipal.cdsTestes_V.Open;
  DtmPrincipal.cdsTestes_V.Filtered := FALSE;
  DtmPrincipal.cdsTestes_V.Filter := 'CODTESTE = ' + cod;
  DtmPrincipal.cdsTestes_V.Filtered := TRUE;
  DtmPrincipal.cdsAvaliacao_V.Open;
  DtmPrincipal.cdsAvaliacao_V.Filtered := FALSE;
  DtmPrincipal.cdsAvaliacao_V.Filter := 'CODTESTE = ' + cod;
  DtmPrincipal.cdsAvaliacao_V.Filtered := TRUE;
  DtmRelatorios.rpRelatorio.Execute;
  LimparFiltros;
end;

procedure TFrmTestes.ScanOpen;
begin
  if FrmTestesCad <> nil then
    FrmTestesCad.Close;
end;

end.
