unit UFrmTestesEdit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, StdCtrls, Buttons, PngBitBtn, ExtCtrls, DBCtrls, DB, Mask,
  PngSpeedButton, Grids, DBGrids, JvExExtCtrls, JvExtComponent, JvDBRadioPanel,
  JvExMask, JvToolEdit, JvMaskEdit, JvCheckedMaskEdit, JvDatePickerEdit,
  JvDBDatePickerEdit, ImgList, PngImageList;

type
  TFrmTestesEdit = class(TForm)
    pgTeste: TPageControl;
    tbTeste: TTabSheet;
    tbMangueiras: TTabSheet;
    tbAvaliacao: TTabSheet;
    dsTestes: TDataSource;
    lblTecnico: TLabel;
    lblDtExec: TLabel;
    dbTecnico: TDBLookupComboBox;
    dsTecnicos: TDataSource;
    dsPecas: TDataSource;
    dsAvaliacao: TDataSource;
    dsTestesMangPecas: TDataSource;
    dsClientes: TDataSource;
    dsMangueiras: TDataSource;
    gbMangSelect: TGroupBox;
    gbMangDisp: TGroupBox;
    dgMangDisp: TDBGrid;
    pTopMangDisp: TPanel;
    btnAddListMang: TPngSpeedButton;
    btnAtualizaListMang: TPngSpeedButton;
    dgMangTeste: TDBGrid;
    pTopMangSelect: TPanel;
    btnAtualizaListTeste: TPngSpeedButton;
    btnRemListTeste: TPngSpeedButton;
    pBottomMang: TPanel;
    gbInfoMang: TGroupBox;
    gbAvaliacao: TGroupBox;
    pBottomAval: TPanel;
    lblCodMang: TLabel;
    lblNumMang: TLabel;
    lblMangDiametro: TLabel;
    lblMangCompNom: TLabel;
    lblMangFab: TLabel;
    dsEditMang: TDataSource;
    lblMangRevest: TLabel;
    lblMangCompReal: TLabel;
    lblMangDtFab: TLabel;
    lblMangLacreAt: TLabel;
    lblCodAval: TLabel;
    lblCodMangAval: TLabel;
    jgMarcAbnt: TJvDBRadioPanel;
    jgVazValvula: TJvDBRadioPanel;
    jgAcuAguaCx: TJvDBRadioPanel;
    lblMarcAbnt: TLabel;
    lblvazValvula: TLabel;
    lblAcuAguaCx: TLabel;
    lblCondAduchAcondi: TLabel;
    jgPresMat: TJvDBRadioPanel;
    lblPresMat: TLabel;
    lblPresInset: TLabel;
    lblAcoplaUnioes: TLabel;
    lblAnelVed: TLabel;
    jgPresInset: TJvDBRadioPanel;
    jgAcoplaUnioes: TJvDBRadioPanel;
    jgAnelVed: TJvDBRadioPanel;
    lblCompLuva: TLabel;
    dbCompLuva: TDBEdit;
    lblEnsaioHidro: TLabel;
    jgReempatacao: TJvDBRadioPanel;
    lblReempatacao: TLabel;
    lblCompReal: TLabel;
    dbCompReal: TDBEdit;
    jgNovoEnsaioHidro: TJvDBRadioPanel;
    jgSubstUnioes: TJvDBRadioPanel;
    jgSubstVed: TJvDBRadioPanel;
    jgSubstAneis: TJvDBRadioPanel;
    jgLimpeza: TJvDBRadioPanel;
    jgSecagem: TJvDBRadioPanel;
    lblSubstAneis: TLabel;
    lblSubstUnioes: TLabel;
    lblSubstVed: TLabel;
    lblNovoEnsaioHidro: TLabel;
    lblLimpeza: TLabel;
    lblSecagem: TLabel;
    tbCliente: TTabSheet;
    edtCodCli: TLabeledEdit;
    lblCnpj: TLabel;
    lblRazao: TLabel;
    lblFantasia: TLabel;
    jgEnsaioHidro: TJvDBRadioPanel;
    jgCondAduchAcond: TJvDBRadioPanel;
    dbDtExec: TJvDBDatePickerEdit;
    cbMangLacreAtual: TDBLookupComboBox;
    dsLacre: TDataSource;
    pBottomTeste: TPanel;
    pBottomCliente: TPanel;
    tbLacres: TTabSheet;
    tbPecas: TTabSheet;
    gbPecasTeste: TGroupBox;
    pBottomPecas: TPanel;
    gbPe�asDisponiveis: TGroupBox;
    pTopPecasDisp: TPanel;
    pBottomPecasDisp: TPanel;
    edtPesquisaPecas: TEdit;
    btnPesquisaPecas: TButton;
    cbPesquisaPecas: TComboBox;
    dgPecasDisp: TDBGrid;
    btnAdicionarPecas: TPngSpeedButton;
    dgPecasUtilizadas: TDBGrid;
    pBottomPecasUtil: TPanel;
    btnRemoverPeca: TPngSpeedButton;
    pBottomLacres: TPanel;
    gbInfoMangLacre: TGroupBox;
    lblCodMangLacre: TLabel;
    lblNumSerieLacre: TLabel;
    lblDiametroLacre: TLabel;
    lblCompNomLacre: TLabel;
    lblFabricante: TLabel;
    lblRevestLacre: TLabel;
    lblCompRealLacre: TLabel;
    lblDtFabLacre: TLabel;
    lblLacreAnterior: TLabel;
    gbLacre: TGroupBox;
    lblCodLacre: TLabel;
    edtCodLacre: TEdit;
    lblNumLacre: TLabel;
    lblDataCadLacre: TLabel;
    pLeftAval: TPanel;
    btnProxAval: TPngSpeedButton;
    btnSalvarAval: TPngSpeedButton;
    btnCancelarAval: TPngSpeedButton;
    btnEditarAval: TPngSpeedButton;
    btnAntAval: TPngSpeedButton;
    pLeftLacre: TPanel;
    btnAntMngLacre: TPngSpeedButton;
    btnCancelarMngLacre: TPngSpeedButton;
    btnEditarMngLacre: TPngSpeedButton;
    btnSalvarMngLacre: TPngSpeedButton;
    btnProxMngLacre: TPngSpeedButton;
    lblObs: TLabel;
    btnStep1Prox: TPngSpeedButton;
    btnStep2Prox: TPngSpeedButton;
    btnStep3Prox: TPngSpeedButton;
    btnStep4Prox: TPngSpeedButton;
    btnStep5Prox: TPngSpeedButton;
    icons: TPngImageList;
    dsEditAvaliacao: TDataSource;
    lblTotalPecas: TLabel;
    dbTotalPreco: TDBEdit;
    btnStep5Voltar: TPngSpeedButton;
    btnStep6Prox: TPngSpeedButton;
    tbRelatorio: TTabSheet;
    Panel1: TPanel;
    btnConcluido: TPngSpeedButton;
    dsLacreNovo: TDataSource;
    btnGerRelatInsp: TPngSpeedButton;
    btnGerCertManut: TPngSpeedButton;
    jgTipoTeste: TJvDBRadioPanel;
    Label1: TLabel;
    lblTesteAvalObs: TLabel;
    dbObsMang: TDBMemo;
    gbResultadoFinal: TGroupBox;
    jgResultFinal: TJvDBRadioPanel;
    dbObs: TDBMemo;
    dbTCliCnpj: TDBText;
    btnProcurarCliente: TPngSpeedButton;
    dbTCliRazao: TDBText;
    dbTCliFantasia: TDBText;
    lblCliContato: TLabel;
    dbTContato: TDBText;
    lblCodCliente: TLabel;
    btnStep1Cancel: TPngSpeedButton;
    btnStep2Cancel: TPngSpeedButton;
    btnStep3Cancel: TPngSpeedButton;
    dbMangCod: TDBText;
    dbMangNumSer: TDBText;
    dbMangDiam: TDBText;
    dbMangCompNom: TDBText;
    dbMangDtFab: TDBText;
    dbMangRevest: TDBText;
    dbMangFab: TDBText;
    dbMangCompReal: TDBText;
    dbCodAval: TDBText;
    dbCodMangAval: TDBText;
    dbNumLacreNovo: TDBText;
    dbDataCadLacreNovo: TDBText;
    btnPesquisarLacre: TPngSpeedButton;
    dbCodMangLacre: TDBText;
    dbRevestLacre: TDBText;
    dbNumSerieLacre: TDBText;
    dbDiamLacre: TDBText;
    dbCompNomLacre: TDBText;
    dbDtFabLacre: TDBText;
    dbFabricante: TDBText;
    dbCompRealLacre: TDBText;
    dbLacreAtual: TDBText;
    lblMangPos: TLabel;
    PngSpeedButton1: TPngSpeedButton;
    procedure FormShow(Sender: TObject);
    procedure btnStep1ProxClick(Sender: TObject);
    procedure btnProcurarClienteClick(Sender: TObject);
    procedure btnStep2ProxClick(Sender: TObject);
    procedure edtCodCliKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure pgTesteChanging(Sender: TObject; var AllowChange: Boolean);
    procedure btnAtualizaListMangClick(Sender: TObject);
    procedure btnAtualizaListTesteClick(Sender: TObject);
    procedure btnAddListMangClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure btnRemListTesteClick(Sender: TObject);
    procedure btnStep3ProxClick(Sender: TObject);
    procedure btnAntAvalClick(Sender: TObject);
    procedure btnProxAvalClick(Sender: TObject);
    procedure btnCancelarAvalClick(Sender: TObject);
    procedure btnEditarAvalClick(Sender: TObject);
    procedure btnSalvarAvalClick(Sender: TObject);
    procedure btnStep4ProxClick(Sender: TObject);
    procedure btnPesquisaPecasClick(Sender: TObject);
    procedure btnAdicionarPecasClick(Sender: TObject);
    procedure dgPecasDispTitleClick(Column: TColumn);
    procedure btnRemoverPecaClick(Sender: TObject);
    procedure btnStep5ProxClick(Sender: TObject);
    procedure btnEditarMngLacreClick(Sender: TObject);
    procedure btnPesquisarLacreClick(Sender: TObject);
    procedure btnCancelarMngLacreClick(Sender: TObject);
    procedure btnSalvarMngLacreClick(Sender: TObject);
    procedure btnAntMngLacreClick(Sender: TObject);
    procedure btnProxMngLacreClick(Sender: TObject);
    procedure btnStep6ProxClick(Sender: TObject);
    procedure btnGerRelatInspClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnConcluidoClick(Sender: TObject);
    procedure lblMarcAbntClick(Sender: TObject);
    procedure lblvazValvulaClick(Sender: TObject);
    procedure lblAcuAguaCxClick(Sender: TObject);
    procedure lblCondAduchAcondiClick(Sender: TObject);
    procedure lblPresMatClick(Sender: TObject);
    procedure lblPresInsetClick(Sender: TObject);
    procedure lblAcoplaUnioesClick(Sender: TObject);
    procedure lblAnelVedClick(Sender: TObject);
    procedure lblEnsaioHidroClick(Sender: TObject);
    procedure lblReempatacaoClick(Sender: TObject);
    procedure lblSubstUnioesClick(Sender: TObject);
    procedure lblSubstVedClick(Sender: TObject);
    procedure lblSubstAneisClick(Sender: TObject);
    procedure lblNovoEnsaioHidroClick(Sender: TObject);
    procedure lblLimpezaClick(Sender: TObject);
    procedure lblSecagemClick(Sender: TObject);
    procedure btnGerCertManutClick(Sender: TObject);
    procedure jgResultFinalChange(Sender: TObject);
    procedure btnStep1CancelClick(Sender: TObject);
    procedure btnStep2CancelClick(Sender: TObject);
    procedure btnStep3CancelClick(Sender: TObject);
    procedure btnStep5VoltarClick(Sender: TObject);
  private
    // Variaveis
    codCliente,codTeste: Integer;
    idxPesquisaPeca,codLacreOld:Integer;
    sTipo:String;
    // Fun��es
    function PesquisaCliente(codigo: Integer): Boolean;
    function PesquisaLacre(codigo: Integer): Boolean;
    function VerificaQntMang(codcli: Integer):Boolean;
    function GeraFiltro:String;
    // Procedimentos
    procedure LimparFiltros;
    procedure AtualizaBotoesMang;
    procedure AtualizaMangAvaliacao;
    procedure AtualizaBotoesAvaliacao;
    procedure AtualizaMangLacre;
    procedure AtualizaBotoesLacre;
    procedure AtualizaBotoesPe�a;
    procedure AtualizaPosMang;
    procedure ModoAvaliacao(modo:Boolean);
    procedure ModoLacre(modo:Boolean);
    procedure SelecionaPagina(pg: Integer);
    procedure AlteraUsoLacre(codlc,codmang:String);
    procedure DescartaLacre(codlc:string);
    procedure PreparaAvalia��o(tipo:String);
    procedure FecharDataSets;
  public
    // Variaveis
    bClienteOk,bLacreOk: Boolean;
  end;

var
  FrmTestesEdit: TFrmTestesEdit;

implementation

uses UDtmPrincipal, UFrmTestesBuscaCliCad, UFrmPecaAdd, UFrmLacre,UDtmRelatorios;
{$R *.dfm}
{ TFrmTestesEdit }

// Teste

// Cliente
procedure TFrmTestesEdit.btnProcurarClienteClick(Sender: TObject);
begin
  if length(edtCodCli.Text) <> 0 then
    if PesquisaCliente(strtoint(edtCodCli.Text)) then
      begin
        dbTCliCnpj.Visible := TRUE;
        dbTCliRazao.Visible := TRUE;
        dbTCliFantasia.Visible := TRUE;
        lblRazao.Visible := TRUE;
        lblFantasia.Visible := TRUE;
        lblCnpj.Visible := TRUE;
        if VerificaQntMang(StrToInt(edtCodCli.Text)) then
          begin
            bClienteOk := TRUE;
          end
        else
          MessageDlg('Aten��o!'+#13+'O Cliente selecionado n�o possui mangueiras cadastradas ou ativa!',mtWarning,[mbOk],0);
      end
    else
      begin
        dbTCliCnpj.Visible := FALSE;
        dbTCliRazao.Visible := FALSE;
        dbTCliFantasia.Visible := FALSE;
        lblRazao.Visible := FALSE;
        lblFantasia.Visible := FALSE;
        lblCnpj.Visible := FALSE;
        bClienteOk := FALSE;
      end
  else
    begin
      if not Assigned(FrmTestesBuscaCliCad) then
        Application.CreateForm(TFrmTestesBuscaCliCad, FrmTestesBuscaCliCad);
      FrmTestesBuscaCliCad.Show;
    end;
end;

function TFrmTestesEdit.PesquisaCliente(codigo: Integer): Boolean;
begin
  result := FALSE;
  DtmPrincipal.cdsClientes_V.Filtered := FALSE;
  DtmPrincipal.cdsClientes_V.Filter := 'CODCLI = ' + inttostr(codigo);
  DtmPrincipal.cdsClientes_V.Filtered := TRUE;
  if DtmPrincipal.cdsClientes_V.RecordCount > 0 then
    begin
      result := TRUE;
    end
  else
    MessageDlg('Cliente n�o encontrado!', mtError, [mbOK], 0);
end;

procedure TFrmTestesEdit.edtCodCliKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 if Key = VK_Return then
    begin
      btnProcurarCliente.Click;
    end;
end;

// Mangueiras

procedure TFrmTestesEdit.btnAddListMangClick(Sender: TObject);
begin
  try
    DtmPrincipal.cdsTesteAvaliacao.Append;
    DtmPrincipal.cdsTesteAvaliacaoCODTESTE.AsInteger := codTeste;
    DtmPrincipal.cdsTesteAvaliacaoCODMANG.AsInteger := DtmPrincipal.cdsMangueiras_VCODMANG.AsInteger;
    DtmPrincipal.cdsTesteAvaliacaoCOMPREAL.AsCurrency := DtmPrincipal.cdsMangueiras_VCOMPREAL.AsCurrency;
    DtmPrincipal.cdsTesteAvaliacaoCOMPLUVA.AsCurrency := DtmPrincipal.cdsMangueiras_VCOMPLUVA.AsCurrency;
    DtmPrincipal.cdsTesteAvaliacaoCODLACRE.AsInteger :=  DtmPrincipal.cdsMangueiras_VCODLACRE.AsInteger;
    DtmPrincipal.cdsTesteAvaliacao.Post;
    DtmPrincipal.cdsTesteAvaliacao.ApplyUpdates(0);
    DtmPrincipal.cdsTesteAvaliacao.Refresh;
    DtmPrincipal.cdsTesteAvaliacao.Refresh;
    DtmPrincipal.cdsTesteAvaliacao.Refresh;
    DtmPrincipal.cdsMangueiras_V.Refresh;
    DtmPrincipal.cdsMangueiras_V.Refresh;
    DtmPrincipal.cdsMangueiras_V.Refresh;
    DtmPrincipal.cdsAvaliacao_V.Refresh;
    DtmPrincipal.cdsAvaliacao_V.Refresh;
    DtmPrincipal.cdsAvaliacao_V.Refresh;
    DtmPrincipal.cdsMangueiras_V.Filtered := FALSE;
    DtmPrincipal.cdsMangueiras_V.Filter := GeraFiltro;
    DtmPrincipal.cdsMangueiras_V.Filtered := TRUE;
    AtualizaBotoesMang;
  except
    on e:Exception do
      begin
        DtmPrincipal.cdsTesteAvaliacao.Cancel;
        DtmPrincipal.cdsTesteAvaliacao.Open;
        MessageDlg('Erro: ' + E.Message, mtError, [mbOK], 0);
      end;
  end;
end;

procedure TFrmTestesEdit.btnRemListTesteClick(Sender: TObject);
begin
  if MessageDlg('Deseja remover da lista a mangueira selecionada?',
    mtInformation, mbYesNo, 0) = mrYes then
  begin
    try
      if DtmPrincipal.cdsTesteAvaliacao.Active then
      begin
        DtmPrincipal.sdsAux.CommandText :=
          'DELETE FROM TB_TESTE_AVALIACAO WHERE CODAVALIACAO = ' +
          DtmPrincipal.cdsAvaliacao_VCODAVALIACAO.AsString;
        DtmPrincipal.sdsAux.ExecSQL(TRUE);
        DtmPrincipal.cdsAvaliacao_V.Refresh;
        DtmPrincipal.cdsAvaliacao_V.Refresh;
        DtmPrincipal.cdsTesteAvaliacao.Refresh;
        DtmPrincipal.cdsTesteAvaliacao.Refresh;
        DtmPrincipal.cdsMangueiras_V.Filtered := FALSE;
        DtmPrincipal.cdsMangueiras_V.Filter := GeraFiltro;
        DtmPrincipal.cdsMangueiras_V.Filtered := TRUE;
        AtualizaBotoesMang;
      end;
    except
      on e: Exception do
      begin
        DtmPrincipal.cdsAvaliacao_V.Cancel;
        DtmPrincipal.cdsAvaliacao_V.Open;
        MessageDlg('Erro: ' + e.Message, mtError, [mbOK], 0);
        MessageDlg(
          'N�o foi poss�vel remover a mangueira, j� existem registros no sistema.\n'
            + ' Remova primeiro os registros de testes e lacres!\n' +
            ' C�d Error:' + e.Message, mtWarning, mbOKCancel, 0);
      end;
      on e: EDatabaseError do
        Showmessage(e.Message);
    end;
  end;
end;

procedure TFrmTestesEdit.btnAtualizaListMangClick(Sender: TObject);
begin
  With DtmPrincipal do
  begin
    DtmPrincipal.cdsMangueiras_V.Filtered := FALSE;
    DtmPrincipal.cdsMangueiras_V.Filter := GeraFiltro;
    DtmPrincipal.cdsMangueiras_V.Filtered := TRUE;
    cdsMangueiras_V.Refresh;
    cdsMangueiras_V.Refresh;
    cdsMangueiras_V.Refresh;
  end;
end;

procedure TFrmTestesEdit.btnAtualizaListTesteClick(Sender: TObject);
begin
  With DtmPrincipal do
  begin
    cdsAvaliacao_V.Close;
    cdsAvaliacao_V.Open;
    cdsAvaliacao_V.Refresh;
    cdsAvaliacao_V.Refresh;
    cdsAvaliacao_V.Refresh;
  end;
end;

// Avalia��o

procedure TFrmTestesEdit.btnCancelarAvalClick(Sender: TObject);
begin
  try
    if (DtmPrincipal.cdsTesteAvaliacao.State in [dsEdit, dsInsert]) then
      begin
        DtmPrincipal.cdsTesteAvaliacao.Cancel;
        DtmPrincipal.cdsMangueiras.Cancel;
        AtualizaMangAvaliacao;
        AtualizaBotoesAvaliacao;
        ModoAvaliacao(TRUE);
      end;
  except on E: Exception do
    begin
      DtmPrincipal.cdsTesteAvaliacao.Cancel;
      DtmPrincipal.cdsTesteAvaliacao.Open;
      ModoAvaliacao(TRUE);
      MessageDlg('Erro: '+E.Message, mtError, [mbOK], 0);
    end;
  end;
end;

procedure TFrmTestesEdit.btnEditarAvalClick(Sender: TObject);
begin
  try
    DtmPrincipal.cdsMangueiras.Edit;
    DtmPrincipal.cdsTesteAvaliacao.Edit;
    ModoAvaliacao(FALSE);
  except
    on E: Exception do
      begin
        DtmPrincipal.cdsTesteAvaliacao.Cancel;
        DtmPrincipal.cdsMangueiras.Cancel;
        DtmPrincipal.cdsTesteAvaliacao.Open;
        DtmPrincipal.cdsMangueiras.Open;
        ModoAvaliacao(TRUE);
        MessageDlg('Erro: ' + E.Message, mtError, [mbOK], 0);
      end;
  end;
end;

procedure TFrmTestesEdit.btnSalvarAvalClick(Sender: TObject);
begin
  try
    if (DtmPrincipal.cdsTesteAvaliacao.State in [dsEdit, dsInsert]) then
    begin
      if MessageDlg(
        'Confirmar dados?', mtInformation, mbYesNo, 0) = mrYes then
        begin
          DtmPrincipal.cdsMangueirasCOMPREAL.AsCurrency := DtmPrincipal.cdsTesteAvaliacaoCOMPREAL.AsCurrency;
          DtmPrincipal.cdsMangueirasCOMPLUVA.AsCurrency := DtmPrincipal.cdsTesteAvaliacaoCOMPLUVA.AsCurrency;
          DtmPrincipal.cdsMangueirasDATAULTISP.AsDateTime := now;
          DtmPrincipal.cdsMangueirasDATAULTMANUT.AsDateTime := now;
          DtmPrincipal.cdsMangueirasDATAPRXMANUT.AsDateTime := IncMonth(now,12);
          DtmPrincipal.cdsMangueirasDATAPRXISP.AsDateTime := IncMonth(now,6);
          DtmPrincipal.cdsTesteAvaliacao.Post;
          DtmPrincipal.cdsMangueiras.Post;
          DtmPrincipal.cdsTesteAvaliacao.ApplyUpdates(0);
          DtmPrincipal.cdsMangueiras.ApplyUpdates(0);
          DtmPrincipal.cdsTesteAvaliacao.Refresh;
          DtmPrincipal.cdsTesteAvaliacao.Refresh;
          DtmPrincipal.cdsMangueiras.Refresh;
          DtmPrincipal.cdsMangueiras.Refresh;
          ModoAvaliacao(TRUE);
          AtualizaMangAvaliacao;
          AtualizaBotoesAvaliacao;
        end;

    end;
  except
    on E: Exception do
    begin
      DtmPrincipal.cdsTesteAvaliacao.Cancel;
      DtmPrincipal.cdsMangueiras.Cancel;
      DtmPrincipal.cdsTesteAvaliacao.Open;
      DtmPrincipal.cdsMangueiras.Open;
      ModoAvaliacao(TRUE);
      MessageDlg('Erro: ' + E.Message, mtError, [mbOK], 0);
    end;
  end;
end;

procedure TFrmTestesEdit.btnAntAvalClick(Sender: TObject);
begin
  try
    DtmPrincipal.cdsTesteAvaliacao.Prior;
    AtualizaMangAvaliacao;
    AtualizaBotoesAvaliacao;
    AtualizaPosMang;
  except
    on e:Exception do
      begin
        DtmPrincipal.cdsTesteAvaliacao.Cancel;
        DtmPrincipal.cdsTesteAvaliacao.Open;
        ModoAvaliacao(TRUE);
        MessageDlg('Erro: ' + E.Message, mtError, [mbOK], 0);
      end;
  end;
end;

procedure TFrmTestesEdit.btnProxAvalClick(Sender: TObject);
begin
  try
    DtmPrincipal.cdsTesteAvaliacao.Next;
    AtualizaMangAvaliacao;
    AtualizaBotoesAvaliacao;
    AtualizaPosMang;
  except
    on e:Exception do
      begin
        DtmPrincipal.cdsTesteAvaliacao.Cancel;
        DtmPrincipal.cdsTesteAvaliacao.Open;
        ModoAvaliacao(TRUE);
        MessageDlg('Erro: ' + E.Message, mtError, [mbOK], 0);
      end;
  end;
end;

procedure TFrmTestesEdit.btnProxMngLacreClick(Sender: TObject);
begin
  DtmPrincipal.cdsTesteAvaliacao.Next;
  AtualizaMangLacre;
  AtualizaBotoesLacre;
end;

procedure TFrmTestesEdit.AtualizaBotoesLacre;
begin
  if DtmPrincipal.cdsTesteAvaliacao.RecNo = 1 then
    btnAntMngLacre.Enabled := FALSE
  else
    btnAntMngLacre.Enabled := TRUE;
  if DtmPrincipal.cdsTesteAvaliacao.RecNo = DtmPrincipal.cdsTesteAvaliacao.RecordCount then
    btnProxMngLacre.Enabled := FALSE
  else
    btnProxMngLacre.Enabled := TRUE;
end;

procedure TFrmTestesEdit.AtualizaBotoesMang;
begin
  if DtmPrincipal.cdsMangueiras_V.RecordCount > 0 then
    btnAddListMang.Enabled := TRUE
  else
    btnAddListMang.Enabled := FALSE;
  if DtmPrincipal.cdsAvaliacao_V.RecordCount > 0 then
    btnRemListTeste.Enabled := TRUE
  else
    btnRemListTeste.Enabled := FALSE;
end;

procedure TFrmTestesEdit.AtualizaMangAvaliacao;
begin
  try
    DtmPrincipal.cdsMangueiras.Filtered := FALSE;
    DtmPrincipal.cdsMangueiras.Filter := 'CODMANG = ' + DtmPrincipal.cdsTesteAvaliacaoCODMANG.AsString;
    DtmPrincipal.cdsMangueiras.Filtered := TRUE;
    if DtmPrincipal.cdsTesteAvaliacao.RecordCount < 1 then
      MessageDlg('Mangueira n�o encontrada!', mtError, [mbOK], 0);
  except
    on e:Exception do
      begin
        DtmPrincipal.cdsMangueiras.Cancel;
        DtmPrincipal.cdsMangueiras.Open;
        MessageDlg('Erro: ' + E.Message, mtError, [mbOK], 0);
      end;
  end;
end;

procedure TFrmTestesEdit.AtualizaBotoesAvaliacao;
begin
  if DtmPrincipal.cdsTesteAvaliacao.RecNo = 1 then
    btnAntAval.Enabled := FALSE
  else
    btnAntAval.Enabled := TRUE;
  if DtmPrincipal.cdsTesteAvaliacao.RecNo = DtmPrincipal.cdsTesteAvaliacao.RecordCount then
    btnProxAval.Enabled := FALSE
  else
    btnProxAval.Enabled := TRUE;
end;

procedure TFrmTestesEdit.ModoAvaliacao(modo: Boolean);
begin
  dbCompReal.ReadOnly := modo;
  dbCompLuva.ReadOnly := modo;
  jgMarcAbnt.ReadOnly := modo;
  jgVazValvula.ReadOnly := modo;
  jgAcuAguaCx.ReadOnly := modo;
  jgPresMat.ReadOnly := modo;
  jgPresInset.ReadOnly := modo;
  jgAcoplaUnioes.ReadOnly := modo;
  jgAnelVed.ReadOnly := modo;
  jgReempatacao.ReadOnly := modo;
  jgNovoEnsaioHidro.ReadOnly := modo;
  jgSubstUnioes.ReadOnly := modo;
  jgSubstVed.ReadOnly := modo;
  jgSubstAneis.ReadOnly := modo;
  jgLimpeza.ReadOnly := modo;
  jgSecagem.ReadOnly := modo;
  jgResultFinal.ReadOnly := modo;
  jgEnsaioHidro.ReadOnly := modo;
  jgCondAduchAcond.ReadOnly := modo;
  btnEditarAval.Enabled := modo;
  btnProxAval.Enabled := modo;
  btnAntAval.Enabled := modo;
  btnSalvarAval.Enabled := not modo;
  btnCancelarAval.Enabled := not modo;
  btnStep4Prox.Enabled := modo;
end;

// Pe�as

procedure TFrmTestesEdit.AtualizaBotoesPe�a;
begin
  if DtmPrincipal.cdsEstoque_V.RecordCount = 0 then
    btnAdicionarPecas.Enabled := FALSE
  else
    btnAdicionarPecas.Enabled := TRUE;
  if DtmPrincipal.cdsTesteMangPecas_V.RecordCount = 0 then
    btnRemoverPeca.Enabled := FALSE
  else
    btnRemoverPeca.Enabled := TRUE;
end;

procedure TFrmTestesEdit.btnPesquisaPecasClick(Sender: TObject);
begin
  if trim(edtPesquisaPecas.Text) = '' then
    begin
      DtmPrincipal.cdsEstoque_V.Filtered := FALSE;
      DtmPrincipal.cdsEstoque_V.Filter := '';
    end
  else
    begin
      case idxPesquisaPeca of
        0:begin
            case cbPesquisaPecas.ItemIndex of
              0:begin
                DtmPrincipal.cdsEstoque_V.Filtered := FALSE;
                DtmPrincipal.cdsEstoque_V.Filter := 'CODESTOQUE = ' + edtPesquisaPecas.Text;
                DtmPrincipal.cdsEstoque_V.Filtered := TRUE;
              end;
              1:begin
                DtmPrincipal.cdsEstoque_V.Filtered := FALSE;
                DtmPrincipal.cdsEstoque_V.Filter := 'CODESTOQUE = ' + edtPesquisaPecas.Text;
                DtmPrincipal.cdsEstoque_V.Filtered := TRUE;
              end;
              2:begin
                DtmPrincipal.cdsEstoque_V.Filtered := FALSE;
                DtmPrincipal.cdsEstoque_V.Filter := 'CODESTOQUE = ' + edtPesquisaPecas.Text;
                DtmPrincipal.cdsEstoque_V.Filtered := TRUE;
              end;
            end;
        end;
      1:begin
            case cbPesquisaPecas.ItemIndex of
              0:begin
                DtmPrincipal.cdsEstoque_V.Filtered := FALSE;
                DtmPrincipal.cdsEstoque_V.Filter := 'DESCESTOQUE LIKE ' + QuotedStr('%'+edtPesquisaPecas.Text+'%');
                DtmPrincipal.cdsEstoque_V.Filtered := TRUE;
              end;
              1:begin
                DtmPrincipal.cdsEstoque_V.Filtered := FALSE;
                DtmPrincipal.cdsEstoque_V.Filter := 'DESCESTOQUE LIKE ' + QuotedStr('%'+edtPesquisaPecas.Text+'%');
                DtmPrincipal.cdsEstoque_V.Filtered := TRUE;
              end;
              2:begin
                DtmPrincipal.cdsEstoque_V.Filtered := FALSE;
                DtmPrincipal.cdsEstoque_V.Filter := 'DESCESTOQUE LIKE ' + QuotedStr('%'+edtPesquisaPecas.Text+'%');
                DtmPrincipal.cdsEstoque_V.Filtered := TRUE;
              end;
            end;
      end;
      end;
    end;
    AtualizaBotoesPe�a;
end;

procedure TFrmTestesEdit.dgPecasDispTitleClick(Column: TColumn);
var
  I:Integer;
begin
  if Column.Index < 2 then
    begin
      for I := 0 to dgPecasDisp.Columns.Count - 1 do
        dgPecasDisp.Columns.Items[I].Color := clWhite;
      Column.Color := clSilver;
      idxPesquisaPeca := Column.Index;
    end
  else
    begin
      MessageDlg('Somente os campos c�digo e descri��o permitem filtro!'
      , mtInformation, [mbOk], 0);
    end;
end;

procedure TFrmTestesEdit.btnAdicionarPecasClick(Sender: TObject);
begin
  if DtmPrincipal.cdsEstoque_VQTDESTOQUE.AsInteger > 0 then
    begin
      try
        DtmPrincipal.cdsTesteMangPecas.Append;
        DtmPrincipal.cdsTesteMangPecasCODTESTE.AsInteger := codTeste;
        DtmPrincipal.cdsTesteMangPecasCODPECA.AsInteger := DtmPrincipal.cdsEstoque_VCODESTOQUE.AsInteger;
        if not(Assigned(FrmPecaAdd)) then
          Application.CreateForm(TFrmPecaAdd,FrmPecaAdd);
        FrmPecaAdd.edtQtd.Text := '1';
        FrmPecaAdd.Show;
      except
        on e:Exception do
          begin
            DtmPrincipal.cdsTesteMangPecas.Cancel;
            DtmPrincipal.cdsTesteMangPecas.Open;
            MessageDlg('Erro: ' + E.Message, mtError, [mbOK], 0);
          end;
      end;
    end
  else
    MessageDlg('Erro!'+#13+'O item est� em falta no estoque!', mtError, [mbOK], 0);
end;

procedure TFrmTestesEdit.btnRemoverPecaClick(Sender: TObject);
begin
  if MessageDlg('Deseja remover da lista a pe�a selecionada?',
    mtInformation, mbYesNo, 0) = mrYes then
  begin
    try
      if DtmPrincipal.cdsTesteMangPecas.Active then
      begin
        DtmPrincipal.sdsAux.CommandText :=
          'DELETE FROM TB_TESTES_MANG_PECAS WHERE CODUSUPECAS = ' +
          DtmPrincipal.cdsTesteMangPecas_VCODUSOPECAS.AsString;
        DtmPrincipal.sdsAux.ExecSQL(TRUE);
        DtmPrincipal.cdsTesteMangPecas_V.Refresh;
        DtmPrincipal.cdsTesteMangPecas_V.Refresh;
        DtmPrincipal.cdsTesteMangPecas.Refresh;
        DtmPrincipal.cdsTesteMangPecas.Refresh;
        AtualizaBotoesPe�a;
      end;
    except
      on e: Exception do
      begin
        DtmPrincipal.cdsTesteMangPecas.Cancel;
        DtmPrincipal.cdsTesteMangPecas.Open;
        MessageDlg('Erro: ' + e.Message, mtError, [mbOK], 0);
        MessageDlg(
          'N�o foi possivel remover a mangueira, j� existem registros no sistema.\n'
            + ' Remova primeiro os registros de testes e lacres!\n' +
            ' C�d Error:' + e.Message, mtWarning, mbOKCancel, 0);
      end;
      on e: EDatabaseError do
        Showmessage(e.Message);
    end;
  end;
end;

// Lacres

function TFrmTestesEdit.PesquisaLacre(codigo: Integer): Boolean;
begin
  result := FALSE;
  DtmPrincipal.cdsMangLacre.Filtered := FALSE;
  DtmPrincipal.cdsMangLacre.Filter := 'CODLACRE = ' + inttostr(codigo);
  DtmPrincipal.cdsMangLacre.Filtered := TRUE;
  if DtmPrincipal.cdsMangLacre.RecordCount > 0 then
    begin
      if DtmPrincipal.cdsMangLacreESTADO.AsInteger = 0 then
        result := TRUE
      else
        if DtmPrincipal.cdsMangLacreESTADO.AsInteger = 1 then
            MessageDlg('Aten��o'+#13+'Este lacre j� se encontra em uso!',mtWarning,[mbOk],0)
        else
          if DtmPrincipal.cdsMangLacreESTADO.AsInteger = 2 then
            MessageDlg('Aten��o'+#13+'Este lacre j� se encontra descartado!',mtWarning,[mbOk],0)
    end
  else
    MessageDlg('Lacre n�o encontrado!', mtError, [mbOK], 0);
end;

procedure TFrmTestesEdit.DescartaLacre(codlc: string);
begin
  if (trim(codlc)<>'0')AND(Length(trim(codlc)) <> 0) then
  begin
    DtmPrincipal.sdsAux.CommandText := 'UPDATE TB_MANG_LACRE SET ESTADO = 2 WHERE CODLACRE = ' + codlc;
    DtmPrincipal.sdsAux.ExecSQL(TRUE);
  end;
end;


procedure TFrmTestesEdit.AlteraUsoLacre(codlc,codmang:String);
begin
  if (trim(codlc)<>'0')AND(Length(trim(codlc)) <> 0) then
  begin
    DtmPrincipal.sdsAux.CommandText := 'UPDATE TB_MANG_LACRE SET ESTADO = 1, MANGNUM = '+ codmang +', DATALACRE = CURRENT_TIMESTAMP WHERE CODLACRE = ' + codlc;
    DtmPrincipal.sdsAux.ExecSQL(TRUE);
  end;
end;

procedure TFrmTestesEdit.btnAntMngLacreClick(Sender: TObject);
begin
  DtmPrincipal.cdsTesteAvaliacao.Prior;
  AtualizaMangLacre;
  AtualizaBotoesLacre;

end;

procedure TFrmTestesEdit.btnSalvarMngLacreClick(Sender: TObject);
begin
  try
    if bLacreOk then
      begin
        DtmPrincipal.cdsTesteAvaliacaoCODLACREOLD.AsInteger := codLacreOld;
        DescartaLacre(inttostr(codLacreOld));
        DtmPrincipal.cdsTesteAvaliacaoCODLACRE.AsInteger := DtmPrincipal.cdsMangLacreCODLACRE.AsInteger;
        DtmPrincipal.cdsTesteAvaliacao.Post;
        DtmPrincipal.cdsTesteAvaliacao.ApplyUpdates(0);
        DtmPrincipal.cdsMangueiras.Open;
        DtmPrincipal.cdsMangueiras.Filtered := FALSE;
        DtmPrincipal.cdsMangueiras.Filter := 'CODMANG = '+ DtmPrincipal.cdsTesteAvaliacaoCODMANG.AsString;
        DtmPrincipal.cdsMangueiras.Filtered := TRUE;
        if DtmPrincipal.cdsMangueiras.RecordCount <> 1 then
          MessageDlg('Erro!'+#13+'Mangueira n�o encontrada ou registro duplicado!',mtError,[mbOK],0)
        else
          begin
            DtmPrincipal.cdsMangueiras.Edit;
            DtmPrincipal.cdsMangueirasCODLACRE.AsInteger := DtmPrincipal.cdsMangLacreCODLACRE.AsInteger;
            DtmPrincipal.cdsMangueiras.Post;
            DtmPrincipal.cdsMangueiras.ApplyUpdates(0);
            DtmPrincipal.cdsMangueiras.Filtered := FALSE;
            DtmPrincipal.cdsMangueiras.Filter := '';
          end;
        AlteraUsoLacre(DtmPrincipal.cdsMangLacreCODLACRE.AsString,DtmPrincipal.cdsTesteAvaliacaoCODMANG.AsString);
        DtmPrincipal.cdsMangueiras.Refresh;
        DtmPrincipal.cdsMangueiras_V.Refresh;
        DtmPrincipal.cdsTesteAvaliacao.Refresh;
        DtmPrincipal.cdsMangLacre.Refresh;
        ModoLacre(TRUE);
        bLacreOk := FALSE;
      end
    else
      MessageDlg('Erro!'+#13+'� necess�rio selecionar um lacre!',mtWarning,[mbOk],0);
  except
    on e:exception do
      begin
        DtmPrincipal.cdsTesteAvaliacao.Cancel;
        DtmPrincipal.cdsTesteAvaliacao.Open;
        DtmPrincipal.cdsMangueiras.Cancel;
        DtmPrincipal.cdsMangueiras.Open;
        ModoLacre(TRUE);
        MessageDlg('Erro: ' + e.Message,mtWarning,[mbOk],0);
      end;
  end;
end;

procedure TFrmTestesEdit.btnEditarMngLacreClick(Sender: TObject);
begin
  try
    codLacreOld := DtmPrincipal.cdsMangueiras_VCODLACRE.AsInteger;
    DtmPrincipal.cdsTesteAvaliacao.Edit;
    ModoLacre(FALSE);
  except
    on E: Exception do
      begin
        DtmPrincipal.cdsTesteAvaliacao.Cancel;
        DtmPrincipal.cdsTesteAvaliacao.Open;
        ModoLacre(TRUE);
        MessageDlg('Erro: ' + E.Message, mtError, [mbOK], 0);
      end;
  end;
end;

procedure TFrmTestesEdit.btnCancelarMngLacreClick(Sender: TObject);
begin
  try
    if (DtmPrincipal.cdsTesteAvaliacao.State in [dsEdit, dsInsert]) then
      begin
        DtmPrincipal.cdsTesteAvaliacao.Cancel;
        AtualizaMangAvaliacao;
        AtualizaBotoesAvaliacao;
        bLacreOk := FALSE;
        ModoLacre(TRUE);
      end;
  except on E: Exception do
    begin
      DtmPrincipal.cdsTesteAvaliacao.Cancel;
      DtmPrincipal.cdsTesteAvaliacao.Open;
      ModoLacre(TRUE);
      MessageDlg('Erro: '+E.Message, mtError, [mbOK], 0);
    end;
  end;
end;

procedure TFrmTestesEdit.btnConcluidoClick(Sender: TObject);
begin
  Close;
end;

procedure TFrmTestesEdit.AtualizaMangLacre;
begin
  try
    DtmPrincipal.cdsMangueiras_V.Filtered := FALSE;
    DtmPrincipal.cdsMangueiras_V.Filter := 'CODMANG = ' + DtmPrincipal.cdsTesteAvaliacaoCODMANG.AsString;
    DtmPrincipal.cdsMangueiras_V.Filtered := TRUE;
    if DtmPrincipal.cdsTesteAvaliacaoCODLACREOLD.AsInteger <> 0 then
      begin
        edtCodLacre.Text := DtmPrincipal.cdsTesteAvaliacaoCODLACRE.AsString;
        DtmPrincipal.cdsMangLacre.Filtered := FALSE;
        DtmPrincipal.cdsMangLacre.Filter := 'CODLACRE = ' + DtmPrincipal.cdsTesteAvaliacaoCODLACRE.AsString;
        DtmPrincipal.cdsMangLacre.Filtered := TRUE;
      end
    else
      begin
        edtCodLacre.Clear;
        lblNumLacre.Visible := FALSE;
        dbNumLacreNovo.Visible := FALSE;
        lblDataCadLacre.Visible := FALSE;
        dbDataCadLacreNovo.Visible := FALSE;
      end;
    if DtmPrincipal.cdsMangueiras_V.RecordCount < 1 then
      MessageDlg('Mangueira n�o encontrada!', mtError, [mbOK], 0);
  except
    on e:Exception do
      begin
        MessageDlg('Erro: ' + E.Message, mtError, [mbOK], 0);
      end;
  end;
end;

procedure TFrmTestesEdit.AtualizaPosMang;
begin
  lblMangPos.Caption := 'Mangueira ' + IntToStr(DtmPrincipal.cdsTesteAvaliacao.RecNo) + ' de ' +
  IntToStr(DtmPrincipal.cdsTesteAvaliacao.RecordCount);
end;

procedure TFrmTestesEdit.ModoLacre(modo: Boolean);
begin
  edtCodLacre.ReadOnly := modo;
  btnPesquisarLacre.Enabled := not modo;
  btnEditarMngLacre.Enabled := modo;
  btnProxMngLacre.Enabled := modo;
  btnAntMngLacre.Enabled := modo;
  btnSalvarMngLacre.Enabled := not modo;
  btnCancelarMngLacre.Enabled := not modo;
  btnStep6Prox.Enabled := modo;
end;

procedure TFrmTestesEdit.btnPesquisarLacreClick(Sender: TObject);
begin
  if length(edtCodLacre.Text) <> 0  then
    if PesquisaLacre(strtoint(edtCodLacre.Text)) then
      begin
        lblNumLacre.Visible := TRUE;
        dbNumLacreNovo.Visible := TRUE;
        lblDataCadLacre.Visible := TRUE;
        dbDataCadLacreNovo.Visible := TRUE;
        bLacreOk := TRUE;
      end
    else
      begin
        lblNumLacre.Visible := FALSE;
        dbNumLacreNovo.Visible := FALSE;
        lblDataCadLacre.Visible := FALSE;
        dbDataCadLacreNovo.Visible := FALSE;
        bLacreOk := FALSE;
      end
  else
    begin
      if not(Assigned(FrmLacre)) then
        Application.CreateForm(TFrmLacre,FrmLacre);
      FrmLacre.Destino := 3;
      FrmLacre.Show;
    end;
end;

// Form

procedure TFrmTestesEdit.pgTesteChanging(Sender: TObject;
  var AllowChange: Boolean);
begin
  AllowChange := FALSE;
end;

procedure TFrmTestesEdit.PreparaAvalia��o(tipo: String);
begin
  case tipo[1] of
    'M':
      begin
        jgMarcAbnt.Enabled := TRUE;
        jgVazValvula.Enabled := TRUE;
        jgAcuAguaCx.Enabled := TRUE;
        jgPresMat.Enabled := TRUE;
        jgPresInset.Enabled := TRUE;
        jgAcoplaUnioes.Enabled := TRUE;
        jgAnelVed.Enabled := TRUE;
        jgReempatacao.Enabled := TRUE;
        jgNovoEnsaioHidro.Enabled := TRUE;
        jgSubstUnioes.Enabled := TRUE;
        jgSubstVed.Enabled := TRUE;
        jgSubstAneis.Enabled := TRUE;
        jgLimpeza.Enabled := TRUE;
        jgSecagem.Enabled := TRUE;
        jgEnsaioHidro.Enabled := TRUE;
        jgCondAduchAcond.Enabled := TRUE;
        jgResultFinal.Enabled := TRUE;
        dbCompLuva.Enabled := TRUE;
        dbCompReal.Enabled := TRUE;
      end;
    'I':
      begin
        jgMarcAbnt.Enabled := TRUE;
        jgVazValvula.Enabled := TRUE;
        jgAcuAguaCx.Enabled := TRUE;
        jgPresMat.Enabled := TRUE;
        jgPresInset.Enabled := TRUE;
        jgAcoplaUnioes.Enabled := TRUE;
        jgAnelVed.Enabled := TRUE;
        jgSubstAneis.Enabled := FALSE;
        jgLimpeza.Enabled := TRUE;
        jgSecagem.Enabled := TRUE;
        jgReempatacao.Enabled := FALSE;
        jgNovoEnsaioHidro.Enabled := FALSE;
        jgSubstUnioes.Enabled := FALSE;
        jgSubstVed.Enabled := TRUE;
        jgEnsaioHidro.Enabled := FALSE;
        jgCondAduchAcond.Enabled := TRUE;
        jgResultFinal.Enabled := TRUE;
        dbCompLuva.Enabled := FALSE;
        dbCompReal.Enabled := FALSE;
      end;
  end;
end;

procedure TFrmTestesEdit.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  FecharDataSets;
  LimparFiltros;
  FreeAndNil(FrmTestesEdit);
end;

procedure TFrmTestesEdit.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
var
  Resposta: Integer;
begin
  if (DtmPrincipal.cdsTestes.Active) and (DtmPrincipal.cdsTestes.State in [dsEdit, dsInsert]) then
    begin
      Resposta := MessageDlg(
        'O registro atual ainda n�o foi salvo!'+#13+'Deseja Salvar as altera��es antes de sair?'
        , mtInformation, mbYesNoCancel, 0);
      case Resposta of
        mrYes:
          begin
            if (dbTecnico.Text <> '') AND (bClienteOk) then
              begin
                DtmPrincipal.cdsTestes.Post;
                DtmPrincipal.cdsTestes.ApplyUpdates(0);
                DtmPrincipal.cdsTestes.Refresh;
                DtmPrincipal.cdsTestes.Refresh;
                LimparFiltros;
                CanClose := TRUE;
              end
            else
              begin
                MessageDlg('Erro!'+#13+'Para salvar este teste � necess�rio preencher os campos T�cnico e Cliente!',mtWarning,[mbOk],0);
                CanClose := FALSE;
              end;
          end;
        mrNo:
          begin
          DtmPrincipal.cdsTestes.Cancel;
          CanClose := TRUE;
          end;
        else
          begin
          CanClose := FALSE;
          exit;
          end;
      end;
    end;
  if (DtmPrincipal.cdsTesteAvaliacao.Active) and (DtmPrincipal.cdsTesteAvaliacao.State in [dsEdit, dsInsert]) then
    begin
      Resposta := MessageDlg(
        'O registro atual ainda n�o foi salvo!'+#13+'Deseja Salvar as altera��es antes de sair?'
        , mtInformation, mbYesNoCancel, 0);
      case Resposta of
        mrYes:
          begin
            if (dbTecnico.Text <> '') AND (bClienteOk) then
              begin
                DtmPrincipal.cdsTesteAvaliacao.Post;
                DtmPrincipal.cdsTesteAvaliacao.ApplyUpdates(0);
                DtmPrincipal.cdsTesteAvaliacao.Refresh;
                DtmPrincipal.cdsTesteAvaliacao.Refresh;
                LimparFiltros;
                CanClose := TRUE;
              end
            else
              begin
                MessageDlg('Erro!'+#13+'Para salvar este teste � necess�rio salvar a edi��o atual!',mtWarning,[mbOk],0);
                CanClose := FALSE;
              end;
          end;
        mrNo:
          begin
          DtmPrincipal.cdsTesteAvaliacao.Cancel;
          CanClose := TRUE;
          end;
        else
          begin
          CanClose := FALSE;
          exit;
          end;
      end;
    end;
  if (DtmPrincipal.cdsTesteMangPecas.Active) and (DtmPrincipal.cdsTesteMangPecas.State in [dsEdit, dsInsert]) then
    begin
      Resposta := MessageDlg(
        'O registro atual ainda n�o foi salvo!'+#13+'Deseja Salvar as altera��es antes de sair?'
        , mtInformation, mbYesNoCancel, 0);
      case Resposta of
        mrYes:
          begin
            if (dbTecnico.Text <> '') AND (bClienteOk) then
              begin
                DtmPrincipal.cdsTesteMangPecas.Post;
                DtmPrincipal.cdsTesteMangPecas.ApplyUpdates(0);
                DtmPrincipal.cdsTesteMangPecas.Refresh;
                DtmPrincipal.cdsTesteMangPecas.Refresh;
                LimparFiltros;
                CanClose := TRUE;
              end
            else
              begin
                MessageDlg('Erro!'+#13+'Para salvar este teste � necess�rio salvar a edi��o atual!',mtWarning,[mbOk],0);
                CanClose := FALSE;
              end;
          end;
        mrNo:
          begin
          DtmPrincipal.cdsTesteMangPecas.Cancel;
          CanClose := TRUE;
          end;
        else
          begin
            CanClose := FALSE;
            exit;
          end;
      end;
    end;
end;

procedure TFrmTestesEdit.FormShow(Sender: TObject);
begin
  try
    idxPesquisaPeca := 0;
    bClienteOk := FALSE;
    bLacreOk := FALSE;
    SelecionaPagina(0);
    DtmPrincipal.cdsTestes.Open;
    DtmPrincipal.cdsTesteTecnico.Open;
    LimparFiltros;
    DtmPrincipal.cdsTestes.Append;
    dbDtExec.Date := now;
  except
    on e: Exception do
    begin
      DtmPrincipal.cdsTestes.Cancel;
      DtmPrincipal.cdsTestes.Open;
      MessageDlg('Erro: ' + e.Message, mtError, [mbOK], 0);
    end;
  end;
end;

function TFrmTestesEdit.GeraFiltro: String;
var
  aux: String;
begin
  aux := '';
  aux := 'CODCLI = ' + IntToStr(codCliente);
  DtmPrincipal.cdsTesteAvaliacao.Close;
  DtmPrincipal.cdsTesteAvaliacao.Filtered := FALSE;
  DtmPrincipal.cdsTesteAvaliacao.Filter := 'CODTESTE = ' + IntToStr(codTeste);
  DtmPrincipal.cdsTesteAvaliacao.Filtered := TRUE;
  DtmPrincipal.cdsTesteAvaliacao.Open;
  DtmPrincipal.cdsTesteAvaliacao.First;
  while not(DtmPrincipal.cdsTesteAvaliacao.Eof) do
    begin
      aux := aux + ' and CODMANG <> ' + DtmPrincipal.cdsTesteAvaliacaoCODMANG.AsString;
      DtmPrincipal.cdsTesteAvaliacao.Next;
    end;
  result := aux;
end;

procedure TFrmTestesEdit.jgResultFinalChange(Sender: TObject);
begin
  if sTipo[1] = 'M' then
    if jgResultFinal.Value = 'M' then
      begin
        MessageDlg('Aten��o!'+#13+'Voc� est� executando uma Manuten��o.'+#13+'O resultado final precisa ser Aprovado ou Condenado!',mtWarning,[mbOk],0);
        jgResultFinal.Value := '';
      end;
end;

procedure TFrmTestesEdit.lblAcoplaUnioesClick(Sender: TObject);
begin
  jgAcoplaUnioes.Value := '';
end;

procedure TFrmTestesEdit.lblAcuAguaCxClick(Sender: TObject);
begin
  jgAcuAguaCx.Value := '';
end;

procedure TFrmTestesEdit.lblAnelVedClick(Sender: TObject);
begin
  jgAnelVed.Value := '';
end;

procedure TFrmTestesEdit.lblCondAduchAcondiClick(Sender: TObject);
begin
  jgCondAduchAcond.Value := '';
end;

procedure TFrmTestesEdit.lblEnsaioHidroClick(Sender: TObject);
begin
  jgEnsaioHidro.Value := '';
end;

procedure TFrmTestesEdit.lblLimpezaClick(Sender: TObject);
begin
  jgLimpeza.Value := '';
end;

procedure TFrmTestesEdit.lblMarcAbntClick(Sender: TObject);
begin
  jgMarcAbnt.Value := '';
end;

procedure TFrmTestesEdit.lblNovoEnsaioHidroClick(Sender: TObject);
begin
  jgNovoEnsaioHidro.Value := '';
end;

procedure TFrmTestesEdit.lblPresInsetClick(Sender: TObject);
begin
  jgPresInset.Value := '';
end;

procedure TFrmTestesEdit.lblPresMatClick(Sender: TObject);
begin
  jgPresMat.Value := '';
end;

procedure TFrmTestesEdit.lblReempatacaoClick(Sender: TObject);
begin
  jgReempatacao.Value := '';
end;

procedure TFrmTestesEdit.lblSecagemClick(Sender: TObject);
begin
  jgSecagem.Value := '';
end;

procedure TFrmTestesEdit.lblSubstAneisClick(Sender: TObject);
begin
  jgSubstAneis.Value := '';
end;

procedure TFrmTestesEdit.lblSubstUnioesClick(Sender: TObject);
begin
  jgSubstUnioes.Value := '';
end;

procedure TFrmTestesEdit.lblSubstVedClick(Sender: TObject);
begin
  jgSubstVed.Value := '';
end;

procedure TFrmTestesEdit.lblvazValvulaClick(Sender: TObject);
begin
  jgVazValvula.Value := '';
end;

procedure TFrmTestesEdit.FecharDataSets;
begin
  With DtmPrincipal do
    begin
      cdsClientes_V.Close;
      cdsClientes.Close;
      cdsTestes_V.Close;
      cdsTestes.Close;
      cdsTesteAvaliacao.Close;
      cdsAvaliacao_V.Close;
      cdsTesteMangPecas.Close;
      cdsMangLacre.Close;
    end;
end;

procedure TFrmTestesEdit.LimparFiltros;
begin
  with DtmPrincipal do
  begin
    cdsMangueiras_V.Filtered := FALSE;
    cdsMangueiras_V.Filter := '';
    cdsClientes_V.Filtered := FALSE;
    cdsClientes_V.Filter := '';
    cdsClientes_V.Open;
    cdsClientes_V.Filtered := FALSE;
    cdsClientes_V.Filter := '';
    cdsTestes_V.Open;
    cdsTestes_V.Filtered := FALSE;
    cdsTestes_V.Filter := '';
    cdsAvaliacao_V.Open;
    cdsAvaliacao_V.Filtered := FALSE;
    cdsAvaliacao_V.Filter := '';
    cdsTesteMangPecas_V.Open;
    cdsTesteMangPecas_V.Filtered := FALSE;
    cdsTesteMangPecas_V.Filter := '';
  end;
end;

procedure TFrmTestesEdit.SelecionaPagina(pg: Integer);
begin
  if pg = 0 then
  begin
    tbTeste.Enabled := TRUE;
  end
  else
  begin
    tbTeste.Enabled := FALSE;
  end;
  if pg = 1 then
  begin
    tbCliente.Enabled := TRUE;
  end
  else
  begin
    tbCliente.Enabled := FALSE;
  end;
  if pg = 2 then
  begin
    tbMangueiras.Enabled := TRUE;
  end
  else
  begin
    tbMangueiras.Enabled := FALSE;
  end;
  if pg = 3 then
  begin
    tbAvaliacao.Enabled := TRUE;
  end
  else
  begin
    tbAvaliacao.Enabled := FALSE;
  end;
  if pg = 4 then
  begin
    tbPecas.Enabled := TRUE;
  end
  else
  begin
    tbPecas.Enabled := FALSE;
  end;
  if pg = 5 then
  begin
    tbLacres.Enabled := TRUE;
  end
  else
  begin
    tbLacres.Enabled := FALSE;
  end;
  if pg = 6 then
  begin
    tbRelatorio.Enabled := TRUE;
  end
  else
  begin
    tbRelatorio.Enabled := FALSE;
  end;
  pgTeste.ActivePageIndex := pg;
end;

function TFrmTestesEdit.VerificaQntMang(codcli: Integer): Boolean;
begin
  Result := FALSE;
  with DtmPrincipal do
    begin
      cdsMangueiras_V.Open;
      cdsMangueiras_V.Filtered := FALSE;
      cdsMangueiras_V.Filter := 'CODCLI ='+inttostr(codcli);
      cdsMangueiras_V.Filtered := TRUE;
      if cdsMangueiras_V.RecordCount > 0 then
        Result := TRUE;
      cdsMangueiras_V.Filtered := FALSE;
      cdsMangueiras_V.Filter := '';
      cdsMangueiras_V.Close;
    end;
end;

//Botoes dos passos
procedure TFrmTestesEdit.btnStep1CancelClick(Sender: TObject);
begin
  DtmPrincipal.cdsTestes.Cancel;
  FecharDataSets;
  Close;
end;

procedure TFrmTestesEdit.btnStep1ProxClick(Sender: TObject);
begin
if jgTipoTeste.Value <> '' then
  if dbTecnico.Text <> '' then
    begin
      with DtmPrincipal do
        begin
          cdsClientes_V.Open;
        end;
      SelecionaPagina(1);
      edtCodCli.SetFocus;
    end
  else
    begin
      MessageDlg('� necess�rio selecionar um t�cnico respons�vel!', mtInformation, [mbOK], 0);
      dbTecnico.SetFocus;
    end
else
  begin
    MessageDlg('� necess�rio selecionar o tipo de teste a ser efetuado!',mtInformation,[mbOk],0);
    jgTipoTeste.SetFocus;
  end;
end;

procedure TFrmTestesEdit.btnStep2CancelClick(Sender: TObject);
begin
  DtmPrincipal.cdsTestes.Cancel;
  FecharDataSets;
  Close;
end;

procedure TFrmTestesEdit.btnStep2ProxClick(Sender: TObject);
begin
  if bClienteOk then
    begin
      try
        if (DtmPrincipal.cdsTestes.State in [dsEdit, dsInsert]) then
          begin
            if MessageDlg('Confirmar os dados e iniciar o registro do teste?',
            mtInformation, mbYesNo, 0) = mrYes then
              begin
                // Cria o registro na tb_teste com a informa��o da pagina teste e cliente
                DtmPrincipal.cdsTestesCODCLI.AsInteger := strtoint(edtCodCli.Text);
                codCliente := strtoint(edtCodCli.Text);
                sTipo := jgTipoTeste.Value;
                DtmPrincipal.cdsTestes.Post;
                DtmPrincipal.cdsTestes.ApplyUpdates(0);
                DtmPrincipal.cdsTestes_V.Refresh;
                DtmPrincipal.cdsTestes_V.Refresh;
                DtmPrincipal.cdsClientes_V.Filtered := FALSE;
                DtmPrincipal.cdsClientes_V.Filter := '';
                DtmPrincipal.cdsTestes.Refresh;
                DtmPrincipal.cdsTestes.Refresh;
                DtmPrincipal.cdsTestes.Last;
                codTeste := DtmPrincipal.cdsTestesCODTESTE.AsInteger;
                DtmPrincipal.cdsTestes.Close;
                DtmPrincipal.cdsTesteTecnico.Close;
                DtmPrincipal.cdsClientes_V.Close;
                // Filtra mangueiras do cliente
                DtmPrincipal.cdsMangueiras_V.Close;
                DtmPrincipal.cdsMangueiras_V.Open;
                DtmPrincipal.cdsMangueiras_V.Filtered := FALSE;
                DtmPrincipal.cdsMangueiras_V.Filter := GeraFiltro;
                DtmPrincipal.cdsMangueiras_V.Filtered := TRUE;
                // Filtra mangueiras do Teste
                DtmPrincipal.cdsAvaliacao_V.Close;
                DtmPrincipal.cdsAvaliacao_V.Open;
                DtmPrincipal.cdsAvaliacao_V.Filtered := FALSE;
                DtmPrincipal.cdsAvaliacao_V.Filter := 'CODTESTE = ' + inttostr(codTeste);
                //Showmessage(DtmPrincipal.cdsTestesCODTESTE.AsString);
                DtmPrincipal.cdsAvaliacao_V.Filtered := TRUE;
                DtmPrincipal.cdsTesteAvaliacao.Open;
                AtualizaBotoesMang;
                // Muda a pagina
                SelecionaPagina(2);
              end;
          end;
      except
        on e: Exception do
          begin
            DtmPrincipal.cdsTestes.Cancel;
            DtmPrincipal.cdsTestes.Open;
            MessageDlg('Erro: ' + e.Message, mtError, [mbOK], 0);
          end;
      end;
    end
  else
    begin
      Showmessage('Erro!'+#13+'� necess�rio selecionar um cliente para prosseguir!');
    end;
end;

procedure TFrmTestesEdit.btnStep3CancelClick(Sender: TObject);
begin
  FecharDataSets;
  Close;
end;

procedure TFrmTestesEdit.btnStep3ProxClick(Sender: TObject);
begin
  try
    if DtmPrincipal.cdsAvaliacao_V.RecordCount > 0 then
      Begin
        DtmPrincipal.cdsAvaliacao_V.Close;
        DtmPrincipal.cdsMangueiras_V.Close;
        DtmPrincipal.cdsTesteAvaliacao.Close;
        DtmPrincipal.cdsTesteAvaliacao.Open;
        DtmPrincipal.cdsTesteAvaliacao.Filtered := FALSE;
        DtmPrincipal.cdsTesteAvaliacao.Filter := 'CODTESTE = ' + IntToStr(codTeste);
        DtmPrincipal.cdsTesteAvaliacao.Filtered := TRUE;
        DtmPrincipal.cdsTesteAvaliacao.First;
        DtmPrincipal.cdsMangueiras.Open;
        DtmPrincipal.cdsMangueiras.Filtered := FALSE;
        DtmPrincipal.cdsMangueiras.Filter := 'CODMANG = ' + DtmPrincipal.cdsTesteAvaliacaoCODMANG.AsString;
        DtmPrincipal.cdsMangueiras.Filtered := TRUE;
        DtmPrincipal.cdsMangLacre.Open;
        SelecionaPagina(3);
        PreparaAvalia��o(sTipo);
        AtualizaBotoesAvaliacao;
        AtualizaPosMang;
        btnEditarAval.click;
      End
    else
      begin
        MessageDlg('Erro!'+#13+'Para prosseguir � necess�rio adicionar pelo menos uma mangueira ao teste!',mtWarning,[mbOk],0);
        dgMangDisp.SetFocus;
      end;
  except
    on e:Exception do
      begin
        MessageDlg('Erro: ' + E.Message, mtError, [mbOK], 0);
      end;
  end;
end;

procedure TFrmTestesEdit.btnStep4ProxClick(Sender: TObject);
begin
  DtmPrincipal.cdsMangueiras.Close;
  DtmPrincipal.cdsTesteAvaliacao.Close;
  DtmPrincipal.cdsMangLacre.Close;
  case sTipo[1] of
    'M':
      begin
        DtmPrincipal.cdsPecas.Open;
        DtmPrincipal.cdsTesteMangPecas.Open;
        DtmPrincipal.cdsTesteMangPecas_V.Open;
        DtmPrincipal.cdsEstoque_V.Open;
        DtmPrincipal.cdsTesteMangPecas.Filtered := FALSE;
        DtmPrincipal.cdsTesteMangPecas.Filter := 'CODTESTE = ' + IntToStr(codTeste);
        DtmPrincipal.cdsTesteMangPecas.Filtered := TRUE;
        DtmPrincipal.cdsTesteMangPecas_V.Filtered := FALSE;
        DtmPrincipal.cdsTesteMangPecas_V.Filter := 'CODTESTE = ' + IntToStr(codTeste);
        DtmPrincipal.cdsTesteMangPecas_V.Filtered := TRUE;
        dgPecasDisp.Columns.Items[0].Color := clSilver;
        AtualizaBotoesPe�a;
        SelecionaPagina(4);
      end;
    'I':
      begin
        SelecionaPagina(6);
        btnGerRelatInsp.Enabled := TRUE;
        btnGerCertManut.Enabled := FALSE;
      end;
  end;
end;

procedure TFrmTestesEdit.btnStep5ProxClick(Sender: TObject);
begin
  SelecionaPagina(5);
  DtmPrincipal.cdsPecas.Close;
  DtmPrincipal.cdsTesteMangPecas.Close;
  DtmPrincipal.cdsTesteMangPecas_V.Close;
  DtmPrincipal.cdsTesteAvaliacao.Open;
  DtmPrincipal.cdsTesteAvaliacao.Filtered := FALSE;
  DtmPrincipal.cdsTesteAvaliacao.Filter := 'CODTESTE = ' + IntToStr(codTeste);
  DtmPrincipal.cdsTesteAvaliacao.Filtered := TRUE;
  DtmPrincipal.cdsTesteAvaliacao.First;
  DtmPrincipal.cdsMangueiras_V.Open;
  DtmPrincipal.cdsMangLacre.Open;
  AtualizaMangLacre;
  AtualizaBotoesLacre;
end;

procedure TFrmTestesEdit.btnStep5VoltarClick(Sender: TObject);
begin
  DtmPrincipal.cdsPecas.Close;
  DtmPrincipal.cdsTesteMangPecas.Close;
  DtmPrincipal.cdsTesteMangPecas_V.Close;
  DtmPrincipal.cdsEstoque_V.CLose;
  DtmPrincipal.cdsTesteMangPecas.Filtered := FALSE;
  DtmPrincipal.cdsTesteMangPecas.Filter := '';
  DtmPrincipal.cdsTesteMangPecas_V.Filtered := FALSE;
  DtmPrincipal.cdsTesteMangPecas_V.Filter := '';
  DtmPrincipal.cdsTesteAvaliacao.Open;
  DtmPrincipal.cdsTesteAvaliacao.Filtered := FALSE;
  DtmPrincipal.cdsTesteAvaliacao.Filter := 'CODTESTE = ' + IntToStr(codTeste);
  DtmPrincipal.cdsTesteAvaliacao.Filtered := TRUE;
  DtmPrincipal.cdsTesteAvaliacao.First;
  DtmPrincipal.cdsMangueiras.Open;
  DtmPrincipal.cdsMangueiras.Filtered := FALSE;
  DtmPrincipal.cdsMangueiras.Filter := 'CODMANG = ' + DtmPrincipal.cdsTesteAvaliacaoCODMANG.AsString;
  DtmPrincipal.cdsMangueiras.Filtered := TRUE;
  DtmPrincipal.cdsMangLacre.Open;
  PreparaAvalia��o(sTipo);
  AtualizaBotoesAvaliacao;
  SelecionaPagina(3);
end;

procedure TFrmTestesEdit.btnStep6ProxClick(Sender: TObject);
begin
  SelecionaPagina(6);
  DtmPrincipal.cdsMangueiras_V.Close;
  DtmPrincipal.cdsMangLacre.Close;
  btnGerRelatInsp.Enabled := FALSE;
  btnGerCertManut.Enabled := TRUE;
end;

procedure TFrmTestesEdit.btnGerRelatInspClick(Sender: TObject);
begin
  DtmPrincipal.cdsClientes_V.Open;
  DtmPrincipal.cdsClientes_V.Filtered := FALSE;
  DtmPrincipal.cdsClientes_V.Filter := 'CODCLI = '+IntToStr(codCliente);
  DtmPrincipal.cdsClientes_V.Filtered := TRUE;
  DtmPrincipal.cdsTestes_V.Open;
  DtmPrincipal.cdsTestes_V.Filtered := FALSE;
  DtmPrincipal.cdsTestes_V.Filter := 'CODTESTE = '+IntToStr(codTeste);
  DtmPrincipal.cdsTestes_V.Filtered := TRUE;
  DtmPrincipal.cdsAvaliacao_V.Open;
  DtmPrincipal.cdsAvaliacao_V.Filtered := FALSE;
  DtmPrincipal.cdsAvaliacao_V.Filter := 'CODTESTE = '+IntToStr(codTeste);
  DtmPrincipal.cdsAvaliacao_V.Filtered := TRUE;
  DtmRelatorios.rpRelatorio.Execute;
  //LimparFiltros;
end;

procedure TFrmTestesEdit.btnGerCertManutClick(Sender: TObject);
begin
  DtmPrincipal.cdsClientes_V.Open;
  DtmPrincipal.cdsClientes_V.Filtered := FALSE;
  DtmPrincipal.cdsClientes_V.Filter := 'CODCLI = '+IntToStr(codCliente);
  DtmPrincipal.cdsClientes_V.Filtered := TRUE;
//  showmessage(inttostr(DtmPrincipal.cdsClientes_V.RecordCount));
  DtmPrincipal.cdsTestes_V.Open;
  DtmPrincipal.cdsTestes_V.Filtered := FALSE;
  DtmPrincipal.cdsTestes_V.Filter := 'CODTESTE = '+IntToStr(codTeste);
  DtmPrincipal.cdsTestes_V.Filtered := TRUE;
//  showmessage(inttostr(DtmPrincipal.cdsTestes_V.RecordCount));
  DtmPrincipal.cdsAvaliacao_V.Open;
  DtmPrincipal.cdsAvaliacao_V.Filtered := FALSE;
  DtmPrincipal.cdsAvaliacao_V.Filter := 'CODTESTE = '+IntToStr(codTeste);
  DtmPrincipal.cdsAvaliacao_V.Filtered := TRUE;
//  showmessage(inttostr(DtmPrincipal.cdsAvaliacao_V.RecordCount));
  DtmPrincipal.cdsTesteMangPecas_V.Open;
  DtmPrincipal.cdsTesteMangPecas_V.Filtered := FALSE;
  DtmPrincipal.cdsTesteMangPecas_V.Filter := 'CODTESTE = '+IntToStr(codTeste);
  DtmPrincipal.cdsTesteMangPecas_V.Filtered := TRUE;
//  showmessage(inttostr(DtmPrincipal.cdsTesteMangPecas_V.RecordCount));
  DtmRelatorios.rpCertificado.Open;
  if DtmPrincipal.cdsTesteMangPecas_V.RecordCount > 0 then
    DtmRelatorios.rpCertificado.SetParam('bSemPecas','')
  else
    DtmRelatorios.rpCertificado.SetParam('bSemPecas','Sem pe�as utilizadas!');
  DtmRelatorios.rpCertificado.Execute;
  DtmRelatorios.rpCertificado.Close;
end;

end.
