unit UFrmLacreCad;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, DB, StdCtrls, JvExMask, JvToolEdit, JvMaskEdit,
  JvCheckedMaskEdit, JvDatePickerEdit, JvDBDatePickerEdit, Mask, DBCtrls,
  Buttons, PngSpeedButton, PngBitBtn;

type
  TFrmLacreCad = class(TForm)
    dsLacre: TDataSource;
    pTop: TPanel;
    pBottom: TPanel;
    btnCancelar: TPngSpeedButton;
    dbNumeracao: TDBEdit;
    Label2: TLabel;
    btnAddSelect: TPngSpeedButton;
    lblDatadoCad: TLabel;
    lblDtCad: TLabel;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnAddSelectClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
  private

  public
    { Public declarations }
  end;

var
  FrmLacreCad: TFrmLacreCad;

implementation

uses UDtmPrincipal, UFrmLacre;

{$R *.dfm}

procedure TFrmLacreCad.btnAddSelectClick(Sender: TObject);
begin
  try
    if (DtmPrincipal.cdsMangLacre.State in [dsEdit, dsInsert]) then
    begin
      if MessageDlg(
        'Confirmar dados?', mtInformation, mbYesNo, 0) = mrYes then
      begin
        DtmPrincipal.cdsMangLacre.Post;
        DtmPrincipal.cdsMangLacre.ApplyUpdates(0);
        DtmPrincipal.cdsMangLacre.Refresh;
        DtmPrincipal.cdsMangLacre.Refresh;
        DtmPrincipal.cdsMangLacre.Last;
        FrmLacre.dgPrincipal.SetFocus;
        FrmLacreCad.Close;
      end;
    end;
  except
    on E: Exception do
    begin
      DtmPrincipal.cdsMangLacre.Cancel;
      DtmPrincipal.cdsMangLacre.Open;
      MessageDlg('Erro: ' + E.Message, mtError, [mbOK], 0);
    end;
  end;
end;

procedure TFrmLacreCad.btnCancelarClick(Sender: TObject);
begin
  try
    if (DtmPrincipal.cdsMangLacre.State in [dsEdit, dsInsert]) then
      begin
        FrmLacreCad.Close;
      end;
  except on E: Exception do
    begin
      DtmPrincipal.cdsMangLacre.Cancel;
      DtmPrincipal.cdsMangLacre.Open;
      MessageDlg('Erro: '+E.Message, mtError, [mbOK], 0);
    end;
  end;
end;

procedure TFrmLacreCad.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  FreeAndNil(FrmLacreCad);
end;

procedure TFrmLacreCad.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
var
  Resposta:integer;
begin
  if (dsLacre.DataSet <> nil) then
    begin
      if (DtmPrincipal.cdsMangLacre.Active) and (DtmPrincipal.cdsMangLacre.State in [dsEdit, dsInsert]) then
        begin
          Resposta := MessageDlg('O registro atual ainda n�o foi salvo! Deseja Salvar as altera��es antes de sair?',mtInformation, mbYesNoCancel, 0);
          case Resposta of
            mrYes:
              begin
                DtmPrincipal.cdsMangLacre.Post;
                DtmPrincipal.cdsMangLacre.ApplyUpdates(0);
                DtmPrincipal.cdsMangLacre.Refresh;
                DtmPrincipal.cdsMangLacre.Refresh;
                CanClose := True;
              end;
            mrNo:
              begin
                DtmPrincipal.cdsMangLacre.Cancel;
                CanCLose := True;
              end;
            else
              begin
                CanClose := False;
                exit;
              end;
          end;
        end;
    end;
end;

procedure TFrmLacreCad.FormShow(Sender: TObject);
begin
  try
    DtmPrincipal.cdsMangLacre.Append;
    DtmPrincipal.cdsMangLacreDATACADLACRE.AsDateTime := now;
    lblDtCad.Caption := DateToStr(now);
    dbNumeracao.SetFocus;
  except
    on E: Exception do
      begin
        DtmPrincipal.cdsMangLacre.Cancel;
        DtmPrincipal.cdsMangLacre.Open;
        MessageDlg('Erro: ' + E.Message, mtError, [mbOK], 0);
      end;
  end;
end;

end.
