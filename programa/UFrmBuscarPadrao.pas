unit UFrmBuscarPadrao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, Grids, DBGrids, ExtCtrls, StdCtrls, Buttons, PngSpeedButton;

type
  TFrmBuscarPadrao = class(TForm)
    pTopo: TPanel;
    dgBusca: TDBGrid;
    pBaixo: TPanel;
    dsBusca: TDataSource;
    edtBusca: TLabeledEdit;
    btnBuscar: TPngSpeedButton;
    rgOpcoes: TRadioGroup;
    btnSelecionar: TPngSpeedButton;
    btnFechar: TPngSpeedButton;
    procedure btnFecharClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmBuscarPadrao: TFrmBuscarPadrao;

implementation

{$R *.dfm}

procedure TFrmBuscarPadrao.btnFecharClick(Sender: TObject);
begin
  Close;
end;

end.
