unit UFrmTestesBuscaCliCad;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UFrmBuscarPadrao, DB, Grids, DBGrids, StdCtrls, ExtCtrls, Buttons,
  PngSpeedButton;

type
  TFrmTestesBuscaCliCad = class(TFrmBuscarPadrao)
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnSelecionarClick(Sender: TObject);
    procedure btnBuscarClick(Sender: TObject);
    procedure dgBuscaDblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmTestesBuscaCliCad: TFrmTestesBuscaCliCad;

implementation

uses UDtmPrincipal, UFrmTestesCad;

{$R *.dfm}

procedure TFrmTestesBuscaCliCad.btnBuscarClick(Sender: TObject);
begin
  inherited;
if trim(edtBusca.Text) <> '' then
    begin
      case rgOpcoes.ItemIndex of
        0:begin
          DtmPrincipal.cdsClientes_V.Filtered := FALSE;
          DtmPrincipal.cdsClientes_V.Filter := 'CODCLI = ' + edtBusca.Text;
          DtmPrincipal.cdsClientes_V.Filtered := TRUE;
        end;
        1:begin
          DtmPrincipal.cdsClientes_V.Filtered := FALSE;
          DtmPrincipal.cdsClientes_V.Filter := 'CNPJ = ' + edtBusca.Text;
          DtmPrincipal.cdsClientes_V.Filtered := TRUE;
        end;
        2:begin
          DtmPrincipal.cdsClientes_V.Filtered := FALSE;
          DtmPrincipal.cdsClientes_V.Filter := 'RAZAO LIKE ' + QuotedStr('%'+edtBusca.Text+'%');
          DtmPrincipal.cdsClientes_V.Filtered := TRUE;
        end;
        3:begin
          DtmPrincipal.cdsClientes_V.Filtered := FALSE;
          DtmPrincipal.cdsClientes_V.Filter := 'FANTASIA LIKE ' + QuotedStr('%'+edtBusca.Text+'%');
          DtmPrincipal.cdsClientes_V.Filtered := TRUE;
        end;
      end;
    end
  else
    begin
      DtmPrincipal.cdsClientes_V.Filtered := FALSE;
      DtmPrincipal.cdsClientes_V.Filter := '';
      DtmPrincipal.cdsClientes_V.Filtered := TRUE;
    end;
end;

procedure TFrmTestesBuscaCliCad.btnSelecionarClick(Sender: TObject);
begin
  inherited;
  FrmTestesCad.dbTCliCnpj.Visible := TRUE;
  FrmTestesCad.dbTCliRazao.Visible := TRUE;
  FrmTestesCad.dbTCliFantasia.Visible := TRUE;
  FrmTestesCad.lblCnpj.Visible := TRUE;
  FrmTestesCad.lblRazao.Visible := TRUE;
  FrmTestesCad.lblFantasia.Visible := TRUE;
  FrmTestesCad.bClienteOk := TRUE;
  FrmTestesCad.edtCodCli.Text := DtmPrincipal.cdsClientes_VCODCLI.AsString;
  FrmTestesBuscaCliCad.Close;
end;

procedure TFrmTestesBuscaCliCad.dgBuscaDblClick(Sender: TObject);
begin
  inherited;
  btnSelecionarClick(sender);
end;

procedure TFrmTestesBuscaCliCad.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  DtmPrincipal.cdsClientes_V.Filtered := FALSE;
  DtmPrincipal.cdsClientes_V.Filter := '';
  FreeAndNil(FrmTestesBuscaCliCad);
end;

end.
