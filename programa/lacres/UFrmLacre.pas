unit UFrmLacre;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, DB, Grids, DBGrids, Buttons, PngSpeedButton, StdCtrls,
  ComCtrls, ImgList, PngImageList, ToolWin;

type
  TFrmLacre = class(TForm)
    dsLacres: TDataSource;
    menuPrincipal: TToolBar;
    imgMenu50: TPngImageList;
    btnAdicionar: TToolButton;
    btnEditar: TToolButton;
    btnFiltrar: TToolButton;
    btnAtualizar: TToolButton;
    dgPrincipal: TDBGrid;
    pBottom: TPanel;
    btnSelecionar: TPngSpeedButton;
    btnFechar: TPngSpeedButton;
    btnShowUsedItens: TToolButton;
    pProcurar: TPanel;
    gbFiltro: TGroupBox;
    rgPesquisaOpcoes: TRadioGroup;
    btnLimparFiltro: TPngSpeedButton;
    edtPesquisa: TEdit;
    btnAddFiltro: TPngSpeedButton;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnSelecionarClick(Sender: TObject);
    procedure btnAtualizarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnAdicionarClick(Sender: TObject);
    procedure btnFecharClick(Sender: TObject);
    procedure btnShowUsedItensClick(Sender: TObject);
    procedure btnFiltrarClick(Sender: TObject);
    procedure btnLimparFiltroClick(Sender: TObject);
    procedure btnAddFiltroClick(Sender: TObject);
  private
    procedure FiltroLacre(filtrar:boolean);
    procedure ScanOpen;
  public
    Destino: Integer;
  end;

var
  FrmLacre: TFrmLacre;

implementation

uses UDtmPrincipal, UFrmMangueirasCad, UFrmLacreCad, UFrmMangueirasEdit,
  UFrmTestesCad;
{$R *.dfm}

procedure TFrmLacre.btnAtualizarClick(Sender: TObject);
begin
  DtmPrincipal.cdsMangLacre.Refresh;
  DtmPrincipal.cdsMangLacre.Refresh;
  DtmPrincipal.cdsMangLacre.Refresh;
end;

procedure TFrmLacre.btnFecharClick(Sender: TObject);
begin
  Close;
end;

procedure TFrmLacre.btnFiltrarClick(Sender: TObject);
begin
  if btnFiltrar.Down then
    begin
      pProcurar.Visible := TRUE;
      edtPesquisa.SetFocus;
    end
  else
    begin
      pProcurar.Visible := FALSE;
      dsLacres.DataSet.Filtered := FALSE;
      dsLacres.DataSet.Filter := '';
    end;
end;

procedure TFrmLacre.btnLimparFiltroClick(Sender: TObject);
begin
  dsLacres.DataSet.Filtered := FALSE;
  dsLacres.DataSet.Filter := '';
  dsLacres.DataSet.Filtered := TRUE;
end;

procedure TFrmLacre.btnSelecionarClick(Sender: TObject);
begin
  try
    case Destino of
      1:
        begin
          FrmMangueirasCad.dbCodLacre.Text := DtmPrincipal.cdsMangLacreCODLACRE.AsString;
          FrmMangueirasCad.btnBuscaLacre.Click
        end;
      2:
        begin
          FrmMangueirasEdit.dbCodLacre.Text := DtmPrincipal.cdsMangLacreCODLACRE.AsString;
          FrmMangueirasEdit.btnBuscaLacre.Click
        end;
      3:
        begin
          FrmTestesCad.edtCodLacre.Text := DtmPrincipal.cdsMangLacreCODLACRE.AsString;
          FrmTestesCad.btnPesquisarLacre.Click;
        end;
    end;
    Close;
  except
    on E: Exception do
      begin
        Showmessage('Erro ' + E.Message);
      end;
  end;
end;

procedure TFrmLacre.btnShowUsedItensClick(Sender: TObject);
begin
  FiltroLacre(not btnShowUsedItens.Down);
  if btnShowUsedItens.Down then
    btnShowUsedItens.ImageIndex := 5
  else
    btnShowUsedItens.ImageIndex := 4;
end;

procedure TFrmLacre.FiltroLacre(filtrar:boolean);
begin
  dsLacres.DataSet.Filtered := FALSE;
  if filtrar then
    begin
      dsLacres.DataSet.Filter := 'ESTADO = 0';
      dsLacres.DataSet.Filtered := TRUE;
      dgPrincipal.Columns.Items[3].Visible := FALSE;
    end
  else
    begin
      dsLacres.DataSet.Filter := '';
      dgPrincipal.Columns.Items[3].Visible := TRUE;
    end;
end;

procedure TFrmLacre.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ScanOpen;
  FreeAndNil(FrmLacre);
end;

procedure TFrmLacre.FormCreate(Sender: TObject);
begin
  FiltroLacre(true);
end;

procedure TFrmLacre.ScanOpen;
begin
  if FrmLacreCad <> nil then
    FrmLacreCad.Close;

end;

procedure TFrmLacre.btnAddFiltroClick(Sender: TObject);
begin
  dsLacres.DataSet.Filtered := FALSE;
  case rgPesquisaOpcoes.ItemIndex of
    0:begin
      dsLacres.DataSet.Filter := 'CODLACRE = '+ edtPesquisa.Text;
    end;
    1:begin
      dsLacres.DataSet.Filter := 'NUMERO = ' + edtPesquisa.Text;
    end;
  end;
  dsLacres.DataSet.Filtered := TRUE;
end;

procedure TFrmLacre.btnAdicionarClick(Sender: TObject);
begin
  if not Assigned(FrmLacreCad) then
    FrmLacreCad := TFrmLacreCad.Create(Self);
  FrmLacreCad.Show;
end;

end.
