inherited FrmMangueirasOrdenar: TFrmMangueirasOrdenar
  Caption = 'Ordenar Mangueiras'
  OnClose = FormClose
  OnCreate = FormCreate
  ExplicitWidth = 874
  PixelsPerInch = 96
  TextHeight = 13
  inherited pCentro: TPanel
    inherited pBotoes: TPanel
      inherited btnOrdenar: TPngSpeedButton
        OnClick = btnOrdenarClick
      end
      inherited btnLimparOrdem: TPngSpeedButton
        OnClick = btnLimparOrdemClick
      end
    end
    inherited rgOpcoes: TRadioGroup
      Columns = 4
      Items.Strings = (
        'Cliente'
        'N'#250'mero'
        'Fabricante'
        'Diametro'
        'Fabrica'#231#227'o'
        'Proxima Manuten'#231#227'o'
        'Ultima Manuten'#231#227'o'
        'Proxima Inspe'#231#227'o'
        'Ultima Inspe'#231#227'o'
        'Lacre'
        'Data do Lacre')
    end
  end
end
