unit UFrmAutenticacao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, pngimage, ExtCtrls, StdCtrls, jpeg, JvExExtCtrls, JvImage, Buttons,
  PngSpeedButton;

type
  TFrmAutenticacao = class(TForm)
    fundoForm: TImage;
    edtSenha: TEdit;
    lblSenha: TLabel;
    lblUsuario: TLabel;
    edtUsuario: TEdit;
    btnFechar: TImage;
    btnEntrar: TPngSpeedButton;
    imgLogo: TImage;
    procedure btnFecharClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnEntrarClick(Sender: TObject);
    procedure edtUsuarioKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtSenhaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormShow(Sender: TObject);
  private
    iTentativas: Integer;
    bUsuarioAutenticado: Boolean;
    procedure Login;
  public
    { Public declarations }
  end;

var
  FrmAutenticacao: TFrmAutenticacao;

implementation

uses UFrmPrincipal, UDtmAutenticacao, UDtmPrincipal;

{$R *.dfm}



procedure TFrmAutenticacao.btnEntrarClick(Sender: TObject);
begin
  Login;
end;

procedure TFrmAutenticacao.btnFecharClick(Sender: TObject);
begin
  Application.Terminate;
end;

procedure TFrmAutenticacao.edtSenhaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 if Key = VK_Return then
    begin
      Login;
    end;
end;

procedure TFrmAutenticacao.edtUsuarioKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_Return then
    begin
      edtSenha.SetFocus;
    end;
end;

procedure TFrmAutenticacao.FormCreate(Sender: TObject);
begin
  if not Assigned(DtmAutenticacao) then
    DtmAutenticacao := TDtmAutenticacao.Create(Self);
  DtmAutenticacao.ConAutentica.Params.Values['Database'] := FrmPrincipal.Banco;
  //showmessage(DtmAutenticacao.ConAutentica.Params.Values['Database']);
  DtmAutenticacao.ConAutentica.Open;
end;

procedure TFrmAutenticacao.FormShow(Sender: TObject);
begin
  if FALSE then
    begin
      edtUsuario.Text := 'rafael';
      edtSenha.Text := '433221';
    end;
end;

procedure TFrmAutenticacao.Login;
begin
  inc(iTentativas);
    if (Trim(edtUsuario.text) <> '') and (Trim(edtSenha.text) <> '') then
    begin
      with DtmAutenticacao do
        begin
          cdsUsuarios.Close;
          cdsUsuarios.Params.ParamValues['pLogin'] := edtUsuario.text;
          cdsUsuarios.Params.ParamValues['pSenha'] := edtSenha.text;
          cdsUsuarios.Open;
          if not(cdsUsuarios.IsEmpty) then
            begin
              if (cdsUsuarios.RecordCount > 1) then
                raise Exception.Create('Existem ' + IntToStr(cdsUsuarios.RecordCount) + ' usu�rio com o mesmo login e senha!');
              bUsuarioAutenticado := True;
              FrmPrincipal.Show;
              FrmPrincipal.lblMsg.caption := 'Seja bem vindo ' + cdsUsuarios.FieldByName('NOME').AsString + '!';
              if (not(Assigned(DtmPrincipal))) then
                DtmPrincipal := TDtmPrincipal.Create(Self);
              DtmPrincipal.conexaoDB.Params.Values['Database'] := FrmPrincipal.Banco;
              DtmPrincipal.conexaoDB.Open;
              FrmAutenticacao.Close;
            end
          else
            MessageDlg('Login e/ou Senha n�o est�o corretos!', mtInformation, [mbOK], 0);
        end;
      end
    else
      MessageDlg('Login e Senha devem ser informados!', mtInformation, [mbOK], 0);
  if not(bUsuarioAutenticado) then
    begin
      edtUsuario.text := '';
      edtSenha.text := '';
      edtUsuario.SetFocus;
    end;
  if (iTentativas >= 3) then
    begin
      MessageDlg('N�mero m�ximo de tentativas excedido!', mtError, [mbOK], 0);
      FrmAutenticacao.Close;
      DtmAutenticacao.destroy;
      Application.Terminate;
    end;
end;

end.
