object FrmFormularioPadrao: TFrmFormularioPadrao
  Left = 0
  Top = 0
  BorderIcons = [biMinimize, biMaximize]
  BorderStyle = bsSingle
  Caption = 'Formul'#225'rio Padr'#227'o'
  ClientHeight = 494
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Visible = True
  WindowState = wsMaximized
  PixelsPerInch = 96
  TextHeight = 13
  object tbMenuForm: TToolBar
    Left = 0
    Top = 25
    Width = 1008
    Height = 56
    AutoSize = True
    ButtonHeight = 56
    ButtonWidth = 57
    HotTrackColor = clWhite
    Images = imgList50
    TabOrder = 0
    object btnMadd: TToolButton
      Left = 0
      Top = 0
      ParentCustomHint = False
      ImageIndex = 42
    end
    object btnMEdit: TToolButton
      Left = 57
      Top = 0
      Caption = 'btnMEdit'
      ImageIndex = 55
    end
    object btnMPesq: TToolButton
      Left = 114
      Top = 0
      Caption = 'btnMPesq'
      ImageIndex = 3
    end
    object btnMFiltro: TToolButton
      Left = 171
      Top = 0
      Caption = 'btnMFiltro'
      ImageIndex = 15
    end
    object btnMOrder: TToolButton
      Left = 228
      Top = 0
      Caption = 'btnMOrder'
      ImageIndex = 0
    end
    object sepM1: TToolButton
      Left = 285
      Top = 0
      Width = 8
      ImageIndex = 35
      Style = tbsSeparator
    end
    object btnMExclui: TToolButton
      Left = 293
      Top = 0
      Caption = 'btnMExclui'
      ImageIndex = 17
    end
    object sepM2: TToolButton
      Left = 350
      Top = 0
      Width = 8
      Caption = 'sepM2'
      ImageIndex = 27
      ParentShowHint = False
      ShowHint = True
      Style = tbsSeparator
    end
    object btnMRelatorios: TToolButton
      Left = 358
      Top = 0
      Caption = 'btnMRelatorios'
      ImageIndex = 34
    end
  end
  object pPrincipal: TPanel
    Left = 0
    Top = 81
    Width = 1008
    Height = 413
    Align = alClient
    TabOrder = 1
    ExplicitTop = 79
    ExplicitHeight = 415
    object dgForm: TDBGrid
      AlignWithMargins = True
      Left = 4
      Top = 4
      Width = 1000
      Height = 405
      Align = alClient
      DataSource = dsForm
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
    end
  end
  object pTitle: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 25
    Align = alTop
    BevelKind = bkFlat
    BevelOuter = bvNone
    Caption = 'Formul'#225'rio Padr'#227'o'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    object btnFechar: TPngSpeedButton
      Left = 983
      Top = 0
      Width = 21
      Height = 21
      Align = alRight
      Caption = 'X'
      Flat = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      OnClick = btnFecharClick
      ExplicitLeft = 981
    end
    object imgIcon: TImage
      Left = 0
      Top = 0
      Width = 21
      Height = 21
      Align = alLeft
      OnClick = imgIconClick
    end
  end
  object imgList50: TPngImageList
    Height = 50
    Width = 50
    PngImages = <>
    Left = 32
    Top = 440
  end
  object dsForm: TDataSource
    Left = 936
    Top = 32
  end
end
