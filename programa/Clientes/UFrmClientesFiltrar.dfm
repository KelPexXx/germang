inherited FrmClientesFiltrar: TFrmClientesFiltrar
  Caption = 'Filtrar Clientes'
  OnClose = FormClose
  ExplicitWidth = 910
  ExplicitHeight = 223
  PixelsPerInch = 96
  TextHeight = 13
  inherited pCentro: TPanel
    inherited pBotoes: TPanel
      inherited btnLimparFiltro: TPngSpeedButton
        OnClick = btnLimparFiltroClick
      end
      inherited btnFiltrar: TPngSpeedButton
        OnClick = btnFiltrarClick
      end
    end
    inherited pTopo: TPanel
      inherited edtFiltro: TLabeledEdit
        Width = 602
        ExplicitWidth = 602
      end
      inherited cbTipoPesquisa: TComboBox
        Left = 656
        Width = 88
        ItemIndex = -1
        Items.Strings = (
          'Come'#231'a com'
          'Cont'#233'm')
        ExplicitLeft = 656
        ExplicitWidth = 88
      end
    end
    inherited rgOpcoes: TRadioGroup
      Columns = 3
      ItemIndex = 0
      Items.Strings = (
        'C'#243'digo'
        'Raz'#227'o Social'
        'Nome Fantasia'
        'CNPJ'
        'Logradouro'
        'Bairro'
        'Cidade'
        'Estado')
    end
  end
end
