unit UDtmRelatorios;

interface

uses
  SysUtils, Classes, RpCon, RpConDS, RpRave, RpDefine, RpBase, RpSystem,
  RpRenderPDF, RpRenderPrinter, RpRender, RpRenderCanvas, RpRenderPreview,
  RvLDCompiler;

type
  TDtmRelatorios = class(TDataModule)
    RvSystem: TRvSystem;
    rpMangueiras: TRvProject;
    rdsEstoque: TRvDataSetConnection;
    rpClientes: TRvProject;
    rdsClientes: TRvDataSetConnection;
    rdsTestes_V: TRvDataSetConnection;
    rdsMangueiras: TRvDataSetConnection;
    rdsMangLacre: TRvDataSetConnection;
    rdsTestes: TRvDataSetConnection;
    rdsMangTipo: TRvDataSetConnection;
    rdsTesteTecnico: TRvDataSetConnection;
    rdsPecas: TRvDataSetConnection;
    rdsTesteMangPecas: TRvDataSetConnection;
    rdsTipoEnd: TRvDataSetConnection;
    rdsSisPrev: TRvDataSetConnection;
    rdsCidade: TRvDataSetConnection;
    rdsEstado: TRvDataSetConnection;
    rdsAtuacao: TRvDataSetConnection;
    rdsRisco: TRvDataSetConnection;
    rpTestesAvaliacao: TRvProject;
    rpTestes: TRvProject;
    rpLacres: TRvProject;
    rpEstoque: TRvProject;
    RvProject7: TRvProject;
    RvProject8: TRvProject;
    RvProject9: TRvProject;
    RvProject10: TRvProject;
    RvProject11: TRvProject;
    RvProject12: TRvProject;
    RvProject13: TRvProject;
    RvProject14: TRvProject;
    RvProject15: TRvProject;
    RvProject16: TRvProject;
    rdsClientes_V: TRvDataSetConnection;
    rdsMangueiras_V: TRvDataSetConnection;
    rdsTesteAvaliacao: TRvDataSetConnection;
    rdsAvaliacao_V: TRvDataSetConnection;
    rdsEstoque_V: TRvDataSetConnection;
    rdsTesteMangPecas_V: TRvDataSetConnection;
    rpRelatorio: TRvProject;
    rpCertificado: TRvProject;
    RvRenderPDF1: TRvRenderPDF;
    RvProject1: TRvProject;
    RvSystem1: TRvSystem;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DtmRelatorios: TDtmRelatorios;

implementation

uses UDtmPrincipal, UFrmPrincipal;

{$R *.dfm}

end.
