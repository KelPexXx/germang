unit UFrmClientesRemover;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UFrmDelRegPadrao, Buttons, PngSpeedButton, StdCtrls, pngimage,
  ExtCtrls, DB, DbxFirebird, DBXCommonResStrs, DBXPlatform, DBXCommon, DBXFirebirdMetaData;

type
  TFrmClientesRemover = class(TFrmDelRegPadrao)
    procedure btnCancelarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnRemoverClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private

  public

  end;

var
  FrmClientesRemover: TFrmClientesRemover;

implementation

uses UDtmPrincipal;

{$R *.dfm}

procedure TFrmClientesRemover.btnCancelarClick(Sender: TObject);
begin
  inherited;
  FrmClientesRemover.Close;
end;

procedure TFrmClientesRemover.btnRemoverClick(Sender: TObject);
begin
  inherited;
  try
    if DtmPrincipal.cdsClientes.Active then
      begin
    DtmPrincipal.cdsClientes.Filtered := FALSE;
    DtmPrincipal.cdsClientes.Filter := 'CODCLI = ' + DtmPrincipal.cdsClientes_VCODCLI.AsString;
    DtmPrincipal.cdsClientes.Filtered := TRUE;
    DtmPrincipal.cdsClientes.Delete;
    DtmPrincipal.cdsClientes.ApplyUpdates(0);
    DtmPrincipal.cdsClientes.Filtered := FALSE;
    DtmPrincipal.cdsClientes.Filter := '';
    DtmPrincipal.cdsClientes_V.Refresh;
    DtmPrincipal.cdsClientes_V.Refresh;
    FrmClientesRemover.Close;
      end;
  except
   on e : Exception do
      begin
        DtmPrincipal.cdsClientes.Cancel;
        DtmPrincipal.cdsClientes.Open;
        MessageDlg('Erro: ' + E.Message, mtError, [mbOK], 0);
          MessageDlg('N�o foi possivel remover o cliente, j� existem registros no sistema.\n'
          +' Remova primeiro os registros de testes, lacres e mangueiras!\n'+'C�d Error:' + e.Message, mtWarning, mbOKCancel, 0);
      end;
    on e : EDatabaseError do
      showmessage(e.Message);
  end;
end;

procedure TFrmClientesRemover.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  DtmPrincipal.cdsClientes.Close;
  DtmPrincipal.sdsClientes.Close;
  FreeAndNil(FrmClientesRemover);
end;

procedure TFrmClientesRemover.FormShow(Sender: TObject);
begin
  inherited;
  DtmPrincipal.sdsClientes.Open;
  DtmPrincipal.cdsClientes.Open;
end;

end.
