unit UFrmDelRegPadrao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Buttons, PngSpeedButton, StdCtrls, pngimage;

type
  TFrmDelRegPadrao = class(TForm)
    pTop: TPanel;
    pBottom: TPanel;
    btnCancelar: TPngSpeedButton;
    btnRemover: TPngSpeedButton;
    imgAlerta: TImage;
    lblTextoMsg1: TLabel;
    lblTextoMsg2: TLabel;
    lblTextoMsg3: TLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmDelRegPadrao: TFrmDelRegPadrao;

implementation

{$R *.dfm}

end.
